/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.datastructure.Chromosome;
import de.unibi.cebitec.gi.unimog.datastructure.Component;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;
import de.unibi.cebitec.gi.unimog.datastructure.Pair;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import de.unibi.cebitec.gi.unimog.GenomeExamples;

import static org.junit.Assert.*;

import de.unibi.cebitec.gi.unimog.datastructure.Genome;
import junit.framework.Assert;

/**
 * @author -Rolf Hilker-
 * <p>
 * Tests all methods in the utilities class.
 * Only the testCheckCotailed is implemented right now.
 */
public class UtilitiesTest {

    private final GenomeExamples genomeExamples = new GenomeExamples();

    public UtilitiesTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of genomeToIntArray method, of class Utilities.
     */
    @Test
    public void testGenomeToIntArray() {
        ArrayList<Chromosome> genome = null;
        int nbOfGenes = 0;
        boolean addCaps = false;
        assertThrows(NullPointerException.class, () -> Utilities.genomeToIntArray(genome, nbOfGenes, addCaps));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of genomeSizeCalculation method, of class Utilities.
     */
    @Test
    public void testGenomeSizeCalculation() {
        Genome genome = null;
        assertThrows(NullPointerException.class, () -> Utilities.genomeSizeCalculation(genome));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of removeDuplicates method, of class Utilities.
     */
    @Test
    public void testRemoveDuplicates() {
        int[] genome = null;
        assert Utilities.removeDuplicates(genome) == null;
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of mergeComponents method, of class Utilities.
     */
    @Test
    public void testMergeComponents() {
        ArrayList<Component> phase1Comps = null;
        ArrayList<Component> phase2Comps = null;
        assertThrows(NullPointerException.class, () -> Utilities.mergeComponents(phase1Comps, phase2Comps));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of genomeCapping method, of class Utilities.
     */
    @Test
    public void testGenomeCapping() {
        Genome genome = null;
        int frontCap = 0;
        int backCap = 0;
        assertThrows(NullPointerException.class, () -> Utilities.genomeCappingToIntArray(genome, frontCap, backCap));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of removeDuplicatesAdvanced method, of class Utilities.
     */
    @Test
    public void testRemoveDuplicatesAdvanced() {
        ArrayList<Genome> genomes = null;
        assert Utilities.removeDuplicatesAdvanced(genomes) == null;
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of onlyLinear method, of class Utilities.
     */
    @Test
    public void testOnlyLinear() {
        Genome genome = null;
        assertThrows(NullPointerException.class, () -> Utilities.onlyLinear(genome));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Tests the check co-tailed genomes method.
     */
    @Test
    public void testCheckCotailed() {
        final Genome genome1 = this.genomeExamples.getGenomeC();
        final Genome genome2 = this.genomeExamples.getGenomeD();
        final Genome genome3 = this.genomeExamples.getGenomeE();
        final Genome genome4 = this.genomeExamples.getGenomeG();

        Assert.assertFalse(Utilities.checkCotailed(genome1, genome2));
        Assert.assertTrue(Utilities.checkCotailed(genome3, genome4));

    }

    /**
     * Test of getGenePos method, of class Utilities.
     */
    @Test
    public void testGetGenePos() {
        Genome genome = null;
        assertThrows(NullPointerException.class, () -> Utilities.getGenePos(genome));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getExtremity method, of class Utilities.
     */
    @Test
    public void testGetExtremity() {
        int geneExt = 0;
        boolean fstExtremity = false;
        int expResult = 0;
        int result = Utilities.getExtremity(geneExt, fstExtremity);
        assert result == expResult;
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of assignInvolvedExtr method, of class Utilities.
     */
    @Test
    public void testAssignInvolvedExtr() {
        int[] involvExtremities = null;
        int[] adjArray1 = null;
        boolean standard = false;
        assertThrows(NullPointerException.class, () -> Utilities.assignInvolvedExtr(involvExtremities, adjArray1, standard));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getSmallerIndexFst method, of class Utilities.
     */
    @Test
    public void testGetSmallerIndexFst() {
        int startIndex = 0;
        int endIndex = 0;
        Pair<Integer, Integer> expResult = new Pair<>(0, 0);
        Pair<Integer, Integer> result = Utilities.getSmallerIndexFst(startIndex, endIndex);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getNeighbourExt method, of class Utilities.
     */
    @Test
    public void testGetNeighbourExt() {
        int ext = 0;
        int expResult = 1;
        int result = Utilities.getNeighbourExt(ext);
        assert result == expResult;
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getBelongingExt method, of class Utilities.
     */
    @Test
    public void testGetBelongingExt() {
        int ext = 0;
        int expResult = -1;
        int result = Utilities.getBelongingExt(ext);
        assert result == expResult;
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of checkExtremities method, of class Utilities.
     */
    @Test
    public void testCheckExtremities() {
        int extremity1 = 0;
        int extremity2 = 0;
        int extremity3 = 0;
        int extremity4 = 0;
        Pair<Pair<Integer,Integer>,Pair<Integer,Integer>> expResult = new Pair<>(new Pair<>(0,0),new Pair<>(0,0));
        Pair<Pair<Integer,Integer>,Pair<Integer,Integer>> result = Utilities.checkExtremities(extremity1, extremity2, extremity3, extremity4);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
}
