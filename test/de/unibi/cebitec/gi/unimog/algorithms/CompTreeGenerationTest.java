/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.GenomeExamples;
import de.unibi.cebitec.gi.unimog.datastructure.AdditionalDataHPDistance;
import de.unibi.cebitec.gi.unimog.datastructure.AdjacencyGraph;
import de.unibi.cebitec.gi.unimog.datastructure.Component;
import de.unibi.cebitec.gi.unimog.datastructure.Data;
import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.MultifurcatedTree;
import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.Node;
import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.NodeType;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tron1c
 */
public class CompTreeGenerationTest {

    private final ComponentIdentification ci;
    private final GenomeExamples genomeExamples = new GenomeExamples();
    private AdjacencyGraph adjGraph = new AdjacencyGraph(this.genomeExamples.getGenomeC(),
            this.genomeExamples.getGenomeD());
    private Data data = new Data(this.genomeExamples.getGenomeC(),
            this.genomeExamples.getGenomeD(), this.adjGraph);
    private AdditionalDataHPDistance addHPDataA;
    // private AdditionalDataHPDistance addHPDataB;

    public CompTreeGenerationTest() {
        this.addHPDataA = new AdditionalDataHPDistance(this.genomeExamples.getGenomeC());
        // this.addHPDataB = new AdditionalDataHPDistance(this.genomeExamples.getGenomeD());

        this.ci = new ComponentIdentification(this.data, this.addHPDataA);
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of generateCompTree method, of class CompTreeGeneration.
     */
    @Test
    public void testGenerateCompTree() {
        //Component identification Test
        final ArrayList<Component> components = this.ci.getComponents();
//        for (int i = 0; i < components.size(); ++i) {
//            System.out.println("Start: " + components.get(i).getStartIndex()
//                    + ", End: " + components.get(i).getEndIndex() + ", Type: "
//                    + components.get(i).getType() + ", oriented: " + components.get(i).isOriented());
//        }

        CompTreeGeneration compTreeGen = new CompTreeGeneration(components, this.addHPDataA.getGenomeCappedMinusArray().length);
        MultifurcatedTree compTree = compTreeGen.getComponentTree();

        Node root = compTree.getRoot();
        assertFalse(compTree.isTreeEmpty());
        assertSame(root.getNodeType(), NodeType.BLACK);
        assertTrue(root.isRoot());
        assertFalse(root.isLeaf());
        assertEquals(0, (int) root.getDepth());
        assertNull(root.getParent());

        Node node = root.getNodeChildren().get(0);
        assertSame(node.getNodeType(), NodeType.SQUARE);
        Node node2 = node.getNodeChildren().get(0);
        Node node3 = node.getNodeChildren().get(1);
        assertSame(node2.getNodeType(), NodeType.GREY);
        assertSame(node3.getNodeType(), NodeType.GREY);

        Node node4 = root.getNodeChildren().get(1);
        assertSame(node4.getNodeType(), NodeType.SQUARE);
        Node node5 = node4.getNodeChildren().get(0);
        assertSame(node5.getNodeType(), NodeType.BLACK);
        Node node6 = node5.getNodeChildren().get(0);
        assertSame(node6.getNodeType(), NodeType.SQUARE);
        Node node9 = node6.getNodeChildren().get(0);
        assertSame(node9.getNodeType(), NodeType.WHITE);

        Node node7 = root.getNodeChildren().get(2);
        assertSame(node7.getNodeType(), NodeType.SQUARE);
        Node node8 = node7.getNodeChildren().get(0);
        assertSame(node8.getNodeType(), NodeType.BLACK);
        assertEquals(true, node8.isLeaf());
        assertEquals(false, node8.isRoot());
        assertEquals(2, (int) node8.getDepth());
    }

    /**
     * Test of getNodeType method, of class CompTreeGeneration.
     */
    @Test
    public void testGetNodeType() {
        Component comp = null;
        assertThrows(NullPointerException.class, () -> CompTreeGeneration.getNodeType(comp));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getComponentTree method, of class CompTreeGeneration.
     */
    @Test
    public void testGetComponentTree() {
        CompTreeGeneration instance = null;
        assertThrows(NullPointerException.class,() -> instance.getComponentTree());
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getCompStarts method, of class CompTreeGeneration.
     */
    @Test
    public void testGetCompStarts() {
        CompTreeGeneration instance = null;
        assertThrows(NullPointerException.class,() -> instance.getCompStarts());
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getNodeToCompMap method, of class CompTreeGeneration.
     */
    @Test
    public void testGetNodeToCompMap() {
        CompTreeGeneration instance = null;
        assertThrows(NullPointerException.class,() -> instance.getNodeToCompMap());
        // TODO review the generated test code and remove the default call to fail.
    }
}
