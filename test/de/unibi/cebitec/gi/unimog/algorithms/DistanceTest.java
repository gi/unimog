package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.datastructure.AdjacencyGraph;
import de.unibi.cebitec.gi.unimog.GraphExamples;
import de.unibi.cebitec.gi.unimog.GenomeExamples;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;
import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.MultifurcatedTree;
import junit.framework.Assert;
import de.unibi.cebitec.gi.unimog.datastructure.AdditionalDataHPDistance;
import de.unibi.cebitec.gi.unimog.datastructure.Data;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;

/**
 * @author -Rolf Hilker-
 * <p>
 * Test class for the distances.
 */
public class DistanceTest {

    private GenomeExamples genomeExamples = new GenomeExamples();
    private GraphExamples agExamples = new GraphExamples();
    private AdjacencyGraph ag2 = this.agExamples.getAG2();

    // get for every genome its reference genome
    private HashMap<Genome, Genome> genome2ref = new HashMap<>() {{
        put(genomeExamples.getGenomeInv1(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv2(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv3(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv4(), genomeExamples.getGenomeInvRef17());
        put(genomeExamples.getGenomeInv5(), genomeExamples.getGenomeInvRef24());
        put(genomeExamples.getGenomeInv6(), genomeExamples.getGenomeInvRef24());
        put(genomeExamples.getGenomeInv7(), genomeExamples.getGenomeInvRef25());
        put(genomeExamples.getGenomeInv8(), genomeExamples.getGenomeInvRef8());
        put(genomeExamples.getGenomeInv9(), genomeExamples.getGenomeInvRef3());
        put(genomeExamples.getGenomeInv10(), genomeExamples.getGenomeInvRef18());
    }};

    // get for every genome its inversion-distance
    private HashMap<Genome, Integer> genome2dist = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), 17);
            put(genomeExamples.getGenomeInv2(), 16);
            put(genomeExamples.getGenomeInv3(), 16);
            put(genomeExamples.getGenomeInv4(), 15);
            put(genomeExamples.getGenomeInv5(), 21);
            put(genomeExamples.getGenomeInv6(), 21);
            put(genomeExamples.getGenomeInv7(), 21);
            put(genomeExamples.getGenomeInv8(), 8);
            put(genomeExamples.getGenomeInv9(), 2);
            put(genomeExamples.getGenomeInv10(), 12);
        }
    };


    public DistanceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Tests the DCJ distance calculation.
     */
    @Test
    public void testCalculateDCJDistance() {
        DistanceDCJ distanceDCJ = new DistanceDCJ();
        Data data2 = new Data();
        data2.setAdjGraph(this.ag2);
        final int dcjDistance2 = distanceDCJ.calculateDistance(data2, null);

        Assert.assertTrue(dcjDistance2 == 11);
    }

    /**
     * Tests the HP distance calculation.
     */
    @Test
    public void testCalculateHPDistance() {
        //        DistanceHP distanceHP = new DistanceHP();
//        Data data2 = new Data(this.genomeExamples.getGenomeC(), this.genomeExamples.getGenomeD());
//        data2.setAdjGraph(this.ag2);
//        AdditionalDataHPDistance additionalData2 = new AdditionalDataHPDistance(this.genomeExamples.getGenomeC());
//        final int hpDistance2 = distanceHP.calculateDistance(data2, additionalData2);
//
//        assert hpDistance2 == 13;

        // add multi-chromosomal genomes for hp model
        HashMap<Genome, Genome> allHpExamples = new HashMap<>(this.genome2ref);
        allHpExamples.putAll(new HashMap<>() {{
            put(genomeExamples.getGenomeHP9(), genomeExamples.getGenomeHP9Ref());
            put(genomeExamples.getGenomeHP10(), genomeExamples.getGenomeHP10Ref());
            put(genomeExamples.getGenomeHP11(), genomeExamples.getGenomeHP11Ref());
        }});

        HashMap<Genome, Integer> hpGenome2dist = new HashMap<>(this.genome2dist);
        hpGenome2dist.putAll(new HashMap<>() {{
            put(genomeExamples.getGenomeHP9(), 13);
            put(genomeExamples.getGenomeHP10(), 14);
            put(genomeExamples.getGenomeHP11(), 21);
        }});

        // copied and adapted from MainClass
        for (int i = 0; i < allHpExamples.size(); i++) {
            Genome a = (Genome) (allHpExamples.keySet().toArray())[i];
            Genome genomeA = ((Genome) (allHpExamples.keySet().toArray())[i]);
            Genome genomeB = allHpExamples.get(genomeA).clone();
            genomeA = genomeA.clone();

            AdditionalDataHPDistance additionalData = new AdditionalDataHPDistance(genomeA);
            final DistanceHP calcHPDist = new DistanceHP();
            Data data = new Data(genomeA.clone(), genomeB.clone(), new AdjacencyGraph(genomeA, genomeB));
            int hpDistance = calcHPDist.calculateDistance(data, additionalData);

            assert hpDistance == hpGenome2dist.get(a);
        }
    }

    /**
     * Tests the inversion distance calculation for special compTrees
     */
    @Test
    public void testCalculateInvDistance() {
        // copied and adapted from MainClass
        for (int i = 0; i < genome2ref.size(); i++) {
            Genome a = (Genome) (genome2ref.keySet().toArray())[i];
            Genome genomeA = ((Genome) (genome2ref.keySet().toArray())[i]);
            Genome genomeB = genome2ref.get(genomeA).clone();
            genomeA = genomeA.clone();
            Genome genomeC = ((Genome) (genome2ref.keySet().toArray())[i]).clone();

            AdditionalDataHPDistance additionalData;
            int invDistance = 0;
            int hpDistance = 0;

            // linear uni-chromosomal genomes have equal hp distance and inversion distance
            additionalData = new AdditionalDataHPDistance(genomeA);
            final DistanceHP calcHPDist = new DistanceHP();
            Data data = new Data(genomeA.clone(), genomeB.clone(), new AdjacencyGraph(genomeA, genomeB));
            hpDistance = calcHPDist.calculateDistance(data, additionalData);

            final boolean linear = Utilities.onlyLinear(genomeA) && Utilities.onlyLinear(genomeB);
            final boolean cotailed = Utilities.checkCotailed(genomeA, genomeB);
            if (!cotailed) {
                int nbGenes = genomeA.getNumberOfGenes();
                genomeB = Utilities.genomeCappingAndNumberAdaption(genomeB, nbGenes + 1, nbGenes + 2);
                genomeC = Utilities.genomeCappingAndNumberAdaptionRev(genomeA.clone(), nbGenes + 1, nbGenes + 2);
                genomeA = Utilities.genomeCappingAndNumberAdaption(genomeA, nbGenes + 1, nbGenes + 2);
            }

            final int sumChroms = genomeA.getNumberOfChromosomes() + genomeB.getNumberOfChromosomes();
            if (linear && sumChroms == 2) {
                additionalData = new AdditionalDataHPDistance(genomeA);
                final DistanceInv calcInvDist = new DistanceInv();
                AdjacencyGraph cappedAdjGraph = new AdjacencyGraph(genomeA, genomeB);
                Data cappedData = new Data(genomeA, genomeB, cappedAdjGraph);
                invDistance = calcInvDist.calculateDistance(cappedData, additionalData);

                if (!cotailed) {// in non cotailed case check second capping distance too
                    AdditionalDataHPDistance addData2 = new AdditionalDataHPDistance(genomeC);
                    AdjacencyGraph cappedAdjGraph2 = new AdjacencyGraph(genomeC, genomeB);
                    Data cappedData2 = new Data(genomeC, genomeB, cappedAdjGraph2);
                    int invDistance2 = calcInvDist.calculateDistance(cappedData2, addData2);

                    if (invDistance2 < invDistance) {
                        invDistance = invDistance2;
                        hpDistance = calcHPDist.calculateDistance(cappedData2, addData2);
                    }
                }
                assert invDistance == genome2dist.get(a);
                assert invDistance == hpDistance;
            }
        }
    }
}
