/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.GenomeExamples;
import de.unibi.cebitec.gi.unimog.datastructure.*;
import de.unibi.cebitec.gi.unimog.GraphExamples;
import de.unibi.cebitec.gi.unimog.exceptions.InputOutputException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.HashMap;

/**
 * @author -Rolf Hilker-
 * <p>
 * Test class for the sorting algorithms.
 */
public class SortingTest {

    private GraphExamples agExamples = new GraphExamples();
    private AdjacencyGraph ag2 = this.agExamples.getAG2();

    private GenomeExamples genomeExamples = new GenomeExamples();

    // get for every genome its reference genome
    private HashMap<Genome, Genome> genome2ref = new HashMap<>() {{
        put(genomeExamples.getGenomeInv1(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv2(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv3(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv4(), genomeExamples.getGenomeInvRef17());
        put(genomeExamples.getGenomeInv5(), genomeExamples.getGenomeInvRef24());
        put(genomeExamples.getGenomeInv6(), genomeExamples.getGenomeInvRef24());
        put(genomeExamples.getGenomeInv7(), genomeExamples.getGenomeInvRef25());
        put(genomeExamples.getGenomeInv8(), genomeExamples.getGenomeInvRef8());
        put(genomeExamples.getGenomeInv9(), genomeExamples.getGenomeInvRef3());
        put(genomeExamples.getGenomeInv10(), genomeExamples.getGenomeInvRef18());
        put(genomeExamples.getGenomeInv13(), genomeExamples.getGenomeInvRef13());
    }};

    private HashMap<Genome, Integer> genome2dist = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), 17);
            put(genomeExamples.getGenomeInv2(), 16);
            put(genomeExamples.getGenomeInv3(), 16);
            put(genomeExamples.getGenomeInv4(), 15);
            put(genomeExamples.getGenomeInv5(), 21);
            put(genomeExamples.getGenomeInv6(), 21);
            put(genomeExamples.getGenomeInv7(), 21);
            put(genomeExamples.getGenomeInv8(), 8);
            put(genomeExamples.getGenomeInv9(), 2);
            put(genomeExamples.getGenomeInv10(), 12);
            put(genomeExamples.getGenomeInv13(), 7);
        }
    };

    public SortingTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Tests the greedy DCJ sorting.
     */
    @Test
    public void testFindOptSortSequence() {
        SortingDCJ sortDCJ = new SortingDCJ();
        Data data2 = new Data();
        data2.setAdjGraph(this.ag2);
        //sortDCJ.findOptSortSequence(data2, null,Genomeparser);
    }

    /**
     * Tests the number of inversion sorting steps.
     **/
    @Test
    public void testFindOptInvSortSequence() throws InputOutputException {

        HashMap<Genome, Boolean> isGenomeCapped = new HashMap<>() {
            {
                put(genomeExamples.getGenomeInv1(), Boolean.FALSE);
                put(genomeExamples.getGenomeInv2(), Boolean.FALSE);
                put(genomeExamples.getGenomeInv3(), Boolean.FALSE);
                put(genomeExamples.getGenomeInv4(), Boolean.FALSE);
                put(genomeExamples.getGenomeInv5(), Boolean.FALSE);
                put(genomeExamples.getGenomeInv6(), Boolean.FALSE);
                put(genomeExamples.getGenomeInv7(), Boolean.FALSE);
                put(genomeExamples.getGenomeInv8(), Boolean.TRUE);
                put(genomeExamples.getGenomeInv9(), Boolean.TRUE);
                put(genomeExamples.getGenomeInv10(), Boolean.FALSE);
                put(genomeExamples.getGenomeInv13(), Boolean.FALSE);
            }
        };


        //copied from MainClass

        for (Genome a : genome2ref.keySet()) {
            AdditionalDataHPDistance additionalData;
            GenomeParser parser = new GenomeParser();
            boolean useOtherScenario = isGenomeCapped.get(a);

            // cast genomes to string and back to genomes because of readGenomes method
            // which only takes strings as input
            Genome fromStringGenomeB = genome2ref.get(a).clone();
            Genome fromStringGenomeA = a.clone();
            String gA = fromStringGenomeA.toString().replaceAll("\\[", "").replaceAll("]", "").replace(",", "");
            String gB = fromStringGenomeB.toString().replaceAll("\\[", "").replaceAll("]", "").replace(",", "");

            DataFramework globalData = parser.readGenomes(String.format(">A\n%s|\n>B\n%s|", gA, gB));
            Pair<Genome, Genome> genomePair = globalData.preprocessGenomePair(0, 1);
            Genome genomeA = genomePair.getFirst();
            Genome genomeB = genomePair.getSecond();

            AdjacencyGraph adjacencyGraph = new AdjacencyGraph(genomeA, genomeB);
            Data data = new Data(genomeA, genomeB, adjacencyGraph);
            Data cappedData;

            boolean cotailed = Utilities.checkCotailed(genomeA, genomeB);
            Genome genomeACapped = null;
            Genome genomeBCapped = null;
            Genome genomeCCapped = null;

            if (!cotailed) { // then both genomes have to be capped and gene numbers adapted
                int nbGenes = genomeA.getNumberOfGenes();
                genomeACapped = Utilities.genomeCappingAndNumberAdaption(genomeA, nbGenes + 1, nbGenes + 2);
                genomeBCapped = Utilities.genomeCappingAndNumberAdaption(genomeB, nbGenes + 1, nbGenes + 2);
                genomeCCapped = Utilities.genomeCappingAndNumberAdaptionRev(genomeA, nbGenes + 1, nbGenes + 2);
                additionalData = new AdditionalDataHPDistance(genomeACapped);
                AdjacencyGraph cappedAdjGraph = new AdjacencyGraph(genomeACapped, genomeBCapped);
                cappedData = new Data(genomeACapped, genomeBCapped, cappedAdjGraph);
            } else {
                cappedData = data;
                additionalData = new AdditionalDataHPDistance(genomeA);
            }

            // //////////// sort genomes according to parameter stored during distance calculation /////////////
            final SortingInv invSorting = new SortingInv();
            OperationList result;
            if (!useOtherScenario) { // remember that chromMaps are not used here, because its unichromosomal!
                result = invSorting.findOptSortSequence(cappedData, additionalData, globalData.getChromMaps().get(0));
            } else {// in non cotailed case check second capping distance too

                AdditionalDataHPDistance addData2 = new AdditionalDataHPDistance(genomeCCapped);
                AdjacencyGraph cappedAdjGraph2 = new AdjacencyGraph(genomeCCapped, genomeBCapped);
                Data cappedData2 = new Data(genomeCCapped, genomeBCapped, cappedAdjGraph2);
                result = invSorting.findOptSortSequence(cappedData2, addData2, globalData.getChromMaps().get(0));
            }

            if (!cotailed) { // undo gene number adaption back to original gene numbers
                // also means decreasing all values in adjacency graphs and operation lists!
                result = Utilities.correctOperationList(result, genomeA.getNumberOfGenes());
            }

            IntermediateGenomesGenerator genomesGenerator = new IntermediateGenomesGenerator(genomeA, globalData.getBackMap(), result, data.getAdjGraph().getAdjacenciesGenome1(),
                    "A", "B");
            assert (genomesGenerator.intermedGenomes.size() - 1) == this.genome2dist.get(a);
        }
    }


    @Test
    public void testFindOptHPSortSequence() throws InputOutputException {

        this.genome2ref.put(this.genomeExamples.getGenomeHP9(), this.genomeExamples.getGenomeHP9Ref());
        this.genome2ref.put(this.genomeExamples.getGenomeHP10(), this.genomeExamples.getGenomeHP10Ref());
        this.genome2ref.put(this.genomeExamples.getGenomeHP11(), this.genomeExamples.getGenomeHP11Ref());
        this.genome2ref.put(this.genomeExamples.getGenomeHP12(), this.genomeExamples.getGenomeHP12Ref());

        this.genome2dist.put(this.genomeExamples.getGenomeHP9(), 13);
        this.genome2dist.put(this.genomeExamples.getGenomeHP10(), 14);
        this.genome2dist.put(this.genomeExamples.getGenomeHP11(), 21);
        this.genome2dist.put(this.genomeExamples.getGenomeHP12(), 23);


        for (Genome a : genome2ref.keySet()) {
            AdditionalDataHPDistance additionalData;
            GenomeParser parser = new GenomeParser();

            // cast genomes to string and back to genomes because of readGenomes method
            // which only takes strings as input
            Genome fromStringGenomeB = genome2ref.get(a).clone();
            Genome fromStringGenomeA = a.clone();
            String gA = fromStringGenomeA.toString().replaceAll("\\[", "").replaceAll("]", "").replace(",", "");
            String gB = fromStringGenomeB.toString().replaceAll("\\[", "").replaceAll("]", "").replace(",", "");

            DataFramework globalData = parser.readGenomes(String.format(">A\n%s|\n>B\n%s|", gA, gB));
            Pair<Genome, Genome> genomePair = globalData.preprocessGenomePair(0, 1);
            Genome genomeA = genomePair.getFirst();
            Genome genomeB = genomePair.getSecond();

            AdjacencyGraph adjacencyGraph = new AdjacencyGraph(genomeA, genomeB);
            Data data = new Data(genomeA, genomeB, adjacencyGraph);
            data.setAdjGraph(adjacencyGraph);

            final SortingHPEasy hpSorting = new SortingHPEasy();
            additionalData = new AdditionalDataHPDistance(data.getGenomeA());
            OperationList result = hpSorting.findOptSortSequence(data, additionalData, globalData.getChromMaps().get(0));

            IntermediateGenomesGenerator genomesGenerator = new IntermediateGenomesGenerator(genomeA, globalData.getBackMap(), result, data.getAdjGraph().getAdjacenciesGenome1(),
                    "A", "B");
            assert (genomesGenerator.intermedGenomes.size() - 1) == this.genome2dist.get(a);
        }

    }

}
