package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.GenomeExamples;
import de.unibi.cebitec.gi.unimog.datastructure.AdditionalDataHPDistance;
import de.unibi.cebitec.gi.unimog.datastructure.AdjacencyGraph;
import de.unibi.cebitec.gi.unimog.datastructure.Data;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;
import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.MultifurcatedTree;
import org.junit.Test;

import java.util.HashMap;

public class ContractedCompTreeTest {

    private GenomeExamples genomeExamples = new GenomeExamples();

    private HashMap<Genome, Genome> genome2ref = new HashMap<>() {{
        put(genomeExamples.getGenomeInv1(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv2(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv3(), genomeExamples.getGenomeInvRef21());
        put(genomeExamples.getGenomeInv4(), genomeExamples.getGenomeInvRef17());
        put(genomeExamples.getGenomeInv5(), genomeExamples.getGenomeInvRef24());
        put(genomeExamples.getGenomeInv6(), genomeExamples.getGenomeInvRef24());
        put(genomeExamples.getGenomeInv7(), genomeExamples.getGenomeInvRef25());
        put(genomeExamples.getGenomeInv8(), genomeExamples.getGenomeInvRef8());
        put(genomeExamples.getGenomeInv9(), genomeExamples.getGenomeInvRef3());
        put(genomeExamples.getGenomeInv10(), genomeExamples.getGenomeInvRef18());
        put(genomeExamples.getGenomeInv11(), genomeExamples.getGenomeInv11Ref());
        put(genomeExamples.getGenomeHP9(), genomeExamples.getGenomeHP9Ref());
        put(genomeExamples.getGenomeHP10(), genomeExamples.getGenomeHP10Ref());
        put(genomeExamples.getGenomeHP11(), genomeExamples.getGenomeHP11Ref());
    }};

    private HashMap<Genome, Integer> genome2shortLeaf = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), 0);
            put(genomeExamples.getGenomeInv2(), 1);
            put(genomeExamples.getGenomeInv3(), 1);
            put(genomeExamples.getGenomeInv4(), 3);
            put(genomeExamples.getGenomeInv5(), 0);
            put(genomeExamples.getGenomeInv6(), 0);
            put(genomeExamples.getGenomeInv7(), 0);
            put(genomeExamples.getGenomeInv8(), 1);
            put(genomeExamples.getGenomeInv9(), 0);
            put(genomeExamples.getGenomeInv10(), 0);
            put(genomeExamples.getGenomeInv11(), 0);
            put(genomeExamples.getGenomeHP9(), 1);
            put(genomeExamples.getGenomeHP10(), 0);
            put(genomeExamples.getGenomeHP11(), 1);
        }
    };
    private HashMap<Genome, Integer> genome2WhiteLeaf = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), 3);
            put(genomeExamples.getGenomeInv2(), 3);
            put(genomeExamples.getGenomeInv3(), 3);
            put(genomeExamples.getGenomeInv4(), 3);
            put(genomeExamples.getGenomeInv5(), 3);
            put(genomeExamples.getGenomeInv6(), 2);
            put(genomeExamples.getGenomeInv7(), 2);
            put(genomeExamples.getGenomeInv8(), 1);
            put(genomeExamples.getGenomeInv9(), 0);
            put(genomeExamples.getGenomeInv10(), 2);
            put(genomeExamples.getGenomeInv11(), 0);
            put(genomeExamples.getGenomeHP9(), 1);
            put(genomeExamples.getGenomeHP10(), 2);
            put(genomeExamples.getGenomeHP11(), 2);
        }
    };

    private HashMap<Genome, Integer> genome2GreyLeaf = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), 0);
            put(genomeExamples.getGenomeInv2(), 0);
            put(genomeExamples.getGenomeInv3(), 0);
            put(genomeExamples.getGenomeInv4(), 0);
            put(genomeExamples.getGenomeInv5(), 0);
            put(genomeExamples.getGenomeInv6(), 0);
            put(genomeExamples.getGenomeInv7(), 0);
            put(genomeExamples.getGenomeInv8(), 2);
            put(genomeExamples.getGenomeInv9(), 0);
            put(genomeExamples.getGenomeInv10(), 1);
            put(genomeExamples.getGenomeInv11(), 0);
            put(genomeExamples.getGenomeHP9(), 2);
            put(genomeExamples.getGenomeHP10(), 0);
            put(genomeExamples.getGenomeHP11(), 1);
        }
    };

    private HashMap<Genome, Integer> genome2WhiteRootNodeIdx = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), 1);
            put(genomeExamples.getGenomeInv2(), 1);
            put(genomeExamples.getGenomeInv3(), 1);
            put(genomeExamples.getGenomeInv4(), -1);
            put(genomeExamples.getGenomeInv5(), 7);
            put(genomeExamples.getGenomeInv6(), 2);
            put(genomeExamples.getGenomeInv7(), 2);
            put(genomeExamples.getGenomeInv8(), -1);
            put(genomeExamples.getGenomeInv9(), -1);
            put(genomeExamples.getGenomeInv10(), 1);
            put(genomeExamples.getGenomeInv11(), -1);
            put(genomeExamples.getGenomeHP9(), -1);
            put(genomeExamples.getGenomeHP10(), -1);
            put(genomeExamples.getGenomeHP11(), -1);
        }
    };
    private HashMap<Genome, Boolean> genomeIsDangerous = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv2(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv3(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv4(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv5(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv6(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv7(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv8(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv9(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv10(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv11(), Boolean.FALSE);
            put(genomeExamples.getGenomeHP9(), Boolean.FALSE);
            put(genomeExamples.getGenomeHP10(), Boolean.FALSE);
            put(genomeExamples.getGenomeHP11(), Boolean.FALSE);
        }
    };

    private HashMap<Genome, Boolean> genomeHasShortDanger = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv2(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv3(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv4(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv5(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv6(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv7(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv8(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv9(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv10(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv11(), Boolean.FALSE);
            put(genomeExamples.getGenomeHP9(), Boolean.FALSE);
            put(genomeExamples.getGenomeHP10(), Boolean.FALSE);
            put(genomeExamples.getGenomeHP11(), Boolean.FALSE);
        }
    };

    private HashMap<Genome, Boolean> isAllInOneHP = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv2(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv3(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv4(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv5(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv6(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv7(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv8(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv9(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv10(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv11(), Boolean.TRUE);
            put(genomeExamples.getGenomeHP9(), Boolean.TRUE);
            put(genomeExamples.getGenomeHP10(), Boolean.TRUE);
            put(genomeExamples.getGenomeHP11(), Boolean.TRUE);
        }
    };

    private HashMap<Genome, Boolean> isAllInOneInv = new HashMap<>() {
        {
            put(genomeExamples.getGenomeInv1(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv2(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv3(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv4(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv5(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv6(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv7(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv8(), Boolean.TRUE);
            put(genomeExamples.getGenomeInv9(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv10(), Boolean.FALSE);
            put(genomeExamples.getGenomeInv11(), Boolean.TRUE);
            put(genomeExamples.getGenomeHP9(), Boolean.TRUE);
            put(genomeExamples.getGenomeHP10(), Boolean.TRUE);
            put(genomeExamples.getGenomeHP11(), Boolean.TRUE);
        }
    };

    /**
     * Tests contraction of component trees with grey leaves.
     **/
    @Test
    public void contractTreeHPModelTest() {
        for (int i = 0; i < genome2ref.size(); i++) {
            Genome genomeA = ((Genome) (genome2ref.keySet().toArray())[i]);
            Genome genomeB = genome2ref.get(genomeA).clone();

            Data data = new Data(genomeA.clone(), genomeB.clone(), new AdjacencyGraph(genomeA, genomeB));
            AdditionalDataHPDistance additionalHPData = new AdditionalDataHPDistance(data.getGenomeA());

            final HPBasedDistPreprocessing hpPreprocess = new HPBasedDistPreprocessing(data, additionalHPData);
            final MultifurcatedTree componentTree = hpPreprocess.getCompTree();

            ContractedCompTree cct = new ContractedCompTree(componentTree.getRoot());

            assert cct.getNbrWhiteLeaves() == genome2WhiteLeaf.get(genomeA);
            assert cct.getNbrGreyLeaves() == genome2GreyLeaf.get(genomeA);
            assert cct.getNbrShortLeaves() == genome2shortLeaf.get(genomeA);
            assert cct.isDangerous() == genomeIsDangerous.get(genomeA);
            assert cct.hasShortDangerousNode() == genomeHasShortDanger.get(genomeA);
            assert cct.getWhiteRootNodeIdx() == genome2WhiteRootNodeIdx.get(genomeA);
            assert cct.isAllInOne() == isAllInOneHP.get(genomeA);
        }
    }

    /**
     * Tests contraction of component trees without grey leaves.
     **/
    @Test
    public void contractTreeInvModelTest() {
        for (int i = 0; i < genome2ref.size(); i++) {
            Genome gA = ((Genome) (genome2ref.keySet().toArray())[i]);
            Genome genomeA = ((Genome) (genome2ref.keySet().toArray())[i]).clone();
            Genome genomeB = genome2ref.get(gA).clone();
            int nbGenes = genomeA.getNumberOfGenes();
            Genome genomeC = Utilities.genomeCappingAndNumberAdaptionRev(gA.clone(), nbGenes + 1, nbGenes + 2);

            final boolean linear = Utilities.onlyLinear(genomeA) && Utilities.onlyLinear(genomeB);
            final boolean cotailed = Utilities.checkCotailed(genomeA, genomeB);
            if (!cotailed) {
                genomeA = Utilities.genomeCappingAndNumberAdaption(genomeA, nbGenes + 1, nbGenes + 2);
                genomeB = Utilities.genomeCappingAndNumberAdaption(genomeB, nbGenes + 1, nbGenes + 2);
            }
            final int sumChroms = genomeA.getNumberOfChromosomes() + genomeB.getNumberOfChromosomes();
            if (linear && sumChroms == 2) {
                AdditionalDataHPDistance additionalData = new AdditionalDataHPDistance(genomeA);

                AdjacencyGraph cappedAdjGraph = new AdjacencyGraph(genomeA, genomeB);
                Data cappedData = new Data(genomeA, genomeB, cappedAdjGraph);
                final HPBasedDistPreprocessing hpPreprocess = new HPBasedDistPreprocessing(cappedData, additionalData);
                final MultifurcatedTree componentTree = hpPreprocess.getCompTree();
                ContractedCompTree cct = new ContractedCompTree(componentTree.getRoot());

                AdditionalDataHPDistance addData2 = new AdditionalDataHPDistance(genomeC);
                if (cotailed) {
                    genomeB = Utilities.genomeCappingAndNumberAdaption(genomeB, nbGenes + 1, nbGenes + 2);
                }
                AdjacencyGraph cappedAdjGraph2 = new AdjacencyGraph(genomeC, genomeB);
                Data cappedData2 = new Data(genomeC, genomeB, cappedAdjGraph2);
                final HPBasedDistPreprocessing hpPreprocess2 = new HPBasedDistPreprocessing(cappedData2, addData2);
                final MultifurcatedTree componentTree2 = hpPreprocess2.getCompTree();
                ContractedCompTree cct2 = new ContractedCompTree(componentTree2.getRoot());

                // because of two different capping scenarios, the tree resulting in the smallest distance is used
                assert cct.getNbrWhiteLeaves() == genome2WhiteLeaf.get(gA)
                        || cct2.getNbrWhiteLeaves() == genome2WhiteLeaf.get(gA);
                assert cct.getNbrShortLeaves() == genome2shortLeaf.get(gA)
                        || cct2.getNbrShortLeaves() == genome2shortLeaf.get(gA);
                assert cct.getWhiteRootNodeIdx() == genome2WhiteRootNodeIdx.get(gA)
                        || cct2.getWhiteRootNodeIdx() == genome2WhiteRootNodeIdx.get(gA);
                assert cct.isAllInOne() == isAllInOneInv.get(gA)
                        || cct2.isAllInOne() == isAllInOneInv.get(gA);
            }
        }
    }
}
