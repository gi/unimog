

package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.datastructure.DataOutput;
import de.unibi.cebitec.gi.unimog.exceptions.InputOutputException;
import de.unibi.cebitec.gi.unimog.framework.MainClass;
import de.unibi.cebitec.gi.unimog.framework.Model;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author gatter
 */
public class dcjInDelSorting {
    
    public dcjInDelSorting() {
        rg = new Random();
    }
    
    private Random rg;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void FullTest() throws InputOutputException, InterruptedException {
        
        int chunklength = 4;
        int chunks = 5;
        int separation_likelyhood = 5;
        int inDel_likelyhood = 50;
        int circular_likelyhood = 10;
        int singleton_likelyhood = 25;
        int inDelIndex = 0;
        
        MainClass main = new MainClass();
        
        for(int i=0; i<1000; i++) {
            
            inDelIndex = 0;
            StringBuilder genome = new StringBuilder();
            inDelIndex = generateGenome(genome, "G1", chunklength, chunks, separation_likelyhood, inDel_likelyhood, circular_likelyhood, singleton_likelyhood, inDelIndex);
            generateGenome(genome, "G2", chunklength, chunks, separation_likelyhood, inDel_likelyhood, circular_likelyhood, singleton_likelyhood, inDelIndex);
           
            System.out.println(genome.toString());
            main.execute(Model.DCJ_INDEL, null, genome.toString());
            
            DataOutput o = main.getOutputData()[0];
            if(o.getDistances()[0]+1 != o.getIntermedGenomes()[0].intermedGenomes.size()) {
                fail(i+genome.toString());
            }
            
        }
        
    }
    
    
    
    private int generateGenome(StringBuilder genome, String id, int chunklength, int chunks , int separation_likelyhood,
            int inDel_likelyhood, int circular_likelyhood, int singleton_likelyhood, int inDelIndex) {
        
        genome.append(">").append(id).append("\n");
        for(int i=0; i<chunks; i++) {
            inDelIndex = builtChunk(genome, i,  chunklength, separation_likelyhood, inDel_likelyhood, circular_likelyhood, inDelIndex);
            genome.append(" | ");
            
            if(100- rg.nextInt(100) <= singleton_likelyhood) {
                genome.append(" l").append(inDelIndex).append(" ");
                inDelIndex++;
                genome.append(" | ");
            }
        }
        
        genome.append("\n");
        
        return inDelIndex;
    }
    
    
    private int builtChunk(StringBuilder genome, int chunkindex, int chunklength, int separation_likelyhood,
            int inDel_likelyhood, int circular_likelyhood, int inDelIndex) {
        
        List<String> comps = new ArrayList<String>();
        for(int i=chunkindex*chunklength+1; i<=chunkindex*chunklength + chunklength; i++) {
            comps.add(Integer.toString(i));
        }
        
        inDelIndex = addIndel(genome, inDelIndex, inDel_likelyhood);
        
        while(comps.size()> 0) {
            
            int index = rg.nextInt(comps.size());
            String adj = comps.remove(index);
            genome.append(" ").append(adj).append(" ");
            
            inDelIndex = addIndel(genome, inDelIndex, inDel_likelyhood);
            
            if (comps.size()> 0) {
                if ( 100- rg.nextInt(100) <= separation_likelyhood ) {
                    if (100 - rg.nextInt(100) <= circular_likelyhood ) {
                        genome.append(" ) ");
                    } else {
                        genome.append(" | ");
                    }
                    
                    inDelIndex = addIndel(genome, inDelIndex, inDel_likelyhood);
                }
            }
        }
        
        return inDelIndex;
    }
    
    private int addIndel(StringBuilder genome, int indelIndex, int inDel_likelyhood) {
        if ( 100- rg.nextInt(100) <= inDel_likelyhood ) {
            genome.append(" l").append(indelIndex).append(" ");
            indelIndex++;
        }
        return indelIndex;
    }
    
    
}
