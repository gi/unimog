/**
 * 
 */
package de.unibi.cebitec.gi.unimog.datastructure.sampling;

import java.util.Random;

import junit.framework.Assert;

import org.junit.Test;

import de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure;

/**
 * @author Eyla Willing
 */
public class ComponentSampleTest {

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#ComponentSample(int, int, de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure, boolean)}
     * .
     */
    @Test
    public final void testComponentSample() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        new ComponentSample(start, end, Structure.getStructure((new Random()).nextInt()), oriented);
    }

    /**
     * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#getStructure()}.
     */
    @Test
    public final void testGetStructure() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        for (int i = 0; i < 5; i++) {
            final ComponentSample component = new ComponentSample(start, end, Structure.getStructure(i), oriented);
            Assert.assertEquals(Structure.getStructure(i), component.getStructure());
        }
    }

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#updateStructure(de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure)}
     * .
     */
    @Test
    public final void testUpdateStructure() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        for (int i = 0; i < 5; i++) {
            final ComponentSample component = new ComponentSample(start, end, Structure.getStructure(i), oriented);
            Assert.assertEquals(Structure.getStructure(i), component.getStructure());
            for (int j = 0; j < 5; j++) {
                component.updateStructure(Structure.getStructure(j));
                Assert.assertEquals(Structure.getStructure(j), component.getStructure());
            }
        }
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#swapPathEnds()}. */
    @Test
    public final void testSwapPathEnds() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        final ComponentSample component = new ComponentSample(start, end, Structure.getStructure((new Random())
            .nextInt()), oriented);
        Assert.assertEquals(start, component.getStartIndex());
        Assert.assertEquals(end, component.getEndIndex());
        component.swapPathEnds();
        Assert.assertEquals(end, component.getStartIndex());
        Assert.assertEquals(start, component.getEndIndex());
        final int newEnd = (new Random()).nextInt(Integer.MAX_VALUE);
        component.updateEndIndex(newEnd);
        Assert.assertEquals(newEnd, component.getEndIndex());
        Assert.assertEquals(end, component.getStartIndex());
        component.swapPathEnds();
        Assert.assertEquals(end, component.getEndIndex());
        Assert.assertEquals(newEnd, component.getStartIndex());
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#updateEndIndex(int)}. */
    @Test
    public final void testUpdateEndIndex() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        final ComponentSample component = new ComponentSample(start, end, Structure.getStructure((new Random())
            .nextInt()), oriented);
        Assert.assertEquals(end, component.getEndIndex());
        final int newEnd = (new Random()).nextInt(Integer.MAX_VALUE);
        component.updateEndIndex(newEnd);
        Assert.assertEquals(newEnd, component.getEndIndex());
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#updateStartIndex(int)}. */
    @Test
    public final void testUpdateStartIndex() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        final ComponentSample component = new ComponentSample(start, end, Structure.getStructure((new Random())
            .nextInt()), oriented);
        Assert.assertEquals(start, component.getStartIndex());
        final int newStart = (new Random()).nextInt(Integer.MAX_VALUE);
        component.updateStartIndex(newStart);
        Assert.assertEquals(newStart, component.getStartIndex());
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#toString()}. */
    @Test
    public final void testToString() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        final Structure structure = Structure.getStructure((new Random()).nextInt());
        final ComponentSample component = new ComponentSample(start, end, structure, oriented);
        Assert.assertEquals(structure.toString() + "\tstart: " + start + "\tend: " + end + "\t", component.toString());
        component.swapPathEnds();
        Assert.assertEquals(structure.toString() + "\tstart: " + end + "\tend: " + start + "\t", component.toString());
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#equals(ComponentSample)}. */
    @Test
    public final void testEquals() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        final int randomIdx = (new Random()).nextInt(5);
        final Structure structure = Structure.getStructure(randomIdx);
        final ComponentSample first = new ComponentSample(start, end, structure, oriented);
        final ComponentSample second = new ComponentSample(start, end, structure, oriented);
        final ComponentSample third = new ComponentSample(start, end, structure, oriented);
        final ComponentSample fourth = new ComponentSample(start, end, structure, oriented);
        final ComponentSample fifth = new ComponentSample(start, end, structure, oriented);
        Assert.assertTrue(first.equals(second));
        Assert.assertNotSame(first, second);
        Assert.assertEquals(first.toString(), second.toString());
        second.updateEndIndex(end+1);
        Assert.assertFalse(first.equals(second));
        Assert.assertNotSame(first, second);
        
        Assert.assertTrue(first.equals(third));
        Assert.assertNotSame(first, third);
        Assert.assertEquals(first.toString(), third.toString());
        third.updateStartIndex(start+1);
        Assert.assertFalse(first.equals(third));
        Assert.assertNotSame(first, third);

        Assert.assertTrue(first.equals(fourth));
        Assert.assertNotSame(first, fourth);
        Assert.assertEquals(first.toString(), fourth.toString());
        fourth.updateStructure(Structure.getStructure(randomIdx+1));
        Assert.assertFalse(first.equals(fourth));
        Assert.assertNotSame(first, fourth);
        
        Assert.assertTrue(first.equals(fifth));
        Assert.assertNotSame(first, fifth);
        Assert.assertEquals(first.toString(), fifth.toString());
        fifth.swapPathEnds();
        Assert.assertFalse(first.equals(fifth));
        Assert.assertNotSame(first, fifth);
    }
    
    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample#clone()}. */
    @Test
    public final void testClone() {
        final int start = (new Random()).nextInt(Integer.MAX_VALUE);
        final int end = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean oriented = (new Random()).nextBoolean();
        final Structure structure = Structure.getStructure((new Random()).nextInt());
        final ComponentSample component = new ComponentSample(start, end, structure, oriented);
        Assert.assertEquals(structure.toString() + "\tstart: " + start + "\tend: " + end + "\t", component.toString());
        final ComponentSample clone = component.clone();
        Assert.assertTrue(component.equals(clone));
    }
    
}