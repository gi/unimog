package de.unibi.cebitec.gi.unimog.datastructure.sampling;

import java.util.Random;

import junit.framework.Assert;

import org.junit.Test;

import de.luschny.math.arithmetic.Xint;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights;

/**
 * Test class for {@link SampleWeights}.
 * 
 * @author Eyla Willing
 */
public class SampleWeightsTest {

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights#SampleWeights(int, int, int, de.luschny.math.arithmetic.Xint, boolean)}
     * .
     */
    @Test
    public final void testSampleWeights() {
        final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
        final int randSplit = (new Random()).nextInt(randIdx);
        final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
        final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean special = (new Random()).nextBoolean();
        new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement), special);
    }

    /**
     * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights#getWeight()}.
     */
    @Test
    public final void testGetWeight() {
        final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
        final int randSplit = (new Random()).nextInt(randIdx);
        final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
        final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
        final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement),
            false);
        final Xint weight = (Xint.valueOf(randIdx + 1).multiply(randSize)).multiply(scenariosPerElement);
        Assert.assertEquals(0, sample.getWeight().compareTo(weight));
        Assert.assertEquals(weight.toString(), sample.getWeight().toString());
        if (randIdx % 2 == 0) {
            final SampleWeights sample2 = new SampleWeights(randIdx + 1, randSplit, randSize,
                Xint.valueOf(scenariosPerElement), true);
            final Xint weight2 = (Xint.valueOf(randIdx + 2).divide(2).multiply(randSize)).multiply(scenariosPerElement);
            Assert.assertEquals(0, sample2.getWeight().compareTo(weight2));
            Assert.assertEquals(weight2.toString(), sample2.getWeight().toString());
        } else {
            final SampleWeights sample2 = new SampleWeights(randIdx, randSplit, randSize,
                Xint.valueOf(scenariosPerElement), true);
            final Xint weight2 = (Xint.valueOf(randIdx + 1).divide(2).multiply(randSize)).multiply(scenariosPerElement);
            Assert.assertEquals(0, sample2.getWeight().compareTo(weight2));
            Assert.assertEquals(weight2.toString(), sample2.getWeight().toString());
        }
    }

    /**
     * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights#getGroupDistance()}.
     */
    @Test
    public final void testGetGroupDistance() {
        final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
        final int randSplit = (new Random()).nextInt(randIdx);
        final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
        final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean special = (new Random()).nextBoolean();
        final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement),
            special);
        Assert.assertEquals(randIdx, sample.getGroupDistance());
    }

    /**
     * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights#getSplitGroupID()}.
     */
    @Test
    public final void testGetSplitGroupID() {
        final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
        final int randSplit = (new Random()).nextInt(randIdx);
        final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
        final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean special = (new Random()).nextBoolean();
        final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement),
            special);
        Assert.assertEquals(randSplit, sample.getSplitGroupID());
    }

    /**
     * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights#getScenariosPerElement()}.
     */
    @Test
    public final void testGetScenariosPerElement() {
        final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
        final int randSplit = (new Random()).nextInt(randIdx);
        final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
        final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean special = (new Random()).nextBoolean();
        final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement),
            special);
        Assert.assertEquals(0, sample.getScenariosPerElement().compareTo(Xint.valueOf(scenariosPerElement)));
        Assert.assertEquals(Xint.valueOf(scenariosPerElement).toString(), sample.getScenariosPerElement().toString());
        // sample.scale(Xint.valueOf((new Random()).nextInt(scenariosPerElement)));
        // Assert.assertEquals(0, sample.getScenariosPerElement().compareTo(Xint.valueOf(scenariosPerElement)));
        // Assert.assertEquals(Xint.valueOf(scenariosPerElement).toString(),
        // sample.getScenariosPerElement().toString());
    }

    //
    // /**
    // * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.SampleWeights#isSpecialSplitGroup()}.
    // */
    // @Test
    // public final void testIsSpecialSplitGroup() {
    // final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
    // final int randSplit = (new Random()).nextInt(randIdx);
    // final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
    // final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
    // final boolean special = (new Random()).nextBoolean();
    // final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement),
    // special);
    // Assert.assertEquals(special, sample.isSpecialSplitGroup());
    // }

    // /**
    // * Test method for
    // * {@link de.unibi.cebitec.gi.unimog.datastructure.SampleWeights#scale(de.luschny.math.arithmetic.Xint)}.
    // */
    // @Test
    // public final void testScale() {
    // final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
    // final int randSplit = (new Random()).nextInt(randIdx);
    // final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
    // final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
    // final boolean special = (new Random()).nextBoolean();
    // final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement),
    // special);
    // Assert.assertEquals(0, sample.getScenariosPerElement().compareTo(Xint.valueOf(scenariosPerElement)));
    // Assert.assertEquals(Xint.valueOf(scenariosPerElement).toString(), sample.getScenariosPerElement().toString());
    // Assert.assertEquals(0, sample.getWeight().compareTo(sample.getScaledWeight()));
    // final int scaleFactor = (new Random()).nextInt(scenariosPerElement);
    // sample.scale(Xint.valueOf(scaleFactor));
    // Assert.assertEquals(0, sample.getScenariosPerElement().compareTo(Xint.valueOf(scenariosPerElement)));
    // Assert.assertEquals(Xint.valueOf(scenariosPerElement).toString(), sample.getScenariosPerElement().toString());
    // Assert.assertEquals(0, sample.getScaledWeight().compareTo(sample.getWeight().divide(scaleFactor)));
    // }
    //
    // /**
    // * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.SampleWeights#getScaledWeight()}.
    // */
    // @Test
    // public final void testGetScaledWeight() {
    // final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
    // final int randSplit = (new Random()).nextInt(randIdx);
    // final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
    // final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
    // final boolean special = (new Random()).nextBoolean();
    // final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement),
    // special);
    // Assert.assertEquals(0, sample.getScenariosPerElement().compareTo(Xint.valueOf(scenariosPerElement)));
    // Assert.assertEquals(Xint.valueOf(scenariosPerElement).toString(), sample.getScenariosPerElement().toString());
    // Assert.assertEquals(0, sample.getWeight().compareTo(sample.getScaledWeight()));
    // final int scaleFactor = (new Random()).nextInt(scenariosPerElement);
    // sample.scale(Xint.valueOf(scaleFactor));
    // Assert.assertEquals(0, sample.getScenariosPerElement().compareTo(Xint.valueOf(scenariosPerElement)));
    // Assert.assertEquals(Xint.valueOf(scenariosPerElement).toString(), sample.getScenariosPerElement().toString());
    // Assert.assertEquals(0, sample.getScaledWeight().compareTo(sample.getWeight().divide(scaleFactor)));
    // }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights#getNumberOfComponents()}. */
    @Test
    public final void testGetNumberOfComponents() {
        final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE - 1) + 1;
        final int randSplit = (new Random()).nextInt(randIdx);
        final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
        final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
        final boolean special = (new Random()).nextBoolean();
        final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize, Xint.valueOf(scenariosPerElement),
            special);
        Assert.assertEquals(randSize, sample.getNumberOfComponents());
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights#getNumberOfSplitGroupElements()}. */
    @Test
    public final void testGetNumberOfSplitGroupElements() {
        final int randIdx = (new Random()).nextInt(Integer.MAX_VALUE - 1) + 1;
        final int randSplit = (new Random()).nextInt(randIdx);
        final int randSize = (new Random()).nextInt(Integer.MAX_VALUE);
        final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
        if (randIdx % 2 == 0) {
            final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize,
                Xint.valueOf(scenariosPerElement), false);
            Assert.assertEquals(randIdx + 1, sample.getNumberOfSplitGroupElements());

        } else {
            final boolean special = (new Random()).nextBoolean();
            final SampleWeights sample = new SampleWeights(randIdx, randSplit, randSize,
                Xint.valueOf(scenariosPerElement), special);
            if (special)
                Assert.assertEquals((randIdx + 1) / 2, sample.getNumberOfSplitGroupElements());
            else
                Assert.assertEquals(randIdx + 1, sample.getNumberOfSplitGroupElements());

        }
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights#toString()}. */
    @Test
    public final void testToString() {
        final int evenGroupDistance = (new Random()).nextInt(((Integer.MAX_VALUE - 3) / 2)) * 2;
        final int oddGroupDistance = evenGroupDistance + 1;
        final int splitGroupID = (new Random()).nextInt(evenGroupDistance);
        final int scenariosPerElement = (new Random()).nextInt(Integer.MAX_VALUE);
        final int numberOfComponents = (new Random()).nextInt(Integer.MAX_VALUE);
        String string = "";
        Xint weight = Xint.ONE;

        final SampleWeights evenSample = new SampleWeights(evenGroupDistance, splitGroupID, numberOfComponents,
            Xint.valueOf(scenariosPerElement), false);
        weight = ((weight.multiply(scenariosPerElement)).multiply(evenGroupDistance + 1)).multiply(numberOfComponents);
        string = "\t" + evenGroupDistance + "\tR_" + splitGroupID + "\t" + scenariosPerElement + "\t*"
            + (evenGroupDistance + 1) + "\t*" + numberOfComponents + "\t" + weight;
        Assert.assertEquals(string, evenSample.toString());

        final SampleWeights oddSample = new SampleWeights(oddGroupDistance, splitGroupID, numberOfComponents,
            Xint.valueOf(scenariosPerElement), false);
        weight = Xint.ONE;
        weight = ((weight.multiply(scenariosPerElement)).multiply(oddGroupDistance + 1)).multiply(numberOfComponents);
        string = "\t" + oddGroupDistance + "\tR_" + splitGroupID + "\t" + scenariosPerElement + "\t*"
            + (oddGroupDistance + 1) + "\t*" + numberOfComponents + "\t" + weight;
        Assert.assertEquals(string, oddSample.toString());

        final SampleWeights oddSample2 = new SampleWeights(oddGroupDistance, splitGroupID, numberOfComponents,
            Xint.valueOf(scenariosPerElement), true);
        weight = Xint.ONE;
        weight = ((weight.multiply(scenariosPerElement)).multiply((oddGroupDistance + 1) / 2))
            .multiply(numberOfComponents);
        string = "\t" + oddGroupDistance + "\tR_" + splitGroupID + "\t" + scenariosPerElement + "\t*"
            + ((oddGroupDistance + 1) / 2) + "\t*" + numberOfComponents + "\t" + weight;
        Assert.assertEquals(string, oddSample2.toString());
    }
}