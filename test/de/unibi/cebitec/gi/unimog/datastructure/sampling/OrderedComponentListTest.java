package de.unibi.cebitec.gi.unimog.datastructure.sampling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure;

/**
 * @author Eyla Willing
 */
public class OrderedComponentListTest {
    private static OrderedComponentList globalOCL;                                              // = new
                                                                                                 // OrderedComponentList();
    private static int                  idx = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ComponentSample temp = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        OrderedComponentListTest.globalOCL = new OrderedComponentList();
        OrderedComponentListTest.globalOCL.addElement(OrderedComponentListTest.idx, temp);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#OrderedComponentList()}. */
    @Test
    public final void testOrderedComponentList() {
        final OrderedComponentList ocl = new OrderedComponentList();
        Assert.assertEquals(0, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(0, ocl.getNumberOfUnsortedComponents());
        boolean thrown = false;
        try {
            ocl.getLastElement();
        } catch (IllegalArgumentException e) {
            thrown = true;
        }
        Assert.assertTrue(thrown);
    }

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#addElement(int, de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample)}
     * .
     */
    @Test
    public final void testAddElement() {
        final List<ComponentSample> list = new ArrayList<ComponentSample>();
        final ComponentSample comp1 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp2 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp3 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        list.add(comp1);
        final OrderedComponentList ocl = new OrderedComponentList();
        final int distance = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;

        // dist: #1 {1}
        Assert.assertTrue(ocl.addElement(distance, comp1));
        Assert.assertEquals(1, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(1, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp1));
        Assert.assertEquals(list, ocl.getComponents(distance));
        Assert.assertEquals(list.size(), ocl.getComponents(distance).size());

        // dist: #1 {1} | 0: does not exist
        Assert.assertFalse(ocl.addElement(0, comp2));
        Assert.assertEquals(1, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(1, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp1));
        Assert.assertEquals(list, ocl.getComponents(distance));
        Assert.assertEquals(list.size(), ocl.getComponents(distance).size());

        // dist: #1 {1} | dist+1: {2}
        Assert.assertTrue(ocl.addElement(distance + 1, comp2));
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp1));
        Assert.assertTrue(ocl.getComponents(distance + 1).get(0).equals(comp2));
        Assert.assertEquals(list, ocl.getComponents(distance));

        // dist: #2 {1,3} | dist+1: {2}
        Assert.assertTrue(ocl.addElement(distance, comp3));
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(2, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp1));
        Assert.assertTrue(ocl.getComponents(distance).get(1).equals(comp3));
        Assert.assertTrue(ocl.getComponents(distance + 1).get(0).equals(comp2));

        // dist: #3 {1,3,2} | dist+1: {2}
        Assert.assertTrue(ocl.addElement(distance, comp2));
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(4, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(3, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp1));
        Assert.assertTrue(ocl.getComponents(distance).get(1).equals(comp3));
        Assert.assertTrue(ocl.getComponents(distance).get(2).equals(comp2));
        Assert.assertTrue(ocl.getComponents(distance + 1).get(0).equals(comp2));

        // dist: #3 {1,3,2} | dist+1: {2}
        Assert.assertFalse(ocl.addElement(distance, comp2));
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(4, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(3, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp1));
        Assert.assertTrue(ocl.getComponents(distance).get(1).equals(comp3));
        Assert.assertTrue(ocl.getComponents(distance).get(2).equals(comp2));
        Assert.assertTrue(ocl.getComponents(distance + 1).get(0).equals(comp2));
    }

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#removeElement(int, de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample)}
     * .
     */
    @Test
    public final void testRemoveElement() {
        final List<ComponentSample> list = new ArrayList<ComponentSample>();
        final ComponentSample comp1 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp2 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp3 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        list.add(comp1);
        final OrderedComponentList ocl = new OrderedComponentList();
        final int distance = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;

        ocl.addElement(distance, comp1);
        // dist: #1 {1}
        Assert.assertEquals(1, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(1, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp1));
        Assert.assertEquals(list, ocl.getComponents(distance));
        Assert.assertEquals(list.size(), ocl.getComponents(distance).size());

        // dist: #0 {}
        Assert.assertTrue(ocl.removeElement(distance, comp1));
        Assert.assertEquals(0, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(0, ocl.getNumberOfUnsortedComponents());
        Assert.assertTrue(ocl.getComponents(distance).isEmpty());

        // dist: #1 {1}
        Assert.assertTrue(ocl.addElement(distance, comp1));
        Assert.assertEquals(1, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(1, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp1));

        // dist: #2 {1,3}
        ocl.addElement(distance, comp3);

        // dist: #1 {3}
        Assert.assertTrue(ocl.removeElement(distance, comp1));
        Assert.assertEquals(1, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(1, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());

        // dist: #2 {3} | dist+1: #1 {2}
        ocl.addElement(distance + 1, comp2);

        // dist: #2 {3} | dist+1: #1 {2}
        Assert.assertFalse(ocl.removeElement(distance, comp2));
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp3));
        Assert.assertTrue(ocl.getComponents(distance + 1).get(0).equals(comp2));

        // dist: #2 {3} | dist+1: #1 {2}
        Assert.assertFalse(ocl.removeElement(distance + 2, comp2));
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
        Assert.assertTrue(ocl.getComponents(distance).get(0).equals(comp3));
        Assert.assertTrue(ocl.getComponents(distance + 1).get(0).equals(comp2));

        // dist: #2 {3} | dist+1: #0 {2}
        Assert.assertFalse(ocl.removeElement(distance, comp2));
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
    }

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#moveElement(int, int, de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample)}
     * .
     */
    @Test
    public final void testMoveElement() {
        final ComponentSample comp1 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp2 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp3 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());

        final OrderedComponentList ocl = new OrderedComponentList();
        final int distance = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;

        // dist: #2 {1,2} | dist+1: #1 {3}
        Assert.assertTrue(ocl.addElement(distance, comp1));
        Assert.assertTrue(ocl.addElement(distance, comp2));
        Assert.assertTrue(ocl.addElement(distance + 1, comp3));
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());

        // dist: #2 {1,2} | dist+1: #1 {3}
        Assert.assertTrue(ocl.moveElement(distance + 1, distance + 1, comp3));
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());

        // dist: #2 {1,2} | dist+1: #0 {} | dist+2: #1 {3}
        Assert.assertTrue(ocl.moveElement(distance + 1, distance + 2, comp3));
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getComponents(distance).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 1).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 2).size());

        // dist: #2 {1,2} | dist+1: #0 {} | dist+2: #1 {3}
        Assert.assertFalse(ocl.moveElement(distance + 1, distance, comp3));
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getComponents(distance).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 1).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 2).size());

        // dist: #2 {1,2,3} | dist+1: #0 {} | dist+2: #0 {}
        Assert.assertTrue(ocl.moveElement(distance + 2, distance, comp3));
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(3, ocl.getComponents(distance).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 1).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 2).size());

        // dist: #2 {1,2,3} | dist+1: #0 {} | dist+2: #0 {}
        Assert.assertFalse(ocl.moveElement(distance + 1, distance + 2, comp3));
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(3, ocl.getComponents(distance).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 1).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 2).size());

        // dist: #2 {1,3} | dist+1: #0 {} | dist+2: #0 {}
        Assert.assertTrue(ocl.moveElement(distance, 0, comp2));
        Assert.assertEquals(2, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(1, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getComponents(distance).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 1).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 2).size());

        // dist: #2 {1,3} | dist+1: #0 {3} | dist+2: #0 {}
        ocl.addElement(distance + 1, comp3);
        Assert.assertFalse(ocl.moveElement(distance, distance + 1, comp3));
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 2).size());

        // dist: #2 {1,3} | dist+1: #0 {3} | dist+2: #0 {}
        Assert.assertFalse(ocl.moveElement(distance, distance + 1, comp2));
        Assert.assertEquals(3, ocl.getNumberOfUnsortedComponents());
        Assert.assertEquals(2, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(2, ocl.getComponents(distance).size());
        Assert.assertEquals(1, ocl.getComponents(distance + 1).size());
        Assert.assertEquals(0, ocl.getComponents(distance + 2).size());
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#getComponents(int)}. */
    @Test
    public final void testGetComponents() {
        final OrderedComponentList ocl = new OrderedComponentList();
        Assert.assertEquals(0, ocl.getNumberOfDifferentDistances());
        Assert.assertTrue(ocl.getComponents((new Random()).nextInt(Integer.MAX_VALUE - 1) + 1).isEmpty());

        final ComponentSample comp1 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());

        final int distance = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;
        ocl.addElement(0, comp1);
        Assert.assertTrue(ocl.getComponents(0).isEmpty());
        ocl.addElement(distance, comp1);
        Assert.assertEquals(1, ocl.getComponents(distance).size());
        Assert.assertEquals(comp1, ocl.getComponents(distance).get(0));
    }

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#getNumberOfDifferentDistances()}.
     */
    @Test
    public final void testGetNumberOfDifferentDistances() {
        final OrderedComponentList ocl = new OrderedComponentList();
        Assert.assertEquals(0, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(1, OrderedComponentListTest.globalOCL.getNumberOfDifferentDistances());
        // TODO
    }

    /**Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#getDistanceList()}.     */
    @Test
    public final void testGetDistanceList() {
        final OrderedComponentList ocl = new OrderedComponentList();
        Assert.assertEquals((new HashMap<Integer, List<ComponentSample>>()).keySet(), ocl.getDistanceList());

        final ComponentSample comp1 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final int distance = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;
        ocl.addElement(distance, comp1);
        List<Integer> dist = new ArrayList<Integer>();
        dist.add(distance);
        Assert.assertEquals(dist.get(0), (ocl.getDistanceList()).iterator().next());
    }

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#getNumberOfUnsortedComponents()}.
     */
    @Test
    public final void testGetNumberOfUnsortedComponents() {
        Assert.assertEquals(1, OrderedComponentListTest.globalOCL.getNumberOfUnsortedComponents());
        final OrderedComponentList ocl = new OrderedComponentList();
        Assert.assertEquals(0, ocl.getNumberOfUnsortedComponents());
        ocl.addElement(0, OrderedComponentListTest.globalOCL.getComponents(OrderedComponentListTest.idx).get(0).clone());
        Assert.assertEquals(0, ocl.getNumberOfUnsortedComponents());
        ocl.addElement((new Random()).nextInt(Integer.MAX_VALUE - 1) + 1, OrderedComponentListTest.globalOCL
            .getComponents(OrderedComponentListTest.idx).get(0).clone());
        Assert.assertEquals(1, ocl.getNumberOfUnsortedComponents());
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#getLastElement()}. */
    @Test
    public final void testGetLastElement() {
        OrderedComponentList ocl = new OrderedComponentList();
        boolean isIllegal = false;
        boolean isBounds = false;
        try {
            ocl.getLastElement();
        } catch (IllegalArgumentException e) {
            isIllegal = true;
        } catch (IndexOutOfBoundsException e) {
            isBounds = true;
        }
        Assert.assertTrue(isIllegal);
        Assert.assertFalse(isBounds);
        isIllegal = false;
        isBounds = false;

        final int randomIdx = (new Random()).nextInt(Integer.MAX_VALUE - 2) + 2;
        ocl.addElement(randomIdx, OrderedComponentListTest.globalOCL.getComponents(OrderedComponentListTest.idx).get(0)
            .clone());
        try {
            ocl.getLastElement();
        } catch (IllegalArgumentException e) {
            isIllegal = true;
        } catch (IndexOutOfBoundsException e) {
            isBounds = true;
        }
        Assert.assertFalse(isIllegal);
        Assert.assertTrue(isBounds);
        isIllegal = false;
        isBounds = false;

        ocl = new OrderedComponentList();
        ocl.addElement(1, OrderedComponentListTest.globalOCL.getComponents(OrderedComponentListTest.idx).get(0).clone());
        ComponentSample lastComp;
        try {
            lastComp = ocl.getLastElement();
            Assert.assertTrue(lastComp.equals(OrderedComponentListTest.globalOCL.getComponents(
                OrderedComponentListTest.idx).get(0)));
        } catch (IllegalArgumentException e) {
            isIllegal = true;
        } catch (IndexOutOfBoundsException e) {
            isBounds = true;
        }
        Assert.assertFalse(isIllegal);
        Assert.assertFalse(isBounds);
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#printElementList()}. */
    @Test
    public final void testPrintKeyList() {
        final ComponentSample comp1 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp2 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp3 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());

        final OrderedComponentList ocl = new OrderedComponentList();
        Assert.assertEquals("", ocl.printElementList());
        final int distance = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;

        ocl.addElement(distance, comp1);
        Assert.assertEquals(distance + ": 1\t", ocl.printElementList());

        ocl.addElement(distance, comp2);
        Assert.assertEquals(distance + ": 2\t", ocl.printElementList());

        ocl.addElement(distance, comp3);
        Assert.assertEquals(distance + ": 3\t", ocl.printElementList());

        ocl.addElement(OrderedComponentListTest.idx, comp1);

        final boolean eq = (ocl.printElementList().equals(OrderedComponentListTest.idx + ": 1\t" + distance + ": 3\t"));
        if (eq)
            Assert.assertEquals(OrderedComponentListTest.idx + ": 1\t" + distance + ": 3\t", ocl.printElementList());
        else
            Assert.assertEquals(distance + ": 3\t" + OrderedComponentListTest.idx + ": 1\t", ocl.printElementList());

        Assert.assertEquals(OrderedComponentListTest.idx + ": 1\t",
            OrderedComponentListTest.globalOCL.printElementList());
    }

    /**
     * Test method for
     * {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#equals(de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList)}
     * .
     */
    @Test
    public final void testEquals() {
        final ComponentSample comp1 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp2 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp3 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());

        final OrderedComponentList oclA = new OrderedComponentList();
        final OrderedComponentList oclB = new OrderedComponentList();
        // A= {}; B= {}
        Assert.assertTrue(oclA.equals(oclB));

        final int distance = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;
        oclA.addElement(distance, comp1);
        // A= dist: #1 {1}; B= {}
        Assert.assertFalse(oclA.equals(oclB));

        oclB.addElement(distance + 1, comp1);
        oclB.addElement(distance + 1, comp2);
        // A= dist: #1 {1}; B= dist+1: #2 {1,2}
        Assert.assertFalse(oclA.equals(oclB));

        oclA.addElement(distance, comp3);
        // A= dist: #2 {1,3}; B= dist+1: #2 {1,2}
        Assert.assertFalse(oclA.equals(oclB));

        oclA.addElement(distance + 1, comp1);
        oclB.addElement(distance, comp2);
        // A= dist: #2 {1,3} dist+1: #1 {1}; B= dist: #1 {2} dist+1: #2 {1,2}
        Assert.assertFalse(oclA.equals(oclB));

        oclA.addElement(distance + 1, comp2);
        oclB.addElement(distance, comp1);
        // A= dist: #2 {1,3} dist+1: #2 {1,2}; B= dist: #2 {2,1} dist+1: #2 {1,2}
        Assert.assertFalse(oclA.equals(oclB));

        oclA.addElement(distance, comp2);
        oclB.addElement(distance, comp3);
        // A= dist: #2 {1,3,2} dist+1: #1 {1,2}; B= dist: #1 {2,1,3} dist+1: #2 {1,2}
        Assert.assertTrue(oclA.equals(oclB));
    }

    /** Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList#clone()}. */
    @Test
    public final void testClone() {
        final ComponentSample comp1 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp2 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());
        final ComponentSample comp3 = new ComponentSample((new Random()).nextInt(Integer.MAX_VALUE),
            (new Random()).nextInt(Integer.MAX_VALUE), Structure.getStructure((new Random()).nextInt(5)),
            (new Random()).nextBoolean());

        final OrderedComponentList ocl = new OrderedComponentList();
        final OrderedComponentList ocl2 = ocl.clone();
        Assert.assertEquals(0, ocl.getNumberOfDifferentDistances());
        Assert.assertEquals(0, ocl2.getNumberOfDifferentDistances());
        Assert.assertNotSame(ocl, ocl2);
        Assert.assertTrue(ocl.equals(ocl2));

        int distance = (new Random().nextInt(Integer.MAX_VALUE - 1)) + 1;
        ocl.addElement(distance, comp1);

        final OrderedComponentList ocl3 = ocl.clone();
        Assert.assertNotSame(ocl, ocl3);
        Assert.assertTrue(ocl.equals(ocl3));

        ocl.addElement(distance, comp2);
        ocl.addElement(distance + 1, comp3);
        // dist: #2 {1,2}, dist+1: #1 {3}
        final OrderedComponentList ocl4 = ocl.clone();
        Assert.assertNotSame(ocl, ocl4);
        Assert.assertTrue(ocl.equals(ocl4));
    }
}