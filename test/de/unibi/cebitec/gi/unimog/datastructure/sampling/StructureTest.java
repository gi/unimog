package de.unibi.cebitec.gi.unimog.datastructure.sampling;

import java.util.Random;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure;

/**
 * Test class for {@link Structure}
 * 
 * @author Eyla Willing
 */
public class StructureTest {

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure#getGuiName()}.
     */
    @Test
    public final void testGetGuiName() {
        Assert.assertEquals("AA  ", Structure.AA.getGuiName());
        Assert.assertEquals("BB  ", Structure.BB.getGuiName());
        Assert.assertEquals("AB  ", Structure.AB.getGuiName());
        Assert.assertEquals("CYCLE", Structure.CYCLE.getGuiName());
    }

    /**
     * Test method for {@link de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure#getStructure(int)}.
     */
    @Test
    public final void testGetStructure() {
        Assert.assertEquals(Structure.NONE, Structure.getStructure(0));
        Assert.assertEquals(Structure.AA, Structure.getStructure(1));
        Assert.assertEquals(Structure.BB, Structure.getStructure(2));
        Assert.assertEquals(Structure.AB, Structure.getStructure(3));
        Assert.assertEquals(Structure.CYCLE, Structure.getStructure(4));
        final Integer randIdx = (new Random()).nextInt(Integer.MAX_VALUE);
        if (randIdx > 5)
            Assert.assertEquals(Structure.NONE, Structure.getStructure(randIdx));
    }
}
