package de.unibi.cebitec.gi.unimog.datastructure;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unibi.cebitec.gi.unimog.GraphExamples;

import static org.junit.Assert.*;

/**
 * Tests the adjacency graph methods.
 *
 * @author -Rolf Hilker-
 */
public class AdjacencyGraphTest {

    private GraphExamples graphExamples = new GraphExamples();
    private AdjacencyGraph ag1 = this.graphExamples.getAG1();
    private AdjacencyGraph ag2 = this.graphExamples.getAG2();

    public AdjacencyGraphTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Tests the parseAdjacencies method.
     */
    @Test
    public void parseAdjacenciesTest() {

//        System.out.println();
        assertEquals(6, this.ag1.getAdjacenciesGenome1()[1]);
        assertEquals(this.ag1.getAdjacenciesGenome1()[6], 1);
        assertEquals(this.ag1.getAdjacenciesGenome1()[2], 2);
        assertEquals(this.ag1.getAdjacenciesGenome1()[3], 10);
        assertEquals(this.ag1.getAdjacenciesGenome1()[10], 3);
        assertEquals(this.ag1.getAdjacenciesGenome1()[4], 9);
        assertEquals(this.ag1.getAdjacenciesGenome1()[9], 4);
        assertEquals(this.ag1.getAdjacenciesGenome1()[5], 7);
        assertEquals(this.ag1.getAdjacenciesGenome1()[7], 5);
        assertEquals(this.ag1.getAdjacenciesGenome1()[8], 8);
        assertEquals(this.ag1.getAdjacenciesGenome1()[11], 14);
        assertEquals(this.ag1.getAdjacenciesGenome1()[12], 12);
        assertEquals(this.ag1.getAdjacenciesGenome1()[13], 13);
        assertEquals(this.ag1.getAdjacenciesGenome1()[14], 11);
    }

    /**
     * Tests the calculatePathsAndCycles method.
     */
    @Test
    public void calculatePathsAndCyclesTest() {
        assertEquals(5, this.ag1.getDCJdistance());
        assertEquals(11, this.ag2.getDCJdistance());
        // assertEquals(this.ag1.getNumberOfCycles() == 1);
        // assertEquals(this.ag1.getNumberOfEvenPaths() == 2);
        // assertEquals(this.ag1.getNumberOfOddPaths() == 2);
        // assertEquals(this.ag2.getNumberOfCycles() == 3);
        // assertEquals(this.ag2.getNumberOfOddPaths() == 6);
        // assertEquals(this.ag2.getNumberOfEvenPaths() == 1);
    }

    @Test
    public void testEquals() {

    }

    @Test
    public void testClone() {
        AdjacencyGraph clone = this.ag1.clone();
//        assertSame(this.ag1,clone);
        // equals() does not work here because nbOfGenes is not set in constructor
        assert this.ag1.nbOfGenes != clone.nbOfGenes;
        assert this.ag1.getDCJdistance() == clone.getDCJdistance();
        assert this.ag1.getCompDistances().equals(clone.getCompDistances());
        assertArrayEquals(this.ag1.getAdjacenciesGenome1(), clone.getAdjacenciesGenome1());
        assertArrayEquals(this.ag1.getAdjacenciesGenome2(), clone.getAdjacenciesGenome2());
        assertArrayEquals(this.ag1.getBelongToEvenPath(), clone.getBelongToEvenPath());
        assertNull(clone.getCappedGenomes());
        // Xint objects cannot be compared with equals()
        assert this.ag1.getLowerBoundScenarios().isZERO() == clone.getLowerBoundScenarios().isZERO();
        assert this.ag1.getLowerBoundScenarios().isONE() == clone.getLowerBoundScenarios().isONE();
        assert this.ag1.getLowerBoundScenarios().crcValue() == clone.getLowerBoundScenarios().crcValue();
        assert this.ag1.getLowerBoundScenarios().signum() == clone.getLowerBoundScenarios().signum();
        assertEquals(this.ag1.getLowerBoundScenarios().toString(), clone.getLowerBoundScenarios().toString());
        assertEquals(this.ag1.getLowerBoundScenarios().getName(), clone.getLowerBoundScenarios().getName());
        assertEquals(this.ag1.getLowerBoundScenarios().toHexString(), clone.getLowerBoundScenarios().toHexString());
    }

    // no longer necessary, code moved to static Toolz class
    // /**
    // * Test of abstractGene method, of class AdjacencyGraph.
    // */
    // @Test
    // public void testAbstractGene() {
    // System.out.println("abstractGene");
    // int gene = 0;
    // boolean left = false;
    // int expResult = 0;
    // int result = Toolz.abstractGene(gene, left);
    // assertEquals(expResult, result);
    // // TODO review the generated test code and remove the default call to fail.
    // fail("The test case is a prototype.");
    // }
}
