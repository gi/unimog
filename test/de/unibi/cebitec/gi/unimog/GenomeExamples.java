package de.unibi.cebitec.gi.unimog;

import java.util.ArrayList;
import java.util.List;

import de.unibi.cebitec.gi.unimog.datastructure.Chromosome;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;

/**
 * @author -Rolf Hilker-
 * <p>
 * Class that contains some examples for Genomes.
 */
public class GenomeExamples {

    private final Chromosome chromA1 = new Chromosome(new int[]{-1, -3, 4}, false);
    private final Chromosome chromA2 = new Chromosome(new int[]{2, 5}, true);
    private final Chromosome chromA3 = new Chromosome(new int[]{-6, -7}, false);
    private final Chromosome chromB1 = new Chromosome(new int[]{1, 2}, true);
    private final Chromosome chromB2 = new Chromosome(new int[]{-3, -4}, false);
    private final Chromosome chromB3 = new Chromosome(new int[]{-5}, false);
    private final Chromosome chromB4 = new Chromosome(new int[]{6, 7}, true);
    private final Chromosome chromC1 = new Chromosome(new int[]{2, 1, 3, 5, 4}, false);
    private final Chromosome chromC2 = new Chromosome(new int[]{6, 7, -11, -9, -10, -8, 12, 16}, false);
    private final Chromosome chromC3 = new Chromosome(new int[]{15, 14, -13, 17}, false);
    private final Chromosome chromD1 = new Chromosome(new int[]{1, 2, 3, 4, 5}, false);
    private final Chromosome chromD2 = new Chromosome(new int[]{6, 7, 8, 9, 10, 11, 12}, false);
    private final Chromosome chromD3 = new Chromosome(new int[]{13, 14, 15}, false);
    private final Chromosome chromD4 = new Chromosome(new int[]{16, 17}, false);
    private final Chromosome chromE1 = new Chromosome(new int[]{-1, 2, 3, 4}, false);
    private final Chromosome chromE2 = new Chromosome(new int[]{5, 6}, false);
    private final Chromosome chromE3 = new Chromosome(new int[]{7, 8}, false);
    private final Chromosome chromE4 = new Chromosome(new int[]{9}, false);
    private final Chromosome chromF1 = new Chromosome(new int[]{3, 8, 6}, false);
    private final Chromosome chromF2 = new Chromosome(new int[]{4, 5, 9}, false);
    private final Chromosome chromF3 = new Chromosome(new int[]{1, 7, 2}, true);
    private final Chromosome chromG1 = new Chromosome(new int[]{-1, 3, 2, 4}, true);
    private final Chromosome chromG2 = new Chromosome(new int[]{5, 6}, false);
    private final Chromosome chromG3 = new Chromosome(new int[]{7, 8}, true);
    private final Chromosome chromG4 = new Chromosome(new int[]{9}, false);
    /**
     * Chroms for duplicate genes test.
     */
    private final Chromosome chromH1 = new Chromosome(new int[]{5, 8, 4, 5, 7}, false);
    private final Chromosome chromH2 = new Chromosome(new int[]{2, 1, 8, 2, 3, 2}, true);

    private Genome genomeA;
    private Genome genomeB;
    private Genome genomeC;
    private Genome genomeD;
    private Genome genomeE;
    private Genome genomeF;
    private Genome genomeG;
    private Genome genomeH;

    private final ArrayList<Chromosome> genomeAList = new ArrayList<Chromosome>();
    private final ArrayList<Chromosome> genomeBList = new ArrayList<Chromosome>();
    private final ArrayList<Chromosome> genomeCList = new ArrayList<Chromosome>();
    private final ArrayList<Chromosome> genomeDList = new ArrayList<Chromosome>();
    private final ArrayList<Chromosome> genomeEList = new ArrayList<Chromosome>();
    private final ArrayList<Chromosome> genomeFList = new ArrayList<Chromosome>();
    private final ArrayList<Chromosome> genomeGList = new ArrayList<Chromosome>();
    private final ArrayList<Chromosome> genomeHList = new ArrayList<Chromosome>();

    /**
     * inversion model examples
     */
    private final ArrayList<Chromosome> chrInv1 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, -5, -3, -4, -2, 6, 8, 10, 12, 11, 13, 9, 14, 16, 18, 17, 19, 15, 20, 7, 21}, false)));
    private final ArrayList<Chromosome> chrInv2 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, -5, -3, -4, -2, 6, 8, 10, 12, 11, 13, 14, 16, 18, 17, 19, 15, 20, 9, 7, 21}, false)));
    private final ArrayList<Chromosome> chrInv3 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, -5, -3, -4, -2, 6, 8, 10, 12, 11, 13, 9, 14, 16, 18, 17, 19, -15, 20, 7, 21}, false)));
    private final ArrayList<Chromosome> chrInv4 = new ArrayList<>(List.of(new Chromosome(
            new int[]{2, 4, 3, 5, -1, 6, -11, -9, -10, -8, 7, 12, -17, -15, -16, -14, 13}, false)));
    private final ArrayList<Chromosome> chrInv5 = new ArrayList<>(List.of(new Chromosome(
            new int[]{-1, 18, 2, 4, 6, 8, 7, 9, 5, 10, 12, 14, 13, 15, 11, 16, 3, 17, 19, 21, 23, 22, 24, -20},
            false)));
    private final ArrayList<Chromosome> chrInv6 = new ArrayList<>(List.of(new Chromosome(
            new int[]{-1, 18, 2, 4, -7, -8, -6, 9, 5, 10, 14, 12, 15, 13, 11, 16, 3, -17, 19, 21, 23, 22, 24, -20},
            false)));
    private final ArrayList<Chromosome> chrInv7 = new ArrayList<>(List.of(new Chromosome(
            new int[]{-1, 18, 2, 4, -7, -8, -6, 9, 5, 10, 14, 12, 15, 13, 11, 16, 3, -17, 19, 21, 23, 22, 24, -20, 25},
            false)));
    private final ArrayList<Chromosome> chrInv8 = new ArrayList<>(List.of(new Chromosome(new int[]{2, 1, 3, 5, 4, 6, 8, 7},
            false)));

    private final ArrayList<Chromosome> chrInv9 = new ArrayList<>(List.of(
            new Chromosome(new int[]{2, 3, 1}, false)
    ));

    private final ArrayList<Chromosome> chrInv10 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, 18, 2, 16, 4, 14, 6, 12, 8, 10, 9, 11, 7, 13, 5, 15, 3, 17}, false)));

    private final ArrayList<Chromosome> chrInv11 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1,3,4,5,17,10,6,8,7,2,9,16,12,14,13,-11,15}, false
    )));

    private final ArrayList<Chromosome> chrInvRef8 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, 2, 3, 4, 5, 6, 7, 8}, false)));
    private final ArrayList<Chromosome> chrInvRef17 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17}, false)));
    private final ArrayList<Chromosome> chrInvRef21 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21}, false)));
    private final ArrayList<Chromosome> chrInvRef24 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24},
            false)));
    private final ArrayList<Chromosome> chrInvRef25 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25},
            false)));
    private final ArrayList<Chromosome> chrInvRef18 = new ArrayList<>(List.of(new Chromosome(
            new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18}, false
    )));

    private final ArrayList<Chromosome> chrInvRef3 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1, 2, 3}, false)
    ));

    private final ArrayList<Chromosome> chrInvRef11 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17},false)
    ));

    private Genome genomeInv1;
    private Genome genomeInv2;
    private Genome genomeInv3;
    private Genome genomeInv4;
    private Genome genomeInv5;
    private Genome genomeInv6;
    private Genome genomeInv7;
    private Genome genomeInv8;

    private Genome genomeInv9;
    private Genome genomeInv10;

    private Genome genomeInv11;

    private Genome genomeInv11Ref;
    private Genome genomeInv13Ref;

    private Genome genomeInvRef8;
    private Genome genomeInvRef17;
    private Genome genomeInvRef21;
    private Genome genomeInvRef24;
    private Genome genomeInvRef25;

    private Genome genomeInvRef3;

    private Genome genomeInvRef18;

    /**
     * HP model examples
     **/

    private final ArrayList<Chromosome> multChrHP9 = new ArrayList<>(List.of(
            new Chromosome(new int[]{2, 1, 3, 5, 4}, false),
            new Chromosome(new int[]{6, 7, -11, -9, -10, -8, 12, 16}, false),
            new Chromosome(new int[]{15, 14, -13, 17}, false)
    ));

    private final ArrayList<Chromosome> multChrHP10 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1, -5, -4, -3, -2, 6}, false),
            new Chromosome(new int[]{8, 10, 12, 11, 13, 9, 14, 16, 18, 17, 19, 15, 20, 7, 21}, false)
    ));

    private final ArrayList<Chromosome> multChrHP11 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1, 3, 2, 4, 6, 8, 7, 9, 10, 11, 12, 5, 13, 15, 16, 17, 18, -14, 19}, false),
            new Chromosome(new int[]{29, 25, 20, 30, 24, 27, 21, 23, 28, 22, 26}, false)
    ));

    private final ArrayList<Chromosome> multChrHP12 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51}, false),
            new Chromosome(new int[]{52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101}, false),
            new Chromosome(new int[]{-146, -145, 291, 292, 103, -167, -166, -165, -164, -163, -162, -161, 182, 183, 184, 185, 180, 181, 276, 277, 278, 306, 307, -122, -262, -261, -260, -259, -258, 267, -273, -272, -271, -270, -269, -308, 123, 124, 125, 126, 263, 264, 132, 133, 134, 135, 136, 137, -290, -289, -288, -287, -286, -285, -284, -283, -282}, false),
            new Chromosome(new int[]{102, 293, 294, -186, -179, -178, -177, -176, -175, 157, -142, -141, 265, -275, -274, 268, -116, -115, -114, -113, -112, -111, -110, 130, 131, -140, -139, -138, -144, 309, 310, 311, 312, 304, 305, 279, 280, 281}, false),
            new Chromosome(new int[]{193, 194}, false),
            new Chromosome(new int[]{195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252}, false),
            new Chromosome(new int[]{253, 254, 255, 256, 257, -266, -160, -159, -158, 143, 117, 118, 119, 120, 121, 127, 128, 129, -191, -190, -189, -188, -187, 295, 296, 297, 298, 168, 169, 170, 171, 172, 173, 174, -156, -155, -154, -153, -152, -151, -150, -149, -148, -147}, false),
            new Chromosome(new int[]{-323, -322, -321, -320, -319, -318, -317, -316, -315, -314, -313, -303, -302, -301, -300, -299, 104, 105, 106, 107, 108, 109, 192}, false),
            new Chromosome(new int[]{324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336}, false)
    ));

    private final ArrayList<Chromosome> multChrHP13 = new ArrayList<>(List.of(new Chromosome(new int[]{-8, 7, 4, 5, 3, 2, 1, 6, -0}, false)));

    private final ArrayList<Chromosome> multChrHPRef13 = new ArrayList<>(List.of(new Chromosome(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8}, false)));

    private final ArrayList<Chromosome> multChrRefHPRef12 = new ArrayList<>(List.of(
            new Chromosome(new int[]{-41, 37, -39, -38, -21, -18, -17}, false),
            new Chromosome(new int[]{9, 10}, false),
            new Chromosome(new int[]{11, 12, 13, 14, 15, 16}, false),
            new Chromosome(new int[]{-8, -7, 4, 5, -2, 22}, false),
            new Chromosome(new int[]{1, -36}, false),
            new Chromosome(new int[]{28, 29, 30, 31, 32, 33, 34, 35}, false),
            new Chromosome(new int[]{42, 40, 19, -6, -25, -20, -24, 3, 26, 27}, false),
            new Chromosome(new int[]{23, 43, 44}, false)
    ));

    private final ArrayList<Chromosome> multChrRefHPRef11 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19}, false),
            new Chromosome(new int[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30}, false)
    ));
    private final Chromosome multChrHPRef10 = new Chromosome(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21}, false);

    private final ArrayList<Chromosome> multChrHPRef9 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1, 2, 3, 4, 5}, false),
            new Chromosome(new int[]{6, 7, 8, 9, 10, 11, 12}, false),
            new Chromosome(new int[]{13, 14, 15}, false),
            new Chromosome(new int[]{16, 17}, false)
    ));

    private Genome genomeHP9;

    private Genome genomeHP10;

    private Genome genomeHP11;

    private Genome genomeHP12;

    private Genome genomeInv13;

    private Genome genomeHP9Ref;

    private Genome genomeHP10Ref;

    private Genome genomeHP11Ref;

    private Genome genomeHP12Ref;

    /**
     * Translocation model examples
     **/

    private final ArrayList<Chromosome> chrTransloc1 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1,7,2,3,4,3,9,8,5,-6,10}, false),
            new Chromosome(new int[]{11,14,-13,-12,15}, false)
    ));

    private final ArrayList<Chromosome> chrTransloc1Ref = new ArrayList<>(List.of(
            new Chromosome(new int[]{1,2,3,4,5,6,7,8,9,10,}, false),
            new Chromosome(new int[]{11,12,13,14,15}, false)
    ));

    private final ArrayList<Chromosome> chrTransloc2 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1,9,3,7,2,6,10,-3,-4,8,10,-11,2,5,13,12,-14,15}, false),
            new Chromosome(new int[]{16,-19,-17,18,20}, false)
    ));

    private final ArrayList<Chromosome> chrTransloc2Ref = new ArrayList<>(List.of(
            new Chromosome(new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}, false),
            new Chromosome(new int[]{16,17,18,19,20}, false)
    ));

    private final ArrayList<Chromosome> chrTransloc3 = new ArrayList<>(List.of(
            new Chromosome(new int[]{0,-9,9,7,-9,8,3,-5,6,7,4,-2,1,10}, false),
            new Chromosome(new int[]{11,14,13,-16,14,15,-12,17,18,19,15,-12,20}, false)
    ));
    private final ArrayList<Chromosome> chrTransloc3Ref = new ArrayList<>(List.of(
            new Chromosome(new int[]{0,1,2,3,4,5,6,7,8,9,10}, false),
            new Chromosome(new int[]{11,12,13,14,15,16,17,18,19,20}, false)
    ));

    private final ArrayList<Chromosome> chrTransloc4 = new ArrayList<>(List.of(
            new Chromosome(new int[]{1,16,4,18,5}, false),
            new Chromosome(new int[]{6,17,-3,-2,-13,-7,-11,-1,20}, false),
            new Chromosome(new int[]{11,8,5,12,-7,-19,16,14}, false),
            new Chromosome(new int[]{15,6,9,10}, false)
    ));

    private final ArrayList<Chromosome> chrTransloc4Ref = new ArrayList<>(List.of(
            new Chromosome(new int[]{1,2,3,4,5}, false),
            new Chromosome(new int[]{6,7,8,9,10}, false),
            new Chromosome(new int[]{11,12,13,14}, false),
            new Chromosome(new int[]{15,16,18,19,20}, false)
    ));

    private Genome genomeTl1;

    private Genome genomeTl1Ref;

    private Genome genomeTl2;

    private Genome genomeTl2Ref;

    private Genome genomeTl3;

    private Genome genomeTl3Ref;

    private Genome genomeTl4;

    private Genome genomeTl4Ref;


    /**
     * Examples...
     */
    public GenomeExamples() {
        this.genomeAList.add(this.chromA1);
        this.genomeAList.add(this.chromA2);
        this.genomeAList.add(this.chromA3);
        this.genomeAList.trimToSize();

        this.genomeBList.add(this.chromB1);
        this.genomeBList.add(this.chromB2);
        this.genomeBList.add(this.chromB3);
        this.genomeBList.add(this.chromB4);
        this.genomeBList.trimToSize();

        this.genomeCList.add(this.chromC1);
        this.genomeCList.add(this.chromC2);
        this.genomeCList.add(this.chromC3);
        this.genomeCList.trimToSize();

        this.genomeDList.add(this.chromD1);
        this.genomeDList.add(this.chromD2);
        this.genomeDList.add(this.chromD3);
        this.genomeDList.add(this.chromD4);
        this.genomeDList.trimToSize();

        this.genomeEList.add(this.chromE1);
        this.genomeEList.add(this.chromE2);
        this.genomeEList.add(this.chromE3);
        this.genomeEList.add(this.chromE4);
        this.genomeEList.trimToSize();

        this.genomeFList.add(this.chromF1);
        this.genomeFList.add(this.chromF2);
        this.genomeFList.add(this.chromF3);
        this.genomeFList.trimToSize();

        this.genomeGList.add(this.chromG1);
        this.genomeGList.add(this.chromG2);
        this.genomeGList.add(this.chromG3);
        this.genomeGList.add(this.chromG4);
        this.genomeGList.trimToSize();

        this.genomeHList.add(this.chromH1);
        this.genomeHList.add(this.chromH2);

        this.genomeA = new Genome(this.genomeAList);
        this.genomeB = new Genome(this.genomeBList);
        this.genomeC = new Genome(this.genomeCList);
        this.genomeD = new Genome(this.genomeDList);
        this.genomeE = new Genome(this.genomeEList);
        this.genomeF = new Genome(this.genomeFList);
        this.genomeG = new Genome(this.genomeGList);
        this.genomeH = new Genome(this.genomeHList);

        /** inversion model examples **/

        this.genomeInv1 = new Genome(this.chrInv1);
        this.genomeInv2 = new Genome(this.chrInv2);
        this.genomeInv3 = new Genome(this.chrInv3);
        this.genomeInv4 = new Genome(this.chrInv4);
        this.genomeInv5 = new Genome(this.chrInv5);
        this.genomeInv6 = new Genome(this.chrInv6);
        this.genomeInv7 = new Genome(this.chrInv7);
        this.genomeInv8 = new Genome(this.chrInv8);
        this.genomeInv9 = new Genome(this.chrInv9);
        this.genomeInv10 = new Genome(this.chrInv10);
        this.genomeInv11 = new Genome(this.chrInv11);
        this.genomeInv13 = new Genome(this.multChrHP13);

        this.genomeInvRef8 = new Genome(this.chrInvRef8);
        this.genomeInvRef17 = new Genome(this.chrInvRef17);
        this.genomeInvRef21 = new Genome(this.chrInvRef21);
        this.genomeInvRef24 = new Genome(this.chrInvRef24);
        this.genomeInvRef25 = new Genome(this.chrInvRef25);
        this.genomeInvRef3 = new Genome(this.chrInvRef3);
        this.genomeInvRef18 = new Genome(this.chrInvRef18);
        this.genomeInv13Ref = new Genome(this.multChrHPRef13);
        this.genomeInv11Ref = new Genome(this.chrInvRef11);


        /** HP model examples **/

        this.genomeHP9 = new Genome(this.multChrHP9);
        this.genomeHP10 = new Genome(this.multChrHP10);
        this.genomeHP11 = new Genome(this.multChrHP11);
        this.genomeHP12 = new Genome(this.multChrHP12);

        this.genomeHP9Ref = new Genome(this.multChrHPRef9);
        this.genomeHP10Ref = new Genome(new ArrayList<>(List.of(this.multChrHPRef10)));
        this.genomeHP11Ref = new Genome(this.multChrRefHPRef11);
        this.genomeHP12Ref = new Genome(this.multChrRefHPRef12);


        /** Translocation model examples **/

        this.genomeTl1 = new Genome(this.chrTransloc1);
        this.genomeTl1Ref = new Genome(this.chrTransloc1Ref);

        this.genomeTl2 = new Genome(this.chrTransloc2);
        this.genomeTl2Ref = new Genome(this.chrTransloc2Ref);

        this.genomeTl3 = new Genome(this.chrTransloc3);
        this.genomeTl3Ref = new Genome(this.chrTransloc3Ref);

        this.genomeTl4 = new Genome(this.chrTransloc4);
        this.genomeTl4Ref = new Genome(this.chrTransloc4Ref);


    }

    public Genome getGenomeA() {
        return this.genomeA;
    }

    public Genome getGenomeB() {
        return this.genomeB;
    }

    public Genome getGenomeC() {
        return this.genomeC;
    }

    public Genome getGenomeD() {
        return this.genomeD;
    }

    public Genome getGenomeE() {
        return this.genomeE;
    }

    public Genome getGenomeF() {
        return this.genomeF;
    }

    public Genome getGenomeG() {
        return this.genomeG;
    }

    public Genome getGenomeH() {
        return this.genomeH;
    }

    //Getter for inversion distance calculations

    public Genome getGenomeInv1() {
        return genomeInv1;
    }

    public Genome getGenomeInv2() {
        return genomeInv2;
    }

    public Genome getGenomeInv3() {
        return genomeInv3;
    }

    public Genome getGenomeInv4() {
        return genomeInv4;
    }

    public Genome getGenomeInv5() {
        return genomeInv5;
    }

    public Genome getGenomeInv6() {
        return genomeInv6;
    }

    public Genome getGenomeInv7() {
        return genomeInv7;
    }

    public Genome getGenomeInv8() {
        return genomeInv8;
    }

    public Genome getGenomeInv9() {
        return genomeInv9;
    }

    public Genome getGenomeInv10() {
        return genomeInv10;
    }

    public Genome getGenomeInvRef18() {
        return genomeInvRef18;
    }

    public Genome getGenomeInvRef3() {
        return genomeInvRef3;
    }

    public Genome getGenomeInvRef8() {
        return genomeInvRef8;
    }

    public Genome getGenomeInvRef17() {
        return genomeInvRef17;
    }

    public Genome getGenomeInvRef21() {
        return genomeInvRef21;
    }

    public Genome getGenomeInvRef24() {
        return genomeInvRef24;
    }

    public Genome getGenomeInvRef25() {
        return genomeInvRef25;
    }

    public Genome getGenomeInv11() {
        return genomeInv11;
    }

    public Genome getGenomeInv11Ref() {
        return genomeInv11Ref;
    }

    public Genome getGenomeHP9() {
        return genomeHP9;
    }

    public Genome getGenomeHP10() {
        return genomeHP10;
    }

    public Genome getGenomeHP11() {
        return genomeHP11;
    }

    public Genome getGenomeHP9Ref() {
        return genomeHP9Ref;
    }

    public Genome getGenomeHP10Ref() {
        return genomeHP10Ref;
    }

    public Genome getGenomeHP11Ref() {
        return genomeHP11Ref;
    }

    public Genome getGenomeHP12() {
        return genomeHP12;
    }

    public Genome getGenomeHP12Ref() {
        return genomeHP12Ref;
    }

    public Genome getGenomeInv13() {
        return genomeInv13;
    }

    public Genome getGenomeInvRef13() {
        return genomeInv13Ref;
    }

    public Genome getGenomeTl1() {
        return genomeTl1;
    }

    public Genome getGenomeTl1Ref() {
        return genomeTl1Ref;
    }

    public Genome getGenomeTl2() {
        return genomeTl2;
    }

    public Genome getGenomeTl2Ref() {
        return genomeTl2Ref;
    }

    public Genome getGenomeTl3() {
        return genomeTl3;
    }

    public Genome getGenomeTl3Ref() {
        return genomeTl3Ref;
    }

    public Genome getGenomeTl4() {
        return genomeTl4;
    }

    public Genome getGenomeTl4Ref() {
        return genomeTl4Ref;
    }
}
