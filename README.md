# UniMoG
UniMoG computes distances and sorting scenarios between two balanced genomes by using up to six different models.

## Requirements
UniMoG requires at least JDK 11.

## Set up an integrated development environment (IDE)
### Intellij
- import options.jar and FactorialBench2011.jar from project's libs directory in `Project Structure->Libraries`
- import options.jar and FactorialBench2011.jar in `Project Structure->Modules`
- import hamcrest.jars in `Project Structure->Modules`
- create UniMoG.jar configuration by adding `JAR->From modules with dependencies` in `Project Structure->Artifacts`

## Parameter
If no correct parameters are handed over the software starts with a graphical user interface.
If more help is needed, start the program in GUI mode first and click the 'Help' button.
- `-m=Integer`: selects model
- `-d`: turns off sorting scenario calculation (only distance is given)
- `-s`: turns on uniform sampling (only implemented for DCJ scenario)
- `-p`: if sorting scenario is calculated, then only a plain version is printed
- `-z`: if zip file is used
- `-h`,`--help`: displays help text

### Model and input genome types

| m   | Model              | Chromosome types        | Number of chromosomes                    |
|-----|--------------------|-------------------------|------------------------------------------|
| 1   | DCJ [4]            | linear, circular, mixed | arbitrary                                |
| 2   | restricted DCJ [4] | linear                  | arbitrary                                |
| 3   | HP [3]             | linear                  | arbitrary                                |
| 4   | Inversion [2]      | linear                  | one per genome                           |
| 5   | Translocation [1]  | linear, co-tailed       | equal for both genomes, <br/> at least 2 |
| 6   | DCJ-indel [5]      | linear, circular, mixed | arbitrary                                |
| 7   | All                | linear, circular, mixed | arbitrary                                |

Co-tailed chromosomes ends occur the same time in both genomes [2].

## Start UniMoG on the command line

### Unix

1. create a jar-file of UniMoG

2. Start it by using:
- If options.jar and FactorialBench2011.jar are in the same directory as/are already included in UniMoG.jar:  
  `java -jar <path_of_UniMoG.jar> -m=Integer [-s] [-d] [-p] [-z] <GenomesInputFiles>`  

- Otherwise:  
  `java -cp <path_of_UniMoG.jar>:<path_of_options.jar>:<path_of_FactorialBench2011.jar> de.unibi.cebitec.gi.unimog.framework.MainClass -m=Integer [-s] [-d] [-p] [-z] <GenomesInputFiles>`

### Windows
* follow Unix instructions and make some changes:
    * use backslash (`&bsol;`) instead of slash (`/`)
    * use semicolons instead of colons
    * paths of `-cp` must be in quotes (e.g. `-cp "C:\...;C:\..."`)

<br><br>
Note: Unimog was not tested on macOS.

### Example
Pairwise comparison of genomes A to D using the HP model (`-m=3`) and only computing the distance (`-d`):

#### File `example.txt` with genomes  

>&gt;A  
-8 -7 -6 4 5 -3 -2 -1 |  
&gt;B  
1 2 3 4 5 6 7 8 |  
&gt;C  
-8 -7 -6 -5 -4 -3 -1 -2 |  
&gt;D  
1 2 3 4 5 6 7 8 |

#### Command line 
`java -jar /home/USER/unimog/out/artifacts/unimog_jar/unimog.jar /home/USER/example.txt -m=3 -d`

#### Result  
Given files: [/home/USER/example.txt]  
HP distance comparisons:  

|     | A | B | C | D |
|-----|---|---|---|---|
| A   | 0 | 1 | 4 | 1 |
| B   | - | 0 | 3 | 0 |
| C   | - | - | 0 | 3 |
| D   | - | - | - | 0 |


HP distance comparisons as PHYLIP matrix:

4  
A           
B         1  
C         4 3  
D         1 0 3  



Steps of each genome comparison:



Adjacencies of each genome comparison after each step:  

## UniMoG input file format

UniMoG requires genomes in a specific format.

- Each genome name starts with `>` (e.g. `>GenomeA`)
- The genome sequence follows on a new line
- Chromosomes are represented as whitespace separated genes
- Linear chromosomes end with a vertical bar `|`
- Circular chromosomes end with the closing round bracket `)`
- Chromosomes may occur all in one line and/or split into several lines
- Gene labels can be integer or strings
- Reverse oriented genes are marked with `-` (e.g. `-gene1`)
- Genomes need to match the input type of the selected model (see table above)
- Lines starting with `//` are ignored and can be used for comments
- UniMoG input files need at least two genomes to compare

## Publication
Hilker, R., Sickinger, C., Pedersen, C. N., & Stoye, J. (2012). UniMoG—a unifying framework for genomic distance calculation and sorting based on DCJ. Bioinformatics, 28(19), 2509-2511.

## References
[1] Bergeron, A., Mixtacki, J., & Stoye, J. (2005). On sorting by translocations. In Research in Computational Molecular Biology: 9th Annual International Conference, RECOMB 2005, Cambridge, MA, USA, May 14-18, 2005. Proceedings 9 (pp. 615-629). Springer Berlin Heidelberg.

[2] Bergeron, A., Mixtacki, J., & Stoye, J. (2007). The inversion distance problem.

[3] Bergeron, A., Mixtacki, J., & Stoye, J. (2008). HP distance via double cut and join distance. In Combinatorial Pattern Matching: 19th Annual Symposium, CPM 2008, Pisa, Italy, June 18-20, 2008 Proceedings 19 (pp. 56-68). Springer Berlin Heidelberg.

[4] Kováč, J., Warren, R., Braga, M. D., & Stoye, J. (2011). Restricted DCJ model: rearrangement problems with chromosome reincorporation. Journal of Computational Biology, 18(9), 1231-1241.

[5] Braga, M. D., Willing, E., & Stoye, J. (2011). Double cut and join with insertions and deletions. Journal of Computational Biology, 18(9), 1167-1184.

[6] Bergeron, A., Mixtacki, J., & Stoye, J. (2006). A unifying view of genome rearrangements. In Algorithms in Bioinformatics: 6th International Workshop, WABI 2006, Zurich, Switzerland, September 11-13, 2006. Proceedings 6 (pp. 163-173). Springer Berlin Heidelberg.

[7] Erdős, P. L., Soukup, L., & Stoye, J. (2011). Balanced vertices in trees and a simpler algorithm to compute the genomic distance. Applied mathematics letters, 24(1), 82-86.

[8] Hannenhalli, S., & Pevzner, P. A. (1995, October). Transforming men into mice (polynomial algorithm for genomic distance problem). In Proceedings of IEEE 36th annual foundations of computer science (pp. 581-592). IEEE.

[9] Hannenhalli, S., & Pevzner, P. A. (1999). Transforming cabbage into turnip: polynomial algorithm for sorting signed permutations by reversals. Journal of the ACM (JACM), 46(1), 1-27.

[10] Pevzner, P., & Tesler, G. (2003, April). Transforming men into mice: the Nadeau-Taylor chromosomal breakage model revisited. In Proceedings of the seventh annual international conference on Research in computational molecular biology (pp. 247-256).

[11] Tannier, E., Bergeron, A., & Sagot, M. F. (2007). Advances on sorting by reversals. Discrete Applied Mathematics, 155(6-7), 881-888

[12] Tesler, G. (2002). Efficient algorithms for multichromosomal genome rearrangements. Journal of Computer and System Sciences, 65(3), 587-609.

[13] Yancopoulos, S., Attie, O., & Friedberg, R. (2005). Efficient sorting of genomic permutations by translocation, inversion and block interchange. Bioinformatics, 21(16), 3340-3346.
## Licence
MIT