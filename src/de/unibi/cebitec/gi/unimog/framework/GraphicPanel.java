package de.unibi.cebitec.gi.unimog.framework;

import de.unibi.cebitec.gi.unimog.algorithms.IntermediateGenomesGenerator;
import de.unibi.cebitec.gi.unimog.algorithms.IntermediateGenomesGeneratorInDel;
import de.unibi.cebitec.gi.unimog.datastructure.ChromosomeFragment;
import de.unibi.cebitec.gi.unimog.datastructure.ChromosomeString;
import de.unibi.cebitec.gi.unimog.datastructure.DataOutput;
import de.unibi.cebitec.gi.unimog.datastructure.Label;
import de.unibi.cebitec.gi.unimog.datastructure.OperationInDel;
import de.unibi.cebitec.gi.unimog.datastructure.OperationListInDel;
import de.unibi.cebitec.gi.unimog.datastructure.Pair;
import de.unibi.cebitec.gi.unimog.utils.Observable;
import de.unibi.cebitec.gi.unimog.utils.Observer;
import de.unibi.cebitec.gi.unimog.utils.Toolz;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker                                     *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @author -Rolf Hilker-
 *
 * Class creating the graphic panel with its content. It visualizes the
 * genomes after each operation.
 * 3 Möglichkeiten zu zeichnen:
 * 1. Alles auf ein Panel -> am besten für kleine Bilder
 * 2. Double buffering in offscreen image -> am besten für nicht zu große Bilder & blitzschnellen Zugriff
 * 3. Einzelne Panels für einzelne Bildbausteine -> am besten für riesige Bilder, da paint() nur von den Komponenten
 * aufgerufen wird, die auch auf dem bildschirm angezeigt werden.
 * 4. Angabe des Bereichs eines Panels, der jeweils gezeichnet werden soll -> schnellste, schönste Methode.
 * Note: um erscheinungsfehler zu beheben: Überschreibe paint & paintAll für jeweils alle genomepanels, die sichtbar sind!
 */
public class GraphicPanel extends JPanel implements Observable {

    private static final long serialVersionUID = 1L;
    private final float fontSize = 14;
    private DataOutput[] results;
    private double zoomF;
    private Model model;
    private List<Observer> observers;

    public GraphicPanel() {
        super();
        this.setOpaque(true);
        this.observers = new ArrayList<Observer>();
    }

    /**
     * Sets the data necessary for a graphical output. When a new result is handed over, all data
     * is always set. Therefore no single setResults method is available.
     * @param results The results to print
     * @param model The model to use (DCJ, HP, ... see)
     * @param zoomFactor The zoom factor to use
     */
    public void setData(final DataOutput[] results, final Model model, final double zoomFactor) {
        this.removeAll();
        this.model = model;
        this.results = results;
        this.zoomF = zoomFactor;
        this.prepareOutput();
    }
    
    public void setResults(DataOutput[] results) {
        this.results = results;
    }

    @SuppressWarnings("unchecked")
    public void prepareOutput() {

        this.setLayout(new GridLayout(0, 1));

        //Overall results level
        if (this.results != null && this.results[0].getIntermedGenomes() != null) {
            this.paint(this.getGraphics());
            
            for (int m = 0; m < this.results.length; ++m) {
                if (this.results[m].getIntermedGenomes() != null) {
                    IntermediateGenomesGenerator[] result = this.results[m].getIntermedGenomes();

                    //Result of one specific pairwise comparison level
                    for (int i = 0; i < result.length; ++i) {
                        if (result[i] != null) { //Remember that for "all" model result[1,2,3] are null!

                            // Before creating description panel get the lower bound of possible
                            // rearrangement scenarios.
                            BigInteger lowerBoundInt = this.results[m].getDCJLowerBound();
                            String lowerBound = "";
                            if (lowerBoundInt != null) {
                                lowerBound = lowerBoundInt.toString();
                                if (model == Model.DCJ_INDEL) {
                                	// do nothing
                                	lowerBound = "";
                                } else if (model != Model.DCJ && model != Model.ALL || model == Model.ALL && i > 0) { // FIXME assoziation?
                                    lowerBound = " (This is only one of roughly ".concat(lowerBound).concat(" sorting sequences)");
                                } else {
                                    lowerBound = " (This is only one of at least ".concat(lowerBound).concat(" sorting sequences)");
                                }
                            }
                            
                            // Create description panel for each comparison 
                            String scenarioName;
                            if (this.model != Model.ALL) {
                                scenarioName = this.model.getGuiName();
                            } else {
                                scenarioName = Model.getModel(i + 1).getGuiName();
                            }
                            final JLabel descriptLabel = new JLabel(scenarioName + " conversion of genome \"".concat(
                                    result[i].getGenomeID1()).concat("\" to \"").concat(
                                    result[i].getGenomeID2()).concat("\" ").concat(
                                    lowerBound).concat(":"));
                            descriptLabel.setFont(descriptLabel.getFont().deriveFont((float) (this.fontSize * this.zoomF)));
                            final JPanel descriptPanel = new JPanel(new BorderLayout());
                            descriptPanel.add(descriptLabel, BorderLayout.CENTER);
                            this.add(descriptPanel);
                            boolean emptyComp = true;

                            //Intermediate genomes of one comparison level
                            int doubled = 0;
                            ArrayList<ChromosomeString[]> genomes = result[i].getIntermedGenomes();
                            HashMap<Integer, String> backMap = result[i].getGeneNameMap();
                            ArrayList<Pair<Integer, Integer>> operationList = result[i].getOperationList();
                            int fstExtremity = -1;
                            int scndExtremity = -1;

                            // cuts with colour
                            List<ChromosomeFragment> cuts = new ArrayList<ChromosomeFragment>();
                            List<ChromosomeFragment> oldcuts;

                            ArrayList<Color> colorList = new ArrayList<Color>();
                            ArrayList<Boolean> chromUnalteredList = new ArrayList<Boolean>();
                            for (int j = 0; j < genomes.size(); ++j) {

                                ////////////// Single intermediate genome level ///////////////////////////////////

                                oldcuts = cuts;
                                cuts = new ArrayList<ChromosomeFragment>();

                                final ArrayList<ChromosomePanel> genomePanels = new ArrayList<ChromosomePanel>();
                                if (genomes.get(j).length > 0) {
                                    final JPanel genomePanel = new JPanel();
                                    genomePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
                                    this.add(genomePanel);

                                    //Create label depicting the current step
                                    final JLabel stepLabel = new JLabel("Step " + j + ":");
                                    stepLabel.setFont(stepLabel.getFont().deriveFont((float) (this.fontSize * this.zoomF)));
                                    final JPanel stepPanel = new JPanel(new BorderLayout());
                                    stepPanel.add(stepLabel, BorderLayout.CENTER);
                                    genomePanel.add(stepPanel);
                                    genomePanel.setBackground(this.getBackground());

                                    final ChromosomeString[] currentGenome = genomes.get(j);
                                    Pair<Integer, Integer> currentOperation = null;
                                    Pair<String, Integer> fstGene = null, sndGene = null;
 
                                    // bool that enables special fragment handling for deletes
                                    boolean insertPostProcessing = false;
                                    
                                    boolean nameset=false;
                                    if ((doubled = j * 2) < operationList.size()) {
                                        currentOperation = operationList.get(doubled); //never allowed to be 0, assure that!
                                        
                                        fstExtremity = currentOperation.getFirst();
                                        scndExtremity = currentOperation.getSecond();
                                        
                                        
                                        // TG: Special condition for InDels
                                        if(currentOperation instanceof OperationInDel) {
                                            
                                            switch (((OperationInDel) currentOperation).getType()) {
                                                case Backward:
              
                                                    fstGene = retriveBackwardInDelFirst(scndExtremity, backMap);
                                                    sndGene = retriveBackwardInDelSecond(fstExtremity, backMap, genomes.get(j+1));
                                                    
                                                    // in some cases sndGene has no neighbour, because telomer
                                                    if (sndGene.getFirst().isEmpty()) {
                                                        fstGene = retriveBackwardInDelFirst(fstExtremity, backMap);
                                                        sndGene = retriveBackwardInDelSecond(scndExtremity, backMap, genomes.get(j+1));
                                                    }
                                                    
                                                    nameset = true;
                                                    break;
                                                case BackwardInDel:
                                                    
                                                    fstGene = retrieveInDelSpecial(fstExtremity, backMap, genomes.get(j+1));
                                                    sndGene = retrieveInDelSpecial(scndExtremity, backMap, genomes.get(j+1));
                                                    
                                                    nameset = true;
                                                    break;
                                                case ForwardInDel:
                                                    
                                                    fstGene = retrieveInDelSpecial(fstExtremity, backMap, genomes.get(j));
                                                    sndGene = retrieveInDelSpecial(scndExtremity, backMap, genomes.get(j));
                                                    
                                                    nameset = true;
                                                    break;
                                                case ForwardHelperOne:
                                                    
                                                    fstGene = retriveBackwardInDelFirst(scndExtremity, backMap);
                                                    sndGene = retrieveInDelSpecial(fstExtremity, backMap, genomes.get(j));
                                                    
                                                    nameset = true;
                                                    break;
                                                case Singleton:
                                                    
                                                    Pair<String, String> l = (Pair<String, String>) ((OperationInDel) currentOperation).getSpecialInfo();
                                                    
                                                    fstGene = new Pair<String, Integer>(l.getFirst(), currentOperation.getFirst());
                                                    sndGene = new Pair<String, Integer>(l.getSecond(), currentOperation.getSecond());
                                                    
                                                    nameset = true;
                                                    break;
                                                case Insert:
                                                    insertPostProcessing = true;
                                                    break;
                                                case Delete:
                                                case Forward:
                                                    // handle this normally
                                                    break;
                                            }
                                        }
                                        
                                    } else {
                                        fstExtremity = 0;
                                        scndExtremity = 0;
                                    }

                                    for (int k = 0; k < currentGenome.length; ++k) {
                                        ///////// Single chromosome level ///////////////////////////
                                        if (colorList.size() < k + 1 || !chromUnalteredList.get(k)) {
                                            colorList.add(Toolz.getBackColor());
                                        }
                                        final String[] genes = currentGenome[k].getGenes();
                                        final boolean circular = currentGenome[k].isCircular();
                                        
                                        ChromosomePanel chromPanel;
                                        
                                        if (nameset) { // special case for InDel
                                            chromPanel = new ChromosomePanel(genes, circular, fstGene.getFirst(),
                                                    sndGene.getFirst(),  fstGene.getSecond(),
                                                    sndGene.getSecond(), oldcuts,
                                                    backMap, this.zoomF, colorList.get(k));
                                        } else {
                                            chromPanel = new ChromosomePanel(genes, circular, fstExtremity,
                                                    scndExtremity, oldcuts, backMap, this.zoomF, colorList.get(k));
                                        }
                                        genomePanel.add(chromPanel);
                                        genomePanels.add(chromPanel);
                                        chromPanel.paint(chromPanel.getGraphics());
                                        
                                        if (!chromPanel.hadACut()) {
                                            chromUnalteredList.add(k, true);
                                        } else {
                                            chromUnalteredList.add(k, false);
                                        }
                                        
                                        cuts.addAll(chromPanel.getCuts());
                                    }
                                        
                                    
                                    if(insertPostProcessing) {
                                        // inserts can not be accounted for in the drawing framework
                                        // therefore this hack has to be applied to correct for this
                                        
                                         createInsertChromFragment(cuts, (Pair<Integer,Pair<String,String>>) ((OperationInDel) currentOperation).getSpecialInfo(), backMap);    
                                    }
                                    
                                 
                                    emptyComp = false;
                                }
                            } //////////////////////////////////////////////////////////////////////////////////////
                            if (emptyComp) {
                                this.remove(descriptPanel);
                            }
                        }
                    }
                }
                this.notifyObservers(m); //send number of current comparison to observers
            }
        }
    }

    /**
     * Sets the zoom factor = the size of the output.
     * @param zoomFactor The zoom factor 
     */
    public void setZoomFactor(final double zoomFactor) {
        if (zoomFactor != this.zoomF) {
            final Rectangle visRect = this.getVisibleRect();
            this.zoomF = zoomFactor;
            final Component[] genomePanels = this.getComponents();
            for (Component genomePanel : genomePanels) {
                final JPanel genomePanel2 = (JPanel) genomePanel;

                final Rectangle rect = genomePanel2.getBounds();
                boolean intersects = false;  //Test whether component is in visible rect
                if ((visRect.y < rect.y && rect.y < visRect.y + visRect.height)
                        || (visRect.y < rect.y + rect.height && rect.y + rect.height < visRect.y + visRect.height)) {
                    intersects = true;
                }

                Component[] panels = genomePanel2.getComponents();
                for (Component panel : panels) {
                    try {
                        final ChromosomePanel chromPanel = (ChromosomePanel) panel;
                        chromPanel.setZoomF(zoomFactor, intersects);
                    } catch (ClassCastException ex) {
                        try { //handle description panels
                            final JPanel descriptPanel = (JPanel) panel;
                            JLabel descriptLabel = (JLabel) descriptPanel.getComponent(0);
                            descriptLabel.setFont(descriptLabel.getFont().deriveFont((float) (this.fontSize * this.zoomF)));
                        } catch (ClassCastException ex2) {
                            try {
                                final JLabel headerLabel = (JLabel) panel;
                                headerLabel.setFont(headerLabel.getFont().deriveFont((float) (this.fontSize * this.zoomF)));
                            } catch (ClassCastException ex3) {
                                //nothing to do here, other components are not handled here
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Sets the new output style. 
     */
    public void setColorMode() {
        final Rectangle visRect = this.getVisibleRect();
        final Component[] genomePanels = this.getComponents();
        for (Component genomePanel : genomePanels) {
            final JPanel genomePanel2 = (JPanel) genomePanel;

            final Rectangle rect = genomePanel2.getBounds();
            boolean intersects = false;  //Test whether component is in visible rect
            if ((visRect.y < rect.y && rect.y < visRect.y + visRect.height)
                    || (visRect.y < rect.y + rect.height && rect.y + rect.height < visRect.y + visRect.height)) {
                intersects = true;
            }

            Component[] panels = genomePanel2.getComponents();
            for (Component panel : panels) {
                try {
                    final ChromosomePanel chromPanel = (ChromosomePanel) panel;
                    //chromPanel.setChromBackgColor(Toolz.getBackColor());
                    chromPanel.setChromBackgColor(MainFrame.COLOR_MODE);
                    if (intersects) {
                        chromPanel.paint(chromPanel.getGraphics());
                    }
                } catch (ClassCastException ex) {
                    //nothing to do here, other components are not handled here
                }
            }
        }
    }
    
    
    
//	/**
//	 * Paints all components of the panel again. Necessary for saving an image.
//	 */
//	public void paintThemAll() {
//		final Component[] genomePanels = this.getComponents();
//		for (Component genomePanel : genomePanels){
//			final JPanel genomePanel2 = (JPanel) genomePanel;			
//			Component[] chromPanels = genomePanel2.getComponents();
//			for (Component chromPanel : chromPanels){
//				chromPanel.paint(chromPanel.getGraphics());
//			}
//		}
//	}

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers(Object data) {
        for (Observer observer : this.observers) {
            observer.update(data);
        }
    }
    
    /**
     * Calls {@link removeAll()}, but also adds a label to the cleared GraphicPanel,
     * stating that the output has been cleared for performance reasons.
     */
    public void removeAllForPerformance() {
        super.removeAll();
        this.add(new JLabel("Graphic output has been cleared for performance!"));
        this.repaint();
    }

    private Pair<String, Integer> retriveBackwardInDelFirst(int fstExtremity, HashMap<Integer, String> backMap) {
        // for first just take name and orientation
        return new Pair<String, Integer>(IntermediateGenomesGenerator.getSignedGeneName((fstExtremity + 1) / 2, backMap), fstExtremity % 2);
    }

    private Pair<String, Integer> retriveBackwardInDelSecond(int scndExtremity, HashMap<Integer, String> backMap, ChromosomeString[] chromosomes) {
        
        String geneNameOrig = IntermediateGenomesGenerator.getSignedGeneName((scndExtremity + 1) / 2, backMap);
        int orientationOrig = scndExtremity % 2;
        String geneNameOrigUnoriented = unsignGene(geneNameOrig);
        
        String geneName = "";
        int orientation = 0;
        

        //TODO: linear implementation, much more complicated constant impl might exist
        for (ChromosomeString chromosome: chromosomes) {
            for(int i=0; i< chromosome.getGenes().length; i++ ) {
                
                String gene = chromosome.getGenes()[i];
                String geneUnoriented = unsignGene(gene);
                
                
                if (geneNameOrigUnoriented.equals(geneUnoriented)) {
                    // original gene was found
                    // is orientation the same?
                    boolean direction = geneNameOrig.equals(gene);
                    boolean left = (direction &&  orientationOrig%2==1 ) || (!direction &&  orientationOrig%2==0 );
                    
                    if(left) {
                        if(i-1 >= 0) {
                            geneName = chromosome.getGenes()[i-1];
                        } else if(chromosome.isCircular()) {
                            geneName = chromosome.getGenes()[chromosome.getGenes().length-1];
                        }
                        orientation = 0;
                    } else {
                        if(i+1 < chromosome.getGenes().length) {
                            geneName = chromosome.getGenes()[i+1];
                        } else if(chromosome.isCircular()) {
                            geneName = chromosome.getGenes()[0];
                        }
                        orientation = 1;
                    }  
                    
                    
                    return new Pair<String, Integer>(geneName, orientation);
                }
            }
        } 

        return new Pair<String, Integer>(geneName, orientation);
    }

    
    /**
     * retrieve the special cut ends BackwardInDel
     * @param scndExtremity
     * @param backMap
     * @param chromosomes
     * @return 
     */
    private Pair<String, Integer> retrieveInDelSpecial(int scndExtremity, HashMap<Integer, String> backMap, ChromosomeString[] chromosomes) {
        
        String geneNameOrig = IntermediateGenomesGenerator.getSignedGeneName((scndExtremity + 1) / 2, backMap);
        int orientationOrig = scndExtremity % 2;
        String geneNameOrigUnoriented = unsignGene(geneNameOrig);
        
        String geneName = "";
        int orientation = 0;
        
        for (ChromosomeString chromosome: chromosomes) {
            for(int i=0; i< chromosome.getGenes().length; i++ ) {
                
                String gene = chromosome.getGenes()[i];
                String geneUnoriented = unsignGene(gene);
                
                
                if (geneNameOrigUnoriented.equals(geneUnoriented)) {
                    // original gene was found
                    // is orientation the same?
                    boolean direction = geneNameOrig.equals(gene);
                    
                    if (direction) {
                        if(orientationOrig == 1) {
                            return new Pair<String, Integer>(chromosome.getGenes()[0], 1);
                        } else {
                            return new Pair<String, Integer>(chromosome.getGenes()[chromosome.getGenes().length-1], 0);
                        }
                    } else {
                        if(orientationOrig == 0) {
                            return new Pair<String, Integer>(chromosome.getGenes()[0], 1);
                        } else {
                            return new Pair<String, Integer>(chromosome.getGenes()[chromosome.getGenes().length-1], 0);
                        }
                    }
                    
                }
            }
        } 

        return new Pair<String, Integer>(geneName, orientation);
    }
    
    
    /**
     * if fstGene and sndGene cuts are defined on same gene string, the ChromosomePanel
     * can't draw all cuts => redefine one cut to other gene
     * @param fstGene
     * @param sndGene
     * @param chromosomes 
     */
    private void disambiguateCuts(Pair<String, Integer> fstGene, Pair<String, Integer> sndGene, ChromosomeString [] chromosomes) {
        
        String fstCutGeneUnor = unsignGene(fstGene.getFirst());
        String scndCutGeneUnor = unsignGene(sndGene.getFirst());

        String fstCutGene = fstGene.getFirst();
        String scndCutGene = sndGene.getFirst();
        int orient1 = fstGene.getSecond();
        int orient2 = sndGene.getSecond();

        // special case fst and second cut are on same Gene
        // somehow the panels can't manage that
        if (fstCutGeneUnor.equals(scndCutGeneUnor)) {
            if ((fstCutGene.equals(scndCutGene) && orient1 % 2 != orient2 % 2)
                    || (!fstCutGene.equals(scndCutGene) && orient1 % 2 == orient2 % 2)) {

                // here there is one cut before and one cut after gene
                // set one cut to other component
                for (ChromosomeString chromosome : chromosomes) {
                    for (int i = 0; i < chromosome.getGenes().length; i++) {

                        String gene = chromosome.getGenes()[i];
                        
                        if(!unsignGene(gene).equals(fstCutGeneUnor)) {
                            continue;
                        }
                        
                        // find the front cut
                        boolean orient = fstCutGene.equals(gene);
                        boolean front = (orient &&  orient1%2==1 ) || (!orient &&  orient1%2==0 );
                                
                        // true: first cut cuts before gene

                        if (i < chromosome.getGenes().length - 2) { // there is a gene to the right to move to
                            if (front) {
                                sndGene.setFirst(chromosome.getGenes()[i + 1]);
                                sndGene.setSecond(1);
                            } else {
                                fstGene.setFirst(chromosome.getGenes()[i + 1]);
                                fstGene.setSecond(1);
                            }
                        } else if (i > 0) { // there is a gene to the right though
                            if (front) {
                                sndGene.setFirst(chromosome.getGenes()[i - 1]);
                                sndGene.setSecond(0);
                            } else {
                                fstGene.setFirst(chromosome.getGenes()[i - 1]);
                                fstGene.setSecond(0);
                            }
                        }
                        
                        return; //we are done :)
                    }
                }
            }

        }
    }
        
    
    /**
     * Correct the given fragment to make it properly drawn insert.
     * @param cuts 
     */
    private void createInsertChromFragment(List<ChromosomeFragment> cuts, Pair<Integer,Pair<String,String>> lable, HashMap<Integer, String> backMap) {
 
        String gene1 = cuts.get(0).getGene1();
        Color colour1 = cuts.get(0).getColour1();
        
        String gene2;
        Color colour2;
        Color insertColour;
        
        if (cuts.size()>1) {
            gene2 = cuts.get(1).getGene2();
            colour2 = cuts.get(1).getColour2();
            insertColour = cuts.get(1).getColour1();
        } else {
            gene2 = cuts.get(0).getGene2();
            colour2 = cuts.get(0).getColour2();
            insertColour = Toolz.getColor();
        }
     
        
        int geneToAdd = (lable.getFirst() + 1) / 2;
        if (lable.getFirst() % 2 == 1) { 
            geneToAdd *= -1;
        }
        
        String geneNameLeft = IntermediateGenomesGenerator.getSignedGeneName(geneToAdd, backMap);
        
        if(geneNameLeft == null) {
            geneNameLeft = "";
        }
        
        Pair<String,String> ends = lable.getSecond();
        
        String gene1Partner, gene2Partner;
        if(gene1.equals(geneNameLeft)) {
            gene1Partner = ends.getFirst();
            gene2Partner = ends.getSecond();
        } else {
            gene2Partner = switchString(ends.getFirst());
            gene1Partner = switchString(ends.getSecond());
        }
 
        cuts.clear();
        cuts.add(new ChromosomeFragment(colour1, insertColour, gene1, gene1Partner));
        cuts.add(new ChromosomeFragment(insertColour, colour2, gene2Partner, gene2));
    }
      
    /**
     * Removes the sign of a gene, if it starts with a minus.
     *
     * @param gene gene to "unsign"
     * @return unsigned gene
     */
    private String unsignGene(String gene) {
        return gene.startsWith("-") ? gene.substring(1) : gene;
    }

    private String switchString(String gene) {

        if (gene == null) {
            return null;
        }

        if (gene.startsWith("-")) {
            gene = gene.substring(1);
        } else {
            gene = "-" + gene;
        }
        return gene;
    }







}
