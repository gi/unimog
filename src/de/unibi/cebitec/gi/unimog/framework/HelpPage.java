package de.unibi.cebitec.gi.unimog.framework;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker                                     *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/**
 * @author -Rolf Hilker-
 * 
 * Provides the content of the help page and the "page" itself.
 * It is designed as a JTextPane with text and images.
 */
public class HelpPage extends JFrame {

    private static final long serialVersionUID = 1L;
    private final Dimension standardSize = new Dimension(800, 600);
    private final String newline = "\n";
    private final static String folder = "helpImages/";
	private final String[] iconPaths = {
			HelpPage.folder.concat("table1.jpg"),
			HelpPage.folder.concat("result1.jpg"),
			HelpPage.folder.concat("result2.jpg"),
			HelpPage.folder.concat("help-dcj-id-scen.jpg"),
			HelpPage.folder.concat("load.jpg"),
			// HelpPage.folder.concat("result3.jpg"),
			// HelpPage.folder.concat("load.jpg"),
			HelpPage.folder.concat("example.jpg"),
			HelpPage.folder.concat("help-scenario-6.jpg"),
			HelpPage.folder.concat("run.jpg"),
			HelpPage.folder.concat("genomes.jpg"),
			HelpPage.folder.concat("adj.jpg"),
			HelpPage.folder.concat("graphic.jpg"),
			HelpPage.folder.concat("help-steps-standard.jpg"),
			HelpPage.folder.concat("colors.jpg"),
			// HelpPage.folder.concat("help-steps-no.jpg"),
			// HelpPage.folder.concat("colors.jpg"),
			// HelpPage.folder.concat("help-sampling.jpg"),
			// HelpPage.folder.concat("colors.jpg"),
			HelpPage.folder.concat("slider.jpg"),
			HelpPage.folder.concat("save.jpg"),
			HelpPage.folder.concat("save2.jpg"),
			HelpPage.folder.concat("clear.jpg"),
			HelpPage.folder.concat("exit.jpg"),
			HelpPage.folder.concat("help.jpg"),
			HelpPage.folder.concat("help-sampling.jpg"),
			HelpPage.folder.concat("recombination.jpeg") };

    public HelpPage() {
        this.createContent();
    }

    /**
     * Creates the content (text and images) of the pane.
     */
    private void createContent() {
    	// check also help constants
        final String space = "     ";
        final String space2 = "        ";
        final String space3 = "       ";
        String[] initString = {                      
            "Startup possibilities".concat(newline).concat(newline),
            "UniMoG can be started in console mode or with a graphical user interface.".
            concat(newline).
            concat("For using the console mode a few parameters have to be passed:").
            concat(newline).
concat("-  \"-m=Integer\" Defines the model: 1 = "
								+ Model.DCJ.getAbbrevGuiName() + ", 2 = "
								+ Model.DCJ_RESTRICTED + ", 3 = "
								+ Model.HP.getAbbrevGuiName() + ", 4 = "
								+ Model.INVERSION.getGuiName() + ", 5 = "
								+ Model.TRANSLOCATION.getGuiName() + ", 6 = "
								+ Model.DCJ_INDEL.getGuiName()
								+ ", 7 = All models")
						.concat(newline)
						.
            concat("-  \"-s\" (optional) turns on the uniform sampling (only implemented for DCJ scenario)").
            concat(newline).
            concat("-  \"-d\" (optional) calculates only distance (no sorting scenario)").
            concat(newline).
            concat("-  \"-p\" (optional) prints the scenario in plain version (if steps are shown)").
            concat(newline).
            concat("-  \"-z\" (optional) if zip file is used").
            concat(newline).
            concat("-  The path(s) to one or more files containing all genomes which should be analyzed").
            concat(newline).
            concat(newline).
            concat("o  Example: java -jar UniMoG -m=7 -s genomefile.txt rickettsia/rco.txt rickettsia/rpo.txt -p").
            concat(newline).
            concat("o  If no correct parameters are passed, UniMoG starts with the graphical user interface").
            concat(newline).concat(newline),
            "Genome Representation".
            concat(newline).concat(newline), //bold
            "The loaded files have to contain at least two, but can contain several genomes represented as sequences of oriented common blocks.".
            concat(newline).
            concat("Every genome starts with a > followed by the genome name.").
            concat(newline).
            concat("In the following line(s) come the genes. They are separated by whitespaces. 'Newlines' do not matter.").
            concat(newline).
            concat("Each chromosome is concluded by ')' or '|'. A ')' concludes a circular chromosome, a '|' concludes a linear chromosome "
                + "and can be omitted for"). 
            concat(newline).
            concat("the last chromosome of a genome.").
            concat(newline).
            concat("For obvious reasons the gene names can contain all signs except whitespaces. A '-' before a gene name means that "
                + "the gene direction is backwards.").
            concat(newline).
            concat("Each pairwise comparison only takes genes into account that occur in both genomes of the pair.").
            concat(newline).
            concat("Every line starting with '//' has no function and is a comment. Everything before the first genome is a comment "
                + "and leading whitespaces of a ").
            concat(newline).
            concat("genome name are ignored, too.").
            concat(newline).concat(newline).
            concat("As an input example consider the three genomes:").
            concat(newline).concat(newline), //regular
            " ", //table1 image
            newline.concat(newline).
            concat("Optimal DCJ(-indel), rDCJ, HP, Inversion and Translocation Distance and Sorting").
            concat(newline).concat(newline), //bold
            "The program can compute six different genomic distances and optimal sorting scenarios: The DCJ, rDCJ, HP "
                + "(Hannenhalli & Pevzner), Inversion".
            concat(newline).
            concat("and Translocation distance and an optimal corresponding sorting. " +
            		"Additionally the DCJ-indel model is offered (DCJ sorting with treating insertions").
    		concat(newline).
    		concat("and deletions). For all models, distance and sorting algorithms were implemented under the ").
            concat("aspect of efficiency and feature the most efficient ").
            concat(newline).
            concat("algorithms known until today. Links to the corresponding papers can be found at the end ").
            concat("of this section.").
            concat(newline).concat(newline), //regular
            "Output example:", //underlined
            newline.
            concat("The output consists of the three tabs 'Genomes', 'Adjacencies' and 'Graphics'. ").
            concat(newline).
            concat("The 'Genomes' tab either starts with informative messages, if a genome comparison fails or singleton genes have to be removed from "). 
            concat(newline).
            concat("a genome, or directly starts with the distance results as a distance matrix, if the input consists of more than two genomes. ").
            concat(newline). 
            concat("Otherwise the distances are returned in their context:").
            concat(newline).concat(newline), //regular
            " ", //result1 image
            newline.concat(newline).
            concat("or if only Genome B and Genome C are compared with the HP model:").
            concat(newline).concat(newline), //regular
            " ", //result2 image 
            newline.concat(newline).
            concat("A '-' in the distance results means that the distances for this pair could not be calculated. "
                + "Certain rules for the different distance models, ").
            concat(newline).
            concat("explained below under 'Rules', have to be obeyed. ").
            concat(newline).concat(newline).
            concat("Next, the distance results are also returned as a matrix in PHYLIP format for further analysis. In this matrix the fixed value 10000").concat(newline).
            concat("represents pairwise comparisons which could not be calculated.").
            concat(newline).
            concat("Below the matrices the single steps of each genome comparison are listed as plain text. ").
            concat(newline).
            concat("Next to the comparison the estimated (\"roughly\" or \"at least\") number of rearrangement scenarios [5] is shown.").
            concat(newline).concat(newline).
            concat("The 'Adjacencies' tab contains the adjacencies of each single step of each genome comparison (-> 'Program Usage - Step by Step').").
            concat(newline).concat(newline).
            concat("The 'Graphics' tab shows the graphical output explained below. We recommend to check this tab for closer analysis.").
            concat(newline).
            concat("The graphical output looks like this for each pairwise comparison:").
            concat(newline).concat(newline), // regular
            " ", //result3 image
            newline.concat(newline).concat("The fragments emerging from the two cuts (red vertical ticks in the chromosome) of each step are highlighted by colored lines, ").concat(
            "each with a " + newline + "different color. The lines for the current step are located below the chromosome, while the lines depicting the fragments of the last step are " + newline).concat(
            "located above the chromosome of the current step.").
            concat(newline).concat("If a deletion occurs, the gene(s) removed will not be shown in the follow-up step (for example gene \"a\" in step 2-3).").
            concat(newline).
            concat("Likewise, an insertion has only the vertical bar and in the follow-up step shows the additional genes underlined (for example gene \"x\" in steps 3-4).").
            concat(newline).concat(newline).
            concat("Detailed explanations of the DCJ model and how it can be used for ").
            concat("calculating the rDCJ, HP, inversion and translocation distances are given in " + newline + "the following papers:").
            concat(newline).concat(newline).
            concat("o  The DCJ model is explicated in [13] and improved and simplified in [3].").
            concat(newline).
            concat("o  The restricted DCJ model is introduced in [9]").
            concat(newline).
            concat("o  How DCJ serves as a basis for HP, inversion and translocation distances is explained in [4], while [6] provides the correct distance " + newline + space + "formula. The running time of all ").concat(
            "distance computations is thus linear.").
            concat(newline).
            concat("o  The DCJ-indel concept is introduced in [YAN-ATT-....]").
            concat(newline).
            concat("o  The DCJ-indel model ....[BRA-WIL-STO-....]").
            concat(newline).concat(newline).
            concat("The other models and the sorting algorithms are described here:").concat(
            newline).concat(newline).concat("o  The HP model was first introduced in [7] and further studied in [9]. The sorting algorithm uses the 'trial-and-error' approach." + newline + space + "It computes the distance and searches for the next optimal (distance reducing) operation to apply.")
                    .concat(newline).concat(
            "o  The inversion model was first solved in polynomial time in [8] and the most efficient, subquadratic sorting algorithm is described in [11]. " + newline + space + "Here, we implement the second most efficient quadratic time algorithm, also described in [10], with the aid of the data structures from [1].").concat(
            newline).concat("o  The translocation model and the implemented cubic sorting algorithm are described in [2]. This algorithm was chosen because in " + newline + space + "practice its running time is almost always linear.").concat(
            newline).concat(newline), //regular
            "Rules".concat(newline).concat(newline), //bold
            "For a successful genome comparison the following rules have to be obeyed:".
            concat(newline).
            concat("o  The DCJ distance calculation can handle all kinds of genomes without restrictions (but will ignore unequal genomic content).").
            concat(newline).
            concat("o  The DCJ-indel model allows -additionally to the DCJ conventions- for extra genes in both genomes.").
            concat(newline).
            concat("o  The restricted DCJ model works for linear chromosomes and directly reincoporates all emerging ").
            concat(newline).concat(space).concat("circular chromosomes in the next step (modelling block interchanges and transpositions).").
            concat(newline).
            concat("o  The HP distance calculation requires linear chromosomes.").concat(newline).concat("o  The translocation distance requires linear chromosomes with the same telomeres. ").
            concat(newline).
            concat("o  The inversion distance requires one linear chromosome with the same telomeres in both genomes.").
            concat(newline).
            concat("o  Sampling is only implemented for the DCJ model (withouth indels) and ignores the recombinations of AA- and BB-paths in the adjacency graph.").            
            concat(newline).
            concat(space).concat("Thus sampling is uniform only for all other instances of adjacency graphs.").            
            concat(newline).
            concat(newline), // regular
            "Program Usage - Step by Step".concat(newline).concat(newline), //bold
            "o  First click on ", //regular
            " ", //load icon
            " and choose a file that contains at least two genomes. Or alternatively click ", //regular
            " ", //example icon
            " to work with a " + newline + space + "small example file.".concat(newline).concat("o  Then choose the genomes to compare in the lists below and select one of the six possible scenarios. " + newline + space), //regular
            " ", // scenario icon
            newline.concat(space + "The 'All' scenario means that all four scenarios are calculated at once, if applicable.").concat(newline).concat(newline).concat("o  By pressing the "), //regular
            " ", // run button
            " button the genomes will be compared. The results are shown in the tabs in the lower part of the window.".concat(newline).concat("    *  The first tab "), // regular 
            " ", // genomes tab
            " first shows the distance in a matrix for more than two genomes or otherwise in context with the chosen " + newline + space2 + "scenario and afterwards the stepwise transformation for all pairs of genomes.".concat(
            newline).concat("    *  The second tab "),
            " ", // adj tab
            " shows the adjacencies (junctions) between the genes. The ending '_h' represents the head of a " + newline + space2 + "gene and '_t' the tail of a gene. Chromosomes are separated by '{content1}  ,  {content2}' ".concat(
            "including the whitespaces for an easier " + newline + space2 + "localization of a chromosome end.").concat(newline).concat("    *  The third tab "), //regular
            " ", // graphic tab
            " contains a graphical representation of the transformation of all pairs of genomes with highlighted " + newline + space2 + "operations.".concat(newline).concat(
            "o  Before running the comparison:").concat(newline).
            	concat("    *  You can decide if only the distances are of interest and the sorting scenarios are not needed (makes the calculation also much faster). " + newline).
            	concat(space2 + "In this case deselect the \"show steps\" checkbox: "), //regular 
            	" ", // show steps image
            	", otherwise keep it selected.".concat(newline).
            	concat("    *  You can choose a coloring method for the graphic:").concat(newline + space2).
            		concat("If you select "), //regular
            		" ", //colors image
            		" each chromosome has a distinct color, otherwise they have a white background.".concat(newline).concat(
            "o  You can also adjust the size of the ").concat("output using the size slider with three different sizes."), // regular
        		" ", //slider image
            	newline.concat(
            "o  A graphic result can be saved as jpg image by clicking on "), //regular
            	" ", //save image
            	newline.concat(
    		"o  The whole textual results can be saved in a txt file by clicking on "), //regular
            	" ", // save2 image
            	newline.concat(
			"o  Sampling for the DCJ model can be selected by checking the checkbox. If it is unchecked, standard DCJ sorting is done.").concat(newline).concat(space), //regular
				" ", // help-sampling image
				newline.concat(space+"For other models uniform sampling is not implemented, and thus not possible to select.")+
				newline.concat("    *  If a pair of AA- and BB-paths are present in the adjacency graph, recombinations are possible. However, these are ignored for sampling and you are notified.").concat(newline).concat(space3), //regular
				" ", // recombination image
				newline.concat(
            "o  All values and results can be cleared by the "), // regular
            	" ", // clear image
            	"button.".concat(newline).concat(
    		"o  The "), //regular
            	" ", // exit image
            	" button terminates the program.".concat(newline).concat(
            "o  If you want to look at the help directly from the program, click on "),
            " ", // help image
            newline.concat(newline).concat("References"), //bold
            newline.concat(newline).concat(
            " 1.   A. Bergeron, J. Mixtacki, and J. Stoye," + newline + space3 + "\"The inversion distance problem.\", " + newline + space3 + "Mathematics of Evolution and Phylogeny, O. Gascuel ed., Oxford University Press, 2005, pp. 262–290."
            		).concat(newline).concat(
            " 2.   A. Bergeron, J. Mixtacki, and J. Stoye," + newline + space3 + " \"On sorting by translocations.\", " + newline + space3 + "Journal of Computational Biology,  vol. 13(2), 2006, pp. 567–578.)"
            		).concat(newline).concat(
            " 3.   A. Bergeron, J. Mixtacki, and J. Stoye," + newline + space3 + " \"A Unifying View of Genome Rearrangements.\", " + newline + space3 + "Algorithms in Bioinformatics, Lecture Notes in Computer Science 4175, Springer Berlin / Heidelberg, 2006, pp. 163-173."
    				).concat(newline).concat(
            " 4.   A. Bergeron, J. Mixtacki, and J. Stoye," + newline + space3 + " \"HP distance via double cut and join distance.\", " + newline + space3 + "In Proceedings of the 14th Annual Symposium on Combinatorial Pattern Matching (CPM 2008), Springer Verlag, pp. 56–68."
					).concat(newline).concat(
            " 5.   M. D. V. Braga and J. Stoye," + newline + space3 + " \"The solution space of sorting by DCJ.\", " + newline + space3 + "Journal of Computational Biology, vol. 17(9), 2010, pp. 1145–1165."
    				).concat(newline).concat(
            " 6.   P.L. Erdös, L. Soukupa, and J. Stoye," + newline + space3 + " \"Balanced Vertices in Trees and a Simpler Algorithm to Compute the Genomic Distance.\", " + newline + space3 + "submitted, 2010."
					).concat(newline).concat(
            " 7.   S. Hannenhalli, and P.A. Pevzner," + newline + space3 + " \"Transforming men into mice (polynomial algorithm for genomic distance problem).\", " + newline + space3 + "In Proceedings of the 36th IEEE Symposium on Foundations of Computer Science (FOCS 1995), IEEE Computer Society Press, pp. 581–592."
					).concat(newline).concat(
            " 8.   S. Hannenhalli, and P.A. Pevzner," + newline + space3 + " \"Transforming cabbage into turnip: Polynomial algorithm for sorting signed permutations by reversals.\", " + newline + space3 + "Journal of the ACM,  vol. 46(1), 1999, pp. 1–27."
            		).concat(newline).concat(
            " 9.   Kováč, J. et al.," + newline + space3 + " \"Restricted DCJ model: rearrangement problems with chromosome reincorporation.\", " + newline + space3 + "Journal of Computational Biology, vol. 18(9), 2011, pp. 1231–1241."
            		).concat(newline).concat(
            " 10. P.A. Pevzner, and G. Tesler," + newline + space3 + " \"Transforming men into mice: the Nadeau-Taylor chromosomal breakage model revisited.\", " + newline + space3 + "In Proceedings of the Seventh Annual Conference on Computational Molecular Biology (RECOMB 2003), ACM Press, pp. 247–256."
    				).concat(newline).concat(
            " 11. E. Tannier, A. Bergeron, and M.-F. Sagot," + newline + space3 + " \"Advances in sorting by reversals.\", " + newline + space3 + "Discrete Applied Mathematics,  vol. 155(6-7), 2007, pp. 881–888."
            		).concat(newline).concat(
            " 13. S. Yancopoulos, O. Attie, and R. Friedberg," + newline + space3 + " \"Efficient sorting of genomic permutations by translocation, inversion and block interchange.\", " + newline + space3 + "Bioinformatics,  vol. 21(16), 2005, pp. 3340–3346."
					).concat(newline)
        };

		String[] initStyles = { "bold", "regular", "bold", "regular", "icon1",
				"bold", "regular", "underlined", "regular", "icon2", "regular",
				"icon3", "regular", "icon4", "regular", "bold", "regular",
				"bold", "regular", "load", "regular", "example", "regular",
				"scenario", "regular", "run", "regular", "genomes", "regular",
				"adj", "regular", "graphic", "regular", "steps", "regular",
				"colors", "regular", "slider", "regular", "save", "regular",
				"save2", "regular", "help-sampling", "regular",
				"recombination", "regular", "clear", "regular", "exit",
				"regular", "help", "bold", "regular" };

        this.setTitle("UniMoG Help Window");
        this.setSize(this.standardSize);
        this.setLayout(new BorderLayout());
        JTextPane helpPane = new JTextPane();
        helpPane.setEditable(false);
        helpPane.setEditorKit(new WrapEditorKit());
        JScrollPane helpScrollPane = new JScrollPane(helpPane);
        this.add(helpScrollPane);

        StyledDocument doc = helpPane.getStyledDocument();
        this.addStylesToDocument(doc);

        try {
            for (int i = 0; i < initString.length; i++) {
                doc.insertString(doc.getLength(), initString[i], doc.getStyle(initStyles[i]));
            }
        } catch (BadLocationException ble) {
            System.err.println("Couldn't insert initial text into text pane.");
        }

        helpPane.setCaretPosition(0); //to start at 0 position of the scrollpane!
    }

    /**
     * Adds the styles to the document.
     * @param doc the document
     */
    private void addStylesToDocument(final StyledDocument doc) {
        //Initialize some styles.
        Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

        Style regular = doc.addStyle("regular", def);
        StyleConstants.setFontFamily(def, "SansSerif");

        Style s = doc.addStyle("italic", regular);
        StyleConstants.setItalic(s, true);

        s = doc.addStyle("bold", regular);
        StyleConstants.setBold(s, true);

        s = doc.addStyle("underlined", regular);
        StyleConstants.setUnderline(s, true);

        s = doc.addStyle("small", regular);
        StyleConstants.setFontSize(s, 10);

        s = doc.addStyle("large", regular);
        StyleConstants.setFontSize(s, 16);

        s = this.getIcon(doc, regular, "icon1", 0);
        s = this.getIcon(doc, regular, "icon2", 1);
        s = this.getIcon(doc, regular, "icon3", 2);
        s = this.getIcon(doc, regular, "icon4", 3);
        s = this.getIcon(doc, regular, "load", 4);
        s = this.getIcon(doc, regular, "example", 5);
        s = this.getIcon(doc, regular, "scenario", 6);
        s = this.getIcon(doc, regular, "run", 7);
        s = this.getIcon(doc, regular, "genomes", 8);
        s = this.getIcon(doc, regular, "adj", 9);
        s = this.getIcon(doc, regular, "graphic", 10);
        s = this.getIcon(doc, regular, "steps", 11);
        s = this.getIcon(doc, regular, "colors", 12);
        s = this.getIcon(doc, regular, "slider", 13);
        s = this.getIcon(doc, regular, "save", 14);
        s = this.getIcon(doc, regular, "save2", 15);
        s = this.getIcon(doc, regular, "clear", 16);
        s = this.getIcon(doc, regular, "exit", 17);
        s = this.getIcon(doc, regular, "help", 18);
        s = this.getIcon(doc, regular, "help-sampling", 19);
        s = this.getIcon(doc, regular, "recombination", 20);

    }

    /**
     * Returns the wanted icon.
     * @param doc doucment to add the icon
     * @param regular style which should be updated
     * @param styleName name of the new stlye
     * @param iconIndex the index of the icon in the array "iconPaths"
     * @return the new stlye
     */
    private Style getIcon(StyledDocument doc, final Style regular, final String styleName, final int iconIndex) {
        Style s = doc.addStyle(styleName, regular);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        ImageIcon tableIcon = this.createImageIcon(this.iconPaths[iconIndex]);
        if (tableIcon != null) {
            StyleConstants.setIcon(s, tableIcon);
        }
        return s;
    }

    /**
     * Returns an ImageIcon, or null if the path was invalid. 
     * @param path the path of the icon
     * @return the icon or null if the path was invalid
     */
    private ImageIcon createImageIcon(final String path) {
        URL imgURL = this.getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}
