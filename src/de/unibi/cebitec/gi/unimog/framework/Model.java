package de.unibi.cebitec.gi.unimog.framework;

import de.unibi.cebitec.gi.unimog.utils.Constants;

/**
 * Provides all types of Scenarios that can be selected as well as a String to
 * be shown instead in the GUI.
 * 
 * @author eyla
 */
public enum Model {

	/** DCJ distance model. */
	DCJ(Constants.DCJ_ST, Constants.DCJ_ST),

	/** Restricted DCJ (minimal use of circular intermediates. */
	DCJ_RESTRICTED(Constants.RDCJ_ST, "rDCJ"),

	/** HP distance model. */
	HP(Constants.HP_ST, Constants.HP_ST),

	/** Inversion distance model. */
	INVERSION(Constants.INV_ST, "Inv"),

	/** Translocation distance model. */
	TRANSLOCATION(Constants.TRANS_ST, "Transloc."),

	/** DCJ with insertions and deletions. */
	DCJ_INDEL(Constants.DCJ_INDEL_ST, "DCJ-id"),

	/** All currently available models should be chosen. */
	ALL("All", "All"),

	/** No model selected */
	NONE("Unknown", "Unkown");

	/** Number of implemented models */
	public static int NB_MODELS = 6;

	private final String guiName;

	private final String abbrevGuiName;

	/**
	 * Creates new type of Model including a gui string.
	 * 
	 * @param guiName
	 *            String to be shown in GUI.
	 */
	private Model(final String guiName, final String abbrevGuiName) {
		this.guiName = guiName;
		this.abbrevGuiName = abbrevGuiName;
	}

	/**
	 * Returns Name of Annotation Type for GUI.
	 * 
	 * @return Annotation String for GUI
	 */
	public String getGuiName() {
		return this.guiName;
	}

	/**
	 * Returns the abbreviated version of the gui name, suitable for checkboxes
	 * and short labels.
	 * 
	 * @return Abbreviated annotation String for GUI
	 */
	public String getAbbrevGuiName() {
		return this.abbrevGuiName;
	}

	/**
	 * Returns the selected {@link Model} depending on the id given by the user.
	 * 
	 * @param id
	 *            from console input
	 * @return {@link Model} according to currently selected id
	 */
	public static Model getModel(final int id) {
		switch (id) {
		case 0:
			return Model.ALL;
		case 1:
			return Model.DCJ;
		case 2:
			return Model.DCJ_RESTRICTED;
		case 3:
			return Model.HP;
		case 4:
			return Model.INVERSION;
		case 5:
			return Model.TRANSLOCATION;
		case 6:
			return Model.DCJ_INDEL;
		case 7:
			return Model.ALL;
		default:
			return Model.NONE;
		}
	}
}