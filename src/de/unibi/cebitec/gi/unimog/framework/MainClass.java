package de.unibi.cebitec.gi.unimog.framework;

import de.luschny.math.arithmetic.Xint;
import de.luschny.math.factorial.FactorialParallelPrimeSwing;
import de.unibi.cebitec.gi.unimog.algorithms.*;
import de.unibi.cebitec.gi.unimog.datastructure.AdditionalDataHPDistance;
import de.unibi.cebitec.gi.unimog.datastructure.AdjacencyGraph;
import de.unibi.cebitec.gi.unimog.datastructure.Data;
import de.unibi.cebitec.gi.unimog.datastructure.DataFramework;
import de.unibi.cebitec.gi.unimog.datastructure.DataOutput;
import de.unibi.cebitec.gi.unimog.datastructure.DcjInDelAdditionalData;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;
import de.unibi.cebitec.gi.unimog.datastructure.IAdditionalData;
import de.unibi.cebitec.gi.unimog.datastructure.LabeledAdjacencyGraph;
import de.unibi.cebitec.gi.unimog.datastructure.OperationList;
import de.unibi.cebitec.gi.unimog.datastructure.OperationListInDel;
import de.unibi.cebitec.gi.unimog.datastructure.Pair;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.AdjacencyGraphSampling;
import de.unibi.cebitec.gi.unimog.exceptions.InputOutputException;
import de.unibi.cebitec.gi.unimog.utils.Constants;
import de.unibi.cebitec.gi.unimog.utils.Toolz;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.print.DocFlavor;
import javax.swing.JOptionPane;

import ml.options.Options;
import ml.options.Options.Multiplicity;
import ml.options.Options.Separator;

/***************************************************************************
 @formatter:off
  *   Copyright (C) 2010 by Rolf Hilker                                     *
  *   rhilker   a t  cebitec.uni-bielefeld.de                               *
  *                                                                         *
  *   This program is free software; you can redistribute it and/or modify  *
  *   it under the terms of the GNU General Public License as published by  *
  *   the Free Software Foundation; either version 2 of the License, or     *
  *   (at your option) any later version.                                   *
  *                                                                         *
  *   This program is distributed in the hope that it will be useful,       *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
  *   GNU General Public License for more details.                          *
  *                                                                         *
  *   You should have received a copy of the GNU General Public License     *
  *   along with this program; if not, write to the                         *
  *   Free Software Foundation, Inc.,                                       *
  *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @author -Rolf Hilker-
 * Main class for starting the program either in gui or console modus.
 * 1 "D:\Programmieren & Studieren\NetbeansProjects\DCJ-UniMoG\tag\2011_may\examples\GenomeExample2.txt"
 * "D:\Programmieren & Studieren\NetbeansProjects\DCJ-UniMoG\tag\2011_may\examples\GenomeExample3.txt"
 * "D:\Programmieren & Studieren\NetbeansProjects\DCJ-UniMoG\tag\2011_may\examples\GenomeExample5.txt"
 * 1 = Standard DCJ model
 * 2 = restricted DCJ model (via DCJ)
 * 3 = HP Distance model via DCJ
 * 4 = Inversion Distance model via DCJ
 * 5 = Translocation Distance model via DCJ
 * 6 = DCJ-indel model (via DCJ)
 * 7 = all distance methods which are included in the framework TODO: change to 0/1 = ALL
 * 4
 * "D:\Programmieren & Studieren\NetbeansProjects\DCJ-UniMoG\tag\2011_may\examples\GenomeExample3.txt"
 * @formatter:on
 */
public class MainClass {

    public static final String ENTER_GENOMES_FST = "You have to enter at least 2 genomes using the correct format before you can compare them!\n";
    public static final String SCENARIO_ERROR = "The selected model is not valid, try again!";
    private Data data;
    private Model model;
    private String filepath;
    private IAdditionalData additionalData;
    private MainFrame mainFrame;
    private boolean guiMode = false;
    private DataFramework globalData;
    private DataOutput[] outputData;
    private boolean showSteps = true;
    private boolean plainScenarioOutput = true;
    private boolean sample = true;
    private ArrayList<Integer> genomeIndices = new ArrayList<Integer>();
    private List<String> notifications = new ArrayList<String>();
    private boolean useOtherScenario = false;
    private boolean interrupted = false;
    private int nbComparisons;
    private int currentComparison;

    /**
     * Standard constructor for gui modus.
     */
    public MainClass() {
        this.data = new Data();
    }

    /**
     * Standard constructor for console modus.
     *
     * @param model     Determining the model to use
     * @param pathnames Path for the file containing the genomes
     * @param sample    <code>true</code> if uniform sampling is turned on, <code>false</code> otherwise
     */
    public MainClass(final Model model, final String[] pathnames, final boolean sample) {

        // this.distances[0] = Constants.ERROR_NUM; //checks if at least one distance could be calculated or if error
        // occurred
        try {
            this.setSampling(sample);
            this.model = model;
            System.out.println("Given files: " + Arrays.toString(pathnames));
            this.execute(model, pathnames, null);
        } catch (final InputOutputException exception) {
            System.out.println(exception.getMessage());
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        final String[] printableResults = OutputPrinter.printResults(this.outputData, this.globalData.getGenomeIDs(), this.model);
        System.out.println(printableResults[0].concat(Constants.LINE_BREAK_OUTPUT));
        System.out.println(printableResults[3].concat(Constants.LINE_BREAK_OUTPUT));
        System.out.println(Constants.LINE_BREAK_OUTPUT.concat("Steps of each genome comparison:").concat(Constants.LINE_BREAK_OUTPUT));
        System.out.println(printableResults[1]);
        System.out.println(Constants.LINE_BREAK_OUTPUT.concat("Adjacencies of each genome comparison after each step:").concat(
                Constants.LINE_BREAK_OUTPUT));
        System.out.println(printableResults[2]);
        System.out.println("");
        // print error messages at the end, to focus them
        for (String msg : this.notifications) {
            System.out.println(msg);
        }
        this.notifications.clear();
    }

    /**
     * Constructor for extended console modus (now allows same options as GUI)
     *
     * @param model               Determining the model to use
     * @param pathnames           Path for the file containing the genomes
     * @param sample              <code>true</code> if uniform sampling is turned on, <code>false</code> otherwise
     * @param onlyDistance        <code> true</code> if only the distance should be displayed, <code>false</code> otherwise
     * @param plainScenarioOutput <code> true</code> if a simplified version of the output should be printed, <code>false</code> otherwise. <br>
     *                            <br>
     *                            Note that <code>plainScenarioOutput</code> as well as <code>sampling</code> are only allowed to be <code>true</code> if model
     *                            printing is turned on (i.e. when onlyDistance is <code>false</code>.)
     */
    public MainClass(final Model model, final String[] pathnames, final boolean sample, final boolean onlyDistance,
                     final boolean plainScenarioOutput) {

        // this.distances[0] = Constants.ERROR_NUM; //checks if at least one distance could be calculated or if error
        // occurred
        this.setScenario(model);
        this.setShowSteps(!onlyDistance);
        this.setPlainScenarioOutput(plainScenarioOutput);
        this.setSampling(sample);

        if (!this.isPlainScenarioOutput()) {
            try {
                System.out.println("Given files: " + Arrays.toString(pathnames));
                this.execute(model, pathnames, null);
            } catch (final InputOutputException exception) {
                System.out.println(exception.getMessage());
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
            final String[] printableResults = OutputPrinter.printResults(this.outputData, this.globalData.getGenomeIDs(), this.model);
            System.out.println(printableResults[0].concat(Constants.LINE_BREAK_OUTPUT));
            System.out.println(printableResults[3].concat(Constants.LINE_BREAK_OUTPUT));
            System.out.println(Constants.LINE_BREAK_OUTPUT.concat("Steps of each genome comparison:").concat(Constants.LINE_BREAK_OUTPUT));
            System.out.println(printableResults[1]);
            System.out.println(Constants.LINE_BREAK_OUTPUT.concat("Adjacencies of each genome comparison after each step:").concat(
                    Constants.LINE_BREAK_OUTPUT));
            System.out.println(printableResults[2]);
            System.out.println("");

            // print error messages at the end, to focus them
            for (String msg : this.notifications) {
                System.out.println(msg);
            }

        } else { // choose simple model output, no path names, tables, distances, notifications, only scenarios
            try {
                this.execute(model, pathnames, null);
            } catch (final InputOutputException exception) {
                System.out.println(exception.getMessage());
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
            final String[] printableResults = OutputPrinter.printScenario(this.outputData, this.globalData.getGenomeIDs(), this.model);
            // final String[] printableResults = OutputPrinter.printResults....
            System.out.println(printableResults[1]);
        }

        this.notifications.clear();
    }

    /**
     * Main method initializes the program.
     *
     * @param args 2 Options: Pass no arguments to start in gui mode Pass the number of the model you want to use +
     *             the path of the file containing the genomes.
     */
    public static void main(String[] args) {
        boolean startGui;
        MainClass mc;

        if (args.length > 0) {
            // Options opt = new Options(args, 0, 4);
            Options opt = new Options(args, 0, Constants.MAX_INPUT_FILE_NBR);
            opt.getSet().addOption("m", Separator.EQUALS, Multiplicity.ZERO_OR_ONE);
            opt.getSet().addOption("s", Multiplicity.ZERO_OR_ONE); // use sampling
            opt.getSet().addOption("d", Multiplicity.ZERO_OR_ONE); // only distance
            opt.getSet().addOption("p", Multiplicity.ZERO_OR_ONE); // plainScenario
            opt.getSet().addOption("h", Multiplicity.ZERO_OR_ONE);
            opt.getSet().addOption("z", Multiplicity.ZERO_OR_ONE); //zip file
            opt.getSet().addOption("-h", Multiplicity.ZERO_OR_ONE);
            opt.getSet().addOption("help", Multiplicity.ZERO_OR_ONE);
            opt.getSet().addOption("-help", Multiplicity.ZERO_OR_ONE);

            if (!opt.check(false, false)) {
                // Print usage hints
                System.out.println(Constants.CONSOLE_OPT);
                System.exit(1);
            }

            startGui = false;
            boolean plainScenarioOutput = false;
            boolean sample = false;
            // processing of options
            if (opt.getSet().isSet("h") || opt.getSet().isSet("help") || opt.getSet().isSet("-h") || opt.getSet().isSet("-help")) {
                System.out.println(Constants.CONSOLE_OPT);
            } else {
                // if no model is given, start gui
                if (!opt.getSet().isSet("m")) {
                    startGui = true;
                } else {
                    int modelID = Integer.decode(opt.getSet().getOption("m").getResultValue(0));

                    // get model chosen by user
                    Model model = Model.NONE;
                    try {
                        model = Model.getModel(modelID);
                    } catch (NumberFormatException e) {
                        System.out.println("The model should be an integer!");
                        System.exit(1);
                    }
                    if (model == Model.NONE) {
                        System.out.println("You entered a model number that is not existent!");
                        System.out.println(Constants.CONSOLE_OPT);
                        System.exit(1);
                    } else if (model == Model.DCJ) {
                        // check for sampling
                        sample = opt.getSet().isSet("s");
                    }

                    // d selected = only distance, no model steps
                    boolean onlyDistance = opt.getSet().isSet("d");
                    if (!onlyDistance) { // if model steps are shown
                        plainScenarioOutput = opt.getSet().isSet("p");
                    }

                    int numberOfFiles = opt.getSet().getData().size();
                    if (numberOfFiles < 1) {
                        System.out.println("No files given.");
                        startGui = true;
                    } else {
                        try {
                            String[] pathnames;
                            String ZIP_EXTENSION = "zip";

                            //z selected = input is one zipped file
                            if (opt.getSet().isSet("z")) {
                                if (opt.getSet().getData().size() > 1) {
                                    throw new IOException("Only one zip file allowed.");
                                }
                                String zipFilePath = opt.getSet().getData().get(0);

                                if (!zipFilePath.contains(ZIP_EXTENSION)) {
                                    throw new IOException("Expected zip file, but got: ".concat(zipFilePath));
                                }
                                pathnames = new String[]{zipFilePath};

                            } else {
                                pathnames = new String[numberOfFiles];
                                for (int i = 0; i < numberOfFiles; ++i) {
                                    String pathname = opt.getSet().getData().get(i);
                                    if (pathname.contains(ZIP_EXTENSION)) {
                                        throw new IOException("Use -z option for zip file: ".concat(pathname));
                                    }
                                    pathnames[i] = pathname;
                                }
                            }

                            // mc = new MainClass(model, pathnames, sample); // DEPRECATED old constructor
                            mc = new MainClass(model, pathnames, sample, onlyDistance, plainScenarioOutput);

                        } catch (IOException ioe) {
                            if (ioe.getMessage() == null) {
                                String zipFileName = opt.getSet().getData().get(0);
                                System.err.println("Could not read zip file: ".concat(zipFileName));
                            } else {
                                System.err.println(ioe.getMessage());
                            }
                            System.exit(1);
                        }
                    }
                }

            }
        } else {
            startGui = true;
        }
        if (startGui) {
            System.out.println("Starting Gui...");
            mc = new MainClass();
            mc.guiMode = true;
            final MainFrame mf = new MainFrame(mc);
            mc.mainFrame = mf;
            mf.setVisible(true);
            // mf.setBlockOnOpen(true);
            // mf.open();
        }
    }

    /**
     * Returns the main frame.
     *
     * @return The main frame
     */
    public MainFrame getMainFrame() {
        return this.mainFrame;
    }

    /**
     * Method for performing all calculations to receive the correct distances
     * according to the chosen model and the given file.
     *
     * @param model     The model value
     * @param pathnames The pathname of the genomes file, <code>null</code> for gui mode
     * @param input     For gui mode the input is handed over, <code>null</code> for console mode
     * @throws InputOutputException exception occurs if no valid input can be detected
     * @throws InterruptedException exception occurs if calculation was stopped by the user
     */
    public void execute(final Model model, final String[] pathnames, String input) throws InputOutputException, InterruptedException {

        this.globalData = null;
        this.outputData = null;
        this.additionalData = null;
        this.data = null;
        this.genomeIndices.clear();
        this.notifications.clear();

        if (this.showSteps) {
            this.notifications.add("Note that the given results depict only one of many possible rearrangement scenarios!"
                    .concat(Constants.LINE_BREAK_OUTPUT));
        }
        boolean validInput = false;
        final GenomeParser parser = new GenomeParser();
        if (pathnames != null) {
            try {
                this.globalData = parser.parseInput(pathnames);
                validInput = true;
            } catch (final IOException | InputOutputException e) {
                String msg = e.getMessage();
                boolean isCompleteErrorMsg = msg.contains(" ");
                if (!isCompleteErrorMsg) {
                    msg = msg.concat(" does not exist.");
                }
                this.handleIOException(msg);
            }
        } else if (input != null) {
            this.globalData = parser.readGenomes(input);
            validInput = true;
        }

        if (validInput) {
            this.data = new Data();
            final ArrayList<Genome> genomes = this.globalData.getGenomes();
            final int size = genomes.size();
            this.nbComparisons = Toolz.gaussianFormula(size - 1);
            this.outputData = new DataOutput[this.nbComparisons];
            if (this.mainFrame != null) {
                this.mainFrame.setNumberComparisons(this.nbComparisons);
            }

            // call distance methods directly
            if (size < 2) {
                if (!this.guiMode) {
                    System.err.println(MainClass.ENTER_GENOMES_FST);
                    System.exit(1);
                } else {
                    throw (new InputOutputException(MainClass.ENTER_GENOMES_FST));
                }
            } else { // Distance and sorting calculations for each comparison
                int counter = 0;
                this.currentComparison = 1;
                for (int i = 0; i < size; ++i) {
                    for (int j = i + 1; j < size; ++j) {
                        // Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Comparing genomes {0}, {1}",
                        // new Object[]{globalData.getGenomeIDs().get(i+1), globalData.getGenomeIDs().get(j+1)});
                        // preprocess a pair of genomes and put it into data
                        final Pair<Genome, Genome> genomePair = this.globalData.preprocessGenomePair(i, j);

                        if (model != Model.DCJ_INDEL) {
                            // add ignored gene names to output
                            this.addIgnoredGenesToNotifications(i, j, genomePair.getFirst().getNumberOfChromosomes(), model);
                        }

                        this.data.setGenomeA(genomePair.getFirst());
                        this.data.setGenomeB(genomePair.getSecond());
                        // decides whether to sample uniformly among all sorting scenarios or just pick the first one
                        AdjacencyGraph adjGraph;
                        if (this.isSampling()) {
                            adjGraph = new AdjacencyGraphSampling(this.data.getGenomeA(), this.data.getGenomeB());
                        } else {
                            adjGraph = new AdjacencyGraph(this.data.getGenomeA(), this.data.getGenomeB());
                        }
                        this.data.setAdjGraph(adjGraph);

                        // ################# distance calculation ####################
                        if (!this.interrupted) {
                            this.outputData[counter] = this.runDistCalculation(model, i, j);
                        } else {
                            this.notifications.clear();
                            this.outputData = new DataOutput[this.nbComparisons];
                            return;
                        }

                        // ################# model calculation ####################
                        if (this.showSteps && this.data.getGenomeA().getNumberOfGenes() > 0) {
                            if (!this.interrupted) {
                                final IntermediateGenomesGenerator[] intermedList = this.sort(model, i, j);
                                // TODO set equal (Xint vs. BigInteger)
                                Xint lowerBound = adjGraph.getLowerBoundScenarios();
                                BigInteger lowerBoundSeq = this.calcLowerBound();
                                this.outputData[counter].setIntermedGenomes(intermedList);
                                this.outputData[counter].setLowerBound(lowerBoundSeq);
                            } else {
                                this.notifications.clear();
                                this.outputData = new DataOutput[this.nbComparisons];
                                return;
                            }
                        }
                        ++counter;
                        ++this.currentComparison;

                        if (this.mainFrame != null) {
                            this.mainFrame.setCurrentComparison(this.currentComparison);
                        }
                        System.gc();
                    }
                }
            }
        } else {
            throw new InputOutputException(MainClass.ENTER_GENOMES_FST);
        }
    }

    /**
     * Adds a string containing all ignored gene names of the current genome comparison
     * seperated by ", " to the list of notifications.
     *
     * @param i index of fst genome of current comparison
     * @param j index of scnd genome of current comparison
     */
    private void addIgnoredGenesToNotifications(int i, int j, int numChromsGenome1, Model model) {
        String ignoredGenes = "";
        for (String ignoredGene : this.globalData.getSuspendedGenes()) {
            ignoredGenes = ignoredGenes.concat(ignoredGene).concat(", ");
        }
        if (!ignoredGenes.isEmpty()) {
            if (model == Model.ALL) {
                if (numChromsGenome1 > 0) {
                    ignoredGenes = ignoredGenes.substring(0, ignoredGenes.length() - 2);
                    this.notifications.add("- Genomes \"" + this.globalData.getGenomeIDs().get(i + 1) + "\" & \""
                            + this.globalData.getGenomeIDs().get(j + 1) + "\": The following singleton genes may be ignored: " + ignoredGenes + ".");
                } else {
                    this.notifications.add("- Genomes \"" + this.globalData.getGenomeIDs().get(i + 1) + "\" & \""
                            + this.globalData.getGenomeIDs().get(j + 1)
                            + "\": When comparison not possible, since no gene is contained in both genomes. PHYLIP distance set to 10000.");
                }
            } else {
                if (numChromsGenome1 > 0) {
                    ignoredGenes = ignoredGenes.substring(0, ignoredGenes.length() - 2);
                    this.notifications.add("- Genomes \"" + this.globalData.getGenomeIDs().get(i + 1) + "\" & \""
                            + this.globalData.getGenomeIDs().get(j + 1) + "\": The following singleton genes were ignored: " + ignoredGenes + ".");
                } else {
                    this.notifications.add("- Genomes \"" + this.globalData.getGenomeIDs().get(i + 1) + "\" & \""
                            + this.globalData.getGenomeIDs().get(j + 1)
                            + "\": Comparison not possible, since no gene is contained in both genomes. PHYLIP distance set to 10000.");
                }
            }
        }
    }

    /**
     * Runs the calculations.
     *
     * @param model The model
     * @param i     index first genome
     * @param j     index second genome
     */
    private DataOutput runDistCalculation(final Model model, final int i, final int j) {

        int[] distances = new int[1];
        distances[0] = Constants.ERROR_NUM;

        // if there are common genes
        if (this.data.getGenomeA().getNumberOfGenes() > 0) {
            switch (model) {

                case DCJ: // ordinary DCJ Distance
                    distances[0] = this.calcDCJDistance();
                    break;

                case DCJ_RESTRICTED:  // Restricted Distance via DCJ
                    distances[0] = this.calcRDCJDistance(i, j);
                    break;

                case DCJ_INDEL: // DCJ with Insertions and Deletions
                    distances[0] = this.calcDCJInDelDistance(i, j);
                    break;

                case HP: // HP Distance via DCJ
                    distances[0] = this.calcHPDistance(i, j);
                    if (distances[0] == Constants.ERROR_NUM) { // TODO: what is this for?
                    }
                    break;

                case INVERSION: // Inversion Distance via DCJ
                    distances[0] = this.calcInvDistance(i, j);
                    break;

                case TRANSLOCATION: // Translocation Distance via DCJ
                    distances[0] = this.calcTransDistance(i, j);
                    break;

                case ALL: // Calculate all distances included in the framework
                    distances = new int[Model.NB_MODELS];
                    distances[0] = this.calcDCJDistance();
                    distances[1] = this.calcRDCJDistance(i, j);
                    distances[2] = this.calcHPDistance(i, j);
                    distances[3] = this.calcInvDistance(i, j);
                    distances[4] = this.calcTransDistance(i, j);
                    distances[5] = this.calcDCJInDelDistance(i, j);
                    break;
                default:
                    break;
            }

            // no common genes -> allow DCJ-indel
        } else if (model == Model.DCJ_INDEL) {
            distances[0] = this.calcDCJInDelDistance(i, j);

            // no common genes -> allow only DCJ-indel
        } else if (model == Model.ALL) {
            distances = new int[Model.NB_MODELS];
            distances[0] = Constants.ERROR_NUM;
            distances[1] = Constants.ERROR_NUM;
            distances[2] = Constants.ERROR_NUM;
            distances[3] = Constants.ERROR_NUM;
            distances[4] = Constants.ERROR_NUM;
            distances[5] = this.calcDCJInDelDistance(i, j);

            // no common genes and only models selected without unique genes
        } else {
            if (model != Model.NONE) {
                distances = new int[Model.NB_MODELS];
                distances[0] = Constants.ERROR_NUM;
                distances[1] = Constants.ERROR_NUM;
                distances[2] = Constants.ERROR_NUM;
                distances[3] = Constants.ERROR_NUM;
                distances[4] = Constants.ERROR_NUM;
                distances[5] = Constants.ERROR_NUM;
            }
        }
        return new DataOutput(i + 1, j + 1, distances);
    }

    /**
     * Runs the sorting algorithm corresponding to the given model.
     *
     * @param model The model to use here
     * @param i     index of first genome in parser
     * @param j     index of second genome in parser
     * @return Intermediate Genomes Generator which contains all data for output
     * @throws InputOutputException
     */
    public IntermediateGenomesGenerator[] sort(final Model model, final int i, final int j) throws InputOutputException {

        IntermediateGenomesGenerator[] intermediateGenomes = new IntermediateGenomesGenerator[1]; // XXX changed from NB_MODELS

        Genome genomeA = this.data.getGenomeA();
        Genome genomeB = this.data.getGenomeB();

        switch (model) {

            case DCJ: // ordinary DCJ sorting
                intermediateGenomes[0] = this.sortDCJ(i, j);
                break;

            case DCJ_RESTRICTED:            // Restricted DCJ sorting
                // check if genomes contain only linear chromosomes
                intermediateGenomes[0] = this.sortRDCJ(genomeA, genomeB, i, j);
                break;

            case DCJ_INDEL:
                intermediateGenomes[0] = this.sortDCJInDel(i, j);
                break;

            case HP: // HP sorting via DCJ
                // check if genomes contain only linear chromosomes
                intermediateGenomes[0] = this.sortHP(genomeA, genomeB, i, j);
                break;

            case INVERSION: // Inversion sorting via DCJ
                intermediateGenomes[0] = this.sortInv(genomeA, genomeB, i, j);
                break;

            case TRANSLOCATION: // Translocation sorting via DCJ
                intermediateGenomes[0] = this.sortTrans(genomeA, genomeB, i, j);
                break;

            case ALL: // Calculate all sorting sequences included in the framework
                intermediateGenomes = new IntermediateGenomesGenerator[Model.NB_MODELS];

                intermediateGenomes[0] = this.sortDCJ(i, j);                    // DCJ Sorting
                intermediateGenomes[1] = this.sortRDCJ(genomeA, genomeB, i, j); // restricted DCJ sorting
                intermediateGenomes[2] = this.sortHP(genomeA, genomeB, i, j);   // HP Sorting
                intermediateGenomes[3] = this.sortInv(genomeA, genomeB, i, j);  // Inversion Sorting
                intermediateGenomes[4] = this.sortTrans(genomeA, genomeB, i, j);// Translocation Sorting
                intermediateGenomes[5] = this.sortDCJInDel(i, j);                // TODO THOMAS DCJ INDEL?
                break;
            default:
                throw new InputOutputException(MainClass.SCENARIO_ERROR);
        }

        return intermediateGenomes;
    }

    /**
     * Calculates the DCJ Distance.
     *
     * @param data Data object containing the genomes.
     * @return DCJ distance
     */
    private int calcDCJDistance() {
        final DistanceDCJ calcDCJDist = new DistanceDCJ();
        int dcjDistance = calcDCJDist.calculateDistance(this.data, null);
        return dcjDistance;
    }

    /**
     * Calculates the restricted DCJ Distance
     *
     * @param data Data object containing the genomes.
     * @return restricted DCJ distance
     */
    private int calcRDCJDistance(int i, int j) {
        final boolean linear = Utilities.onlyLinear(this.data.getGenomeA()) && Utilities.onlyLinear(this.data.getGenomeB());
        int resDCJDistance = 0;
        if (linear) {
            final DistanceDCJ calcResDCJDist = new DistanceDCJ();
            resDCJDistance = calcResDCJDist.calculateDistance(this.data, null);
        } else {
            resDCJDistance = Constants.ERROR_NUM;
            this.notifications.add("- Genomes \"" + this.globalData.getGenomeIDs().get(i + 1) + "\" & \"" + this.globalData.getGenomeIDs().get(j + 1)
                    + "\": Restricted DCJ comparison failed: Remove circular chromosomes! PHYLIP distance set to 10000.");
        }
        return resDCJDistance;
    }

    // TODOX: JavaDoc
    private int calcDCJInDelDistance(int i, int j) {
        // TODOX: Distance calculation
        LabeledAdjacencyGraph g = new LabeledAdjacencyGraph(data.getGenomeA(), data.getGenomeB(), data.getAdjGraph(),
                this.globalData.getSuspendedGenes(), this.globalData.getBackMap(), this.globalData.getGenomes().get(i), this.globalData
                .getGenomesMap2().get(i), this.globalData.getGenomes().get(j), this.globalData.getGenomesMap2().get(j));
        final DistanceDCJInDel calcDCJidDist = new DistanceDCJInDel();
        int dist = calcDCJidDist.calculateDistance(data, g);
        return dist;
    }

    /**
     * Calculates the HP Distance.
     *
     * @param data Data object containing the genomes.
     * @return HP distance
     */
    private int calcHPDistance(int i, int j) {
        final boolean linear = Utilities.onlyLinear(this.data.getGenomeA()) && Utilities.onlyLinear(this.data.getGenomeB());
        int hpDistance = 0;
        if (linear) {
            this.additionalData = new AdditionalDataHPDistance(this.data.getGenomeA());
            final DistanceHP calcHPDist = new DistanceHP();
            try {
                hpDistance = calcHPDist.calculateDistance(this.data, this.additionalData);
            } catch (final ClassCastException e) {
                this.handleClassCastExcep();
            }
        } else {
            hpDistance = Constants.ERROR_NUM;
            this.notifications.add("- Genomes \"" + this.globalData.getGenomeIDs().get(i + 1) + "\" & \"" + this.globalData.getGenomeIDs().get(j + 1)
                    + "\": HP comparison failed: Remove circular chromosomes! PHYLIP distance set to 10000.");
        }
        return hpDistance;
    }

    /**
     * Calculates the Inversion Distance.
     *
     * @param data Data object containing the genomes.
     * @return Inversion distance
     */
    private int calcInvDistance(int i, int j) {
        // check if genome contains only 1 chromosome!
        Genome genomeA = this.data.getGenomeA().clone();
        Genome genomeB = this.data.getGenomeB().clone();
        Genome genomeC = null;
        final boolean linear = Utilities.onlyLinear(genomeA) && Utilities.onlyLinear(genomeB);
        final boolean cotailed = Utilities.checkCotailed(genomeA, genomeB);
        if (!cotailed) {
            int nbGenes = genomeA.getNumberOfGenes();
            genomeA = Utilities.genomeCappingAndNumberAdaption(genomeA, nbGenes + 1, nbGenes + 2);
            genomeB = Utilities.genomeCappingAndNumberAdaption(genomeB, nbGenes + 1, nbGenes + 2);
            genomeC = Utilities.genomeCappingAndNumberAdaptionRev(this.data.getGenomeA().clone(), nbGenes + 1, nbGenes + 2);
        }
        int invDistance = 0;
        final int sumChroms = genomeA.getNumberOfChromosomes() + genomeB.getNumberOfChromosomes();
        if (linear && sumChroms == 2) {
            this.additionalData = new AdditionalDataHPDistance(genomeA);
            final DistanceInv calcInvDist = new DistanceInv();
            try {
                AdjacencyGraph cappedAdjGraph = new AdjacencyGraph(genomeA, genomeB);
                Data cappedData = new Data(genomeA, genomeB, cappedAdjGraph);
                invDistance = calcInvDist.calculateDistance(cappedData, this.additionalData);

                if (!cotailed) {// in non cotailed case check second capping distance too
                    AdditionalDataHPDistance addData2 = new AdditionalDataHPDistance(genomeC);
                    AdjacencyGraph cappedAdjGraph2 = new AdjacencyGraph(genomeC, genomeB);
                    Data cappedData2 = new Data(genomeC, genomeB, cappedAdjGraph2);
                    int invDistance2 = calcInvDist.calculateDistance(cappedData2, addData2);

                    if (invDistance2 < invDistance) {
                        invDistance = invDistance2;
                        this.useOtherScenario = true;
                    }
                }
            } catch (ClassCastException e) {
                this.handleClassCastExcep();
            }
        } else {
            invDistance = Constants.ERROR_NUM;
            this.notifications.add("- Genomes \"" + this.globalData.getGenomeIDs().get(i + 1) + "\" & \"" + this.globalData.getGenomeIDs().get(j + 1)
                    + "\": Inversion comparison failed. Ensure that: both genomes have only "
                    + "one chromosome and it is not circular. PHYLIP distance set to 10000.");
        }
        return invDistance;
    }

    /**
     * Calculates the Translocation Distance.
     *
     * @param data Data object containing the genomes.
     * @return Translocation distance
     */
    private int calcTransDistance(int i, int j) {
        final boolean linear = Utilities.onlyLinear(this.data.getGenomeA()) && Utilities.onlyLinear(this.data.getGenomeB());
        final boolean cotailed = Utilities.checkCotailed(this.data.getGenomeA(), this.data.getGenomeB());
        final int nbChroms = this.data.getGenomeA().getNumberOfChromosomes();
        final boolean equalNbChroms = nbChroms == this.data.getGenomeB().getNumberOfChromosomes();
        int transDistance = 0;
        if (linear && cotailed && equalNbChroms && nbChroms > 1) {
            this.additionalData = new AdditionalDataHPDistance(this.data.getGenomeA());
            final DistanceTrans calcTransDist = new DistanceTrans();
            try {
                transDistance = calcTransDist.calculateDistance(this.data, this.additionalData);
            } catch (ClassCastException e) {
                this.handleClassCastExcep();
            }
        } else {
            transDistance = Constants.ERROR_NUM;
            this.notifications.add("- Genomes \"" + this.globalData.getGenomeIDs().get(i + 1) + "\" & \"" + this.globalData.getGenomeIDs().get(j + 1)
                    + "\": Translocation comparison failed. Ensure that: both genomes "
                    + "have an equal number of at least two cotailed chromosomes and they are not circular. PHYLIP distance set to 10000.");
        }
        return transDistance;
    }

    /**
     * Calculates a DCJ sorting model for the genomes with id i and j.
     *
     * @param i index of genome A
     * @param j index of genome B
     * @return the intermediate genomes of the calculated sorting model.
     */
    private IntermediateGenomesGenerator sortDCJ(int i, int j) {
        OperationList result;

        if (this.isSampling()) { // sample model uniformly
            final SortingDCJuniformly dcjRandomSorting = new SortingDCJuniformly(this.mainFrame);
            result = dcjRandomSorting.findOptSortSequence(this.data, null, this.globalData.getChromMaps().get(i));

        } else { // otherwise sort as usual
            final SortingDCJ dcjSorting = new SortingDCJ();
            result = dcjSorting.findOptSortSequence(this.data, null, this.globalData.getChromMaps().get(i));
        }

        return new IntermediateGenomesGenerator(this.data.getGenomeA(), this.globalData.getBackMap(), result, this.data.getAdjGraph()
                .getAdjacenciesGenome1(), this.globalData.getGenomeIDs().get(i + 1), this.globalData.getGenomeIDs().get(j + 1));
    }

    private IntermediateGenomesGenerator sortDCJInDel(int i, int j) {

        final SortingDCJInDel dcjindelSorting = new SortingDCJInDel();

        LabeledAdjacencyGraph g = new LabeledAdjacencyGraph(this.data.getGenomeA(), this.data.getGenomeB(), this.data.getAdjGraph(),
                this.globalData.getSuspendedGenes(), this.globalData.getBackMap(), this.globalData.getGenomes().get(i), this.globalData
                .getGenomesMap2().get(i), this.globalData.getGenomes().get(j), this.globalData.getGenomesMap2().get(j));

        DcjInDelAdditionalData additionalData = new DcjInDelAdditionalData(g, this.globalData.getBackMap());

        OperationList result = dcjindelSorting.findOptSortSequence(this.data, additionalData, this.globalData.getChromMaps().get(i));

        // TODO: THIS NEEDS TO BE EXPENDED
        return new IntermediateGenomesGeneratorInDel(this.data.getGenomeA(), this.globalData.getBackMap(), (OperationListInDel) result, this.data
                .getAdjGraph().getAdjacenciesGenome1(), this.globalData.getGenomeIDs().get(i + 1), this.globalData.getGenomeIDs().get(j + 1));
    }

    /**
     * Calculates a restricted DCJ sorting model for the genomes with id i and j.
     *
     * @param i index of genome A
     * @param j index of genome B
     * @return the intermediate genomes of the calculated sorting model.
     */
    private IntermediateGenomesGenerator sortRDCJ(Genome genomeA, Genome genomeB, int i, int j) {
        boolean linear = Utilities.onlyLinear(genomeA) && Utilities.onlyLinear(genomeB);

        if (linear) {
            final SortingRestrictedDCJ resSorting = new SortingRestrictedDCJ();
            OperationList result = resSorting.findOptSortSequence(this.data, null, this.globalData.getChromMaps().get(i));
            return new IntermediateGenomesGenerator(this.data.getGenomeA(), this.globalData.getBackMap(), result, this.data.getAdjGraph()
                    .getAdjacenciesGenome1(), this.globalData.getGenomeIDs().get(i + 1), this.globalData.getGenomeIDs().get(j + 1));
        } else {
            return null;
        }
    }

    /**
     * Calculates a HP sorting model for the genomes A and B with id i and j.
     *
     * @param genomeA genomeA
     * @param genomeB genomeB
     * @param i       index of genome A
     * @param j       index of genome B
     * @return the intermediate genomes of the calculated sorting model.
     */
    private IntermediateGenomesGenerator sortHP(Genome genomeA, Genome genomeB, int i, int j) {
        // check if genomes contain only linear chromosomes
        boolean linear = Utilities.onlyLinear(genomeA) && Utilities.onlyLinear(genomeB);
        if (linear) {
             final SortingHPEasy hpSorting = new SortingHPEasy();
            /*
             *  sorting hp algorithm fails finding optimal sorting scenarios:
             *  d_HP = 6, steps_sortingHP = 7 for
             *  >A 7 4 5 3 2 1 6| >B 1 2 3 4 5 6 7|
             *  or
             *  >A 7 4 5 3 2 1 6| 8 9 10 | >B 1 2 3 4 5 6 7 | 8 9 10 |
             */
//            final SortingHP hpSorting = new SortingHP();
            this.additionalData = new AdditionalDataHPDistance(genomeA);
            OperationList result = hpSorting.findOptSortSequence(this.data, this.additionalData, this.globalData.getChromMaps().get(i));
            return new IntermediateGenomesGenerator(genomeA, this.globalData.getBackMap(), result, this.data.getAdjGraph().getAdjacenciesGenome1(),
                    this.globalData.getGenomeIDs().get(i + 1), this.globalData.getGenomeIDs().get(j + 1));
        } else {
            return null;
        }
    }

    /**
     * Calculates an inversion sorting model for the genomes A and B with id i and j.
     *
     * @param genomeA genomeA
     * @param genomeB genomeB
     * @param i       index of genome A
     * @param j       index of genome B
     * @return the intermediate genomes of the calculated sorting model.
     */
    private IntermediateGenomesGenerator sortInv(Genome genomeA, Genome genomeB, int i, int j) {
        // check if genomes contain only 1 chromosome & are linear & co-tailed!
        boolean linear = Utilities.onlyLinear(genomeA) && Utilities.onlyLinear(genomeB);
        boolean cotailed = Utilities.checkCotailed(genomeA, genomeB);
        Genome genomeACapped = null;
        Genome genomeBCapped = null;
        Genome genomeCCapped = null;
        final int sumChroms = genomeA.getNumberOfChromosomes() + genomeB.getNumberOfChromosomes();

        if (linear && sumChroms == 2) {
            Data cappedData;

            if (!cotailed) { // then both genomes have to be capped and gene numbers adapted
                int nbGenes = genomeA.getNumberOfGenes();
                genomeACapped = Utilities.genomeCappingAndNumberAdaption(genomeA, nbGenes + 1, nbGenes + 2);
                genomeBCapped = Utilities.genomeCappingAndNumberAdaption(genomeB, nbGenes + 1, nbGenes + 2);
                genomeCCapped = Utilities.genomeCappingAndNumberAdaptionRev(genomeA, nbGenes + 1, nbGenes + 2);
                this.additionalData = new AdditionalDataHPDistance(genomeACapped);
                AdjacencyGraph cappedAdjGraph = new AdjacencyGraph(genomeACapped, genomeBCapped);
                cappedData = new Data(genomeACapped, genomeBCapped, cappedAdjGraph);
            } else {
                cappedData = this.data;
                this.additionalData = new AdditionalDataHPDistance(genomeA);
            }

            // //////////// sort genomes according to parameter stored during distance calculation /////////////
            final SortingInv invSorting = new SortingInv();
            OperationList result;
            if (!this.useOtherScenario) { // remember that chromMaps are not used here, because its unichromosomal!
                result = invSorting.findOptSortSequence(cappedData, this.additionalData, this.globalData.getChromMaps().get(i));
            } else {// in non cotailed case check second capping distance too

                AdditionalDataHPDistance addData2 = new AdditionalDataHPDistance(genomeCCapped);
                AdjacencyGraph cappedAdjGraph2 = new AdjacencyGraph(genomeCCapped, genomeBCapped);
                Data cappedData2 = new Data(genomeCCapped, genomeBCapped, cappedAdjGraph2);
                result = invSorting.findOptSortSequence(cappedData2, addData2, this.globalData.getChromMaps().get(i));
                this.useOtherScenario = false;
            }
            // ///////////////////////////////////////////////////////////////////////////////////////////////////

            if (!cotailed) { // undo gene number adaption back to original gene numbers
                // also means decreasing all values in adjacency graphs and operation lists!
                result = Utilities.correctOperationList(result, genomeA.getNumberOfGenes());
            }
            return new IntermediateGenomesGenerator(genomeA, this.globalData.getBackMap(), result, this.data.getAdjGraph().getAdjacenciesGenome1(),
                    this.globalData.getGenomeIDs().get(i + 1), this.globalData.getGenomeIDs().get(j + 1));
        } else {
            return null;
        }
    }

    /**
     * Calculates a translocation sorting model for the genomes A and B with id i and j.
     *
     * @param genomeA genomeA
     * @param genomeB genomeB
     * @param i       index of genome A
     * @param j       index of genome B
     * @return the intermediate genomes of the calculated sorting model.
     */
    private IntermediateGenomesGenerator sortTrans(Genome genomeA, Genome genomeB, int i, int j) {
        // check if genomes contain only linear chromosomes & are co-tailed!
        boolean linear = Utilities.onlyLinear(genomeA) && Utilities.onlyLinear(genomeB);
        boolean cotailed = Utilities.checkCotailed(genomeA, genomeB);
        int nbchroms = genomeA.getNumberOfChromosomes();
        boolean equalNbChroms = (nbchroms == genomeB.getNumberOfChromosomes());
        if (linear && cotailed && equalNbChroms && nbchroms > 1) {
            final SortingTrans transSorting = new SortingTrans();
            this.additionalData = new AdditionalDataHPDistance(genomeA); // FIXME why called "HP data?" Answer: Because these scenarios use the same data structures as
            //the hp algorithm - so it is based on data structures originally developed for HP

            OperationList result = transSorting.findOptSortSequence(this.data.clone(), ((AdditionalDataHPDistance) this.additionalData).clone(), this.globalData.getChromMaps().get(i));

            return new IntermediateGenomesGenerator(genomeA, this.globalData.getBackMap(), result, this.data.getAdjGraph().getAdjacenciesGenome1(),
                    this.globalData.getGenomeIDs().get(i + 1), this.globalData.getGenomeIDs().get(j + 1));
        } else {
            return null;
        }
    }

    /**
     * Method for handling a class cast exception for Additional data classes.
     */
    private void handleClassCastExcep() {
        if (!this.guiMode) {
            System.err.println("Wrong Additional Data for this model!");
            System.exit(1);
        } else {
            JOptionPane.showMessageDialog(this.getMainFrame(), "Wrong additional data object for this model!", "Additonal data error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Method for handling an IOException.
     */
    private void handleIOException(String errorMsg) {
        if (!this.guiMode) {
            System.err.println(errorMsg);
            System.exit(1);
        } else {
            JOptionPane.showMessageDialog(this.getMainFrame(), errorMsg, "Input error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * @return the currently selected {@link Model}
     */
    public Model getScenario() {
        return this.model;
    }

    /**
     * Sets the {@link Model}.
     *
     * @param model currently selected {@link Model}
     */
    public void setScenario(final Model model) {
        this.model = model;
    }

    /**
     * Returns the file path.
     *
     * @return the file path
     */
    public String getFilepath() {
        return this.filepath;
    }

    /**
     * Sets the model.
     *
     * @param filepath the model
     */
    public void setFilepath(final String filepath) {
        this.filepath = filepath;
    }

    /**
     * Sets the data object.
     *
     * @param data the data to set
     */
    public void setData(final Data data) {
        this.data = data;
    }

    /**
     * Returns the data object.
     *
     * @return the data
     */
    public Data getData() {
        return this.data;
    }

    /**
     * Sets the additional data object.
     *
     * @param additionalData the additionalData to set
     */
    public void setAdditionalData(final IAdditionalData additionalData) {
        this.additionalData = additionalData;
    }

    /**
     * Returns the additional data.
     *
     * @return the additionalData
     */
    public IAdditionalData getAdditionalData() {
        return this.additionalData;
    }

    /**
     * Sets the option of showing the steps true or false.
     *
     * @param showSteps <code>true</code> if steps (model) is desired, <code>false</code> otherwise
     */
    public void setShowSteps(final boolean showSteps) {
        this.showSteps = showSteps;
    }

    /**
     * Returns value of <code>showSteps</code> (whether it is set or not).
     *
     * @return <code>true</code> if model (the steps) should be printed, <code>false</code> otherwise (if only distance is desired).
     */
    public boolean isShowSteps() {
        return this.showSteps;
    }

    /**
     * Sets the option of showing a plain output for scenarios (no tables or warnings).<br>
     * If no model is desired at all (<code>showSteps</code> is <code>false</code>) then this can only be <code>false</code>.
     *
     * @param plainScenarioOutput <code>true</code> if model should be printed plainly (no tables or warnings), <code>false</code> otherwise (standard output).
     */
    public void setPlainScenarioOutput(final boolean plainScenarioOutput) {
        this.plainScenarioOutput = plainScenarioOutput;
    }

    /**
     * Returns value of <code>plainScenarioOutput</code> (whether it is set or not).<br>
     * If no model is desired at all (<code>showSteps</code> is <code>false</code>) then this can only be <code>false</code>.
     *
     * @return <code>true</code> if model should be printed plainly (no tables or warnings), <code>false</code> otherwise (standard output).
     */
    public boolean isPlainScenarioOutput() {
        return this.plainScenarioOutput;
    }

    /**
     * Sets the option of sampling uniformly among all DCJ sorting scenarios to <code>true</code> or <code>false</code>.
     *
     * @param sampling <code>true</code> if sampling is desired, <code>false</code> otherwise (same model in each call).
     */
    public void setSampling(final boolean sampling) {
        this.sample = sampling;
    }

    /**
     * Returns the option of sampling uniformly among all DCJ sorting scenarios to <code>true</code> or <code>false</code>.
     *
     * @return
     */
    public boolean isSampling() {
        return this.sample;
    }

    /**
     * Returns the genome inidices of the genomes which could be compared in the order
     * they were compared in. -> (gi[0] = 3 & gi[1] = 5 means comparison of genomes 3 & 5 f.e.
     *
     * @return the genomeIndices
     */
    public ArrayList<Integer> getGenomeIndices() {
        return this.genomeIndices;
    }

    /**
     * Returns the output data after a comparison.
     *
     * @return the outputData
     */
    public DataOutput[] getOutputData() {
        return this.outputData;
    }

    /**
     * Returns the output data after a comparison.
     *
     * @return the outputData
     */
    public DataFramework getGlobalData() {
        return this.globalData;
    }

    /**
     * @return the list of error messages. It only contains messages, if at least
     * one genome comparison could not be carried out, because the genome formats
     * or the gene content did not match the chosen model.
     */
    public List<String> getNotifications() {
        return this.notifications;
    }

    /**
     * Clears the error message list.
     */
    public void clearNotifications() {
        this.notifications.clear();
    }

    /**
     * @return calculates the lower bound of all possible dcj sorting sequences
     * for the adjacency graph
     */
    private BigInteger calcLowerBound() {

        FactorialParallelPrimeSwing fac = new FactorialParallelPrimeSwing();
        List<Integer> compDistances = this.data.getAdjGraph().getCompDistances();
        int numeratorInt = 0;
        BigInteger numerator = new BigInteger("0");
        BigInteger denominator = new BigInteger("1");
        BigInteger product = new BigInteger("1");

        for (Integer dist : compDistances) {
            if (dist > 0) {

                numeratorInt += dist;

                Xint faculty = fac.factorial(dist);
                denominator = denominator.multiply(new BigInteger(faculty.toString()));

                Xint pow = Xint.valueOf(dist + 1).toPowerOf(dist - 1);
                product = product.multiply(new BigInteger(String.valueOf(pow)));
            }

        }
        numerator = new BigInteger(fac.factorial(numeratorInt).toString());

        return product.multiply(numerator.divide(denominator));
    }

    /**
     * @return <code>true</code>, if the calculations were aborted, <code>false</code> otherwise.
     */
    public boolean isInterrupted() {
        return this.interrupted;
    }

    /**
     * @param interrupted set true, if the calculations should be aborted, set false
     *                    otherwise. If this method is not called at all, it is always set to false.
     */
    public void setInterrupted(boolean interrupted) {
        this.interrupted = interrupted;
    }
}