package de.unibi.cebitec.gi.unimog.framework;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLEditorKit;

import de.unibi.cebitec.gi.unimog.algorithms.IntermediateGenomesGenerator;
import de.unibi.cebitec.gi.unimog.datastructure.DataOutput;
import de.unibi.cebitec.gi.unimog.exceptions.InputOutputException;
import de.unibi.cebitec.gi.unimog.utils.Constants;
import de.unibi.cebitec.gi.unimog.utils.Observer;
import de.unibi.cebitec.gi.unimog.utils.Toolz;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker                                     *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @author -Rolf Hilker-
 *         Main frame of the program. Assembles all visual program content.
 */
public class MainFrame extends JFrame implements Observer {
	
	// /**
	// * Uniform sampling among all optimal DCJ sorting scenarios is selected (<code>true</code>) or not (
	// * <code>false</code>).
	// */
	// public static boolean SAMLPE = true;
	/** If <code>true</code> the user has confirmed that recombinations are ignored. */
	public static boolean		IGNORE_RECOMBS		= false;
	public static int			COLOR_MODE			= Constants.OUTPUT_1;
	private static final long	serialVersionUID	= 1L;
	private static final int	INIT_WIDTH			= 800;
	private static final int	INIT_HEIGHT			= 600;
	private static final int	CONTROL_HEIGHT		= 45;
	private static final int	OFFSET				= 18;
	private static final int	I_GRAPHIC_OUTPUT	= 2;
	private final MainClass		mainClass;
	private Model				model			= Model.DCJ;
	private double				zoomFactor			= Constants.ZOOM_FACTOR1;
	private JPanel				northPanel;
	private JPanel				northLeftPanel;
	private JPanel				northRightPanel;
	private GraphicPanel		graphicPanel;
	private JPanel				southPanel;
	private JSplitPane			workingSplitPane;
	private JTextArea			inputTextArea;
	private JTextPane			stdOutputArea;
	private JTextArea			adjacenciesArea;
	private JTabbedPane			outputPane;
	private JScrollPane			graphicScrollPane;
	private HelpPage			helpPane;
	private Runnable			workingRun;
	private Thread				workingThread;
	private Font				fontGeneral;
	private ButtonGroup			scenarioButtons;
	private JButton				loadButton;
	private JButton				clearButton;
	private JButton				exampleButton;
	private JButton				helpButton;
	private JButton				runButton;
	private JButton				saveTextButton;
	private JButton				saveImageButton;
	private JButton				exitButton;
	//models
//	private JComboBox modelComboBox;
//	private JOptionPane			modelPane;
	private JRadioButton		selectAll;
	private JRadioButton		selectDCJ;
	private JRadioButton		selectHP;
	private JRadioButton		selectINV;
	private JRadioButton		selectTrans;
	private JRadioButton		selectRDCJ;
	private JRadioButton		selectDCJid;
	// other options
	private JCheckBox			stepsCheckBox;
	private JCheckBox			plainScenarioCheckBox;
	private JCheckBox			samplingCheckBox;
	private JCheckBox			colorCheckBox;
	// private JComboBox colorComboBox;
	private JSlider				sizeSlider;
	private File[]				files;
	private JProgressBar		progressBar;
	private JDialog				dialog;
	private int					nbComparisons;
	private int					currentComparison;
	
	/**
	 * Main Constructor.
	 * 
	 * @param mainClass
	 *            The main class instance.
	 */
	public MainFrame(final MainClass mainClass) {
		// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		this.fontGeneral = new Font("Dialog", 1, 10);
		this.mainClass = mainClass;
		this.setTitle("Unified Model of Genomic Distance Computation via Double Cut & Join 1.0");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.initGUI();
		this.addActions();
	}
	
	/** Initializes the gui with all its components. */
	private void initGUI() {
		
		this.setSize(MainFrame.INIT_WIDTH, MainFrame.INIT_HEIGHT);
		final Container contentPane = this.getContentPane();
		
		final FlowLayout thisLayout = new FlowLayout();
		contentPane.setLayout(thisLayout);
		
		// creating a panel for controls in the north
		this.northPanel = new JPanel();
		final GridLayout northPanelLayout = new GridLayout(1, 2);
		this.northPanel.setPreferredSize(new Dimension(this.getWidth() - MainFrame.OFFSET, MainFrame.CONTROL_HEIGHT));
		this.northPanel.setLayout(northPanelLayout);
		this.northPanel.setBorder(BorderFactory.createEtchedBorder());
		contentPane.add(this.northPanel);
		
		this.createInnerNorthPanels(this.northPanel);
		// creating a split pane for the main working areas in the center
		this.createSplitPane(contentPane);
		
		// creating a panel for controls in the south
		this.southPanel = new JPanel();
		final GridBagLayout southPanelLayout = new GridBagLayout();
		this.southPanel.setLayout(southPanelLayout);
		this.southPanel.setPreferredSize(new Dimension(this.getWidth() - MainFrame.OFFSET, MainFrame.CONTROL_HEIGHT));
		this.southPanel.setSize(this.getWidth() - MainFrame.OFFSET, MainFrame.CONTROL_HEIGHT);
		contentPane.add(this.southPanel);
		
		this.createSouthPanelComps(this.southPanel);
		
		this.setToolTips();
		this.setVisible(true);
		
		// enlarge size, if model panel cannot be shown completely
		int currWidth = this.northRightPanel.getSize().width;
		int prefWidth = this.northRightPanel.getPreferredSize().width;
		if (currWidth < prefWidth) {
			Dimension newSize = new Dimension(this.getWidth() + 2 * (prefWidth - currWidth) + 50, this.getHeight());
			this.setPreferredSize(newSize);
			this.setSize(newSize);
		}
	}
	
	/**
	 * Creates the north panel containing the basic controls to create a model.
	 * 
	 * @param northPanel
	 *            the panel to display the components on
	 */
	private void createInnerNorthPanels(final JPanel northPanel) {
		
		// create left panel containing control buttons
		this.northLeftPanel = new JPanel();
		final GridBagLayout northLeftLayout = new GridBagLayout();
		this.northLeftPanel.setLayout(northLeftLayout);
		this.northLeftPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		northPanel.add(this.northLeftPanel);
		
		this.loadButton = (JButton) this.setJCompProps(new JButton("Load File(s)"), this.northLeftPanel);
		this.clearButton = (JButton) this.setJCompProps(new JButton("Clear"), this.northLeftPanel);
		this.exampleButton = (JButton) this.setJCompProps(new JButton("Example"), this.northLeftPanel);
		this.helpButton = (JButton) this.setJCompProps(new JButton("Help"), this.northLeftPanel);
		
		// create right panel containing model selection buttons
		this.northRightPanel = new JPanel();
		final GridBagLayout northRightLayout = new GridBagLayout();
		this.northRightPanel.setLayout(northRightLayout);
		this.northRightPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		northPanel.add(this.northRightPanel);
		
		this.setJCompProps(new JLabel("Model: "), this.northRightPanel); // no need to store in a variable
		
//		this.modelPane = new JOptionPane();
//		this.modelComboBox = new JComboBox();
		
		this.selectDCJ = (JRadioButton) this.setJCompProps(new JRadioButton(Model.DCJ.getAbbrevGuiName()), this.northRightPanel);
		this.selectDCJ.setSelected(true);
		this.selectRDCJ = (JRadioButton) this.setJCompProps(new JRadioButton(Model.DCJ_RESTRICTED.getAbbrevGuiName()), this.northRightPanel);
		this.selectDCJid = (JRadioButton) this.setJCompProps(new JRadioButton(Model.DCJ_INDEL.getAbbrevGuiName()), this.northRightPanel);
		this.selectHP = (JRadioButton) this.setJCompProps(new JRadioButton(Model.HP.getAbbrevGuiName()), this.northRightPanel);
		this.selectINV = (JRadioButton) this.setJCompProps(new JRadioButton(Model.INVERSION.getAbbrevGuiName()), this.northRightPanel);
		this.selectTrans = (JRadioButton) this.setJCompProps(new JRadioButton(Model.TRANSLOCATION.getGuiName()), this.northRightPanel);
		this.selectAll = (JRadioButton) this.setJCompProps(new JRadioButton(Model.ALL.getGuiName()), this.northRightPanel);
		
		// put model buttons in a button group
		this.scenarioButtons = new ButtonGroup();
		this.scenarioButtons.add(this.selectDCJ);
		this.scenarioButtons.add(this.selectHP);
		this.scenarioButtons.add(this.selectINV);
		this.scenarioButtons.add(this.selectTrans);
		this.scenarioButtons.add(this.selectRDCJ);
		this.scenarioButtons.add(this.selectDCJid);
		this.scenarioButtons.add(this.selectAll);
		
	}
	
	/**
	 * Creates the split pane containing the two working areas.
	 * 
	 * @param contentPane
	 *            The content pane of the main frame
	 */
	private void createSplitPane(final Container contentPane) {
		
		// creating the input area
		this.inputTextArea = new JTextArea();
		final JScrollPane inputScrollPane = new JScrollPane(this.inputTextArea);
		
		// creating the output areas & embrace them by a tabbed pane
		this.stdOutputArea = new JTextPane();
		this.stdOutputArea.setEditorKit(new HTMLEditorKit());
		final JScrollPane outputScrollPane = new JScrollPane(this.stdOutputArea);
		this.stdOutputArea.setFont(Toolz.getFontOutput());
		this.stdOutputArea.setEditable(false);
		this.adjacenciesArea = new JTextArea();
		final JScrollPane adjacencyScrollPane = new JScrollPane(this.adjacenciesArea);
		this.adjacenciesArea.setEditable(false);
		this.graphicPanel = new GraphicPanel();
		this.graphicPanel.registerObserver(this);
		this.graphicPanel.setLayout(new BorderLayout());
		this.graphicScrollPane = new JScrollPane(this.graphicPanel);
		this.graphicScrollPane.getViewport().setScrollMode(JViewport.BACKINGSTORE_SCROLL_MODE);
		final int scrollInc = 15;
		this.graphicScrollPane.getHorizontalScrollBar().setUnitIncrement(scrollInc);
		this.graphicScrollPane.getVerticalScrollBar().setUnitIncrement(scrollInc);
		
		this.outputPane = new JTabbedPane(JTabbedPane.TOP);
		this.outputPane.setFont(this.fontGeneral);
		this.outputPane.addTab("Genomes", outputScrollPane);
		this.outputPane.addTab("Adjacencies", adjacencyScrollPane);
		this.outputPane.addTab("Graphics", graphicScrollPane);
		
		// put both components in a split pane
		this.workingSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, inputScrollPane, this.outputPane);
		this.workingSplitPane.setOneTouchExpandable(true);
		this.workingSplitPane.setDividerLocation(300);
		this.workingSplitPane.setPreferredSize(new Dimension(MainFrame.INIT_WIDTH - MainFrame.OFFSET, 460));
		contentPane.add(this.workingSplitPane, FlowLayout.CENTER);
	}
	
	/**
	 * Creates the south panel containing containing controls & output options.
	 * 
	 * @param southPanel
	 *            the panel to display the components on
	 */
	private void createSouthPanelComps(final JPanel southPanel) {
		
		this.runButton = (JButton) this.setJCompProps(new JButton("Run"), southPanel);
		this.stepsCheckBox = (JCheckBox) this.setJCompProps(new JCheckBox("Show Steps", true), southPanel);
		// southPanel.add(new JLabel("       Chromosome colors: "));
		// this.colorComboBox = new JComboBox(new String[] {"Uncolored", "Chromosomes"});
		// southPanel.add(this.colorComboBox);
		this.plainScenarioCheckBox = (JCheckBox) this.setJCompProps(new JCheckBox("Plain output"), southPanel);
		this.samplingCheckBox = (JCheckBox) this.setJCompProps(new JCheckBox("Uniform sampling DCJ", true), southPanel);
		southPanel.add(this.samplingCheckBox);
		this.colorCheckBox = (JCheckBox) this.setJCompProps(new JCheckBox("Colored chromosomes"), southPanel);
		final int minSize = 0;
		final int maxSize = 2;
		this.sizeSlider = (JSlider) this.setJCompProps(new JSlider(JSlider.HORIZONTAL, minSize, maxSize, minSize), southPanel);
		this.sizeSlider.setPreferredSize(new Dimension(60, MainFrame.CONTROL_HEIGHT));
		this.sizeSlider.setSnapToTicks(true);
		this.sizeSlider.setMinorTickSpacing(1);
		this.sizeSlider.setPaintTicks(true);
		this.saveTextButton = (JButton) this.setJCompProps(new JButton("Save Text"), southPanel);
		this.saveImageButton = (JButton) this.setJCompProps(new JButton("Save Graphics"), southPanel);
		this.exitButton = (JButton) this.setJCompProps(new JButton("Exit"), southPanel);
	}
	
	/**
	 * Finishes a JComponent by setting its general properties. Currently this only includes to set the font to the
	 * general font defined in MainFrame.java.
	 * 
	 * @param comp
	 *            the component
	 * @return the finished JComponent
	 */
	private JComponent setJCompProps(JComponent comp, JComponent parent) {
		comp.setFont(this.fontGeneral);
		parent.add(comp);
		return comp;
	}
	
	/**
	 * Adds the tooltips to all components.<br>
	 * Has to be called after creation of the components.
	 */
	private void setToolTips() {
		this.loadButton.setToolTipText("Load file(s) with genome content");
		this.clearButton.setToolTipText("Clear input and output areas");
		this.exampleButton.setToolTipText("Show an input example");
		this.helpButton.setToolTipText("Show detailed help window");
		final String scenarioString = "Select model for genome comparisons";
		this.northRightPanel.setToolTipText(scenarioString);
		this.selectDCJ.setToolTipText(scenarioString);
		this.selectHP.setToolTipText(scenarioString);
		this.selectINV.setToolTipText(scenarioString);
		this.selectTrans.setToolTipText(scenarioString);
		this.selectRDCJ.setToolTipText(scenarioString);
		this.selectDCJid.setToolTipText(scenarioString);
		this.selectAll.setToolTipText("All models are run after another");
		this.inputTextArea.setToolTipText("Input area, enter at least two genomes");
		this.stdOutputArea.setToolTipText("Standard output of distances and genomes during comparison");
		this.adjacenciesArea.setToolTipText("Displays sets of adjacencies of genomes during comparison");
		this.graphicPanel.setToolTipText("Displays genomes during comparison graphically");
		this.runButton.setToolTipText("Runs genome comparison with selected options and given input");
		this.stepsCheckBox.setToolTipText("Select if additional to distances also optimal sorting sequence should be calculated");
		this.plainScenarioCheckBox.setToolTipText("Select for a simplified sorting model output.");
		this.samplingCheckBox.setToolTipText("Uniformly samples a DCJ sorting model from all optimal scenarios.");
		this.colorCheckBox.setToolTipText("Select color mode of the output");
		this.saveTextButton.setToolTipText("Save output from 'Genomes' and 'Adjacencies' in text file");
		this.saveImageButton.setToolTipText("Save graphical output as image file");
		this.exitButton.setToolTipText("Closes the program");
		this.sizeSlider.setToolTipText("Select size of the graphical output");
	}
	
	/** Adds the actions to all buttons and controls. */
	private void addActions() {
		
		this.loadButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.openFileChooser(Constants.OPEN);
			}
		});
		
		this.clearButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.inputTextArea.setText("");
				MainFrame.this.stdOutputArea.setText("");
				MainFrame.this.adjacenciesArea.setText("");
				MainFrame.this.graphicPanel.setData(null, MainFrame.this.model, Constants.ZOOM_FACTOR1);
				MainFrame.this.graphicPanel.getGraphics().clearRect(0, 0, MainFrame.this.graphicPanel.getWidth(),
						MainFrame.this.graphicPanel.getHeight());
				MainFrame.this.outputPane.getSelectedComponent().repaint();
			}
		});
		
		this.exampleButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.inputTextArea.setText(Constants.GENOME_EXAMPLE);
			}
		});
		
		this.helpButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (MainFrame.this.helpPane == null) {
					MainFrame.this.helpPane = new HelpPage();
				}
				MainFrame.this.helpPane.setVisible(true);
			}
		});
		
		this.selectDCJ.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.model = Model.DCJ;
				
				// when steps are printed -> sampling allowed for DCJ
				if (MainFrame.this.stepsCheckBox.isSelected()) {
					MainFrame.this.samplingCheckBox.setEnabled(true);
				}
			}
		});
		
		this.selectRDCJ.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.model = Model.DCJ_RESTRICTED;
				MainFrame.this.samplingCheckBox.setEnabled(false);
			}
		});
		
		this.selectDCJid.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.model = Model.DCJ_INDEL;
				MainFrame.this.samplingCheckBox.setEnabled(false);
			}
		});
		
		this.selectHP.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.model = Model.HP;
				MainFrame.this.samplingCheckBox.setEnabled(false);
			}
		});
		
		this.selectINV.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.model = Model.INVERSION;
				MainFrame.this.samplingCheckBox.setEnabled(false);
			}
		});
		
		this.selectTrans.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.model = Model.TRANSLOCATION;
				MainFrame.this.samplingCheckBox.setEnabled(false);
			}
		});
		
		this.selectAll.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.model = Model.ALL;
				if (MainFrame.this.stepsCheckBox.isSelected()) {
					// if steps are printed, sampling is allowed, plain output is allowed
					MainFrame.this.plainScenarioCheckBox.setEnabled(true);
					MainFrame.this.samplingCheckBox.setEnabled(true);
				} else {
					// no model is printed -> no sampling is allowed, no plain output is allowed
					MainFrame.this.plainScenarioCheckBox.setEnabled(false);
					MainFrame.this.samplingCheckBox.setEnabled(false);
				}
			}
		});
		
		this.runButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				
				if (MainFrame.this.stepsCheckBox.isSelected()) {
					// model is printed -> check for plain or standard output (and enable)
					MainFrame.this.mainClass.setShowSteps(true);
					MainFrame.this.mainClass.setPlainScenarioOutput(MainFrame.this.plainScenarioCheckBox.isSelected());
					final boolean samplingAllowed = (MainFrame.this.samplingCheckBox.isEnabled() && MainFrame.this.samplingCheckBox.isSelected());
					MainFrame.this.mainClass.setSampling(samplingAllowed);
				} else {
					// model is not printed at all -> also no plain output, no sampling
					MainFrame.this.mainClass.setShowSteps(false);
					MainFrame.this.mainClass.setPlainScenarioOutput(false);
					MainFrame.this.mainClass.setSampling(false);
				}
				MainFrame.this.startProgressBar();
				MainFrame.this.workingRun = new WorkingRunnable();
				workingThread = new Thread(MainFrame.this.workingRun);
				workingThread.start();
			}
		});
		
		this.stepsCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				if (MainFrame.this.stepsCheckBox.isSelected()) {
					// model is printed -> plain output is allowed, sampling may be allowed
					MainFrame.this.samplingCheckBox.setEnabled(true);
					MainFrame.this.plainScenarioCheckBox.setEnabled(true);
				} else {
					MainFrame.this.mainClass.setShowSteps(false);
					// model is not printed at all -> also no plain output, no sampling
					MainFrame.this.plainScenarioCheckBox.setEnabled(false);
					MainFrame.this.samplingCheckBox.setEnabled(false);
				}
			}
		});
		
		this.colorCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				if (MainFrame.this.colorCheckBox.isSelected()) {
					MainFrame.COLOR_MODE = Constants.OUTPUT_2;
				} else {
					MainFrame.COLOR_MODE = Constants.OUTPUT_1;
				}
				MainFrame.this.graphicPanel.setColorMode();
				MainFrame.this.outputPane.getSelectedComponent().repaint();
			}
		});
		
		this.sizeSlider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				switch (MainFrame.this.sizeSlider.getValue()) {
					case 0:
						MainFrame.this.zoomFactor = Constants.ZOOM_FACTOR1;
						break;
					case 1:
						MainFrame.this.zoomFactor = Constants.ZOOM_FACTOR2;
						break;
					case 2:
						MainFrame.this.zoomFactor = Constants.ZOOM_FACTOR3;
						break;
					default:
						MainFrame.this.zoomFactor = Constants.ZOOM_FACTOR1;
				}
				MainFrame.this.graphicPanel.setZoomFactor(MainFrame.this.zoomFactor);
				if (MainFrame.this.outputPane.getSelectedIndex() != MainFrame.I_GRAPHIC_OUTPUT) {
					MainFrame.this.outputPane.getSelectedComponent().repaint();
				}
			}
		});
		
		this.saveTextButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.openFileChooser(Constants.SAVE_TXT);
			}
		});
		
		this.saveImageButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.openFileChooser(Constants.SAVE_IMG);
			}
		});
		
		this.exitButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(final ActionEvent e) {
				MainFrame.this.dispose();
				System.exit(0);
			}
		});
		
		this.inputTextArea.addMouseListener(new MenuEvent(true));
		this.stdOutputArea.addMouseListener(new MenuEvent(false));
		this.adjacenciesArea.addMouseListener(new MenuEvent(false));
	}
	
	@Override
	public void validate() {
		MainFrame.this.adaptComponentSize();
		super.validate();
	}
	
	/** Adapts the size of the components according to the window size. */
	private void adaptComponentSize() {
		final int offset = 55;
		this.northPanel.setPreferredSize(new Dimension(this.getWidth() - MainFrame.OFFSET, MainFrame.CONTROL_HEIGHT));
		this.workingSplitPane.setPreferredSize(new Dimension(this.getWidth() - MainFrame.OFFSET, this.getHeight() - (2 * MainFrame.CONTROL_HEIGHT)
				- offset));
		this.southPanel.setPreferredSize(new Dimension(this.getWidth() - MainFrame.OFFSET, MainFrame.CONTROL_HEIGHT));
		this.getContentPane().repaint();
	}
	
	/** Opens a file chooser for input file selection. */
	private void openFileChooser(final int eventType) {
		final String error = "Cannot find look and feel for file chooser.";
		final LookAndFeel stdLookAndFeel = UIManager.getLookAndFeel();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException exception) {
			System.err.println(error);
		} catch (InstantiationException exception) {
			System.err.println(error);
		} catch (IllegalAccessException exception) {
			System.err.println(error);
		} catch (UnsupportedLookAndFeelException exception) {
			System.err.println(error);
		}
		final JFileChooser jfc = new JFileChooser();
		try {
			String directory = !(this.files == null) && !this.files[0].getAbsolutePath().equals("") ? this.files[0].getAbsolutePath() : new File(".")
					.getCanonicalPath();
			jfc.setCurrentDirectory(new File(directory));
		} catch (final IOException exception) {
			jfc.setCurrentDirectory(null);
		}
		int option;
		if (eventType == Constants.OPEN) {
			jfc.setMultiSelectionEnabled(true);
			option = jfc.showOpenDialog(null);
		} else {
			if (eventType == Constants.SAVE_TXT) {
				jfc.setFileFilter(new FileNameExtensionFilter("txt", "txt"));
			} else {
				jfc.setFileFilter(new FileNameExtensionFilter("jpg", "jpg", "jpeg"));
			}
			option = jfc.showSaveDialog(null);
		}
		
		try {
			UIManager.setLookAndFeel(stdLookAndFeel);
		} catch (final UnsupportedLookAndFeelException e) {
			// Do nothing and keep old look and feel
		}
		if (option == JFileChooser.CANCEL_OPTION) {
			return;
		}
		if (eventType == Constants.OPEN) {
			this.files = jfc.getSelectedFiles();
			this.displayText(this.files);
		} else { // a filechooser for storing a file
			File file = jfc.getSelectedFile();
			String fileLocation = file.getAbsolutePath();
			
			if (eventType == Constants.SAVE_TXT) {
				file = new File(this.addFileExtension(fileLocation, Constants.SAVE_TXT));
			} else { // if (eventType == Constants.SAVE_IMG){
				file = new File(this.addFileExtension(fileLocation, Constants.SAVE_IMG));
			}
			
			if (file.exists()) {
				final int overwriteFile = JOptionPane.showConfirmDialog(jfc, "File already exists. Do you want to overwrite the existing file?",
						"Overwrite File Dialog", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (overwriteFile == JOptionPane.YES_OPTION) {
					if (eventType == Constants.SAVE_TXT) {
						this.saveToTxtFile(file.getAbsolutePath());
					} else {
						this.saveToImgFile(file.getAbsolutePath());
					}
				} else {
					this.openFileChooser(eventType);
				}
				
			} else {
				if (eventType == Constants.SAVE_TXT) {
					this.saveToTxtFile(file.getAbsolutePath());
				} else {
					this.saveToImgFile(file.getAbsolutePath());
				}
			}
		}
	}
	
	/**
	 * Adds the file extension "txt" to the output file name.
	 * 
	 * @param absolutePath
	 *            the file path including the name
	 * @param extensionType
	 *            the type of extension to add (txt or jpg)
	 * @return absolutePath including correct file extension
	 */
	private String addFileExtension(String absolutePath, final int extensionType) {
		if (extensionType == Constants.SAVE_TXT) {
			if (!absolutePath.endsWith(".txt") && !absolutePath.endsWith(".TXT")) {
				return absolutePath.concat(".txt");
			}
		} else {
			if (!absolutePath.endsWith(".jpg") && !absolutePath.endsWith(".jpeg") && !absolutePath.endsWith(".JPG")
					&& !absolutePath.endsWith(".JPEG")) {
				return absolutePath.concat(".jpg");
			}
		}
		
		return absolutePath;
	}
	
	/**
	 * Saves all text in the text areas to a text file.
	 * 
	 * @param fileLocation
	 *            the location & name of the file to create
	 */
	private void saveToTxtFile(String fileLocation) {
		try {
			String output = "Data generated by UniMoG (http://http://bibiserv.cebitec.uni-bielefeld.de/dcj?id=dcj_unimog)\n\nInput:\n\n"
					.concat(this.inputTextArea.getText()).concat("\nOutput:\n\n")
					.concat(this.stdOutputArea.getDocument().getText(0, this.stdOutputArea.getDocument().getLength()))
					.concat("Adjacencies of each genome comparison after each step:\n\n").concat(this.adjacenciesArea.getText());
			final FileWriter fileStream = new FileWriter(fileLocation);
			final BufferedWriter outputWriter = new BufferedWriter(fileStream);
			int index = output.indexOf(Constants.LINE_BREAK);
			while (index != -1 && index < output.length()) {
				// outputWriter.write(Constants.LINE_BREAK_OUTPUT);
				outputWriter.write(output.substring(0, index));
				outputWriter.write(Constants.LINE_BREAK_OUTPUT);
				output = output.substring(index + 1);
				index = output.indexOf(Constants.LINE_BREAK);
			}
			outputWriter.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Error: " + e.getMessage(), "Write file error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Saves the graphics to a jpg file.
	 * 
	 * @param fileLocation
	 *            the location & name of the file to create
	 */
	private void saveToImgFile(final String fileLocation) {
		String fileLocus = this.addFileExtension(fileLocation, Constants.SAVE_IMG);
		try {
			BufferedImage bufferedImage = new BufferedImage(MainFrame.this.graphicPanel.getWidth(), MainFrame.this.graphicPanel.getHeight(),
					BufferedImage.TYPE_3BYTE_BGR);
			final Graphics graphic = bufferedImage.createGraphics();
			MainFrame.this.graphicPanel.paintAll(graphic);
			
			try {
				ImageIO.write(bufferedImage, "jpg", new File(fileLocus));
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, "Picture could not be saved, try again.", "Save Picture Error", JOptionPane.ERROR_MESSAGE);
				
			}
		} catch (OutOfMemoryError e) {
			try {
				Robot robot = new Robot();
				Rectangle rect = this.getBounds();
				Rectangle rect2 = this.outputPane.getBounds();
				BufferedImage bufferedImage = robot.createScreenCapture(new Rectangle(rect.x + rect2.x + 13, rect.y + rect2.y + 113,
						rect2.width - 24, rect2.height - 48));
				try {
					ImageIO.write(bufferedImage, "jpg", new File(fileLocus));
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(this, "Picture could not be saved, try again.", "Save Picture Error", JOptionPane.ERROR_MESSAGE);
				}
				JOptionPane.showMessageDialog(this, "Picture is too big for your memory, only saving screenshot of visible area.",
						"Picture Size Problem", JOptionPane.ERROR_MESSAGE);
			} catch (AWTException e2) {
				JOptionPane.showMessageDialog(this, "Error with retrieving the picture from the GUI.", "Save Picture Error",
						JOptionPane.ERROR_MESSAGE);
			}
			
		}
	}
	
	/**
	 * Reads & displays the text from a file.
	 * 
	 * @param files
	 *            The location of a file
	 */
	private void displayText(final File[] files) {
		if (files != null) {
			this.inputTextArea.setText("");
			for (File file : files) {
				if (file != null) {
					if (!file.exists() && !file.canRead()) {
						JOptionPane.showMessageDialog(this, "Cannot read file from ".concat(file.getAbsolutePath()), "Input Error",
								JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							final BufferedReader in = new BufferedReader(new FileReader(file));
							String readLine = "";
							while ((readLine = in.readLine()) != null) {
								this.inputTextArea.append(readLine.concat("\n"));
							}
							in.close();
						} catch (final FileNotFoundException e) {
							JOptionPane.showMessageDialog(this, "Cannot find file from ".concat(file.getAbsolutePath()), "Input Error",
									JOptionPane.ERROR_MESSAGE);
						} catch (final IOException e) {
							JOptionPane.showMessageDialog(this, "Cannot read file from ".concat(file.getAbsolutePath()), "Input Error",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		}
	}
	
	@Override
	public void update(Object value) {
		if (value instanceof Integer) {
			this.setCurrentComparison((Integer) value);
		}
	}
	
	/**
	 * Creates the working Runnable, which is needed when the run button was pressed an all calculations have to be
	 * carried out. At the end it hides again the waiting popup.
	 */
	private class WorkingRunnable implements Runnable {
		
		@Override
		public void run() {
			MainFrame.this.selectDCJ.setEnabled(false);
			MainFrame.this.selectRDCJ.setEnabled(false);
			MainFrame.this.selectDCJid.setEnabled(false);
			MainFrame.this.selectHP.setEnabled(false);
			MainFrame.this.selectINV.setEnabled(false);
			MainFrame.this.selectTrans.setEnabled(false);
			MainFrame.this.selectAll.setEnabled(false);
			MainFrame.this.runButton.setEnabled(false);
			MainFrame.this.stepsCheckBox.setEnabled(false);
			MainFrame.this.plainScenarioCheckBox.setEnabled(false);
			MainFrame.this.samplingCheckBox.setEnabled(false);
			MainFrame.this.colorCheckBox.setEnabled(false);
			MainFrame.this.sizeSlider.setEnabled(false);
			MainFrame.this.northRightPanel.setEnabled(false);
			MainFrame.this.saveTextButton.setEnabled(false);
			MainFrame.this.saveImageButton.setEnabled(false);
			
			MainFrame.this.graphicPanel.removeAllForPerformance();
			
			final String input = MainFrame.this.inputTextArea.getText().concat("\n");
			try {
				// MainFrame.this.mainClass.setSampling(SAMLPE); // DEPRECATED (done in run?)
				MainFrame.this.mainClass.execute(MainFrame.this.model, null, input);
				
				if (!MainFrame.this.mainClass.isInterrupted()) {
					// updating progress bar
					if (MainFrame.this.progressBar != null) {
						MainFrame.this.dialog.setTitle("Preparing output...");
						MainFrame.this.progressBar.setIndeterminate(true);
						MainFrame.this.progressBar.setStringPainted(false);
					}
					
					// ################# generate output ####################
					// empty results because later differs print Model and print Results
					String[] printableResult = { "", "", "", "" };
					
					// clear all windows that need it
					MainFrame.this.graphicPanel.removeAll();
					MainFrame.this.stdOutputArea.setText("");
					
					StyledDocument doc = MainFrame.this.stdOutputArea.getStyledDocument();
					
					// plain output (only genome names and scenarios
					if (MainFrame.this.mainClass.isPlainScenarioOutput()) {
						printableResult = OutputPrinter.printScenario(MainFrame.this.mainClass.getOutputData(), MainFrame.this.mainClass
								.getGlobalData().getGenomeIDs(), MainFrame.this.model);
						try {
							MainFrame.this.mainClass.clearNotifications();
							doc.insertString(doc.getLength(), printableResult[1], doc.getStyle(StyleContext.DEFAULT_STYLE));
						} catch (BadLocationException e1) {
							MainFrame.this.stdOutputArea.setText("Couldn't insert initial text into text pane.");
						}
						
						// standard output
					} else {
						printableResult = OutputPrinter.printResults(MainFrame.this.mainClass.getOutputData(), MainFrame.this.mainClass
								.getGlobalData().getGenomeIDs(), MainFrame.this.model);
						
						StyleConstants.setUnderline(
								doc.addStyle("underlined", StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE)), true);
						try {
							for (String msg : mainClass.getNotifications()) {
								doc.insertString(doc.getLength(), msg.concat(Constants.LINE_BREAK), doc.getStyle(StyleContext.DEFAULT_STYLE));
							}
							doc.insertString(doc.getLength(), Constants.LINE_BREAK, doc.getStyle(StyleContext.DEFAULT_STYLE));
							MainFrame.this.mainClass.clearNotifications();
							doc.insertString(doc.getLength(), printableResult[0], doc.getStyle("underlined"));
							doc.insertString(
									doc.getLength(),
									Constants.LINE_BREAK.concat(Constants.LINE_BREAK).concat(printableResult[3]).concat(Constants.LINE_BREAK)
											.concat(Constants.LINE_BREAK).concat(printableResult[1]), doc.getStyle(StyleContext.DEFAULT_STYLE));
						} catch (BadLocationException e1) {
							MainFrame.this.stdOutputArea.setText("Couldn't insert initial text into text pane.");
						}
						MainFrame.this.stdOutputArea.setPreferredSize(new Dimension(Short.MAX_VALUE, MainFrame.this.stdOutputArea.getHeight()));
						MainFrame.this.stdOutputArea.setSize(new Dimension(Short.MAX_VALUE, MainFrame.this.stdOutputArea.getHeight()));
						MainFrame.this.adjacenciesArea.setText(printableResult[2]);
					}
					
					// updating progress bar
					if (MainFrame.this.progressBar != null) {
						MainFrame.this.dialog.setTitle("Painting...");
						MainFrame.this.progressBar.setIndeterminate(false);
						MainFrame.this.progressBar.setValue(0);
						MainFrame.this.progressBar.setStringPainted(true);
						MainFrame.this.calcNbComparisonsToPaint();
					}
					
					// painting in graphic panel
					MainFrame.this.graphicPanel.setData(MainFrame.this.mainClass.getOutputData(), MainFrame.this.model, MainFrame.this.zoomFactor);
					MainFrame.this.graphicPanel.setSize(MainFrame.this.graphicPanel.getPreferredSize());
					if (!MainFrame.this.mainClass.isShowSteps()) {
						MainFrame.this.adjacenciesArea.setText("Showing the sorting steps is deactivated, "
								+ "switch to the genome tab in order to see distances.");
					}
				}
			} catch (InputOutputException inputException) {
				JOptionPane.showMessageDialog(MainFrame.this, inputException.getMessage(), "No valid input Error", JOptionPane.ERROR_MESSAGE);
				MainFrame.this.outputPane.setSelectedIndex(0);
				MainFrame.this.stdOutputArea.setText("");
				MainFrame.this.adjacenciesArea.setText("");
			} catch (InterruptedException e) {
				// do nothing, because it occurred because of the thread interruption
			}
			MainFrame.this.finishedCalc();
		}
	}
	
	/** Opens the dialog with the progress bar and the "stop calculation"-button. */
	private void startProgressBar() {
		final JPanel waitingPanel = new JPanel();
		waitingPanel.setLayout(new BorderLayout());
		
		progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		waitingPanel.add(progressBar, BorderLayout.CENTER);
		JButton stopButton = new JButton("Stop Calculation");
		stopButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				MainFrame.this.mainClass.setInterrupted(true);
			}
		});
		waitingPanel.add(stopButton, BorderLayout.SOUTH);
		
		JOptionPane pane = new JOptionPane(waitingPanel, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
		dialog = pane.createDialog(MainFrame.this, "Calculating...");
		dialog.setModalityType(Dialog.ModalityType.MODELESS);
		dialog.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		MainFrame.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		MainFrame.this.inputTextArea.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		// List<JButton> buttons = new ArrayList<JButton>();
		// this.getButtons(pan, buttons);
		// for (JButton button : buttons) {
		// button.setVisible(false);
		// }
		
		dialog.setSize(new Dimension(250, 87));
		dialog.setVisible(true);
	}
	
	/**
	 * Confirm dialog shown to the use in case uniform sampling is switched on,
	 * and the adjacency graph contains at least one AA-path and at least one
	 * BB-path. Then recombinations would be possible, but the uniform sampler
	 * implemented ignores recombinations.
	 */
	public void confirmNoRecombs() {
		int n = JOptionPane
				.showConfirmDialog(
						MainFrame.this,
						"The adjacency graph of the input genomes contains AA- and BB-paths.\nHowever, during sampling, the recombination of these types will be ignored.",
						"Information", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE);
		if (n == 0) {
			MainFrame.IGNORE_RECOMBS = true;
		}
	}
	
	/**
	 * Sets the number of pairwise genome comparisons for a calculation. Also
	 * initializes the progress bar values for the determinate mode, if the progress
	 * bar was initialized before.
	 * 
	 * @param nbComparisons
	 *            the total number of pairwise genome comparisons
	 */
	public void setNumberComparisons(int nbComparisons) {
		this.nbComparisons = nbComparisons;
		if (this.progressBar != null) {
			this.progressBar.setMinimum(0);
			this.progressBar.setMaximum(this.nbComparisons);
			this.progressBar.setValue(0);
			this.progressBar.setIndeterminate(false);
			this.progressBar.setStringPainted(true);
		}
	}
	
	/**
	 * Updates the value of the currently running comparison and also updates
	 * the progress bar, if the progress bar was initialized before.
	 * 
	 * @param currentComparison
	 *            the number of the currently running comparison
	 */
	public void setCurrentComparison(int currentComparison) {
		this.currentComparison = currentComparison;
		if (this.progressBar != null) {
			this.progressBar.setValue(this.currentComparison);
		}
	}
	
	private int calcNbComparisonsToPaint() {
		int nbComparisonsToPaint = 0;
		DataOutput[] outputData = MainFrame.this.mainClass.getOutputData();
		if (outputData != null) {
			for (int m = 0; m < outputData.length; ++m) {
				if (outputData[m].getIntermedGenomes() != null) {
					IntermediateGenomesGenerator[] result = outputData[m].getIntermedGenomes();
					nbComparisonsToPaint += result.length;
				}
			}
		}
		return nbComparisonsToPaint;
	}
	
	// private void getButtons(JComponent comp, List<JButton> buttons) {
	// if (comp == null) {
	// return;
	// }
	//
	// for (Component c : comp.getComponents()) {
	// if (c instanceof JButton) {
	// buttons.add((JButton) c);
	//
	// } else if (c instanceof JComponent) {
	// this.getButtons((JComponent) c, buttons);
	// }
	// }
	// }
	
	/** Method to be called when the calculations are finished or where aborted. */
	private void finishedCalc() {
		this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		MainFrame.this.inputTextArea.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		this.selectDCJ.setEnabled(true);
		this.selectRDCJ.setEnabled(true);
		this.selectDCJid.setEnabled(true);
		this.selectHP.setEnabled(true);
		this.selectINV.setEnabled(true);
		this.selectTrans.setEnabled(true);
		this.selectAll.setEnabled(true);
		this.runButton.setEnabled(true);
		this.stepsCheckBox.setEnabled(true);
		if (this.stepsCheckBox.isSelected()) {
			this.plainScenarioCheckBox.setEnabled(true);
			if (this.selectDCJ.isSelected() || this.selectAll.isSelected()) {
				this.samplingCheckBox.setEnabled(true);
			}
		}
		this.colorCheckBox.setEnabled(true);
		this.sizeSlider.setEnabled(true);
		this.northRightPanel.setEnabled(true);
		this.saveTextButton.setEnabled(true);
		this.saveImageButton.setEnabled(true);
		// this.outputPane.setSelectedIndex(0); // commented it out because user might not want to switch back to graphical output
		this.outputPane.repaint();
		// if (this.graphicPanel.getComponentCount() > 0) {
		// this.outputPane.setSelectedIndex(2);
		// }
		
		this.dialog.dispose();
		this.dialog = null;
		this.mainClass.setInterrupted(false);
	}
}