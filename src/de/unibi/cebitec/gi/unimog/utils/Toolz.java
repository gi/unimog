package de.unibi.cebitec.gi.unimog.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Random;

import de.luschny.math.arithmetic.Xint;
import de.luschny.math.factorial.FactorialPrimeSwing;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.VertexInfo;

/**************************************************************************
*   Copyright (C) 2010 by Rolf Hilker                                     *
*   rhilker   a t  cebitec.uni-bielefeld.de                               *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

/**
 * @author -Rolf Hilker-
 * 
 * Class containing some basic tools.
 */
public class Toolz {

    public static final Color colorBackg = Color.getHSBColor(0.2f, 0.125f, 1f);
    public static int colorFactor  = 0;
    public static int colorFactor2 = 90;
    public static int colorFactor3 = 0;
    public static int colorFactor4 = 90;

    /**
     * Calculates the "Gaußsche Summenformel" or "Kleiner Gauß", which means it returns the sum of the first consecutive
     * numbers from 1 to finalNumber.<br>
     * E.g.: finalNumber = 5: 1 + 2 + 3 + 4 + 5 = 15.
     * @param finalNumber the last number until the sum should continue.
     * @return the sum of the first consecutive numbers from 1 to finalNumber.
     */
    public static int gaussianFormula(final int finalNumber) {
        return (finalNumber * (finalNumber + 1)) / 2;
    }

    /**
     * Generates a new color for the background according to the chosen color mode
     * by always increasing the r g b values in a different frequency.
     * @return the new color
     */
    // Color getColor(int i, boolean light){
    // int add = light?127:0;
    // Color c;
    // c = new Color(add+(4&i)*32,add+(2&i)*64,add+(1&i)*128);
    // if (Settings.colorMode==0 && light) c=Color.WHITE;
    // return c;
    public static Color getBackColor() {
        // Color color = Toolz.colorBackg;
        // if (MainFrame.colorMode == Constants.OUTPUT_2){
        // color = new Color(Toolz.colorFactor, 145, Toolz.colorFactor2);
        // } else {
        // return color;
        // }
        final Color color = new Color(120, Toolz.colorFactor3, Toolz.colorFactor4);
        Toolz.colorFactor3 += 38;
        if (Toolz.colorFactor3 > 255) {
            Toolz.colorFactor3 -= 255;
        }
        Toolz.colorFactor4 += 86;
        if (Toolz.colorFactor4 > 255) {
            Toolz.colorFactor4 -= 255;
        }

        return color;
    }

    /**
     * Generates a new color by always increasing the r g b values in a different frequency.
     * @return the new color
     */
    public static Color getColor() {
        final  Color color = new Color(Toolz.colorFactor, 120, Toolz.colorFactor2);
        Toolz.colorFactor += 38;
        if (Toolz.colorFactor > 255) {
            Toolz.colorFactor = Toolz.colorFactor - 255;
        }
        Toolz.colorFactor2 += 86;
        if (Toolz.colorFactor2 > 255) {
            Toolz.colorFactor2 = Toolz.colorFactor2 - 255;
        }
        return color;
    }

    /**
     * Returns the font used for the output. It fst tries to assign "Lucida Console".<br>
     * If that font doesn't exist on the system "Courier New" is tried out. If that also
     * doesn't exist the system default "Monospaced" font is used.
     * @return the font to use: plain and size 12
     */
    public static Font getFontOutput() {
        final HashMap<TextAttribute, Object> textAttribute = new HashMap<TextAttribute, Object>();
        // textAttribute.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        Font font = new Font("Lucida Console", Font.PLAIN, 12).deriveFont(textAttribute);
        if (!font.getFontName().equals("Lucida Console")) {
            font = new Font("Courier New", Font.PLAIN, 12).deriveFont(textAttribute);
            if (!font.getFontName().equals("Courier New")) {
                font = new Font("Monospaced", Font.PLAIN, 12).deriveFont(textAttribute);
            }
        }
        return font;
    }

    /**
     * Abstracts a gene to a positive value of 2*gene or 2*gene-1. If the gene is a left telomere then its position is
     * right in the adjacency and left for a right telomere. For all inner adjacencies the left gene is on the left side
     * of the adjacency and therefore a head if the gene is in reverse orientation and a tail otherwise.<br>
     * The same holds for the right gene of an adjacency just in the other way round.
     * 
     * @param gene
     *            the gene to be abstracted
     * @param left
     *            if the gene is the left or right one in adjacency
     * @return the abstracted gene
     */
    public static int abstractGene(final int gene, final boolean left) {
        /* Entry is either right, thus a tail if > 0 or left, thus a tail if < 0 */
        if (gene <= 0 && !left || gene >= 0 && left) {
            return 2 * Math.abs(gene);
        }
        // else : Entry is either right, thus a tail if < 0 or left, thus a tail if > 0
        return 2 * Math.abs(gene) - 1;
    }
    
    /**
     * Parses the adjacency graph. The result contains at each index (which represents a head or tail of a gene) its
     * neighbor and if there it is a telomere it contains itself.
     * @param genome Genome to parse
     * @param genomeLength Length of the genome as int value
     * @return list of adjacent extremities
     */
    public static int[] parseAdjacencies(final Genome genome, final int genomeLength) {

        // genome: -1 3 4| 2 5| -6,-7| array from 1-14 for 7 genes!
        // head = odd, tail = even, entry 0 is empty!
        final int[] genomeAdjacencies = new int[genomeLength];

        for (int i = 0; i < genome.getNumberOfChromosomes(); ++i) {
            final int[] chromosome = genome.getChromosome(i).getGenes();
            final int length = chromosome.length;
            int geneNumberLeft;
            int geneNumberRight;

            // Part for processing telomeres, thus we choose "false" as input for "abstractGene"
            geneNumberLeft = Toolz.abstractGene(chromosome[0], false);
            geneNumberRight = Toolz.abstractGene(chromosome[length - 1], true); // Right telomere is left in adjacency:
                                                                                // true

            if (genome.getChromosome(i).isCircular()) {
                genomeAdjacencies[geneNumberLeft] = geneNumberRight; // save
                genomeAdjacencies[geneNumberRight] = geneNumberLeft;
                // can already save telomere entries
            } else {
                genomeAdjacencies[geneNumberLeft] = geneNumberLeft;
                genomeAdjacencies[geneNumberRight] = geneNumberRight;
            }

            for (int j = 1; j < chromosome.length; ++j) {

                geneNumberLeft = Toolz.abstractGene(chromosome[j - 1], true);
                geneNumberRight = Toolz.abstractGene(chromosome[j], false);

                genomeAdjacencies[geneNumberLeft] = geneNumberRight; // save
                genomeAdjacencies[geneNumberRight] = geneNumberLeft;
            }
        }
        return genomeAdjacencies;
    }
    

    /**
     * Uniform sampling of a very large number which is given in {@link Xint}. A number is sampled between such that 0
     * is included and <code>upperBound</code> is excluded.
     * 
     * @param upperBound
     *            of sampling (excluding)
     * @return uniformly sampled {@link Xint}
     */
    public static Xint generateRandomXint(final Xint upperBound) {
        final String sumString = upperBound.toString();
        int lengthOfUpperBound = sumString.length();
        if (lengthOfUpperBound < 10) {
            // just sample once;
            return Xint.valueOf((new Random()).nextInt(Integer.parseInt(sumString)));
        }
        // otherwise at least 1 bucket of size 9
        int numberOf9Buckets = (int) Math.floor(lengthOfUpperBound / 9.0);
        int remainderDigits = lengthOfUpperBound % 9;
        if (remainderDigits == 0) {
            --numberOf9Buckets;
            remainderDigits = 9;
        }
        int firstBucketInterval = Integer.parseInt(sumString.substring(0, 9));
        Xint generatedRandomXint;
        boolean reject = true;

        resample: do {
            generatedRandomXint = Xint.ZERO;

            // ##### sample first bucket #####
            int firstRandom = (new Random()).nextInt(firstBucketInterval + 1);
            while (firstRandom > firstBucketInterval) {
                firstRandom = (new Random()).nextInt(firstBucketInterval + 1);
            }
            // used our first 9-bucket
            generatedRandomXint = Xint.valueOf(firstRandom);
            generatedRandomXint = generatedRandomXint.multiply((int) Math.pow(10, remainderDigits));
            for (int i = 0; i < numberOf9Buckets - 1; i++) {
                generatedRandomXint = generatedRandomXint.multiply(1000000000);
            }

            // ##### continue sampling all other buckets #####
            if (firstRandom == firstBucketInterval) {
                int bucketsSampled = 1;
                while (bucketsSampled < numberOf9Buckets) {
                    int nextBucketInterval = Integer.parseInt(sumString.substring(bucketsSampled * 9,
                        ((bucketsSampled + 1) * 9)));
                    int next9digitBlock = ((new Random().nextInt(1000000000)));
                    if (next9digitBlock > nextBucketInterval) {
                        continue resample; // reject
                    }

                    generatedRandomXint = generatedRandomXint.add(Toolz.sample9digitBlock(numberOf9Buckets
                        - bucketsSampled, remainderDigits, next9digitBlock));
                    ++bucketsSampled;
                    if (next9digitBlock < nextBucketInterval) {
                        return Toolz.dontCare(generatedRandomXint, numberOf9Buckets - bucketsSampled, remainderDigits);
                    } // else continue careful sampling
                }
                // if we are here, then we scratch upper border, sample last digits
                generatedRandomXint = Toolz.dontCare(generatedRandomXint, 0, remainderDigits);
                if (Toolz.aKleinerB(generatedRandomXint, upperBound)) {
                    reject = false; // got our number
                }
            } else { // no comparison of follow-ups necessary
                generatedRandomXint = Toolz.dontCare(generatedRandomXint, numberOf9Buckets - 1, remainderDigits);
                reject = false;
            }
            // else reject

        } while (reject);
        return generatedRandomXint;
    }

    private static Xint dontCare(Xint generatedRandomXint, final int numberOf9Buckets, final int remainderDigits) {
        Xint remainderNumber = generatedRandomXint;
        for (int blockID = numberOf9Buckets; blockID > 0; blockID--) {
            remainderNumber = remainderNumber.add(Toolz.sample9digitBlock(blockID, remainderDigits));
        }
        // sample last digits
        Xint temp = Xint.valueOf((new Random()).nextInt((int) Math.pow(10, remainderDigits)));
        return remainderNumber.add(temp);
    }

    private static Xint sample9digitBlock(final int blockID, final int remainderDigits, final int... next9digitBlock) {
        Xint temp = Xint.valueOf((new Random().nextInt(1000000000)));
        if (next9digitBlock.length > 0) {
            temp = Xint.valueOf(next9digitBlock[0]);
        }
        for (int i = 1; i < blockID; i++) {
            temp = temp.multiply(1000000000);
        }
        return temp.multiply((int) Math.pow(10, remainderDigits));
    }

    /**
     * Answers the inequality: <b>a < b</b> ?
     * 
     * @param a
     *            left of inequality sign
     * @param b
     *            right of inequality sign
     * @return <code>true</code> if <b>a</b> is numerically smaller than <b>b</b>, <code>false</code> otherwise (a >=
     *         b).
     */
    public static boolean aKleinerB(final Xint a, final Xint b) {
        if (a.compareTo(b) < 0) {
            return true;
        }
        return false;
    }

    /**
     * Answers the inequality: <b>a <= b</b>?
     * 
     * @param a
     *            left of inequality sign
     * @param b
     *            right of inequality sign
     * @return <code>true</code> if <b>a</b> is numerically smaller than or equal to <b>b</b>, <code>false</code>
     *         otherwise (a > b).
     */
    public static boolean aKleinerGleichB(final Xint a, final Xint b) {
        if (a.compareTo(b) > 0) {
            return false;
        }
        return true;
    }

    /**
     * Samples two vertices within a given {@link ComponentSample} such that these two vertices are (2*j) edges apart.
     * The two components produced will have distance (d-j) and (j-1).<br>
     * In the return array the two sampled vertices, four extremities of these as well as the distance of the produced
     * "left" component are stored. <br>
     * A special case is a BB-path where the second sampled vertex is the last one, such that the BB-path will be split
     * into 2 AB-paths.
     * 
     * @param sampledDistance
     *            distance of considered component
     * @param j
     *            split group ID
     * @param sampledComponent
     *            component for which to sample the adjacencies
     * @param extremitiesA
     *            adjacency array of genome A
     * @param extremitiesB
     *            adjacency array of genome B
     * @return Collection of sampled vertices, extremities and the new distance for the left component part
     */
    public static VertexInfo sampledAdjacencies(final int sampledDistance, final int j,
        final ComponentSample sampledComponent, final int[] extremitiesA, final int[] extremitiesB) {

        if (sampledComponent.getStructure() == Structure.NONE)
            throw new IllegalArgumentException("Structure of component is unknown. Cannot perform a walkthrough");

        int firstVertex = (new Random()).nextInt(sampledDistance + 1);// [0,1,2,...sampledDistance]
        int secondVertex = ((firstVertex + sampledDistance + 1) + ((new Random().nextBoolean() ? j : -j)))
            % (sampledDistance + 1);
        // [ sampledDistance + j, sampledDistance - j]

        if (firstVertex > secondVertex) {
            final int temp = secondVertex;
            secondVertex = firstVertex;
            firstVertex = temp;
        } // firstVertex is always before secondVertex
          // => firstVertex [0,1,2,...sampledDistance-1], secondVertex [1,2, ... sampledDistance]
          // sampledValues[5] = firstVertex; sampledValues[6] = secondVertex;

        VertexInfo vi;
        if ((secondVertex - firstVertex) == j) { // newDistance = d-j; c_b distance = j-1
            vi = new VertexInfo(sampledDistance - j, firstVertex, secondVertex);// sampledValues[0] = sampledDistance-j;
        } else { // newDistance = j-1; c_b distance = d-j
            vi = new VertexInfo(j - 1, firstVertex, secondVertex);// sampledValues[0] = j - 1;
        }

        // now find adjacencies, for vertex 0 it is:
        int currentVertex = 0;
        int p = sampledComponent.getStartIndex();
        int q = extremitiesA[p];
        // if AA-/AB-path this will be a telomere °q => p=q
        // if CYCLE p=startIndex, q = endindex??

        // now go 2 edges to next vertex, update p and q
        while (currentVertex < firstVertex) {
            // neighbour of q in B is next p
            p = extremitiesB[q]; // = next p

            // follow second extremity of adj
            // walked next edge to pq where p = neighbourInB and q = partner
            q = extremitiesA[p];
            ++currentVertex;
        } // just walked 2 edges -> jumped 1 vertex

        vi.setFirstAdjacency(p, q); // sampledValues[1] = p; // sampledValues[2] = q;
        if (p == 0 || q == 0) {
            System.err.println("EXTREMITY is 0");
        }
        // if finished we walked 2edges for each vertex index in A
        if (sampledComponent.getStructure() == Structure.BB && secondVertex == sampledDistance) {
            return vi;
        }

        while (currentVertex < secondVertex) {
            // neighbour of q in B is next p
            p = extremitiesB[q]; // = next p

            // follow second extremity of adj
            // walked next edge to pq where p = neighbourInB and q = partner
            q = extremitiesA[p];
            ++currentVertex;
        }

        vi.setSecondAdjacency(p, q);// sampledValues[3] = p; // sampledValues[4] = q;
        if (p == 0 || q == 0) {
            System.err.println("EXTREMITY is 0");
        }
        return vi;
    }

    /**
     * Calculates the number of scenarios of a single representative for component distance <code>d_i</code> and split
     * group id <code>j</code>.
     * 
     * @param p_AG
     *            number of scenarios for current adjacency graph
     * @param d_AG
     *            DCJ distance of current adjacency graph
     * @param d_i
     *            distance of considered distance group
     * @param j
     *            split group ID
     * @return number of scenarios for given parameters
     */
    public static Xint getScenariosOfRepresentative(Xint p_AG, int d_AG, int d_i, final int j) {
        Xint p_ij = p_AG;
        if (d_i == 1) { // remove d_i from formula
            return (p_ij.divide(d_AG));
        }
        // every other case is d_i > 1

        if (d_i == 2) { // d_a only matters little, d_b = 0, j must be 1
            return (((p_ij.multiply(2)).divide(3)).divide(d_AG));
        }
        if (j == 1) {
            // adapt formula where d_a = d_i-1 and d_b=0
            Xint zaehler = p_ij.multiply((Xint.valueOf(d_i)).toPowerOf(d_i - 1));
            final Xint nenner = ((Xint.valueOf(d_i + 1)).toPowerOf(d_i - 1)).multiply(d_AG);
            return zaehler.divide(nenner);
            // return ((this.scenariosXint.multiply((Xint.valueOf(groupDistance)).toPowerOf(groupDistance - 1)))
            // .divide((Xint.valueOf(groupDistance + 1)).toPowerOf(groupDistance - 1))).divide(this.d);
        }

        // every other case has d_a and d_b > 0
        int d_a = d_i - j;
        int d_b = j - 1;

        // update product of d_a and d_b
        final Xint prod_a = (Xint.valueOf(d_a + 1)).toPowerOf(d_a - 1);
        p_ij = p_ij.multiply(prod_a);
        final Xint prod_b = (Xint.valueOf(j)).toPowerOf(j - 2);
        p_ij = p_ij.multiply(prod_b);

        for (int i = d_a + 1; i <= d_i; i++) {
            p_ij = p_ij.multiply(i);
        }
        Xint nenner = new FactorialPrimeSwing().factorial(d_b);
        nenner = nenner.multiply(d_AG);
        final Xint prod_i = (Xint.valueOf(d_i + 1)).toPowerOf(d_i - 1);
        nenner = nenner.multiply(prod_i);
        // p_ij = p_ij.divide(this.d);
        // p_ij = p_ij.divide(new FactorialPrimeSwing().factorial(d_b));
        return p_ij.divide(nenner);
    }

//    public static Xint calculateNewScenarios(final OrderedComponentList orderedC, final int d_i, final int j,
//        final int d) {
//        final int d_a = d_i - j;
//        final int d_b = j - 1;
//        final OrderedComponentList tempC = orderedC.clone();
//        final ComponentSample tempcomp = tempC.getComponents(d_i).get(0);
//        tempC.removeElement(d_i, tempcomp);
//        tempC.addElement(d_a, new ComponentSample(0, 1, Structure.BB, false));
//        tempC.addElement(d_b, new ComponentSample(2, 4, Structure.AA, false));
//        Xint prod = Xint.ONE;
//        Xint nenner = Xint.ONE;
//        for (Integer dist : tempC.getDistanceList()) {
//            if (dist < 2)
//                continue;
//            for (int i = 0; i < tempC.getComponents(dist).size(); i++) {
//                nenner = nenner.multiply(new FactorialPrimeSwing().factorial(dist));
//                final Xint prod_i = (Xint.valueOf(dist + 1)).toPowerOf(dist - 1);
//                prod = prod.multiply(prod_i);
//            }
//        }
//        final Xint zaehler = ((new FactorialPrimeSwing().factorial(d)));
//        Xint ergebnis = zaehler.multiply(prod);
//        ergebnis = ergebnis.divide(nenner);
//        // Xint ergebnis = (((new FactorialPrimeSwing().factorial(zaehlerSumme))).multiply(prod)).divide(nenner);
//        System.out.println(">> " + ergebnis + "\td: " + (d) + "\t" + tempC.printElementList());
//        return ergebnis;
//    }
}