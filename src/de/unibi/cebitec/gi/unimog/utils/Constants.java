package de.unibi.cebitec.gi.unimog.utils;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker                                     *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/**
 * @author -Rolf Hilker-
 *
 * This class contains all global constants used in this framework.
 */
public final class Constants {

	/** Maximum number of input files **/
	public static final int MAX_INPUT_FILE_NBR = 100;

	/** Line break string "\n". */
	public static final String LINE_BREAK = "\n";

	/** Line break for output: "\r\n". */
	public static final String LINE_BREAK_OUTPUT = "\r\n";

	/** Error String. */
	public static final String ERROR = "Error: At least 2 genomes necessary";

	/** Number representing an error while calculating a distance. */
	public static final int ERROR_NUM = -1;

	public static final String CIRCULAR_CHR = ")";

	public static final String LINEAR_CHR = "|";

	/** Genome example for gui. */
	public static final String GENOME_EXAMPLE = ">Genome_A\n1 2 -3 4 5 6 | 7 9 -10 11 -12 13 -14 15 -8 16 | 17 -18 19 )".concat(
			"\n\n>Genome_B\n1 2 3 4 5 6 | 7 8 9 10 11 12 13 14 15 16 | 17 18 19 )\n".concat(
					"\n>Genome_C\ngene1 gene6 gene9 -gene5 -gene4 | -gene3 -gene2 gene7 gene8 |\n")).concat(
			"\n>Genome_D\ngene1 -gene2 gene6 gene7 gene3 | gene4 gene5 -gene9 gene8 |\n");

	/** Number representing the open file chooser event. */
	public static final int OPEN = 0;

	/** Number representing the save text event. */
	public static final int SAVE_TXT = 1;

	/** Number representing the save image event. */
	public static final int SAVE_IMG = 2;

	/** Output Style 1: Just Black & white with colored fragments. */
	public static final int OUTPUT_1 = 0;

	/** Output Style 2: Colored Chromosomes. */
	public static final int OUTPUT_2 = 1;

	/** Zoom factor for output = 0.7. */
	public static final double ZOOM_FACTOR1 = 0.7143;

	/** Zoom factor for output = 1. */
	public static final double ZOOM_FACTOR2 = 1;

	/** Zoom factor for output = 1.5. */
	public static final double ZOOM_FACTOR3 = 1.5714;

	/** 'DCJ' String. */
	public static final String DCJ_ST = "DCJ";

	/** 'HP' String. */
	public static final String HP_ST = "HP";

	/** 'Translocation' String. */
	public static final String TRANS_ST = "Translocation";

	/** 'Inversion' String. */
	public static final String INV_ST = "Inversion";

	/** 'Restricted DCJ' String. */
	public static final String RDCJ_ST = "Restricted DCJ";

	/** 'DCJ InDel' String. */
	public static final String DCJ_INDEL_ST = "DCJ-indel";

	/** int for boolean 'true' = 1. */
	public static final int TRUE = 1;

	/** int for boolean 'false' = 0. */
	public static final int FALSE = 0;

	/** number of extremities of a cut = 4. */
	public static final int NB_EXTREMITIES = 4;

	/** Number of a PI cap = 1. */
	public static final int PI_CAP = 1;

	/** Number of an L cap = 2. */
	public static final int L_CAP = 2;

	/** String displaying the options and help for console input. */
	public static final String CONSOLE_OPT = "usage: java -jar UniMoG.jar -m=Integer [-s] [-d] [-p] <GenomesInputFiles>\n"
			+ " -m=M\t\tspecify model used (M: 1 = "
			+ Constants.DCJ_ST
			+ ", 2 = "
			+ Constants.RDCJ_ST
			+ ", 3 = "
			+ Constants.HP_ST
			+ ", 4 = "
			+ Constants.INV_ST
			+ ", 5 = "
			+ Constants.TRANS_ST
			+ ",  6 = "
			+ Constants.DCJ_INDEL_ST
			+ ", 7 = All scenarios at once)\n"
			+ " -s\t\tturns on uniform sampling (only implemented for DCJ scenario)\n"
			+ " -d\t\tturns off sorting scenario calculation (only distance is given)\n"
			+ " -p\t\tif sorting scenario is calculated, then only a plain version is printed\n"
			+ " -z\t\tif zip file is used\n"
			+ " -h,--help,\tdisplays help text\n"
			+ "If no correct parameters are handed over the software starts with a graphical "
			+ "user interface.\r\nIf more help is needed, start the program "
			+ "in GUI mode first and click the 'Help' button.";


	/** Split group number for small distances */
	public static int[] SMALL_SPLIT_GROUP_NUMBERS = new int[] { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7,
			8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15 };

	/**
	 * Do not instantiate.
	 */
	private Constants(){

	}
}
