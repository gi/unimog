package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.Node;
import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.NodeType;
import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.VisitorUtils;

import java.util.*;
import java.util.stream.Collectors;

public class ContractedCompTree {

    /**
     * Root node of contracted component tree
     **/
    private Node cctRoot;

    /**
     * List of all white leaves
     **/
    private List<Node> whiteLeaves;

    /**
     * Number of leaves that do not belong to a long path or have a branching node as parent
     **/
    private int nbrShortLeaves;

    /**
     * List of grey leaves (only used for HP-model)
     **/
    private List<Node> greyLeaves;

    /**
     * Boolean to indicate if there is dangerous node (only used for HP-model)
     **/
    private boolean isDangerous;

    /**
     * Boolean to indicate if there is a short leaf that is dangerous (only used for HP-model)
     **/
    private boolean hasShortDangerousNode;

    /**
     * Index of white root node
     **/
    private int whiteRootNodeIdx = -1;

    /**
     * Boolean to indicate if there is a white root node
     **/
    private boolean allInOne = true;

    public ContractedCompTree(Node root) {
        this.whiteLeaves = new ArrayList<>();
        this.greyLeaves = new ArrayList<>();
        this.cctRoot = root;
        if (!root.isLeaf()) {
            setNodeIndexDFT(); // set node index by using depth first traversal to address components during sorting
            contractTree();
            countShortLeaves();
        }
    }

    /**
     * Set node index by traversing tree using depth first traversal.
     **/
    private void setNodeIndexDFT() {
        Stack<Node> undiscovered = new Stack<>();
        List<Node> children = this.cctRoot.getNodeChildren();
        int nodeIndex = 0;

        // use lastNode to prevent skipping while loop because of less than two chromosomes
        Node lastNode = new Node(NodeType.BLACK, null);
        undiscovered.push(lastNode);

        // traverse tree by using depth first traversal
        for (int i = children.size() - 1; i >= 1; i--) {
            undiscovered.push(children.get(i));
        }
        Node currentChild = children.get(0);
        while (!undiscovered.isEmpty()) {
            if (!currentChild.getNodeType().equals(NodeType.SQUARE)) {
                currentChild.setIndex(nodeIndex);
                nodeIndex++;
            }
            if (currentChild.isLeaf()) {
                currentChild = undiscovered.pop();
                if (currentChild == lastNode) {
                    break;
                }
            } else if (currentChild.getNodeChildren().size() == 1) {
                currentChild = currentChild.getNodeChildren().get(0);
            } else {
                for (int i = currentChild.getNodeChildren().size() - 1; i >= 1; i--) {
                    undiscovered.push(currentChild.getNodeChildren().get(i));
                }
                currentChild = currentChild.getNodeChildren().get(0);
            }
        }
    }

    /**
     * Contracts component tree by merging or cutting black/square nodes.
     **/
    private void contractTree() {
        List<Node> children = new ArrayList<>(this.cctRoot.getNodeChildren());
        int nbrContractionsBefore = 0;
        int nbrContractionsAfter = 0;
        boolean wasContracted = true;
        while (wasContracted) {
            for (Node child : children) {
                Node parent = child.getParent();
                if (child.hasDarkColor()) {
                    // merge two adjacent black nodes
                    if (parent.hasDarkColor()) {
                        parent.getNodeChildren().remove(child);
                        parent.addAllChildren(child.getNodeChildren());
                        nbrContractionsAfter++;
                        // cut black node and pass black node's children to parent
                    } else if (child.getNodeChildren().size() == 1) {
                        parent.getNodeChildren().remove(child);
                        parent.addChild(child.getNodeChildren().get(0));
                        nbrContractionsAfter++;
                        // black leaves are cut
                    } else if (child.isLeaf()) {
                        parent.getNodeChildren().remove(child);
                        nbrContractionsAfter++;
                    }
                }
                if (!child.hasDarkColor() && (child.isLeaf() || VisitorUtils.becomesALeaf(child))) {
                    if (child.isWhiteNode() && !this.whiteLeaves.contains(child)) {
                        this.whiteLeaves.add(child);
                    }
                }
            }
            // collect all children of next depth
            children = children.stream().map(Node::getNodeChildren).flatMap(List::stream).collect(Collectors.toList());

            if (children.isEmpty()) {
                children = new ArrayList<>(this.cctRoot.getNodeChildren());
                // stop contraction, if tree was not contracted anymore
                if (nbrContractionsAfter - nbrContractionsBefore == 0) {
                    wasContracted = false;
                }
                nbrContractionsBefore = nbrContractionsAfter;
            }

        }

        // run last contraction, if black cctRoot has two white/grey children where one is a leaf
        if (this.cctRoot.getNodeChildren().size() == 2 && this.cctRoot.hasDarkColor()) {
            boolean replaceRootByLeftChild = this.cctRoot.getNodeChildren().get(0).isLeaf()
                    && (this.cctRoot.getNodeChildren().get(0).isWhiteNode()
                    || this.cctRoot.getNodeChildren().get(0).isGreyNode())
                    && (this.cctRoot.getNodeChildren().get(1).isWhiteNode()
                    || this.cctRoot.getNodeChildren().get(1).isGreyNode());
            if (replaceRootByLeftChild) {
                Node newChild = this.cctRoot.getNodeChildren().get(1);
                this.cctRoot = this.cctRoot.getNodeChildren().get(0);
                this.cctRoot.setParent(null);
                this.cctRoot.addChild(newChild);
                newChild.setParent(this.cctRoot);
            } else {
                boolean replaceRootByRightChild = this.cctRoot.getNodeChildren().get(1).isLeaf()
                        && (this.cctRoot.getNodeChildren().get(1).isWhiteNode()
                        || this.cctRoot.getNodeChildren().get(1).isGreyNode())
                        && (this.cctRoot.getNodeChildren().get(0).isWhiteNode()
                        || this.cctRoot.getNodeChildren().get(0).isGreyNode());
                if (replaceRootByRightChild) {
                    Node newChild = this.cctRoot.getNodeChildren().get(0);
                    this.cctRoot = this.cctRoot.getNodeChildren().get(1);
                    this.cctRoot.setParent(null);
                    this.cctRoot.addChild(newChild);
                    newChild.setParent(this.cctRoot);
                }
            }
        }

        // remove black root with only child
        while (this.cctRoot.getNodeChildren().size() == 1 && this.cctRoot.hasDarkColor()) {
            // replace black or grey root node by next white or branching node
            this.cctRoot = this.cctRoot.getNodeChildren().get(0);
            this.cctRoot.setParent(null);
        }

        // count grey leaves
        this.greyLeaves = this.cctRoot.getNodeChildren().stream().filter(n -> n.isLeaf() && n.isGreyNode()).collect(Collectors.toList());
        if (this.cctRoot.isGreyNode()) {
            this.greyLeaves.add(this.cctRoot);
        }

        // create T_w (Tree without grey nodes)
        // if dangerous nodes exist, set root to next white node or branching node
        while (this.cctRoot.getNodeChildren().size() == 1 && this.cctRoot.isGreyNode()) {
            // replace black or grey root node by next white or branching node
            this.cctRoot = this.cctRoot.getNodeChildren().get(0);
            this.cctRoot.setParent(null);
        }


        if (this.cctRoot.isWhiteNode()) {
            this.allInOne = false;
        }

        // check if white root node is dangerous
        // root must not be a leaf, because white leaves were already collected
        if (!this.cctRoot.isLeaf() && this.cctRoot.isWhiteNode()) {
            this.whiteRootNodeIdx = this.cctRoot.getIndex();
            if (!this.whiteLeaves.contains(this.cctRoot)) {
                this.whiteLeaves.add(this.cctRoot);
            }
            this.isDangerous = true;
        }
    }

    /**
     * Counts the number of short leaves.
     * A leaf is short if it a white leaf is adjacent to a black node in the contracted component tree.
     * All black nodes are real branching nodes in the contracted component tree.
     */
    private void countShortLeaves() {
        int nbrShortLeaves = 0;

        if (this.cctRoot.isLeaf() && this.cctRoot.isWhiteNode()) {
            this.nbrShortLeaves = 1;
            return;
        }

        for (Node leaf : this.whiteLeaves) {
            // if root is white and its only child is black, increase number of short leaves
            if (leaf.isRoot() && leaf.isWhiteNode()) {
                if (leaf.getNodeChildren().size() == 1 && leaf.getNodeChildren().get(0).hasDarkColor()) {
                    // leaf was inner white node which became a leaf after contraction
                    nbrShortLeaves++;
                    if (!leaf.getNodeChildren().get(0).isLeaf()) {
                        this.hasShortDangerousNode = true;
                    }
                }
                continue;
            }
            Node parent = leaf.getParent();
            // if parent is black, increase number of short leaves
            if (!parent.isRoot() && parent.hasDarkColor()) {
                nbrShortLeaves++;
            }

            // if parent is branching node or has only a single white leaf-child, increase number of short leaves
            if (parent.isRoot() && parent.hasDarkColor() && parent.getNodeChildren().size() != 2) {
                nbrShortLeaves++;
            }
        }

        this.nbrShortLeaves = nbrShortLeaves;
    }

    /**
     * Checks, if white leaf belongs to a super hurdle/long path
     *
     * @param whiteLeaf to test
     * @return true, if parent or child of of leaf is white
     **/
    public boolean belongsToSuperHurdle(Node whiteLeaf) {
        return (whiteLeaf.isRoot()
                && whiteLeaf.getNodeChildren().size() == 1
                && whiteLeaf.getNodeChildren().get(0).isWhiteNode())
                || !whiteLeaf.isRoot() && whiteLeaf.getParent().isWhiteNode();
    }

    public ArrayList<Node> getWhiteLeaves() {
        return new ArrayList<>(whiteLeaves);
    }

    public int getNbrWhiteLeaves() {
        return whiteLeaves.size();
    }

    public int getNbrGreyLeaves() {
        return this.greyLeaves.size();
    }

    public int getNbrShortLeaves() {
        return nbrShortLeaves;
    }

    public boolean isDangerous() {
        return isDangerous;
    }

    public boolean hasShortDangerousNode() {
        return hasShortDangerousNode;
    }

    public int getWhiteRootNodeIdx() {
        return whiteRootNodeIdx;
    }

    public boolean isAllInOne() {
        return allInOne;
    }
}
