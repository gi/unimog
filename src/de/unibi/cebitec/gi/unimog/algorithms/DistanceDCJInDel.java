/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.datastructure.Data;
import de.unibi.cebitec.gi.unimog.datastructure.IAdditionalData;
import de.unibi.cebitec.gi.unimog.datastructure.LabeledAdjacencyGraph;
import de.unibi.cebitec.gi.unimog.datastructure.Pair;
import de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree.MultifurcatedTree;
import de.unibi.cebitec.gi.unimog.utils.Constants;

/**
 * Class for calculating the DCJ-InDel-Distance for two given Genomes.
 *
 * @author Malte Mattheis
 */
public class DistanceDCJInDel implements IDistance {

    private int lambda;
    private int aaA;
    private int aaB;
    private int aaAB;
    private int bbA;
    private int bbB;
    private int bbAB;
    private int abAB;
    private int abBA;

    @Override
    public int calculateDistance(Data data, IAdditionalData additionalData) {
        LabeledAdjacencyGraph g = (LabeledAdjacencyGraph) additionalData;
        lambda = g.getLambda();
        aaA = g.getAaA();
        aaB = g.getAaB();
        aaAB = g.getAaAB();
        bbB = g.getBbB();
        bbA = g.getBbA();
        bbAB = g.getBbAB();
        abAB = g.getAbAB();
        abBA = g.getAbBA();
        int p, q, t, s, m, n;
        p = calculateP();
        q = calculateQ();
        t = calculateT();
        s = calculateS();
        Pair<Integer, Integer> mn = calculateMN();
        m = mn.getFirst();
        n = mn.getSecond();
//        System.out.println(p + " " + q + " " + t + " " + s + " " + m + " " + n);
        return data.getAdjGraph().getDCJdistance() + lambda - 2 * p - 3 * q - 2 * t - s - 2 * m - n;
    }

    @Override
    public int calculateDistance(Data data, IAdditionalData additionalData, MultifurcatedTree componentTree) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * 
     * @return Calculated value of P
     */
    private int calculateP() {
        int p = Math.min(aaAB, bbAB);
        aaAB -= p;
        bbAB -= p;
        return p;
    }

    /**
     * 
     * @return Calculated value of Q 
     */
    private int calculateQ() {
        int q = 0;
        if (aaAB > 0) {
            q = (int) Math.min(Math.floor(aaAB / 2.0), Math.min(bbA, bbB));
            aaAB -= 2 * q;
            bbA -= q;
            bbB -= q;
        } else if (bbAB > 0) {
            q = (int) Math.min(Math.floor(bbAB / 2.0), Math.min(aaA, aaB));
            bbAB -= 2 * q;
            aaA -= q;
            aaB -= q;
        }
        return q;
    }

    /**
     * 
     * @return calculated value of T
     */
    private int calculateT() {
        int t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t7 = 0, t8 = 0;
        if (aaAB > 0) {
            t5 = (int) Math.min(aaAB, Math.min(abAB, bbA));
            aaAB -= t5;
            abAB -= t5;
            bbA -= t5;
            t6 = (int) Math.min(aaAB, Math.min(abBA, bbB));
            aaAB -= t6;
            abBA -= t6;
            bbB -= t6;
            t1 = (int) Math.min(Math.floor(aaAB / 2.0), bbA);
            aaAB -= 2 * t1;
            bbA -= t1;
            t2 = (int) Math.min(Math.floor(aaAB / 2.0), bbB);
            aaAB -= 2 * t2;
            bbB -= t2;
        } else if (bbAB > 0) {
            t7 = (int) Math.min(bbAB, Math.min(abBA, aaA));
            bbAB -= t7;
            abBA -= t7;
            aaA -= t7;
            t8 = (int) Math.min(bbAB, Math.min(abAB, aaB));
            bbAB -= t8;
            abAB -= t8;
            aaB -= t8;
            t3 = (int) Math.min(Math.floor(bbAB / 2.0), aaA);
            bbAB -= 2 * t3;
            aaA -= t3;
            t4 = (int) Math.min(Math.floor(bbAB / 2.0), aaB);
            bbAB -= 2 * t4;
            aaB -= t4;
        }

        return t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8;
    }

    /**
     * 
     * @return calculated value of S
     */
    private int calculateS() {
        int s1 = 0;
        int s2 = 0;
        int s13 = 0;
        int s5 = 0;
        int s6 = 0;
        int s7_8 = 0;
        int s11 = 0;
        int s3 = 0;
        int s4 = 0;
        int s9_10 = 0;
        int s12 = 0;

        s1 = Math.min(aaA, bbA);
        aaA -= s1;
        bbA -= s1;
        s2 = Math.min(aaB, bbB);
        aaB -= s2;
        bbB -= s2;
        s13 = Math.min(abAB, abBA);
        abAB -= s13;
        abBA -= s13;

        if (aaAB == 0 && bbAB == 0) {
            return s1 + s2 + s13;
        } else if (aaAB > 0) {
            s5 = Math.min(aaAB, bbA);
            aaAB -= s5;
            bbA -= s5;
            s6 = Math.min(aaAB, bbB);
            aaAB -= s6;
            bbB -= s6;
            if (abAB > 0 && abBA == 0) {
                s7_8 = Math.min(aaAB, abAB);
                abAB -= s7_8;
            } else if (abAB == 0 && abBA > 0) {
                s7_8 = Math.min(aaAB, abBA);
                abBA -= s7_8;
            }
            s11 = (int) Math.floor((aaAB - s7_8) / 2.0);
        } else if (bbAB > 0) {
            s3 = Math.min(bbAB, aaA);
            bbAB -= s3;
            aaA -= s3;
            s4 = Math.min(bbAB, aaB);
            bbAB -= s4;
            aaB -= s4;
            if (abAB > 0 && abBA == 0) {
                s9_10 = Math.min(bbAB, abAB);
                abAB -= s9_10;
            } else if (abAB == 0 && abBA > 0) {
                s9_10 = Math.min(bbAB, abBA);
                abBA -= s9_10;
            }
            s12 = (int) Math.floor((bbAB - s9_10) / 2.0);
        }

        return s1 + s2 + s13 + s5 + s6 + s7_8 + s11 + s3 + s4 + s9_10 + s12;
    }

    /**
     * Calculates Values M and N
     * @return values for M and N as Pair (M, N)
     */
    private Pair<Integer, Integer> calculateMN() {
        int m = 0;
        int n = 0;
        int n1 = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        int n6 = 0;

        if (aaA == bbA && aaA == 0) {
            n3 = (int) Math.min(Math.floor(abAB / 2.0), aaB);
            n6 = (int) Math.min(Math.floor(abBA / 2.0), bbB);
        } else if (aaA > 0) {
            if (aaB > 0) {
                n3 = (int) Math.min(Math.floor(abAB / 2.0), aaB);
            } else if (bbB > 0) {
                m = (int) Math.min(Math.floor(abBA / 2.0), Math.min(aaA, bbB));
                abBA -= 2 * m;
                aaA -= m;
                n2 = Math.min(abBA, Math.min(aaA, bbB - m));
                abBA -= n2;
                n6 = (int) Math.min(Math.floor(abBA / 2.0), bbB - m - n2);
            }
            n5 = (int) Math.min(Math.floor((abBA - 2 * n6) / 2.0), aaA - n2);
        } else if (bbA > 0) {
            if (aaB > 0) {
                m = (int) Math.min(Math.floor(abAB / 2.0), Math.min(aaB, bbA));
                abAB -= 2 * m;
                aaB -= m;
                bbA -= m;
                n1 = Math.min(abAB, Math.min(aaB, bbA));
                abAB -= n1;
                n3 = (int) Math.min(Math.floor(abAB / 2.0), aaB - n1);
            } else if (bbB > 0) {
                n6 = (int) Math.min(Math.floor(abBA / 2.0), bbB);
            }
            n4 = (int) Math.min(Math.floor((abAB - 2 * n3) / 2.0), bbA - n1);
        }
        n = n1 + n2 + n3 + n4 + n5 + n6;

        return new Pair(m, n);
    }
}
