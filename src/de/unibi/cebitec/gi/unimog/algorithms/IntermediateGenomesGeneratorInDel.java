/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. */

package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.datastructure.ChromosomeString;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;
import de.unibi.cebitec.gi.unimog.datastructure.Label;
import de.unibi.cebitec.gi.unimog.datastructure.LabelIndexPair;
import de.unibi.cebitec.gi.unimog.datastructure.OperationListInDel;
import de.unibi.cebitec.gi.unimog.datastructure.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Extends the IntermediateGenomesGenerator to InDel.
 * 
 * @author Thomas Gatter - tgatter(at)cebitec.uni-bielefeld.de
 */
public class IntermediateGenomesGeneratorInDel extends IntermediateGenomesGenerator {
	
	/**
	 * Extended operations to contain suspended genes.
	 */
	private OperationListInDel								operationList;
	private HashMap<Integer, List<String>>		baseLabels;
	private HashMap<Integer, LabelIndexPair>	subLabels;
	
	public IntermediateGenomesGeneratorInDel(final Genome genomeA, final HashMap<Integer, String> geneNameMap,
			final OperationListInDel operationList, final int[] adjacenciesG1, final String genomeID1, final String genomeID2) {
		this.genomeA = genomeA;
		this.geneNameMap = geneNameMap;
		this.operationList = operationList;
		this.baseLabels = operationList.getBaseLabels();
		this.subLabels = operationList.getSubLabels();
		this.adjacenciesGA = adjacenciesG1;
		this.intermedGenomes = this.generateIntermediateGenomes(this.genomeA, this.operationList, this.adjacenciesGA);
		this.genomeID1 = genomeID1;
		this.genomeID2 = genomeID2;
	}
	
	/**
	 * Generates the intermediate genomes for all operations contained in the
	 * given operationList.
	 * 
	 * @param genomeA
	 *          The original old genome which should be transformed
	 * @param operationList
	 *          List of operations to perform on the genome
	 * @param adjacenciesGA
	 *          Adjacencies of genomeA
	 * @return The list of intermediate genomes according to the operation list
	 */
	private ArrayList<ChromosomeString[]> generateIntermediateGenomes(final Genome genomeA,
			final OperationListInDel operationList, final int[] adjacenciesGA) {
		
		ArrayList<int[]> adjacenciesListG1 = operationList.getAdjacencyArrayListG1();
		ArrayList<int[]> adjacencyLabelsG1 = operationList.getAdjacencyLabelsG1();
		
		List<HashMap<Integer, Label>> labelsG1 = operationList.getActiveLabelsG1();
		
		List<List<Integer>> single = operationList.getSingletonsG1();
		List<List<Integer>> circular = operationList.getSingletonsCircularG1();
		
		ArrayList<ChromosomeString[]> intermediateGenomes = new ArrayList<ChromosomeString[]>();
		
		// construct a new genome for each operation done to transform the genome
		for (int i = 0; i < adjacenciesListG1.size(); ++i) {
			intermediateGenomes.add(this.generateOutputGenome(adjacenciesListG1.get(i), adjacencyLabelsG1.get(i).clone(),
					labelsG1.get(i), single.get(i), circular.get(i)));
		}
		
		return intermediateGenomes;
	}
	
	/**
	 * Method for generating a new genome according to an adjacency array.
	 * 
	 * @param adjacencyArray
	 *          the adjacencies of the genome which should be
	 *          created.
	 * @param labelArray
	 *          InDel labels indexes
	 * @param labels
	 *          map of labels for indexes
	 * @return The new genome
	 */
	public ChromosomeString[] generateOutputGenome(int[] adjacencyArray, int[] labelArray,
			HashMap<Integer, Label> labels, List<Integer> singletons, List<Integer> circular) {
		
		ArrayList<ChromosomeString> newGenome = new ArrayList<ChromosomeString>();
		ArrayList<String> currentChrom = new ArrayList<String>();
		boolean[] visited = new boolean[adjacencyArray.length];
		
		for (int j = 1; j < adjacencyArray.length; ++j) {
			
			if (visited[j] == false) {
				// so we start with the chromosome containing gene 1 = 0,1
				int currentAdj1 = j;
				int currentAdj2 = adjacencyArray[currentAdj1]; // j is first element, currentAdj scnd
				int geneToAdd = (currentAdj1 + 1) / 2; // (1h, 2t) --> add 1 & in while schleife 2 before 1
				if (currentAdj1 % 2 == 0) { // if tail gene is negative
					geneToAdd *= -1;
				}
				
				String geneName = IntermediateGenomesGenerator.getSignedGeneName(geneToAdd, this.geneNameMap);
				addInDel(currentAdj1, currentAdj2, labelArray, labels, currentChrom, true);
				currentChrom.add(geneName);
				visited[currentAdj1] = true;
				
				// first direction 1t, extend to left
				while (((currentAdj2 % 2 == 1 && !visited[currentAdj2 + 1]) || (currentAdj2 % 2 == 0 && !visited[currentAdj2 - 1]))
						&& currentAdj1 != currentAdj2) { // as long as no circle or telomere
					geneToAdd = (currentAdj2 + 1) / 2;
					if (currentAdj2 % 2 == 1) { // if head gene is negative
						geneToAdd *= -1;
					}
					
					geneName = IntermediateGenomesGenerator.getSignedGeneName(geneToAdd, this.geneNameMap);
					addInDel(currentAdj1, currentAdj2, labelArray, labels, currentChrom, true);
					currentChrom.add(0, geneName);
					visited[currentAdj2] = true;
					
					// get values of next adjacency
					if (currentAdj2 % 2 == 0) { // currently tail was reviewed, f.e. (1t,3h) -> j=3t
						currentAdj1 = --currentAdj2;
					} else {
						currentAdj1 = ++currentAdj2;
					}
					currentAdj2 = adjacencyArray[currentAdj1]; // f.e. currentAdj = list[3t] = 5h
					visited[currentAdj1] = true;
				}
				
				if (currentAdj1 != currentAdj2
						&& ((currentAdj2 % 2 == 1 && visited[currentAdj2 + 1]) || (currentAdj2 % 2 == 0 && visited[currentAdj2 - 1]))) {
					// if circle chrom is complete
					
					geneToAdd = (currentAdj2 + 1) / 2;
					if (currentAdj2 % 2 == 1) { // if head gene is negative
						geneToAdd *= -1;
					}
					
					geneName = IntermediateGenomesGenerator.getSignedGeneName(geneToAdd, this.geneNameMap);
					addInDel(currentAdj1, currentAdj2, labelArray, labels, currentChrom, true);
					
					newGenome.add(new ChromosomeString(currentChrom, true));
					visited[currentAdj1] = true;
					visited[currentAdj2] = true;
				} else {
					// second direction if not circular, way to scnd telomere
					// get values of next adjacency
					if (currentAdj1 == currentAdj2) {
						visited[currentAdj1] = true;
						
						geneName = currentChrom.get(0);
						addInDel(currentAdj1, currentAdj2, labelArray, labels, currentChrom, true);
						
						if (j % 2 == 0) { // currently tail was reviewed, f.e. (3t) -> currentAdj1=3h
							currentAdj1 = j - 1;
						} else {
							currentAdj1 = j + 1;
						}
					} else {
						// System.out.println("case exists IntermedGenomeGenerator");
						if (currentAdj2 % 2 == 0) { // currently tail was reviewed, f.e. (3t) -> currentAdj1=3h
							currentAdj1 = --currentAdj2;
						} else {
							currentAdj1 = ++currentAdj2;
							// System.out.println("Case exists: intermed genomes generator 1");
						} // TODO: does this case exist?
					}
					
					currentAdj2 = adjacencyArray[currentAdj1]; // f.e. currentAdj = list[3t] = 5h
					
					if (currentAdj1 == currentAdj2) {
						geneName = currentChrom.get(currentChrom.size() - 1);
						addInDel(currentAdj1, currentAdj2, labelArray, labels, currentChrom, false);
					}
					
					while (currentAdj1 != currentAdj2) { // !visited[currentAdj] cannot occur here
						geneToAdd = (currentAdj2 + 1) / 2;
						if (currentAdj2 % 2 == 0) { // if tail gene is negative
							geneToAdd *= -1;
						}
						
						geneName = IntermediateGenomesGenerator.getSignedGeneName(geneToAdd, this.geneNameMap);
						addInDel(currentAdj1, currentAdj2, labelArray, labels, currentChrom, false);
						currentChrom.add(geneName);
						
						visited[currentAdj1] = true;
						visited[currentAdj2] = true;
						
						// get values of next adjacency
						if (currentAdj2 % 2 == 0) { // currently tail was reviewed, f.e. (1t,3t) -> j=3h
							currentAdj1 = --currentAdj2;
							if (currentAdj1 < 0) {
								System.err.println("CURRENT ADJACENCY1=" + currentAdj1); // FIXME error determination?
								System.err.println("CURRENT ADJACENCY2=" + currentAdj2);// FIXME error determination?
							}
						} else {
							currentAdj1 = ++currentAdj2;
						}
						currentAdj2 = adjacencyArray[currentAdj1]; // f.e. currentAdj = list[3h] = 5t
						try {
							currentAdj2 = adjacencyArray[currentAdj1]; // f.e. currentAdj = list[3h] = 5t
						} catch (ArrayIndexOutOfBoundsException e) {
							System.err.println("currentAdj1=" + currentAdj1);// FIXME error determination?
							System.err.println("currentAdj1=" + currentAdj2);// FIXME error determination?
						}
						
						if (currentAdj1 == currentAdj2) {
							geneName = currentChrom.get(currentChrom.size() - 1);
							addInDel(currentAdj1, currentAdj2, labelArray, labels, currentChrom, false);
						}
						
					}
					newGenome.add(new ChromosomeString(currentChrom, false));
					visited[currentAdj1] = true;
				}
				currentChrom = new ArrayList<String>();
			}
		}
		
		for (int i : singletons) {
			Label label = labels.get(i);
			List<String> labelString = new ArrayList<String>();
			retriveLabels(label.getLabel().getIndex(), label.getLabel().isOrientation(), labelString);
			newGenome.add(new ChromosomeString((String[]) labelString.toArray(new String[0]), false));
		}
		for (int i : circular) {
			Label label = labels.get(i);
			List<String> labelString = new ArrayList<String>();
			retriveLabels(label.getLabel().getIndex(), label.getLabel().isOrientation(), labelString);
			newGenome.add(new ChromosomeString((String[]) labelString.toArray(new String[0]), true));
		}
		
		ChromosomeString[] newGenomeCorrect = new ChromosomeString[newGenome.size()];
		return newGenome.toArray(newGenomeCorrect);
	}
	
	private void addInDel(int adj1, int adj2, int[] labelArray, HashMap<Integer, Label> labels, List<String> chromosome,
			boolean front) {
		
		int index = labelArray[adj1];
		if (index > 0) {
			
			labelArray[adj1] = 0;
			labelArray[adj2] = 0;
			
			Label fullInDel = labels.get(index);
			
			boolean retrieveOrient = fullInDel.getLabel().isOrientation();
			if (front) {
				if (adj1 != fullInDel.getRight()) {
					retrieveOrient = !retrieveOrient;
				}
			} else {
				if (adj1 != fullInDel.getLeft()) {
					retrieveOrient = !retrieveOrient;
				}
			}
			
			List<String> inDellabels = new ArrayList<String>();
			retriveLabels(fullInDel.getLabel().getIndex(), retrieveOrient, inDellabels);
			
			if (front) {
				chromosome.addAll(0, inDellabels);
				
			} else {
				chromosome.addAll(inDellabels);
			}
		}
	}
	
	private void retriveLabels(int index, boolean orientation, List<String> labelString) {
		
		if (baseLabels.containsKey(index)) {
			
			List<String> base = baseLabels.get(index);
			if (orientation) {
				labelString.addAll(base);
			} else {
				labelString.addAll(reverseArray(base));
			}
			return;
		}
		
		LabelIndexPair pair = subLabels.get(index);
		
		if (orientation) {
			retriveLabels(pair.getLeft().getIndex(), pair.getLeft().isOrientation(), labelString);
			retriveLabels(pair.getRight().getIndex(), pair.getRight().isOrientation(), labelString);
		} else {
			retriveLabels(pair.getRight().getIndex(), !pair.getRight().isOrientation(), labelString);
			retriveLabels(pair.getLeft().getIndex(), !pair.getLeft().isOrientation(), labelString);
		}
	}
	
	private List<String> reverseArray(List<String> nums) {
		
		List<String> reversed = new ArrayList<String>(nums.size());
		
		for (int i = 0; i < nums.size(); i++) {
			
			String rev = nums.get(nums.size() - 1 - i);
			if (rev.startsWith("-")) {
				rev = rev.substring(1);
			} else {
				rev = "-" + rev;
			}
			reversed.add(rev);
		}
		return reversed;
	}
	
	@Override
	public ArrayList<Pair<Integer, Integer>> getOperationList() {
		return this.operationList.getOperationList();
	}
	
	public OperationListInDel getOperationListObject() {
		return operationList;
	}
}