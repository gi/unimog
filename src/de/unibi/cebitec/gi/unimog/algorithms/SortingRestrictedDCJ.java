package de.unibi.cebitec.gi.unimog.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import de.unibi.cebitec.gi.unimog.datastructure.AdjacencyGraph;
import de.unibi.cebitec.gi.unimog.datastructure.CappedGenomes;
import de.unibi.cebitec.gi.unimog.datastructure.Chromosome;
import de.unibi.cebitec.gi.unimog.datastructure.Data;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;
import de.unibi.cebitec.gi.unimog.datastructure.IAdditionalData;
import de.unibi.cebitec.gi.unimog.datastructure.OperationList;
import de.unibi.cebitec.gi.unimog.datastructure.Pair;
import de.unibi.cebitec.gi.unimog.datastructure.SplayNode;
import de.unibi.cebitec.gi.unimog.datastructure.SplayTree;
import de.unibi.cebitec.gi.unimog.utils.Toolz;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker and Corinna Sickinger               *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * In this class the restricted DCJ is implemented. If a circular intermediate is created in a step in the DCJ sorting
 * it is reincorporated immediatly in the next step.
 * 
 * @author Corinna Sickinger
 */
public class SortingRestrictedDCJ implements ISorting {

    private SplayTree          splayTree;
    private int                genomeLength;
    private ArrayList<Integer> myGenomeAList;
    private ArrayList<Integer> myGenomeBList;
    private int                lengthForAdjacency;
    private CappedGenomes      cappedGenomes;

    @Override
    public OperationList findOptSortSequence(Data data, IAdditionalData additionalData,
        HashMap<Integer, Integer> chromMap) {
        final OperationList operationList = new OperationList();

        AdjacencyGraph adjToCap = new AdjacencyGraph(data.getGenomeA(), data.getGenomeB(), true);
        this.cappedGenomes = adjToCap.getCappedGenomes();
        this.cappedGenomes.performCapping();

        Genome cappedGenomeA = this.cappedGenomes.getCappedGenome1();
        Genome cappedGenomeB = this.cappedGenomes.getCappedGenome2();

        int[] adjacenciesG2 = data.getAdjGraph().getAdjacenciesGenome2();
        this.splayTree = new SplayTree(cappedGenomeA);
        this.genomeLength = cappedGenomeA.getNumberOfGenes();
        this.splayTree.initialize(this.genomeLength);
        this.lengthForAdjacency = 2 * data.getGenomeA().getNumberOfGenes() + 1;
        int[] adjacenciesGenomeB = Toolz.parseAdjacencies(cappedGenomeB, 2 * cappedGenomeB.getNumberOfGenes() + 1);

        this.myGenomeAList = new ArrayList<Integer>(Arrays.asList(0));
        for (int j = 0; j < cappedGenomeA.getNumberOfChromosomes(); j++) {
            Integer[] bnodes = SplayTree.intToInteger(cappedGenomeA.getChromosome(j).getGenes());
            this.myGenomeAList.addAll(Arrays.asList(bnodes));
            this.myGenomeAList.add(0);
        }

        this.myGenomeBList = new ArrayList<Integer>(Arrays.asList(0));
        for (int i = 0; i < cappedGenomeB.getNumberOfChromosomes(); i++) {
            Integer[] forNode = SplayTree.intToInteger(cappedGenomeB.getChromosome(i).getGenes());
            this.myGenomeBList.addAll(Arrays.asList(forNode));
            this.myGenomeBList.add(0);
        }

        ArrayList<Integer> myFirstGenomeList = this.splayTree.traverseTree();
        boolean myFirstChanged = this.changeChromosomeOrientation(myFirstGenomeList);
        if (myFirstChanged) {
            myFirstGenomeList = this.splayTree.traverseTree();
        }
        this.myGenomeAList = (ArrayList<Integer>) myFirstGenomeList.clone();

        // //First Operation to get Node with Marker 1
        SplayNode firstNode = this.splayTree.getNode(1);
        if (this.splayTree.getChromosome(firstNode) != 1) {
            // Translocate
            int chromOfOne = this.splayTree.getChromosome(firstNode);
            SplayNode rightChromNode = this.splayTree.getNode(this.genomeLength + chromOfOne);
            int rChromPos = rightChromNode.getPosition();
            SplayNode rightNode = this.splayTree.findIthNode(rChromPos);
            rightNode.splay();
            int rightTrans = rightNode.getMarker();
            int markerforTrans = this.splayTree.findIthNode(1).getMarker();
            translocate(markerforTrans, 2, rightTrans, this.splayTree.getChromosome(firstNode) + 1);
            myGenomeAList = this.splayTree.traverseTree();
        }

        // Shows if a marker needs to be a telomer or not
        boolean telomeric = false;
        boolean operationOccured = false;

        int firstOpFirst;
        int firstOpSecond;
        int secondOpFirst;
        int secondOpSecond;
        boolean firstOpFirstOri;
        boolean firstOpSecondOri;
        boolean scndOpFirstOri;
        boolean scndOpScndOri;

        for (int i = 1; i <= this.genomeLength; i++) {
            operationOccured = false;
            SplayNode currentNode = this.splayTree.getNode(i);
            currentNode.splay();
            int currentMarker = currentNode.getMarker();
            int posCurr = currentNode.getPosition();
            SplayNode leftOfCurr = this.splayTree.findIthNode(posCurr - 2);
            leftOfCurr.splay();
            int leftMarker = leftOfCurr.getMarker();

            SplayNode lastNode = null;
            int lastMarker = 0;
            if (i > 1) {
                lastNode = this.splayTree.getNode(i - 1);
                lastNode.splay();
                lastMarker = lastNode.getMarker();
            }

            // If the marker is negativ a sorting operation needs to be performed.
            // If the markers are already next to each other, the don't need to be sorted,
            // (expect for Fission this is performed at the end of the sorting scenario).
            // If the marker is right of a zero and has to be telomeric in the sorted genome, it doesn't
            // need to be sorted.
            boolean toSort = false;
            telomeric = (adjacenciesGenomeB[i * 2 - 1] == i * 2 - 1);

            // Fusion, not sure if with capping Fusion is even needed as extra Operation
            if (leftMarker == 0) {
                if (!telomeric) {
                    toSort = true;
                }
            }

            // ArrayList<Integer> debugList = splaytree.traverseTree();
            // System.out.println(debugList);

            if ((currentMarker < 0) || (leftMarker + 1 != i)) {

                int firstChrom = 0;
                int secondChrom = this.splayTree.getChromosome(currentNode);

                if (i > 1) {
                    firstChrom = this.splayTree.getChromosome(lastNode);
                }

                // Translocation
                if ((i > 1) && (firstChrom != secondChrom) && (!telomeric)) {
                    operationOccured = true;

                    if (firstChrom > secondChrom) {
                        SplayNode rightChromNode = this.splayTree.getNode(this.genomeLength + secondChrom);
                        int rChromPos = rightChromNode.getPosition();
                        SplayNode rightNode = this.splayTree.findIthNode(rChromPos);
                        rightNode.splay();
                        int rightTrans = rightNode.getMarker();

                        SplayNode leftChromNode = this.splayTree.getNode(this.genomeLength + firstChrom);
                        int leftChromPos = leftChromNode.getPosition();
                        SplayNode leftNode = this.splayTree.findIthNode(leftChromPos);
                        leftNode.splay();
                        int leftTrans = leftNode.getMarker();

                        this.translocate(rightTrans, secondChrom + 1, leftTrans, firstChrom + 1);
                        firstChrom = this.splayTree.getChromosome(lastNode);
                        secondChrom = this.splayTree.getChromosome(currentNode);
                    }

                    if (secondChrom - firstChrom != 1) {
                        SplayNode leftChromNode = this.splayTree.getNode(this.genomeLength + firstChrom + 1);
                        int leftChromPos = leftChromNode.getPosition();
                        SplayNode leftNode = this.splayTree.findIthNode(leftChromPos);
                        leftNode.splay();
                        int leftTrans = leftNode.getMarker();

                        SplayNode rightChromNode = this.splayTree.getNode(this.genomeLength + secondChrom);
                        int rChromPos = rightChromNode.getPosition();
                        SplayNode rightNode = this.splayTree.findIthNode(rChromPos);
                        rightNode.splay();
                        int rightTrans = rightNode.getMarker();
                        this.translocate(leftTrans, firstChrom + 2, rightTrans, secondChrom + 1);

                        firstChrom = this.splayTree.getChromosome(lastNode);
                        secondChrom = this.splayTree.getChromosome(currentNode);
                    }
                    int transPos = lastNode.getPosition();
                    SplayNode transNode = this.splayTree.findIthNode(transPos);
                    transNode.splay();
                    int transMarker = transNode.getMarker();

                    int secondPos = currentNode.getPosition();
                    int secondleftMarker = this.splayTree.findIthNode(secondPos - 2).getMarker();

                    // negativ translokation
                    if (currentMarker < 0) {

                        SplayNode rightOfSecond = this.splayTree.findIthNode(secondPos);
                        rightOfSecond.splay();
                        int rightOfScnd = rightOfSecond.getMarker();

                        if (transMarker < 0) {
                            secondOpFirst = Math.abs(transMarker) * 2;
                            scndOpFirstOri = false;
                        } else {
                            secondOpFirst = transMarker * 2 - 1;
                            scndOpFirstOri = true;
                        }
                        if (rightOfScnd < 0) {
                            secondOpSecond = Math.abs(rightOfScnd) * 2;
                            scndOpScndOri = false;
                        } else {
                            secondOpSecond = rightOfScnd * 2 - 1;
                            scndOpScndOri = true;
                        }
                        if (transMarker == 0) {
                            secondOpFirst = secondOpSecond;
                            scndOpFirstOri = scndOpScndOri;
                        }

                        Pair<Integer, Integer> toAdd = this.cappedGenomes.mapOperations((i - 1) * 2, true,
                            Math.abs(currentMarker) * 2 - 1, true);
                        if (toAdd.getFirst().equals(toAdd.getSecond())) {
                            operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst, scndOpFirstOri,
                                secondOpSecond, scndOpScndOri));
                            operationList.addOperation(toAdd);
                        } else {
                            operationList.addOperation(toAdd);
                            operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst, scndOpFirstOri,
                                secondOpSecond, scndOpScndOri));
                        }
                        this.negativeTranslocation(transMarker, firstChrom + 1, i, secondChrom);
                        // Fusion, not sure if this case exists anymore (after capping)
                    } else if ((transMarker == 0 && secondleftMarker == 0) && (!telomeric)) {

                        int secondOp;
                        boolean secOri;
                        if (currentMarker < 0) {
                            secondOp = Math.abs(currentMarker) * 2;
                            secOri = false;
                        } else {
                            secondOp = currentMarker * 2 - 1;
                            secOri = true;
                        }

                        operationList.addOperation(this.cappedGenomes
                            .mapOperations((i - 1) * 2, true, secondOp, secOri));
                        operationList.addOperation(new Pair<Integer, Integer>(0, 0));
                        this.performFusion(lastMarker, currentMarker);
                        // ordinary translocation
                    } else {

                        if (transMarker < 0) {
                            secondOpSecond = Math.abs(transMarker) * 2;
                            scndOpScndOri = false;
                        } else {
                            secondOpSecond = transMarker * 2 - 1;
                            scndOpScndOri = true;
                        }

                        if (secondleftMarker == 0) {
                            secondOpFirst = secondOpSecond;
                            scndOpFirstOri = scndOpScndOri;
                        } else if (secondleftMarker < 0) {
                            secondOpFirst = Math.abs(secondleftMarker) * 2 - 1;
                            scndOpFirstOri = false;
                        } else {
                            secondOpFirst = secondleftMarker * 2;
                            scndOpFirstOri = true;
                        }

                        if (transMarker == 0) {
                            secondOpSecond = secondOpFirst;
                            scndOpScndOri = scndOpFirstOri;
                        }

                        Pair<Integer, Integer> toAdd = this.cappedGenomes.mapOperations((i - 1) * 2, true,
                            currentMarker * 2 - 1, true);
                        if (toAdd.getFirst().equals(toAdd.getSecond())) {
                            operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst, scndOpFirstOri,
                                secondOpSecond, scndOpScndOri));
                            operationList.addOperation(toAdd);
                        } else {
                            operationList.addOperation(toAdd);
                            operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst, scndOpFirstOri,
                                secondOpSecond, scndOpScndOri));
                        }
                        this.translocate(transMarker, firstChrom + 1, i, secondChrom + 1);
                    }
                } else if (currentMarker < 0) {
                    // Revision/Inversion
                    operationOccured = true;
                    int inversPos = lastNode.getPosition();
                    SplayNode nodeInfrom = this.splayTree.findIthNode(inversPos);
                    nodeInfrom.splay();
                    int inverseFrom = Math.abs(nodeInfrom.getMarker());
                    int scndPos = currentNode.getPosition();
                    SplayNode rightOfSecond = this.splayTree.findIthNode(scndPos);
                    rightOfSecond.splay();
                    int rightofSecMarker = rightOfSecond.getMarker();
                    if (inverseFrom == 0) {
                        firstOpFirst = (i) * 2 - 1;
                        int telo = this.splayTree.getChromosome(currentNode) + this.genomeLength;
                        int teloPos = this.splayTree.getNode(telo).getPosition();
                        inverseFrom = this.splayTree.findIthNode(teloPos).getMarker();
                    } else {
                        firstOpFirst = (i - 1) * 2;
                    }
                    if (inverseFrom < 0) {
                        secondOpFirst = Math.abs(inverseFrom) * 2;
                        scndOpFirstOri = false;
                    } else {
                        secondOpFirst = inverseFrom * 2 - 1;
                        scndOpFirstOri = true;
                    }
                    if (rightofSecMarker == 0) {
                        secondOpSecond = secondOpFirst;
                        scndOpScndOri = scndOpFirstOri;
                    } else if (rightofSecMarker < 0) {
                        secondOpSecond = Math.abs(rightofSecMarker) * 2;
                        scndOpScndOri = false;
                    } else {
                        secondOpSecond = rightofSecMarker * 2 - 1;
                        scndOpScndOri = true;
                    }
                    Pair<Integer, Integer> toAdd = this.cappedGenomes.mapOperations(firstOpFirst, true, (i) * 2 - 1,
                        true);
                    if (toAdd.getFirst().equals(toAdd.getSecond())) {
                        operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst, scndOpFirstOri,
                            secondOpSecond, scndOpScndOri));
                        operationList.addOperation(toAdd);
                    } else {
                        operationList.addOperation(toAdd);
                        operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst, scndOpFirstOri,
                            secondOpSecond, scndOpScndOri));
                    }
                    this.inverse(inverseFrom, currentMarker);
                } else if (leftMarker != 0) {
                    // block exchange
                    operationOccured = true;

                    int m = getBiggestMarker(currentMarker);
                    SplayNode mNode = this.splayTree.getNode(m);
                    int mNodeChromNum = this.splayTree.getChromosome(mNode);

                    mNode.splay();
                    int mMarker = mNode.getMarker();

                    SplayNode mPlusOneNode = this.splayTree.getNode(m + 1);
                    int mplusChromNum = this.splayTree.getChromosome(mPlusOneNode);
                    mPlusOneNode.splay();
                    int mPlusOneMarker = mPlusOneNode.getMarker();

                    if (mNodeChromNum != mplusChromNum) {

                        if (mNodeChromNum > mplusChromNum) {
                            SplayNode rightChromNode = this.splayTree.getNode(this.genomeLength + mplusChromNum);
                            int rChromPos = rightChromNode.getPosition();
                            SplayNode rightNode = this.splayTree.findIthNode(rChromPos);
                            rightNode.splay();
                            int rightTrans = rightNode.getMarker();

                            SplayNode leftChromNode = this.splayTree.getNode(this.genomeLength + mNodeChromNum);
                            int leftChromPos = leftChromNode.getPosition();
                            SplayNode leftNode = this.splayTree.findIthNode(leftChromPos);
                            leftNode.splay();
                            int leftTrans = leftNode.getMarker();

                            this.translocate(rightTrans, mplusChromNum + 1, leftTrans, mNodeChromNum + 1);
                            mNodeChromNum = this.splayTree.getChromosome(mNode);
                            mplusChromNum = this.splayTree.getChromosome(mPlusOneNode);
                        }

                        int posOfM = mNode.getPosition();
                        int posOfMplusOne = mPlusOneNode.getPosition();
                        boolean positive;
                        SplayNode lOfM = this.splayTree.findIthNode(posOfM - 2);
                        lOfM.splay();
                        int markerLeftOfM = lOfM.getMarker();
                        SplayNode rOfM = this.splayTree.findIthNode(posOfM);
                        rOfM.splay();
                        int markerRightOfM = rOfM.getMarker();
                        SplayNode lOfMplus = this.splayTree.findIthNode(posOfMplusOne - 2);
                        lOfMplus.splay();
                        int markerLeftofMplusOne = lOfMplus.getMarker();
                        SplayNode rOfMplus = this.splayTree.findIthNode(posOfMplusOne);
                        rOfMplus.splay();
                        int markerRightOfMPlusOne = rOfMplus.getMarker();

                        // ordinary translocation
                        if ((mMarker > 0 && mPlusOneMarker > 0) || (mMarker < 0 && mPlusOneMarker < 0)) {
                            positive = true;
                            int transFrom;
                            int transTo;
                            if (mMarker > 0) {
                                SplayNode rightOfMNode = this.splayTree.findIthNode(posOfM);
                                rightOfMNode.splay();
                                transFrom = rightOfMNode.getMarker();
                                transTo = mPlusOneMarker;
                                firstOpFirst = mMarker;
                                firstOpSecond = mPlusOneMarker;
                                secondOpFirst = markerLeftofMplusOne;
                                secondOpSecond = markerRightOfM;
                            } else {
                                transFrom = mMarker;
                                SplayNode rightOfMPlusOne = this.splayTree.findIthNode(posOfMplusOne);
                                rightOfMPlusOne.splay();
                                transTo = rightOfMPlusOne.getMarker();
                                firstOpFirst = mPlusOneMarker;
                                firstOpSecond = mMarker;
                                secondOpFirst = markerLeftOfM;
                                secondOpSecond = markerRightOfMPlusOne;
                            }

                            this.translocate(transFrom, mNodeChromNum + 1, transTo, mplusChromNum + 1);

                            // One of the markers is negative, the other is positive, a negative translocation is
                            // performed
                        } else {
                            positive = false;
                            int transFrom;
                            int transTo;

                            if (mMarker > 0) {
                                SplayNode rightOfM = this.splayTree.findIthNode(posOfM);
                                rightOfM.splay();
                                transFrom = rightOfM.getMarker();
                                transTo = mPlusOneMarker;
                                firstOpFirst = mMarker;
                                firstOpSecond = mPlusOneMarker * (-1);
                                secondOpFirst = markerRightOfM * (-1);
                                secondOpSecond = markerRightOfMPlusOne;

                            } else {
                                transFrom = mMarker;
                                SplayNode leftOfMPlusOne = this.splayTree.findIthNode(posOfMplusOne - 2);
                                leftOfMPlusOne.splay();
                                transTo = leftOfMPlusOne.getMarker();
                                firstOpFirst = mMarker * (-1);
                                firstOpSecond = mPlusOneMarker;
                                secondOpFirst = markerLeftOfM;
                                secondOpSecond = markerLeftofMplusOne * (-1);
                            }
                            this.negativeTranslocation(transFrom, mNodeChromNum + 1, transTo, mplusChromNum);
                        }

                        Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> myOps = this.calculateOperations(
                            firstOpFirst, firstOpSecond, secondOpFirst, secondOpSecond);
                        operationList.addOperation(myOps.getFirst());
                        operationList.addOperation(myOps.getSecond());

                        ArrayList<Integer> genomeList = this.splayTree.traverseTree();
                        myGenomeAList = (ArrayList<Integer>) genomeList.clone();
                        genomeList.remove(0);

                        int[] adjacenciesG1 = this.createAdjacencyGraph(genomeList, null);
                        operationList.trimAdjArrayList();
                        operationList.addAdjacencyArrayG1(adjacenciesG1);

                        // Second translocation to move i+1 next to i
                        int transPos = lastNode.getPosition();
                        SplayNode rightOfI = this.splayTree.findIthNode(transPos);
                        rightOfI.splay();
                        int transmarker = rightOfI.getMarker();
                        int posOfIplusOne = currentNode.getPosition();

                        // positive translocation
                        if (positive) {
                            SplayNode leftOfIPlusOne = this.splayTree.findIthNode(posOfIplusOne - 2);
                            leftOfIPlusOne.splay();
                            secondOpFirst = leftOfIPlusOne.getMarker();
                            secondOpSecond = transmarker;
                            this.translocate(transmarker, mNodeChromNum + 1, i, mplusChromNum + 1);
                            // negative translocation
                        } else {
                            secondOpFirst = transmarker * (-1);
                            SplayNode rightOfIplusOne = this.splayTree.findIthNode(posOfIplusOne);
                            rightOfIplusOne.splay();
                            secondOpSecond = rightOfIplusOne.getMarker();
                            this.negativeTranslocation(transmarker, mNodeChromNum + 1, i, mplusChromNum);
                        }
                        Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> sndOps = this.calculateOperations(i - 1,
                            currentMarker, secondOpFirst, secondOpSecond);
                        operationList.addOperation(sndOps.getFirst());
                        operationList.addOperation(sndOps.getSecond());

                    } else {
                        // m and m+1 have different orientations
                        // Two Reversals
                        if ((mMarker > 0) && (mPlusOneMarker < 0)) {
                            int positionM = mNode.getPosition();
                            SplayNode rightOfMNode = this.splayTree.findIthNode(positionM);
                            rightOfMNode.splay();
                            int markerRightOfM = rightOfMNode.getMarker();

                            int mplusPos = mPlusOneNode.getPosition();
                            SplayNode rightOfMplusOne = this.splayTree.findIthNode(mplusPos);
                            rightOfMplusOne.splay();
                            int markerRightOfMplusOne = rightOfMplusOne.getMarker();

                            if (markerRightOfM < 0) {
                                secondOpFirst = Math.abs(markerRightOfM) * 2;
                                scndOpFirstOri = false;
                            } else {
                                secondOpFirst = markerRightOfM * 2 - 1;
                                scndOpFirstOri = true;
                            }

                            if (markerRightOfMplusOne == 0) {
                                secondOpSecond = secondOpFirst;
                                scndOpScndOri = scndOpFirstOri;
                            } else if (markerRightOfMplusOne < 0) {
                                secondOpSecond = Math.abs(markerRightOfMplusOne) * 2 - 1;
                                scndOpScndOri = false;
                            } else {
                                secondOpSecond = markerRightOfMplusOne * 2;
                                scndOpScndOri = true;
                            }

                            Pair<Integer, Integer> firstOp = this.cappedGenomes.mapOperations(mMarker * 2, true,
                                Math.abs(mPlusOneMarker) * 2 - 1, true);
                            if (firstOp.getFirst().equals(firstOp.getSecond())) {
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                                operationList.addOperation(firstOp);
                            } else {
                                operationList.addOperation(firstOp);
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                            }

                            this.inverse(markerRightOfM, mPlusOneMarker);

                            ArrayList<Integer> genomeList = this.splayTree.traverseTree();
                            this.myGenomeAList = (ArrayList<Integer>) genomeList.clone();
                            genomeList.remove(0);
                            int[] adjacenciesG1 = this.createAdjacencyGraph(genomeList, null);
                            operationList.trimAdjArrayList();
                            operationList.addAdjacencyArrayG1(adjacenciesG1);

                            int secPos = currentNode.getPosition();
                            SplayNode rightofSecond = this.splayTree.findIthNode(secPos);
                            rightofSecond.splay();
                            int rightOfScndMarker = rightofSecond.getMarker();

                            int inversPos = lastNode.getPosition();
                            SplayNode nodeInfrom = this.splayTree.findIthNode(inversPos);
                            if (nodeInfrom.getMarker() == 0) {
                                nodeInfrom = this.splayTree.findIthNode(inversPos + 1);
                                firstOpFirst = (i) * 2 - 1;
                            } else {
                                firstOpFirst = (i - 1) * 2;
                            }
                            nodeInfrom.splay();
                            int inverseFrom = nodeInfrom.getMarker();

                            if (inverseFrom < 0) {
                                secondOpFirst = Math.abs(inverseFrom) * 2;
                                scndOpFirstOri = false;
                            } else {
                                secondOpFirst = inverseFrom * 2 - 1;
                                scndOpFirstOri = true;
                            }
                            if (rightOfScndMarker == 0) {
                                secondOpSecond = secondOpFirst;
                                scndOpScndOri = scndOpFirstOri;
                            } else if (rightOfScndMarker < 0) {
                                secondOpSecond = Math.abs(rightOfScndMarker) * 2 - 1;
                                scndOpScndOri = false;
                            } else {
                                secondOpSecond = rightOfScndMarker * 2;
                                scndOpScndOri = true;
                            }

                            Pair<Integer, Integer> toAdd = this.cappedGenomes.mapOperations(firstOpFirst, true,
                                (i) * 2 - 1, true);
                            if (toAdd.getFirst().equals(toAdd.getSecond())) {
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                                operationList.addOperation(toAdd);
                            } else {
                                operationList.addOperation(toAdd);
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                            }
                            this.inverse(inverseFrom, i);
                        } else if ((mMarker < 0) && (mPlusOneMarker > 0)) {
                            int positionM = mNode.getPosition();
                            SplayNode leftOfMNode = this.splayTree.findIthNode(positionM - 2);
                            leftOfMNode.splay();
                            int markerLeftOfM = leftOfMNode.getMarker();

                            int mplusPos = mPlusOneNode.getPosition();
                            SplayNode mMinusOne = this.splayTree.findIthNode(mplusPos - 2);
                            mMinusOne.splay();
                            int mMinusMarker = mMinusOne.getMarker();

                            if (mMinusMarker < 0) {
                                firstOpSecond = Math.abs(mMinusMarker) * 2 - 1;
                                firstOpSecondOri = false;
                            } else {
                                firstOpSecond = mMinusMarker * 2;
                                firstOpSecondOri = true;
                            }
                            if (markerLeftOfM == 0) {
                                firstOpFirst = firstOpSecond;
                                firstOpFirstOri = firstOpSecondOri;
                            } else if (markerLeftOfM < 0) {
                                firstOpFirst = Math.abs(markerLeftOfM) * 2 - 1;
                                firstOpFirstOri = false;
                            } else {
                                firstOpFirst = markerLeftOfM * 2;
                                firstOpFirstOri = true;
                            }

                            if (mMarker < 0) {
                                secondOpFirst = Math.abs(mMarker) * 2;
                                scndOpFirstOri = false;
                            } else {
                                secondOpFirst = mMarker * 2 - 1;
                                scndOpFirstOri = true;
                            }

                            if (mPlusOneMarker < 0) {
                                secondOpSecond = Math.abs(mPlusOneMarker) * 2;
                                scndOpScndOri = false;
                            } else {
                                secondOpSecond = mPlusOneMarker * 2 - 1;
                                scndOpScndOri = true;
                            }

                            Pair<Integer, Integer> firstOp = this.cappedGenomes.mapOperations(firstOpFirst,
                                firstOpFirstOri, firstOpSecond, firstOpSecondOri);
                            if (firstOp.getFirst().equals(firstOp.getSecond())) {
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                                operationList.addOperation(firstOp);
                            } else {
                                operationList.addOperation(firstOp);
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                            }

                            this.inverse(mMarker, mMinusMarker);

                            ArrayList<Integer> genomeList = this.splayTree.traverseTree();
                            this.myGenomeAList = (ArrayList<Integer>) genomeList.clone();
                            genomeList.remove(0);
                            int[] adjacenciesG1 = this.createAdjacencyGraph(genomeList, null);
                            operationList.trimAdjArrayList();
                            operationList.addAdjacencyArrayG1(adjacenciesG1);

                            int secPos = currentNode.getPosition();
                            SplayNode rightOfSecond = this.splayTree.findIthNode(secPos);
                            rightOfSecond.splay();
                            int rightOfSecMarker = rightOfSecond.getMarker();

                            int inversePos = lastNode.getPosition();
                            SplayNode nodeInFrom = this.splayTree.findIthNode(inversePos);
                            if (nodeInFrom.getMarker() == 0) {
                                nodeInFrom = this.splayTree.findIthNode(inversePos + 1);
                                firstOpFirst = (i) * 2 - 1;
                            } else {
                                firstOpFirst = (i - 1) * 2;
                            }
                            nodeInFrom.splay();
                            int inverseFrom = nodeInFrom.getMarker();

                            if (inverseFrom < 0) {
                                secondOpFirst = Math.abs(inverseFrom) * 2;
                                scndOpFirstOri = false;
                            } else {
                                secondOpFirst = inverseFrom * 2 - 1;
                                scndOpFirstOri = true;
                            }

                            if (rightOfSecMarker < 0) {
                                secondOpSecond = Math.abs(rightOfSecMarker) * 2 - 1;
                                scndOpScndOri = false;
                            } else {
                                secondOpSecond = rightOfSecMarker * 2;
                                scndOpScndOri = false;
                            }

                            Pair<Integer, Integer> secondOp = this.cappedGenomes.mapOperations(firstOpFirst, true,
                                (i) * 2 - 1, true);

                            if (secondOp.getFirst().equals(secondOp.getSecond())) {
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                                operationList.addOperation(secondOp);
                            } else {
                                operationList.addOperation(secondOp);
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                            }
                            this.inverse(inverseFrom, i);
                            // m and m+1 both have a positive orientation
                            // Block exchange
                        } else if (mMarker > 0) {

                            int positionM = mNode.getPosition();
                            int positionIPlusOne = currentNode.getPosition();

                            // position right before i
                            int positionI = lastNode.getPosition();
                            int startBlockA;
                            SplayNode leftOfI;

                            leftOfI = this.splayTree.findIthNode(positionI);
                            leftOfI.splay();
                            startBlockA = leftOfI.getMarker();
                            firstOpFirst = (i - 1) * 2;

                            int startBlockB = 0;
                            int stopBlockB = 0;

                            if ((positionIPlusOne != positionM + 1)) {
                                int posM = mNode.getPosition();
                                SplayNode nextToMNode = this.splayTree.findIthNode(posM);
                                nextToMNode.splay();
                                startBlockB = nextToMNode.getMarker();

                                SplayNode nextToIplusOne = this.splayTree.findIthNode(positionIPlusOne - 2);
                                nextToIplusOne.splay();
                                stopBlockB = nextToIplusOne.getMarker();
                            }

                            int cPlusOne = 0;
                            SplayNode stopCNode = null;
                            int stopCMarker = 0;

                            int mPlusOnePos = mPlusOneNode.getPosition();
                            stopCNode = this.splayTree.findIthNode(mPlusOnePos - 2);
                            stopCNode.splay();
                            stopCMarker = stopCNode.getMarker();
                            cPlusOne = m + 1;

                            if (startBlockA < 0) {
                                secondOpFirst = Math.abs(startBlockA) * 2;
                                scndOpFirstOri = false;
                            } else {
                                secondOpFirst = startBlockA * 2 - 1;
                                scndOpFirstOri = true;
                            }

                            if (stopBlockB == 0) {
                                secondOpSecond = mMarker * 2;
                                scndOpScndOri = true;
                            } else if (stopBlockB < 0) {
                                secondOpSecond = Math.abs(stopBlockB) * 2 - 1;
                                scndOpScndOri = false;
                            } else {
                                secondOpSecond = stopBlockB * 2;
                                scndOpScndOri = true;
                            }

                            Pair<Integer, Integer> firstOp = this.cappedGenomes.mapOperations(firstOpFirst, true,
                                (i) * 2 - 1, true);
                            if (firstOp.getFirst().equals(firstOp.getSecond())) {
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                                operationList.addOperation(firstOp);
                            } else {
                                operationList.addOperation(firstOp);
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                            }
                            Pair<SplayNode, SplayNode> firstEx = blockExchangePart1(startBlockA, i);

                            // create genome for operationslist, block exchange takes two DCJ operations.
                            ArrayList<Integer> linearGenomeList = firstEx.getFirst().traverse(
                                firstEx.getFirst().isReverseFlag());
                            linearGenomeList.remove(0);
                            ArrayList<Integer> circIntermediate = firstEx.getSecond().traverse(
                                firstEx.getSecond().isReverseFlag());

                            int[] adjacenciesG1 = this.createAdjacencyGraph(linearGenomeList, circIntermediate);
                            operationList.trimAdjArrayList();
                            operationList.addAdjacencyArrayG1(adjacenciesG1);

                            if (stopCMarker < 0) {
                                firstOpFirst = Math.abs(stopCMarker) * 2 - 1;
                                firstOpFirstOri = false;
                            } else {
                                firstOpFirst = stopCMarker * 2;
                                firstOpFirstOri = true;
                            }

                            if (startBlockB == 0) {
                                firstOpSecond = secondOpFirst;
                                firstOpSecondOri = scndOpFirstOri;
                            } else if (startBlockB < 0) {
                                firstOpSecond = Math.abs(startBlockB) * 2;
                                firstOpSecondOri = false;
                            } else {
                                firstOpSecond = startBlockB * 2 - 1;
                                firstOpSecondOri = true;
                            }

                            if (mPlusOneMarker < 0) {
                                secondOpSecond = Math.abs(mPlusOneMarker) * 2;
                                scndOpScndOri = false;
                            } else {
                                secondOpSecond = mPlusOneMarker * 2 - 1;
                                scndOpScndOri = true;
                            }

                            // second part of the block exchange, the circular intermediate is reincorporated into the
                            // chromosomes

                            Pair<Integer, Integer> secondOp = this.cappedGenomes.mapOperations(firstOpFirst,
                                firstOpFirstOri, firstOpSecond, firstOpSecondOri);
                            if (secondOp.getFirst().equals(secondOp.getSecond())) {
                                operationList.addOperation(this.cappedGenomes.mapOperations(mMarker * 2, true,
                                    secondOpSecond, scndOpScndOri));
                                operationList.addOperation(secondOp);
                            } else {
                                operationList.addOperation(secondOp);
                                operationList.addOperation(this.cappedGenomes.mapOperations(mMarker * 2, true,
                                    secondOpSecond, scndOpScndOri));
                            }
                            this.blockExchangePart2(startBlockA, startBlockB, stopBlockB, cPlusOne);

                            // m and m+1 both have a negative marker
                        } else {
                            int positionM = mNode.getPosition();

                            SplayNode leftOfM = this.splayTree.findIthNode(positionM - 2);
                            leftOfM.splay();
                            int markerLeftOfM = leftOfM.getMarker();

                            int positionI = lastNode.getPosition();
                            int startBlockA;

                            startBlockA = this.splayTree.findIthNode(positionI).getMarker();
                            firstOpFirst = (i - 1) * 2;

                            int startBlockB = 0;
                            int stopBlockB = 0;

                            // startB und StopB
                            if (!(positionM == positionI + 1)) {
                                startBlockB = mMarker;
                                int posIPlusOne = currentNode.getPosition();
                                stopBlockB = this.splayTree.findIthNode(posIPlusOne - 2).getMarker();
                            }

                            int posC = mPlusOneNode.getPosition();
                            SplayNode stopCplusOneNode = this.splayTree.findIthNode(posC);
                            stopCplusOneNode.splay();
                            int markerStopCPlusOne = stopCplusOneNode.getMarker();

                            if (startBlockA < 0) {
                                secondOpFirst = Math.abs(startBlockA) * 2;
                                scndOpFirstOri = false;
                            } else {
                                secondOpFirst = startBlockA * 2 - 1;
                                scndOpFirstOri = true;
                            }

                            if (stopBlockB == 0) {
                                secondOpSecond = Math.abs(mMarker) * 2 - 1;
                                scndOpScndOri = false;
                            } else if (stopBlockB < 0) {
                                secondOpSecond = Math.abs(stopBlockB) * 2 - 1;
                                scndOpScndOri = false;
                            } else {
                                secondOpSecond = stopBlockB * 2;
                                scndOpScndOri = true;
                            }

                            Pair<Integer, Integer> firstOp = this.cappedGenomes.mapOperations(firstOpFirst, true,
                                (i) * 2 - 1, true);
                            if (firstOp.getFirst().equals(firstOp.getSecond())) {
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                                operationList.addOperation(firstOp);
                            } else {
                                operationList.addOperation(firstOp);
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                            }

                            Pair<SplayNode, SplayNode> firstEx = blockExchangePart1(startBlockA, i);
                            // create genome for operationslist, block exchange takes two DCJ operations.
                            ArrayList<Integer> linearGenomeList = firstEx.getFirst().traverse(
                                firstEx.getFirst().isReverseFlag());
                            linearGenomeList.remove(0);
                            ArrayList<Integer> circIntermediate = firstEx.getSecond().traverse(
                                firstEx.getSecond().isReverseFlag());

                            int[] adjacenciesG1 = this.createAdjacencyGraph(linearGenomeList, circIntermediate);

                            operationList.trimAdjArrayList();
                            operationList.addAdjacencyArrayG1(adjacenciesG1);

                            if ((markerLeftOfM == (i - 1)) || (markerLeftOfM == 0)) {
                                if (stopBlockB == 0) {
                                    secondOpFirst = Math.abs(mMarker) * 2 - 1;
                                    scndOpFirstOri = false;
                                } else if (stopBlockB < 0) {
                                    secondOpFirst = Math.abs(stopBlockB) * 2 - 1;
                                    scndOpFirstOri = false;
                                } else {
                                    secondOpFirst = stopBlockB * 2;
                                    scndOpFirstOri = true;
                                }
                            } else if (markerLeftOfM < 0) {
                                secondOpFirst = Math.abs(markerLeftOfM) * 2 - 1;
                                scndOpFirstOri = false;
                            } else {
                                secondOpFirst = markerLeftOfM * 2;
                                scndOpFirstOri = true;
                            }

                            if (markerStopCPlusOne == 0) {
                                int chromOfC = this.splayTree.getChromosome(mPlusOneNode);
                                markerStopCPlusOne = chromOfC + this.genomeLength + 1;
                                secondOpSecond = secondOpFirst;
                                scndOpScndOri = scndOpFirstOri;
                            } else if (markerStopCPlusOne < 0) {
                                secondOpSecond = Math.abs(markerStopCPlusOne) * 2;
                                scndOpScndOri = false;
                            } else {
                                secondOpSecond = markerStopCPlusOne * 2 - 1;
                                scndOpScndOri = true;
                            }

                            Pair<Integer, Integer> secondOp = this.cappedGenomes.mapOperations(
                                Math.abs(mPlusOneMarker) * 2 - 1, false, Math.abs(mMarker) * 2, false);
                            if (secondOp.getFirst().equals(secondOp.getSecond())) {
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                                operationList.addOperation(secondOp);
                            } else {
                                operationList.addOperation(secondOp);
                                operationList.addOperation(this.cappedGenomes.mapOperations(secondOpFirst,
                                    scndOpFirstOri, secondOpSecond, scndOpScndOri));
                            }
                            // second part of the block exchange, the circular intermediate is reincorporated into the
                            // chromosomes
                            this.blockExchangePart2(startBlockA, startBlockB, stopBlockB, markerStopCPlusOne);
                        }
                    }
                }

                if (operationOccured) {
                    ArrayList<Integer> genomeList = this.splayTree.traverseTree();
                    boolean changed = this.changeChromosomeOrientation(genomeList);
                    if (changed) {
                        genomeList = this.splayTree.traverseTree();
                    }

                    this.myGenomeAList = (ArrayList<Integer>) genomeList.clone();
                    genomeList.remove(0);

                    int[] adjacenciesG1 = this.createAdjacencyGraph(genomeList, null);
                    operationList.trimAdjArrayList();
                    operationList.addAdjacencyArrayG1(adjacenciesG1);
                }
            }
        }

        // Fission check
        if (!operationList.getAdjacencyArrayListG1().isEmpty()) {
            int[] adjacenciesG1 = operationList.getAdjacencyArrayListG1().get(
                operationList.getAdjacencyArrayListG1().size() - 1);

            for (int i = 1; i < adjacenciesG2.length; ++i) {
                if (i == adjacenciesG2[i] && i != adjacenciesG1[i]) {
                    int adj1 = adjacenciesG1[i];
                    // e.g. B: (1,1), A:(4,1) = (1,1), (4,4)
                    // Replace old adjacencies
                    adjacenciesG1[i] = i;
                    adjacenciesG1[adj1] = adj1;
                    operationList.addOperation(new Pair<Integer, Integer>(i, i));
                    operationList.addOperation(new Pair<Integer, Integer>(adj1, adj1));
                    operationList.addAdjacencyArrayG1(adjacenciesG1.clone());
                }
            }
        }

        return operationList;
    }

    /**
     * Method to get the biggest marker on a chromosome. The marker is searched between the start of the chromosome and
     * the marker given to the method.
     * 
     * @param stop
     *            The right border of the interval.
     * @return the biggest marker in the interval
     */
    private int getBiggestMarker(int stop) {
        SplayNode stopNode = this.splayTree.getNode(stop);
        int chromosome = this.splayTree.getChromosome(stopNode);
        SplayNode startOfChromosome = this.splayTree.getNode(this.genomeLength + chromosome);
        startOfChromosome.splay();
        this.splayTree.split(startOfChromosome, false);

        stopNode.splay();
        Pair<SplayNode, SplayNode> secondSplit = this.splayTree.split(stopNode, false);

        stopNode.initializeBiggestChild();
        int back = stopNode.getBiggestChild();

        this.splayTree.merge(stopNode, secondSplit.getSecond(), false);
        SplayNode finalMerge = this.splayTree.merge(startOfChromosome, stopNode, false);

        this.splayTree.setRoot(finalMerge);
        this.splayTree.getRoot().setSize();
        this.splayTree.getRoot().initializeBiggestChild();

        return back;
    }

    private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> calculateOperations(Integer fstOp1, Integer fstOp2,
        Integer sndOp1, Integer sndOp2) {

        Integer firstOpFirst;
        Integer firstOpSecond;
        Integer secondOpfirst;
        Integer secondOpSecond;
        boolean fstOpFstOri;
        boolean fstOpScndOri;
        boolean scndOpFstOri;
        boolean scndOpScndOri;

        Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> back = new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(
            null, null);
        if (fstOp1 > 0) {
            fstOpFstOri = true;
            firstOpFirst = fstOp1 * 2;
        } else {
            fstOpFstOri = false;
            firstOpFirst = Math.abs(fstOp1) * 2 - 1;
        }

        if (fstOp2 > 0) {
            fstOpScndOri = true;
            firstOpSecond = fstOp2 * 2 - 1;
        } else {
            fstOpScndOri = false;
            firstOpSecond = Math.abs(fstOp2) * 2;
        }

        if (sndOp1 > 0) {
            scndOpFstOri = true;
            secondOpfirst = sndOp1 * 2;
        } else {
            scndOpFstOri = false;
            secondOpfirst = Math.abs(sndOp1) * 2 - 1;
        }

        if (sndOp2 > 0) {
            scndOpScndOri = true;
            secondOpSecond = sndOp2 * 2 - 1;
        } else {
            scndOpScndOri = false;
            secondOpSecond = Math.abs(sndOp2) * 2;
        }

        if (fstOp1.equals(0)) {
            firstOpFirst = firstOpSecond;
            fstOpFstOri = fstOpScndOri;
        } else if (fstOp2.equals(0)) {
            firstOpSecond = firstOpFirst;
            fstOpScndOri = fstOpFstOri;
        }

        if (sndOp1.equals(0)) {
            secondOpfirst = secondOpSecond;
            scndOpFstOri = scndOpScndOri;
        } else if (sndOp2.equals(0)) {
            secondOpSecond = secondOpfirst;
            scndOpScndOri = fstOpFstOri;
        }

        Pair<Integer, Integer> first = this.cappedGenomes.mapOperations(firstOpFirst, fstOpFstOri, firstOpSecond,
            fstOpScndOri);

        if (first.getFirst().equals(first.getSecond())) {
            back.setFirst(this.cappedGenomes.mapOperations(secondOpfirst, scndOpFstOri, secondOpSecond, scndOpScndOri));
            back.setSecond(first);
        } else {
            back.setFirst(first);
            back.setSecond(this.cappedGenomes.mapOperations(secondOpfirst, scndOpFstOri, secondOpSecond, scndOpScndOri));
        }

        return back;
    }

    // private void exchangeChromosomes(Integer firstChrom, Integer secondChrom) {
    // if (secondChrom > firstChrom) {
    // return;
    // }
    // SplayNode rightChromNode =this.splayTree.getNode(this.genomeLength + secondChrom);
    // int rChromPos = rightChromNode.getPosition();
    // SplayNode rightNode =this.splayTree.findIthNode(rChromPos);
    // rightNode.splay();
    // int rightTrans = rightNode.getMarker();
    //
    // SplayNode leftChromNode =this.splayTree.getNode(this.genomeLength + firstChrom);
    // int leftChromPos = leftChromNode.getPosition();
    // SplayNode leftNode =this.splayTree.findIthNode(leftChromPos);
    // leftNode.splay();
    // int leftTrans = leftNode.getMarker();
    //
    // this.translocate(rightTrans, secondChrom + 1, leftTrans, firstChrom + 1);
    // }
    // /**
    // * Method to perform a fission of a chromosome.
    // *
    // * @param node node representing the start of a chromosome.
    // */
    // private void performFission(SplayNode node) {
    //
    // node.splay();
    // Pair<SplayNode, SplayNode> fissionSplit =this.splayTree.split(node, false);
    //
    // int chromNum =this.splayTree.getChromosome(node);
    // SplayNode telomere = new SplayNode(chromNum + 1,this.splayTree);
    //
    // this.splayTree.insertNode(chromNum + 1, telomere);
    //
    // this.splayTree.merge(fissionSplit.getFirst(), telomere, false);
    // telomere.splay();
    // SplayNode fissionmerge =this.splayTree.merge(telomere, fissionSplit.getSecond(), false);
    //
    // this.splayTree.setRoot(fissionmerge);
    // this.splayTree.getRoot().setSize();
    // this.splayTree.getRoot().initializeBiggestChild();
    // }
    /**
     * Method to perform a fusion between two chromosoms.
     * 
     * @param markerA
     *            last marker of the left chromosome.
     * @param markerB
     *            first marker of the right chromosome.
     */
    private void performFusion(int markerA, int markerB) {

        SplayNode toFuse = this.splayTree.getNode(markerA);
        int chrom = this.splayTree.getChromosome(toFuse);
        SplayNode telomere = this.splayTree.getNode(this.genomeLength + chrom + 1);
        telomere.splay();

        Pair<SplayNode, SplayNode> firstSplit = this.splayTree.split(telomere, true);
        this.splayTree.split(telomere, false);

        SplayNode nodeB = this.splayTree.getNode(markerB);
        nodeB.splay();
        SplayNode fusionComplete = null;
        if (nodeB.getLeftChild() == null) {
            fusionComplete = this.splayTree.merge(firstSplit.getFirst(), nodeB, true);
        } else {
            Pair<SplayNode, SplayNode> thirdSplit = this.splayTree.split(nodeB, true);
            SplayNode fusionNode = this.splayTree.merge(firstSplit.getFirst(), thirdSplit.getSecond(), true);

            SplayNode biggestNode = fusionNode.getPositionBiggestNode(fusionNode.isReverseFlag());
            biggestNode.splay();
            fusionComplete = this.splayTree.merge(biggestNode, thirdSplit.getFirst(), false);
        }
        this.splayTree.deleteNode(chrom);

        this.splayTree.setRoot(fusionComplete);
        this.splayTree.getRoot().setSize();
        this.splayTree.getRoot().initializeBiggestChild();
    }

    /**
     * Method for the translocation.
     */
    private void translocate(int markerA, int chromosomeA, int markerB, int chromosomeB) {
        // splayA == null for example: 1 2 3 | 6 7 4 5 6

        SplayNode splayA = this.splayTree.getNode(markerA);
        SplayNode chromA = this.splayTree.getNode(this.genomeLength + chromosomeA);
        SplayNode splayB = this.splayTree.getNode(markerB);
        SplayNode chromB = this.splayTree.getNode(this.genomeLength + chromosomeB);

        SplayNode treeT1 = null;
        if (splayA != null) {
            // Here the first tree with nodes smaller then markerA is created
            splayA.splay();
            Pair<SplayNode, SplayNode> firstsplit = this.splayTree.split(splayA, true);
            treeT1 = firstsplit.getFirst();
        }

        // The second tree with nodes from markerA to smaller chromosomeA
        // This is the first part, that should be translocated
        chromA.splay();
        Pair<SplayNode, SplayNode> secondSplit = this.splayTree.split(chromA, true);
        SplayNode treeT2 = secondSplit.getFirst();

        // Tree 3 with nodes between chromA and smaller than markerB
        SplayNode treeT3 = null;
        if (splayB != null) {
            splayB.splay();
            Pair<SplayNode, SplayNode> thirdSplit = this.splayTree.split(splayB, true);
            treeT3 = thirdSplit.getFirst();
        }

        // thirdsplit.getFirst();

        // Here Tree 4 and 5 are created. Tree4 is the second part, that should
        // be translocated. Tree5 with all the nodes equal and more to chromB
        chromB.splay();
        Pair<SplayNode, SplayNode> fourthSplit = this.splayTree.split(chromB, true);

        SplayNode treeT5 = fourthSplit.getSecond();

        // Now T4 and T2 are exchanged and the trees are merged.
        // First merge T2 and T5, because there is no splaying involved
        // splayA and splayB are never both null
        if (splayA != null) {
            this.splayTree.merge(treeT2, treeT5, true);
        } else {
            chromB.splay();
            this.splayTree.merge(treeT3, chromB, true);
        }

        // T1 and T4 are merged
        SplayNode tree1Merge4 = null;
        if (splayB != null) {
            splayB.splay();
            if (splayA != null) {
                tree1Merge4 = this.splayTree.merge(treeT1, splayB, true);
            } else {
                tree1Merge4 = this.splayTree.merge(treeT2, splayB, true);
            }
        }
        // merge T1merge4 with T3
        SplayNode tree143 = null;
        chromA.splay();
        if (splayB != null) {
            tree143 = this.splayTree.merge(tree1Merge4, chromA, true);
        } else {
            tree143 = this.splayTree.merge(treeT1, chromA, true);
        }
        // finally merge T143 with T2merge5
        SplayNode lastMerge;
        if (splayA != null) {
            splayA.splay();
            lastMerge = this.splayTree.merge(tree143, splayA, true);
        } else {
            lastMerge = tree143;
        }

        this.splayTree.setRoot(lastMerge);
        this.splayTree.getRoot().setSize();
        this.splayTree.getRoot().initializeBiggestChild();
    }

    private void negativeTranslocation(int markerA, int chromosomeA, int markerB, int chromosomeB) {
        // splay A == null for example: 1 2 3 | -5 -4 6 7 8

        SplayNode splayA = this.splayTree.getNode(markerA);
        SplayNode chromA = this.splayTree.getNode(this.genomeLength + chromosomeA);
        SplayNode splayB = this.splayTree.getNode(markerB);
        SplayNode chromB = this.splayTree.getNode(this.genomeLength + chromosomeB);

        SplayNode treeT1 = null;
        if (splayA != null) {
            splayA.splay();
            Pair<SplayNode, SplayNode> firstSplit = this.splayTree.split(splayA, true);
            treeT1 = firstSplit.getFirst();
        }

        chromA.splay();
        Pair<SplayNode, SplayNode> secondSplit = this.splayTree.split(chromA, true);
        SplayNode treeT2 = secondSplit.getFirst();

        chromB.splay();
        Pair<SplayNode, SplayNode> thirdSplit = this.splayTree.split(chromB, false);
        SplayNode unaffectedTree = thirdSplit.getFirst();

        SplayNode treeT5 = null;
        if (splayB == null) {
            treeT5 = thirdSplit.getSecond();
        }

        SplayNode treeT4 = null;

        if (splayB != null) {
            splayB.splay();
            Pair<SplayNode, SplayNode> fourthSplit = this.splayTree.split(splayB, false);
            treeT4 = fourthSplit.getFirst();
            treeT5 = fourthSplit.getSecond();
        }
        // Set reverse
        if (splayA != null) {
            treeT2.splay();
            treeT2.flipFlag();
        }
        if (splayB != null) {
            treeT4.flipFlag();
        }

        SplayNode firstMerge = null;
        if (splayB != null) {
            if (splayA != null) {
                SplayNode biggestT1 = treeT1.getPositionBiggestNode(treeT1.isReverseFlag());
                biggestT1.splay();
                firstMerge = this.splayTree.merge(biggestT1, treeT4, false);
            } else {
                SplayNode biggestT2 = treeT2.getPositionBiggestNode(treeT2.isReverseFlag());
                biggestT2.splay();
                firstMerge = this.splayTree.merge(biggestT2, treeT4, false);
            }
        }

        SplayNode smallest = treeT5.getPositionSmallestNode(treeT5.isReverseFlag());
        smallest.splay();
        SplayNode secondMerge;
        if (splayA != null) {
            treeT2.splay();
            secondMerge = this.splayTree.merge(treeT2, smallest, true);
        } else {
            secondMerge = this.splayTree.merge(unaffectedTree, smallest, true);
        }

        SplayNode mergeTree = null;
        chromA.splay();
        if (splayB != null) {
            mergeTree = this.splayTree.merge(firstMerge, chromA, true);
        } else {
            SplayNode biggestT1 = treeT1.getPositionBiggestNode(treeT1.isReverseFlag());
            biggestT1.splay();
            mergeTree = this.splayTree.merge(biggestT1, chromA, false);
        }

        SplayNode lastMerge;
        if (splayA != null) {
            SplayNode biggest = chromA.getPositionBiggestNode(chromA.isReverseFlag());
            biggest.splay();
            lastMerge = this.splayTree.merge(biggest, secondMerge, false);
        } else {
            lastMerge = mergeTree;
        }

        this.splayTree.setRoot(lastMerge);
        this.splayTree.getRoot().setSize();
        this.splayTree.getRoot().initializeBiggestChild();

    }

    /**
     * Method for the inversion. With split three trees are created, T<i, Ti-j, T>j. Then the reverseflag of Ti-j is
     * flipped, and the trees are merged.
     */
    private void inverse(int from, int to) {
        // Case if only one marker need to be inversed and not a whole sequence.
        if (this.splayTree.getNode(from).equals(this.splayTree.getNode(to))) {
            this.splayTree.getNode(from).flipMarker();
        } else {
            SplayNode fromsplay = this.splayTree.getNode(from);
            fromsplay.splay();
            Pair<SplayNode, SplayNode> firstsplit = this.splayTree.split(fromsplay, true);
            SplayNode tosplay = this.splayTree.getNode(to);
            tosplay.splay();
            Pair<SplayNode, SplayNode> secondsplit = this.splayTree.split(tosplay, false);
            secondsplit.getFirst().flipFlag();

            SplayNode biggest = firstsplit.getFirst().getPositionBiggestNode(firstsplit.getFirst().isReverseFlag());
            biggest.splay();

            SplayNode node0toj = this.splayTree.merge(biggest, secondsplit.getFirst(), false);

            SplayNode smallest = secondsplit.getSecond().getPositionSmallestNode(
                secondsplit.getSecond().isReverseFlag());
            smallest.splay();

            // One last merge to get just one tree again.
            SplayNode lastmerge = this.splayTree.merge(node0toj, smallest, true);
            this.splayTree.setRoot(lastmerge);
            this.splayTree.getRoot().setSize();
            this.splayTree.getRoot().initializeBiggestChild();
        }

    }

    /**
     * Cut a Block out of the Genome, return two parts the Genome without the block and the part that is a circulare
     * intermediate. We have a chromosome with blocks a,b,c in this order. After the first part we get a tree without
     * the blocks a,b and a tree with only a and b. The second tree is the part that creates the circulare intermediate.
     */
    private Pair<SplayNode, SplayNode> blockExchangePart1(int firstA, int firstC) {

        SplayNode firstElemA = this.splayTree.getNode(firstA);
        SplayNode firstElemC = this.splayTree.getNode(firstC);

        firstElemA.splay();
        Pair<SplayNode, SplayNode> firstsplit = this.splayTree.split(firstElemA, true);
        SplayNode TreeT1 = firstsplit.getFirst();

        firstElemC.splay();
        Pair<SplayNode, SplayNode> secondsplit = this.splayTree.split(firstElemC, true);
        SplayNode treeT2 = secondsplit.getSecond();
        SplayNode intermediate = secondsplit.getFirst();

        // merge T1 and T2, last part of the tree stays alone
        SplayNode treewithout = this.splayTree.merge(TreeT1, treeT2, true);

        return new Pair<SplayNode, SplayNode>(treewithout, intermediate);

    }

    /**
     * In this part the real block exchange happens. First we had a chromosome with this layout: abc. After this step
     * the layout will be cba. The blocks a and c will have changed their places.
     */
    private void blockExchangePart2(int firstA, int firstB, int lastB, int lastCplusOne) {

        SplayNode aAndBNode = null;

        if ((firstB != 0) && (lastB != 0) && (firstA != firstB)) {
            SplayNode firstElemANode = this.splayTree.getNode(firstA);
            SplayNode firstElemBNode = this.splayTree.getNode(firstB);
            SplayNode lastElemBNode = this.splayTree.getNode(lastB);
            // First nodes A and B are exchanged
            firstElemBNode.splay();
            this.splayTree.split(firstElemBNode, true);

            firstElemANode.splay();
            lastElemBNode.splay();
            aAndBNode = this.splayTree.merge(lastElemBNode, firstElemANode, true);

        }

        aAndBNode = this.splayTree.getNode(firstA);

        // Now the tree without A and B is split and A and B are reincorporated after C
        SplayNode elemAfterCNode = this.splayTree.getNode(lastCplusOne);

        elemAfterCNode.splay();
        Pair<SplayNode, SplayNode> splitCandCplusOne = this.splayTree.split(elemAfterCNode, true);
        SplayNode splitC = splitCandCplusOne.getFirst();

        aAndBNode.splay();
        this.splayTree.merge(aAndBNode, splitCandCplusOne.getSecond(), true);

        SplayNode lastMergeNode = null;
        if ((firstB != 0) && (lastB != 0)) {
            SplayNode firstElemBNode = this.splayTree.getNode(firstB);
            firstElemBNode.splay();
            lastMergeNode = this.splayTree.merge(splitC, firstElemBNode, true);
        } else {
            aAndBNode.splay();
            lastMergeNode = this.splayTree.merge(splitC, aAndBNode, true);
        }

        this.splayTree.setRoot(lastMergeNode);
        this.splayTree.getRoot().setSize();
        this.splayTree.getRoot().initializeBiggestChild();

    }

    /**
     * Method to change the orientation of a chromosome, if the last gene has a negative orientation and has a smaller
     * marker than the first gene on the chromosome.
     * 
     * @param genome
     *            The genome on which the change operation is performed.
     * @return true if the orientation was changed, false otherwise.
     */
    private boolean changeChromosomeOrientation(ArrayList<Integer> genome) {

        boolean back = false;
        int start = genome.get(1);
        int ende = 0;
        for (int i = 1; i < genome.size(); i++) {

            if (genome.get(i) == 0) {
                ende = genome.get(i - 1);
                if ((ende < 0) && (Math.abs(ende) < Math.abs(start))
                    || ((ende < 0) && (Math.abs(ende) == Math.abs(start)))) {
                    this.inverse(Math.abs(start), Math.abs(ende));
                    back = true;
                }
                if (i != genome.size() - 1) {
                    start = genome.get(i + 1);
                }
            }
        }
        return back;
    }

    /**
     * Method to create an adjacency graph from a list of Integers, representing the genome. The mapping from the capped
     * genome to the uncapped Genome is also performed in this method
     * 
     * @param linear
     *            A linear Genome, chromosomes are split at 0.
     * @param circular
     *            A circulare intermediate, null if there is no intermediate
     * @return an adjacency graph represented by an array of ints
     */
    private int[] createAdjacencyGraph(ArrayList<Integer> linear, ArrayList<Integer> circular) {

        ArrayList<Integer> linearGenomeList = linear;
        Genome genomeForAdjList = new Genome();
        ArrayList<Integer> chrom = new ArrayList<Integer>();
        for (int j = 0; j < linearGenomeList.size(); j++) {
            if (!linearGenomeList.get(j).equals(0)) {
                Integer originalGene = this.cappedGenomes.getOriginalGene(linearGenomeList.get(j));
                if (!originalGene.equals(0)) {
                    chrom.add(originalGene);
                }
            } else {
                if (!chrom.isEmpty()) {
                    Chromosome chromtoAdd = new Chromosome(chrom, false);
                    genomeForAdjList.addChromosome(chromtoAdd);
                }
                chrom = new ArrayList<Integer>();
            }
        }

        if (circular != null) {
            ArrayList<Integer> circIntermediate = circular;
            ArrayList<Integer> circChrom = new ArrayList<Integer>();
            for (int a = 0; a < circIntermediate.size(); a++) {
                Integer origGene = this.cappedGenomes.getOriginalGene(circIntermediate.get(a));
                if (!origGene.equals(0)) {
                    circChrom.add(origGene);
                }
            }
            if (!circChrom.isEmpty()) {
                Chromosome circ = new Chromosome(circChrom, true);
                genomeForAdjList.addChromosome(circ);
            }
        }

        int[] adjacenciesG1 = Toolz.parseAdjacencies(genomeForAdjList, this.lengthForAdjacency);
        return adjacenciesG1;
    }
}
