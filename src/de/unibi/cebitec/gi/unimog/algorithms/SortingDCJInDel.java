package de.unibi.cebitec.gi.unimog.algorithms;

import de.unibi.cebitec.gi.unimog.datastructure.Data;
import de.unibi.cebitec.gi.unimog.datastructure.DcjInDelAdditionalData;
import de.unibi.cebitec.gi.unimog.datastructure.IAdditionalData;
import de.unibi.cebitec.gi.unimog.datastructure.Label;
import de.unibi.cebitec.gi.unimog.datastructure.LabelIndex;
import de.unibi.cebitec.gi.unimog.datastructure.LabelIndexPair;
import de.unibi.cebitec.gi.unimog.datastructure.LabeledAdjacencyGraph;
import de.unibi.cebitec.gi.unimog.datastructure.OperationInDel;
import de.unibi.cebitec.gi.unimog.datastructure.OperationList;
import de.unibi.cebitec.gi.unimog.datastructure.OperationListInDel;
import de.unibi.cebitec.gi.unimog.datastructure.Pair;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * @author Thomas Gatter - tgatter@techfak.uni-bielefeld.de
 *         Class which implements the greedy DCJ InDel sorting algorithm. Runs in linear
 *         time.
 */
public class SortingDCJInDel implements ISorting {
	
	/** Adjacencies Genome 1 */
	private int[]								adjacenciesG1;
	
	/** Adjacencies Genome 2 */
	private int[]								adjacenciesG2;
	
	private HashMap<Integer, Label>				labels;
	private HashMap<Integer, LabelIndexPair>	subLabels;
	private HashMap<Integer, List<String>>		baseLabels;
	private int									sublabelCount;
	
	/** Labels in Genome 1. Index is extremity Index and value is LabelID. 0 == no Label */
	private int[]								adjacencyLabelsGenom1;
	/** Labels in Genome 2. Index is extremity Index and value is LabelID. 0 == no Label */
	private int[]								adjacencyLabelsGenom2;
	
	private List<Integer>						components;
	
	/** Singletons for both genomes */
	private ArrayList<Integer>					aaAsingletons;
	private ArrayList<Integer>					bbBsingletons;
	private ArrayList<Integer>					aaAsingletonsCircular;
	private ArrayList<Integer>					bbBsingletonsCircular;
	
	/** Operationlist for Genome 1 */
	private List<OperationInDel>				operationList1;
	private List<int[]>							intermediateAdjacencyArrayList1;
	private List<int[]>							intermediateLabelArrayList1;
	private List<HashMap<Integer, Label>>		activeLabelsList1;
	private List<List<Integer>>					intermediateSingletons1;
	private List<List<Integer>>					intermediateSingletons1Circular;
	
	/** Operationlist for Genome 2 */
	private List<OperationInDel>				operationList2;
	private List<int[]>							intermediateAdjacencyArrayList2;
	private List<int[]>							intermediateLabelArrayList2;
	private List<HashMap<Integer, Label>>		activeLabelsList2;
	private List<List<Integer>>					intermediateSingletons2;
	private List<List<Integer>>					intermediateSingletons2Circular;
	
	private HashMap<Integer, String>			geneNameMap;
	
	/** Recombination Groups */
	private Map<Integer, Integer>				aaAStarts;
	private Map<Integer, Integer>				aaBStarts;
	private Map<Integer, Integer>				aaABStarts;
	private Map<Integer, Integer>				bbAStarts;
	private Map<Integer, Integer>				bbBStarts;
	private Map<Integer, Integer>				bbABStarts;
	private Map<Integer, Integer>				abABStarts;
	private Map<Integer, Integer>				abBAStarts;
	
	@SuppressWarnings("javadoc")
	@Override
	public OperationList findOptSortSequence(final Data data, final IAdditionalData additionalData, final HashMap<Integer, Integer> gNM) {
		
		DcjInDelAdditionalData ad = (DcjInDelAdditionalData) additionalData;
		
		LabeledAdjacencyGraph g = ad.getGraph();
		geneNameMap = ad.getGeneNameMap();
		
		operationList1 = new ArrayList<OperationInDel>();
		operationList2 = new ArrayList<OperationInDel>();
		intermediateAdjacencyArrayList1 = new ArrayList<int[]>();
		intermediateAdjacencyArrayList2 = new ArrayList<int[]>();
		intermediateLabelArrayList1 = new ArrayList<int[]>();
		intermediateLabelArrayList2 = new ArrayList<int[]>();
		activeLabelsList1 = new ArrayList<HashMap<Integer, Label>>();
		activeLabelsList2 = new ArrayList<HashMap<Integer, Label>>();
		intermediateSingletons1 = new ArrayList<List<Integer>>();
		intermediateSingletons2 = new ArrayList<List<Integer>>();
		intermediateSingletons1Circular = new ArrayList<List<Integer>>();
		intermediateSingletons2Circular = new ArrayList<List<Integer>>();
		
		OperationListInDel result = new OperationListInDel();
		
		adjacenciesG1 = g.getAdjacenciesGenome1();
		adjacenciesG2 = g.getAdjacenciesGenome2();
		
		labels = g.getActiveLabels();
		subLabels = g.getSubLabels();
		baseLabels = g.getBaseLabels();
		sublabelCount = g.getSubLabelCount();
		
		adjacencyLabelsGenom1 = g.getAdjacencyLabelsGenom1();
		adjacencyLabelsGenom2 = g.getAdjacencyLabelsGenom2();
		
		aaAsingletons = g.getaaAsingletons();
		bbBsingletons = g.getbbBsingletons();
		aaAsingletonsCircular = g.getaaAsingletonsCircular();
		bbBsingletonsCircular = g.getbbBsingletonsCircular();
		
		result.addAdjacencyArrayG1(adjacenciesG1.clone());
		result.addAdjacencyLabelsG1(adjacencyLabelsGenom1.clone());
		result.addActiveLabelsG1((HashMap<Integer, Label>) labels.clone());
		
		intermediateAdjacencyArrayList2.add(adjacenciesG2.clone());
		intermediateLabelArrayList2.add(adjacencyLabelsGenom2.clone());
		activeLabelsList2.add((HashMap<Integer, Label>) labels.clone());
		
		addSingletons2();
		
		components = g.getComponents();
		
		// do the sorting
		cleanAndAccumulate(false);
		
		// recount recomb groups
		g.countPaths();
		
		components = new ArrayList<Integer>();
		aaAStarts = g.getaaAStarts();
		aaBStarts = g.getaaBStarts();
		aaABStarts = g.getaaABStarts();
		bbAStarts = g.getbbAStarts();
		bbBStarts = g.getbbBStarts();
		bbABStarts = g.getbbABStarts();
		abABStarts = g.getabABStarts();
		abBAStarts = g.getabBAStarts();
		
		// recomb groups
		applyRecombinationGroups();
		
		// add the rest of the components
		components.addAll(g.getComponents());
		
		// combinations will create the need to accumulate again
		cleanAndAccumulate(false);
		split();
		cleanAndAccumulate(true);
		deleteInsertLabel();
		sortLabelFreeRemains();
		
		// remove last element of Genome 2 lists, since it is the same as last Genome 1 per definition
		intermediateAdjacencyArrayList2.remove(intermediateAdjacencyArrayList2.size() - 1);
		intermediateLabelArrayList2.remove(intermediateLabelArrayList2.size() - 1);
		activeLabelsList2.remove(activeLabelsList2.size() - 1);
		
		result.setBaseLabels(baseLabels);
		result.setSubLabels(subLabels);
		
		// first add operations
		for (Pair<Integer, Integer> p : operationList1) {
			result.addOperation(p);
		}
		for (ListIterator iterator = operationList2.listIterator(operationList2.size()); iterator.hasPrevious();) {
			Pair<Integer, Integer> p1 = (Pair<Integer, Integer>) iterator.previous();
			if (iterator.hasPrevious()) {
				Pair<Integer, Integer> p2 = (Pair<Integer, Integer>) iterator.previous();
				result.addOperation(p2);
				result.addOperation(p1);
			}
			// result.addOperation(p1);
		}
		
		// add intermediate genomes
		for (int[] p : intermediateAdjacencyArrayList1) {
			result.addAdjacencyArrayG1(p);
		}
		for (ListIterator iterator = intermediateAdjacencyArrayList2.listIterator(intermediateAdjacencyArrayList2.size()); iterator.hasPrevious();) {
			int[] p = (int[]) iterator.previous();
			result.addAdjacencyArrayG1(p);
		}
		
		// add intermediate labels
		for (int[] p : intermediateLabelArrayList1) {
			result.addAdjacencyLabelsG1(p);
		}
		for (ListIterator iterator = intermediateLabelArrayList2.listIterator(intermediateLabelArrayList2.size()); iterator.hasPrevious();) {
			int[] p = (int[]) iterator.previous();
			result.addAdjacencyLabelsG1(p);
		}
		
		for (HashMap<Integer, Label> p : activeLabelsList1) {
			result.addActiveLabelsG1(p);
		}
		for (ListIterator iterator = activeLabelsList2.listIterator(activeLabelsList2.size()); iterator.hasPrevious();) {
			HashMap<Integer, Label> p = (HashMap<Integer, Label>) iterator.previous();
			result.addActiveLabelsG1(p);
		}
		
		for (List<Integer> s : intermediateSingletons1) {
			result.addSingletonsG1(s);
		}
		for (ListIterator iterator = intermediateSingletons2.listIterator(intermediateSingletons2.size()); iterator.hasPrevious();) {
			List<Integer> s = (List<Integer>) iterator.previous();
			result.addSingletonsG1(s);
		}
		
		for (List<Integer> s : intermediateSingletons1Circular) {
			result.addSingletonsCircularG1(s);
		}
		for (ListIterator iterator = intermediateSingletons2Circular.listIterator(intermediateSingletons2Circular.size()); iterator.hasPrevious();) {
			List<Integer> s = (List<Integer>) iterator.previous();
			result.addSingletonsCircularG1(s);
		}
		
		return result;
	}
	
	// /////////////////////////////////////////////////////////////////////////
	// Accumulate, Split, InDel //
	// /////////////////////////////////////////////////////////////////////////
	
	private void sortLabelFreeRemains() {
		
		int adj1 = 0;
		int adj2 = 0;
		int adj3 = 0;
		
		for (int i = 1; i < adjacenciesG2.length; ++i) {
			adj1 = adjacenciesG1[i];
			adj2 = adjacenciesG2[i];
			if (i != adj2 && adj1 != adj2) { // i is not telomere & not already in same adjacency in g1 & 2
				// f.e. B: (4,5), A:(4,1) , (5,2) = (4,5) , (2,1)
				// B: (i,adj2) A:(i,adj1),(adj2,adj3) = (i,adj2), (adj3,adj1)
				adj3 = adjacenciesG1[adj2];
				// Replace old adjacencies
				adjacenciesG1[i] = adj2;
				adjacenciesG1[adj2] = i;
				operationList1.add(new OperationInDel(i, adj2));
				// Distinguish between telomere & adjacency in genome1
				if (adj2 == adj3 && i != adj1) {
					adjacenciesG1[adj1] = adj1;
					operationList1.add(new OperationInDel(adj1, adj1));
				} else {// second distinction between telomere & adjacency
					if (i == adj1 && adj2 != adj3) {
						adjacenciesG1[adj3] = adj3;
						operationList1.add(new OperationInDel(adj3, adj3));
					} else if (i != adj1 && adj2 != adj3) {
						adjacenciesG1[adj1] = adj3;
						adjacenciesG1[adj3] = adj1;
						operationList1.add(new OperationInDel(adj1, adj3));
					} else {
						operationList1.add(new OperationInDel(0, 0));
					}
				}
				
				// save operations
				intermediateAdjacencyArrayList1.add(adjacenciesG1.clone());
				intermediateLabelArrayList1.add(adjacencyLabelsGenom1.clone());
				activeLabelsList1.add((HashMap<Integer, Label>) labels.clone());
				
				addSingletons1();
			}
			
		}
		for (int i = 1; i < adjacenciesG2.length; ++i) {
			if (i == adjacenciesG2[i] && i != adjacenciesG1[i]) {
				adj1 = adjacenciesG1[i];
				// f.e. B: (1,1), A:(4,1) = (1,1), (4,4)
				// Replace old adjacencies
				adjacenciesG1[i] = i;
				adjacenciesG1[adj1] = adj1;
				operationList1.add(new OperationInDel(i, i));
				operationList1.add(new OperationInDel(adj1, adj1));
				intermediateAdjacencyArrayList1.add(adjacenciesG1.clone());
				intermediateLabelArrayList1.add(adjacencyLabelsGenom1.clone());
				activeLabelsList1.add((HashMap<Integer, Label>) labels.clone());
				
				addSingletons1();
			}
		}
	}
	
	private void deleteInsertLabel() {
		
		Iterator<Integer> it = aaAsingletons.iterator();
		while (it.hasNext()) {
			int i = it.next();
			deleteSingleton(i);
			it.remove();
		}
		it = aaAsingletonsCircular.iterator();
		while (it.hasNext()) {
			int i = it.next();
			deleteSingleton(i);
			it.remove();
		}
		
		for (int i = 1; i < adjacenciesG1.length; ++i) {
			int labelIndex = adjacencyLabelsGenom1[i];
			if (labelIndex > 0) {
				
				labels.remove(labelIndex);
				adjacencyLabelsGenom1[i] = 0;
				adjacencyLabelsGenom1[adjacenciesG1[i]] = 0;
				
				intermediateAdjacencyArrayList1.add(adjacenciesG1.clone());
				intermediateLabelArrayList1.add(adjacencyLabelsGenom1.clone());
				activeLabelsList1.add((HashMap<Integer, Label>) labels.clone());
				
				addSingletons1();
				
				operationList1.add(new OperationInDel(i, adjacenciesG1[i], OperationInDel.OperationType.Delete));
				operationList1.add(new OperationInDel(i, adjacenciesG1[i], OperationInDel.OperationType.Delete));
			}
		}
		
		it = bbBsingletons.iterator();
		while (it.hasNext()) {
			int i = it.next();
			it.remove();
			insertSingleton(i);
		}
		it = bbBsingletonsCircular.iterator();
		while (it.hasNext()) {
			int i = it.next();
			it.remove();
			insertSingleton(i);
		}
		
		for (int i = 1; i < adjacenciesG2.length; ++i) {
			int labelIndex = adjacencyLabelsGenom2[i];
			if (labelIndex > 0) {
				Label label = labels.get(labelIndex);
				
				labels.remove(labelIndex);
				adjacencyLabelsGenom2[i] = 0;
				adjacencyLabelsGenom2[adjacenciesG2[i]] = 0;
				
				intermediateAdjacencyArrayList2.add(adjacenciesG2.clone());
				intermediateLabelArrayList2.add(adjacencyLabelsGenom2.clone());
				activeLabelsList2.add((HashMap<Integer, Label>) labels.clone());
				addSingletons2();
				
				String left = getLeftmost(label.getLabel().getIndex(), label.getLabel().isOrientation());
				String right = getRightmost(label.getLabel().getIndex(), label.getLabel().isOrientation());
				
				Pair<String, String> labelStrings = new Pair<String, String>(left, right);
				Pair<Integer, Pair<String, String>> labelData = new Pair<Integer, Pair<String, String>>(label.getLeft(), labelStrings);
				
				operationList2.add(new OperationInDel(i, adjacenciesG2[i], OperationInDel.OperationType.Insert, labelData));
				operationList2.add(new OperationInDel(i, adjacenciesG2[i], OperationInDel.OperationType.Insert, labelData));
			}
		}
	}
	
	private void deleteSingleton(int i) {
		Label fullLabel = labels.get(i);
		
		String left = getLeftmost(fullLabel.getLabel().getIndex(), fullLabel.getLabel().isOrientation());
		String right = getRightmost(fullLabel.getLabel().getIndex(), fullLabel.getLabel().isOrientation());
		
		labels.remove(i);
		
		intermediateAdjacencyArrayList1.add(adjacenciesG1.clone());
		intermediateLabelArrayList1.add(adjacencyLabelsGenom1.clone());
		activeLabelsList1.add((HashMap<Integer, Label>) labels.clone());
		
		operationList1.add(new OperationInDel(1, 0, OperationInDel.OperationType.Singleton, new Pair<String, String>(left, right)));
		operationList1.add(new OperationInDel(1, 0, OperationInDel.OperationType.Singleton, new Pair<String, String>(left, right)));
		
		addSingletons1();
	}
	
	private void insertSingleton(int i) {
		Label fullLabel = labels.get(i);
		
		String left = getLeftmost(fullLabel.getLabel().getIndex(), fullLabel.getLabel().isOrientation());
		String right = getRightmost(fullLabel.getLabel().getIndex(), fullLabel.getLabel().isOrientation());
		
		labels.remove(i);
		
		intermediateAdjacencyArrayList2.add(adjacenciesG2.clone());
		intermediateLabelArrayList2.add(adjacencyLabelsGenom2.clone());
		activeLabelsList2.add((HashMap<Integer, Label>) labels.clone());
		
		operationList2.add(new OperationInDel(1, 0, OperationInDel.OperationType.Singleton, new Pair<String, String>(left, right)));
		operationList2.add(new OperationInDel(1, 0, OperationInDel.OperationType.Singleton, new Pair<String, String>(left, right)));
		
		addSingletons2();
	}
	
	/**
	 * Split components
	 */
	private void split() {
		
		// true: modify Genome 1
		// false: modify Genome 2
		boolean mode1;
		
		List<Integer> newComponents = new ArrayList<Integer>();
		
		for (int startpos : components) {
			
			// start assuming first run is in in the genome of first telomer
			// this choice is arbitrary
			// true: telomer/start in Genome 1
			// false: telomer/start in Genome 2
			mode1 = true;
			if (adjacenciesG2[startpos] == startpos) {
				mode1 = false;
			} // else circular or telomer in Genome 1
			
			int pos = startpos; // current position
			
			newComponents.add(startpos);
			
			boolean endfound = false;
			while (!endfound) { // move through component
			
				// test if the end has been reached already
				if (mode1) {
					int adj = adjacenciesG2[pos];
					
					// test for telomers
					if (adj == pos) { // || adjacenciesG1[adj] == adj
						endfound = true;
						continue;
					}
					
					// test for circle ends and sorted telomers
					if (adj == startpos || adjacenciesG1[adj] == startpos) {
						endfound = true;
						continue;
					}
					
					// test for newly created circles
					int adj2 = adjacenciesG2[pos];
					int adj3 = adjacenciesG1[adj2];
					
					if (adj3 == pos) {
						endfound = true;
						continue;
					}
					
				} else {
					int adj = adjacenciesG1[pos];
					
					// test for telomers
					if (adj == pos) {
						endfound = true;
						continue;
					}
					
					// test for circle ends and sorted telomers
					if (adj == startpos || adjacenciesG2[adj] == startpos) {
						endfound = true;
						continue;
					}
					
					// test for newly created circles
					int adj2 = adjacenciesG1[pos];
					int adj3 = adjacenciesG2[adj2];
					
					if (adj3 == pos) {
						endfound = true;
						continue;
					}
				}
				
				// test for mode switches
				if (mode1) {
					int nextindex = adjacenciesG2[pos];
					if (adjacencyLabelsGenom1[pos] == 0) {
						pos = nextindex;
						mode1 = false;
						continue;
					}
				} else {
					int nextindex = adjacenciesG1[pos];
					if (adjacencyLabelsGenom2[pos] == 0) {
						pos = nextindex;
						mode1 = true;
						continue;
					}
				}
				
				if (mode1) { // change Genome 1
					pos = resolveCycle(pos, adjacenciesG1, adjacenciesG2, operationList1, OperationInDel.OperationType.Forward,
							intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1, adjacencyLabelsGenom1, newComponents,
							true, true);
				} else { // change Genome 2
					pos = resolveCycle(pos, adjacenciesG2, adjacenciesG1, operationList2, OperationInDel.OperationType.Backward,
							intermediateAdjacencyArrayList2, intermediateLabelArrayList2, activeLabelsList2, adjacencyLabelsGenom2, newComponents,
							true, false);
				}
			}
		}
		
		components = newComponents;
	}
	
	/**
	 * Use optimal DCJs to remove inner areas without label from components.
	 * Accumulate consecutive runs.
	 */
	private void cleanAndAccumulate(boolean closeEndCycles) {
		
		List<Integer> newComponents = new ArrayList<Integer>();
		
		for (int startpos : components) {
			
			newComponents.add(startpos);
			
			resolveComponent(startpos, closeEndCycles, newComponents);
			
		}
		
		components = newComponents;
		
	}
	
	private Pair<Integer, Integer> resolveComponent(int startpos, boolean closeEndCycles, List<Integer> newComponents) {
		
		// true: modify Genome 1
		// false: modify Genome 2
		boolean mode1;
		
		// start assuming first run is in in the genome of first telomer
		// this choice is arbitrary
		// true: telomer/start in Genome 1
		// false: telomer/start in Genome 2
		mode1 = true;
		if (adjacenciesG2[startpos] == startpos) {
			mode1 = false;
		} // else circular or telomer in Genome 1
		
		int pos = startpos; // current position
		int endpos = pos;
		int pos_pre = pos;
		
		boolean endfound = false;
		while (!endfound && pos > 0) { // move through component
		
			// test if the end has been reached already
			if (mode1) {
				int adj = adjacenciesG2[pos];
				
				// test for telomers
				if (adj == pos) {
					endfound = true;
					endpos = pos;
					continue;
				}
				
				// test for circle ends and sorted telomers
				if (adj == startpos || adjacenciesG1[adj] == startpos) {
					endfound = true;
					endpos = pos;
					continue;
				}
				
				// test for newly created circles
				int adj2 = adjacenciesG2[pos];
				int adj3 = adjacenciesG1[adj2];
				
				if (adj3 == pos) {
					endfound = true;
					endpos = pos_pre;
					continue;
				}
				
			} else {
				int adj = adjacenciesG1[pos];
				
				// test for telomers
				if (adj == pos) {
					endfound = true;
					endpos = pos;
					continue;
				}
				
				// test for circle ends and sorted telomers
				if (adj == startpos || adjacenciesG2[adj] == startpos) {
					endfound = true;
					endpos = pos;
					continue;
				}
				
				// test for newly created circles
				int adj2 = adjacenciesG1[pos];
				int adj3 = adjacenciesG2[adj2];
				
				if (adj3 == pos) {
					endfound = true;
					endpos = pos_pre;
					continue;
				}
			}
			
			// test for mode switches
			if (mode1) {
				int nextindex = adjacenciesG2[pos];
				if (adjacencyLabelsGenom2[pos] != 0) {
					pos = nextindex;
					mode1 = false;
					continue;
				}
			} else {
				int nextindex = adjacenciesG1[pos];
				if (adjacencyLabelsGenom1[pos] != 0) {
					pos = nextindex;
					mode1 = true;
					continue;
				}
			}
			
			if (mode1) { // change Genome 1
			
				int oldpos = pos;
				pos_pre = adjacenciesG1[pos];
				
				pos = resolveCycle(pos, adjacenciesG1, adjacenciesG2, operationList1, OperationInDel.OperationType.Forward,
						intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1, adjacencyLabelsGenom1, newComponents,
						closeEndCycles, true);
				
				if (oldpos == startpos && pos != -1) {
					startpos = pos;
				}
				
				if (pos == -1) {
					endpos = adjacenciesG2[oldpos];
				}
				
			} else { // change Genome 2
			
				int oldpos = pos;
				pos_pre = adjacenciesG2[pos];
				
				pos = resolveCycle(pos, adjacenciesG2, adjacenciesG1, operationList2, OperationInDel.OperationType.Backward,
						intermediateAdjacencyArrayList2, intermediateLabelArrayList2, activeLabelsList2, adjacencyLabelsGenom2, newComponents,
						closeEndCycles, false);
				
				if (oldpos == startpos && pos != -1) {
					startpos = pos;
				}
				if (pos == -1) {
					endpos = adjacenciesG1[oldpos];
				}
			}
			
		}
		
		return new Pair<Integer, Integer>(startpos, endpos);
	}
	
	private int resolveCycle(int pos, int[] adjacenciesG1, int[] adjacenciesG2, List<OperationInDel> operationList,
			OperationInDel.OperationType operationType, List<int[]> intermediateAdjacencyArrayList, List<int[]> intermediateLabelList,
			List<HashMap<Integer, Label>> activeLabelsList, int[] adjacencyLabels, List<Integer> newComponents, boolean closeEndCycles, boolean type) {
		
		int adj1, adj2, adj3;
		
		adj1 = adjacenciesG1[pos];
		adj2 = adjacenciesG2[pos];
		adj3 = adjacenciesG1[adj2];
		
		if (!closeEndCycles && pos == adj1 && adj2 == adj3) {
			return -1; // nonsense next condition to avoid endless cycling
		}
		
		if (pos != adj2 && adj1 != adj2) {
			
			// Replace old adjacencies
			adjacenciesG1[pos] = adj2;
			adjacenciesG1[adj2] = pos;
			
			operationList.add(new OperationInDel(pos, adj2, operationType));
			
			// Distinguish between telomere & adjacency in genome1
			if (adj2 == adj3 && pos != adj1) {
				adjacenciesG1[adj1] = adj1;
				
				moveLabels(adjacencyLabels, pos, adj1, adj2, adj3);
				
				newComponents.add(adj2);
				
				operationList.add(new OperationInDel(adj1, adj1, operationType));
			} else {// second distinction between telomere & adjacency
				if (pos == adj1 && adj2 != adj3) {
					
					// add movement operation
					adjacenciesG1[adj3] = adj3;
					
					moveLabels(adjacencyLabels, pos, adj1, adj2, adj3);
					
					newComponents.add(adj3);
					
					operationList.add(new OperationInDel(adj3, adj3, operationType));
				} else if (pos != adj1 && adj2 != adj3) {
					adjacenciesG1[adj1] = adj3;
					adjacenciesG1[adj3] = adj1;
					
					moveLabels(adjacencyLabels, pos, adj1, adj2, adj3);
					
					newComponents.add(adj3);
					
					operationList.add(new OperationInDel(adj1, adj3, operationType));
				} else {
					moveLabels(adjacencyLabels, 0, adj1, 0, adj3);
					
					if (operationType == OperationInDel.OperationType.Backward) {
						// special case editing the last operation to find
						operationList.remove(operationList.size() - 1); // kill last
						operationList.add(new OperationInDel(pos, adj2, OperationInDel.OperationType.BackwardInDel));
					}
					
					if (operationType == OperationInDel.OperationType.Forward) {
						// special case editing the last operation to find
						operationList.remove(operationList.size() - 1); // kill last
						operationList.add(new OperationInDel(pos, adj2, OperationInDel.OperationType.ForwardInDel));
					}
					
					operationList.add(new OperationInDel(0, 0, operationType));
				}
				
			}
			
			intermediateAdjacencyArrayList.add(adjacenciesG1.clone());
			intermediateLabelList.add(adjacencyLabels.clone());
			activeLabelsList.add((HashMap<Integer, Label>) labels.clone());
			
			if (type) { // genome 1
				addSingletons1();
			} else { // genome 2
				addSingletons2();
			}
		}
		
		return adj3;
	}
	
	private void addSingletons1() {
		intermediateSingletons1.add((List<Integer>) aaAsingletons.clone());
		intermediateSingletons1Circular.add((List<Integer>) aaAsingletonsCircular.clone());
	}
	
	private void addSingletons2() {
		intermediateSingletons2.add((List<Integer>) bbBsingletons.clone());
		intermediateSingletons2Circular.add((List<Integer>) bbBsingletonsCircular.clone());
	}
	
	// /////////////////////////////////////////////////////////////////////////
	// Recombination Groups //
	// /////////////////////////////////////////////////////////////////////////
	
	private void applyRecombinationGroups() {
		
		while (applyP()) {}
		while (applyQ()) {}
		
		boolean t = true;
		while (t) {
			t = false;
			// T is devided into 2 subgroup
			if (applyT1()) { // no reusable
				t = true;
			}
			if (applyT2()) { // reusable
				t = true;
			}
		}
		
		boolean s = true;
		while (s) {
			s = false;
			if (applyS1()) { // no reusable
				s = true;
			}
			if (applyS2()) { // 1 reuseable
				s = true;
			}
			if (applyS3()) { // 2 reuseable
				s = true;
			}
		}
		while (applyM()) {}
		
		while (applyN()) {}
	}
	
	// -------------------------- P ------------------------------------
	
	private boolean applyP() {
		boolean applied = false;
		while (aaABStarts.size() >= 1 && bbABStarts.size() >= 1) {
			
			// retrieve components
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> bb = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(bb.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAab_BBab(aa, bb);
			components.add(res1.getFirst().getFirst());
			components.add(res1.getSecond().getFirst());
			
			applied = true;
		}
		
		return applied;
	}
	
	// -------------------------- Q ------------------------------------
	
	private boolean applyQ() {
		
		boolean applied = false;
		
		// singleton free
		while (aaABStarts.size() >= 2 && bbAStarts.size() >= 1 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(a.getKey());
			
			Map.Entry<Integer, Integer> b = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBa_AAab(a, ab1);
			components.add(res1.getFirst().getFirst());
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_AAab(b, ab2);
			components.add(res2.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res2.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos.getFirst(), newpos.getSecond());
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = ABab_ABba(ABab, ABba);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		// singleton bbB
		while (aaABStarts.size() >= 2 && bbAStarts.size() >= 1 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(a.getKey());
			
			int bIndex = bbBsingletons.remove(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBa_AAab(a, ab1);
			components.add(res1.getFirst().getFirst());
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_AAabSingleton(bIndex, ab2);
			
			components.add(res2.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(res2.getSecond().getFirst(), res2.getSecond()
					.getSecond());
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = ABab_ABba(ABab, ABba);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		// singleton free
		while (bbABStarts.size() >= 2 && aaAStarts.size() >= 1 && aaBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(a.getKey());
			
			Map.Entry<Integer, Integer> b = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAb_BBab(b, ab1);
			components.add(res1.getFirst().getFirst());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAa_BBab(a, ab2);
			components.add(res2.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res2.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos.getFirst(), newpos.getSecond());
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = ABab_ABba(ABab, ABba);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		// Singleton aaA
		while (bbABStarts.size() >= 2 && aaAsingletons.size() >= 1 && aaBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab2.getKey());
			
			int aIndex = aaAsingletons.get(0);
			
			Map.Entry<Integer, Integer> b = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAb_BBab(b, ab1);
			components.add(res1.getFirst().getFirst());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAa_BBabSingleton(aIndex, ab2);
			aaAsingletons.remove(0);
			components.add(res2.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(res2.getSecond().getFirst(), res2.getSecond()
					.getSecond());
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = ABab_ABba(ABab, ABba);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		return applied;
	}
	
	// -------------------------- T ------------------------------------
	
	private boolean applyT1() {
		
		boolean applied = false;
		
		while (aaABStarts.size() >= 1 && bbAStarts.size() >= 1 && abABStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ab = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab.getKey());
			
			Map.Entry<Integer, Integer> a = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBa_AAab(a, aa);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = ABab_ABba(ab, ABba);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
			
		}
		while (bbABStarts.size() >= 1 && aaBStarts.size() >= 1 && abABStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ab = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab.getKey());
			
			Map.Entry<Integer, Integer> a = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAb_BBab(a, aa);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = ABab_ABba(ab, ABba);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		
		// singleton free
		while (aaABStarts.size() >= 1 && bbBStarts.size() >= 1 && abBAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			Map.Entry<Integer, Integer> a = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBb_AAab(a, aa);
			components.add(res1.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos.getFirst(), newpos.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = ABab_ABba(ABab, ab);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		// with bbB singleton
		while (aaABStarts.size() >= 1 && bbBsingletons.size() >= 1 && abBAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			int aIndex = bbBsingletons.remove(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBb_AAabSingleton(aIndex, aa);
			
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = ABab_ABba(ABab, ab);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		// without singleton
		while (bbABStarts.size() >= 1 && aaAStarts.size() >= 1 && abBAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			Map.Entry<Integer, Integer> a = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_BBab(a, aa);
			components.add(res1.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(newpos.getFirst(), newpos.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = ABab_ABba(ABba, ab);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		// with aaA singleton
		while (bbABStarts.size() >= 1 && aaAsingletons.size() >= 1 && abBAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			int aIndex = aaAsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_BBabSingleton(aIndex, aa);
			aaAsingletons.remove(0);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = ABab_ABba(ABab, ab);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		return applied;
	}
	
	private boolean applyT2() {
		
		boolean applied = false;
		
		while (aaABStarts.size() >= 2 && bbAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBa_AAab(a, ab1);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAab_ABba(ab2, ABba);
			components.add(res2.getFirst().getFirst());
			aaBStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		
		while (bbABStarts.size() >= 2 && aaBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAb_BBab(a, ab1);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABba = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBab_ABba(ab2, ABba);
			components.add(res2.getFirst().getFirst());
			bbAStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		
		// without singletons
		while (aaABStarts.size() >= 2 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> b = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBb_AAab(b, ab1);
			components.add(res1.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos.getFirst(), newpos.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAab_ABab(ab2, ABab);
			components.add(res2.getFirst().getFirst());
			aaAStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		// with bbB singleton
		while (aaABStarts.size() >= 2 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(ab2.getKey());
			
			int bIndex = bbBsingletons.remove(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBb_AAabSingleton(bIndex, ab1);
			
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAab_ABab(ab2, ABab);
			components.add(res2.getFirst().getFirst());
			aaAStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		
		// without singleton
		while (bbABStarts.size() >= 2 && aaAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_BBab(a, ab1);
			components.add(res1.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos.getFirst(), newpos.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBab_ABab(ab2, ABab);
			components.add(res2.getFirst().getFirst());
			bbBStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		// with aaA singleton
		while (bbABStarts.size() >= 2 && aaAsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(ab2.getKey());
			
			int aIndex = aaAsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_BBabSingleton(aIndex, ab1);
			aaAsingletons.remove(0);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> ABab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBab_ABab(ab2, ABab);
			components.add(res2.getFirst().getFirst());
			bbBStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		
		return applied;
	}
	
	// -------------------------- S ------------------------------------
	
	private boolean applyS1() {
		
		boolean applied = false;
		
		// without singletons
		while (aaAStarts.size() >= 1 && bbAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> bb = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(bb.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_BBa(aa, bb);
			components.add(res1.getFirst().getFirst());
			components.add(res1.getSecond().getFirst());
			
			applied = true;
		}
		// with aaA singletons
		while (aaAsingletons.size() >= 1 && bbAStarts.size() >= 1) {
			
			int aaIndex = aaAsingletons.get(0);
			
			Map.Entry<Integer, Integer> bb = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(bb.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_BBaSingleton(aaIndex, bb);
			aaAsingletons.remove(0);
			components.add(res1.getFirst().getFirst());
			components.add(res1.getSecond().getFirst());
			
			applied = true;
		}
		
		// without singletons
		while (aaBStarts.size() >= 1 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> bb = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(bb.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAb_BBb(aa, bb);
			components.add(res1.getFirst().getFirst());
			components.add(res1.getSecond().getFirst());
			
			applied = true;
		}
		// with bbB singleton
		while (aaBStarts.size() >= 1 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(aa.getKey());
			
			int bbIndex = bbBsingletons.remove(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBb_AAbSingleton(bbIndex, aa);
			
			components.add(res1.getFirst().getFirst());
			components.add(res1.getSecond().getFirst());
			
			applied = true;
		}
		
		while (abABStarts.size() >= 1 && abBAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab.getKey());
			
			Map.Entry<Integer, Integer> ba = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ba.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = ABab_ABba(ab, ba);
			components.add(res1.getFirst().getFirst());
			components.add(res1.getSecond().getFirst());
			
			applied = true;
		}
		
		return applied;
	}
	
	private boolean applyS2() {
		
		boolean applied = false;
		
		while (aaABStarts.size() >= 1 && bbAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ba = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(ba.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBa_AAab(ba, aa);
			components.add(res1.getFirst().getFirst());
			abBAStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		
		// without singletons
		while (aaABStarts.size() >= 1 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> bb = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(bb.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBb_AAab(bb, aa);
			components.add(res1.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			abABStarts.put(newpos.getFirst(), newpos.getSecond());
			
			applied = true;
		}
		// with bbB singleton
		while (aaABStarts.size() >= 1 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			int bbIndex = bbBsingletons.remove(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBb_AAabSingleton(bbIndex, aa);
			
			components.add(res1.getFirst().getFirst());
			abABStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		
		// without singletons
		while (bbABStarts.size() >= 1 && aaAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> bb = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(bb.getKey());
			
			Map.Entry<Integer, Integer> aa = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(aa.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_BBab(aa, bb);
			components.add(res1.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			abABStarts.put(newpos.getFirst(), newpos.getSecond());
			
			applied = true;
		}
		// with aaA singleton
		while (bbABStarts.size() >= 1 && aaAsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> bb = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(bb.getKey());
			
			int aaIndex = aaAsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_BBabSingleton(aaIndex, bb);
			aaAsingletons.remove(0);
			components.add(res1.getFirst().getFirst());
			abABStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		
		while (bbABStarts.size() >= 1 && aaBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> bb = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(bb.getKey());
			
			Map.Entry<Integer, Integer> aa = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(aa.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAb_BBab(aa, bb);
			components.add(res1.getFirst().getFirst());
			abBAStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		
		while (aaABStarts.size() >= 1 && abABStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ab = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAab_ABab(aa, ab);
			components.add(res1.getFirst().getFirst());
			aaAStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		
		while (aaABStarts.size() >= 1 && abBAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> aa = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa.getKey());
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAab_ABba(aa, ab);
			components.add(res1.getFirst().getFirst());
			aaBStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		
		while (bbABStarts.size() >= 1 && abABStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> bb = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(bb.getKey());
			
			Map.Entry<Integer, Integer> ab = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBab_ABab(bb, ab);
			components.add(res1.getFirst().getFirst());
			bbBStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		
		while (bbABStarts.size() >= 1 && abBAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> bb = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(bb.getKey());
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBab_ABba(bb, ab);
			components.add(res1.getFirst().getFirst());
			bbAStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		
		return applied;
	}
	
	private boolean applyS3() {
		
		boolean applied = false;
		
		while (aaABStarts.size() >= 2) {
			
			Map.Entry<Integer, Integer> aa1 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa1.getKey());
			
			Map.Entry<Integer, Integer> aa2 = aaABStarts.entrySet().iterator().next();
			aaABStarts.remove(aa2.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAab_AAab(aa1, aa2);
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getFirst().getFirst(), false, newComponents);
			
			aaAStarts.put(newpos.getFirst(), newpos.getSecond());
			aaBStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			applied = true;
		}
		while (bbABStarts.size() >= 2) {
			
			Map.Entry<Integer, Integer> aa1 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(aa1.getKey());
			
			Map.Entry<Integer, Integer> aa2 = bbABStarts.entrySet().iterator().next();
			bbABStarts.remove(aa2.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = BBab_BBab(aa1, aa2);
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			bbAStarts.put(res1.getFirst().getFirst(), res1.getFirst().getSecond());
			bbBStarts.put(newpos.getFirst(), newpos.getSecond());
			
			applied = true;
		}
		
		return applied;
	}
	
	// -------------------------- M ------------------------------------
	
	private boolean applyM() {
		
		boolean applied = false;
		
		while (abABStarts.size() >= 2 && aaBStarts.size() >= 1 && bbAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(a.getKey());
			
			Map.Entry<Integer, Integer> b = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAb_ABab(a, ab1);
			components.add(res1.getFirst().getFirst());
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBa_ABab(b, ab2);
			components.add(res2.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			Map.Entry<Integer, Integer> BBab = new AbstractMap.SimpleEntry<Integer, Integer>(res2.getSecond().getFirst(), res2.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = AAab_BBab(AAab, BBab);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		// without singletons
		while (abBAStarts.size() >= 2 && aaAStarts.size() >= 1 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(a.getKey());
			
			Map.Entry<Integer, Integer> b = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_ABba(a, ab1);
			components.add(res1.getFirst().getFirst());
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_ABba(b, ab2);
			components.add(res2.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos1 = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			Pair<Integer, Integer> newpos2 = resolveComponent(res2.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos1.getFirst(), newpos1.getSecond());
			Map.Entry<Integer, Integer> BBab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos2.getFirst(), newpos2.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = AAab_BBab(AAab, BBab);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		// with aaA singleton
		while (abBAStarts.size() >= 2 && aaAsingletons.size() >= 1 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab2.getKey());
			
			int aIndex = aaAsingletons.get(0);
			
			Map.Entry<Integer, Integer> b = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_ABbaSingleton(aIndex, ab1);
			aaAsingletons.remove(0);
			components.add(res1.getFirst().getFirst());
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_ABba(b, ab2);
			components.add(res2.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos2 = resolveComponent(res2.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			Map.Entry<Integer, Integer> BBab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos2.getFirst(), newpos2.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = AAab_BBab(AAab, BBab);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		// with bbB singleton
		while (abBAStarts.size() >= 2 && aaAStarts.size() >= 1 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(a.getKey());
			
			int bIndex = bbBsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_ABba(a, ab1);
			components.add(res1.getFirst().getFirst());
			
			bbBsingletons.remove(0);
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_ABbaSingleton(bIndex, ab2);
			
			components.add(res2.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos1 = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos1.getFirst(), newpos1.getSecond());
			Map.Entry<Integer, Integer> BBab = new AbstractMap.SimpleEntry<Integer, Integer>(res2.getSecond().getFirst(), res2.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = AAab_BBab(AAab, BBab);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		// with aaA singleton AND bbB Singleton
		while (abBAStarts.size() >= 2 && aaAsingletons.size() >= 1 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab2.getKey());
			
			int aIndex = aaAsingletons.get(0);
			
			int bIndex = bbBsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_ABbaSingleton(aIndex, ab1);
			aaAsingletons.remove(0);
			bbBsingletons.remove(0);
			components.add(res1.getFirst().getFirst());
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_ABbaSingleton(bIndex, ab2);
			
			components.add(res2.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			Map.Entry<Integer, Integer> BBab = new AbstractMap.SimpleEntry<Integer, Integer>(res2.getSecond().getFirst(), res2.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res3 = AAab_BBab(AAab, BBab);
			components.add(res3.getFirst().getFirst());
			components.add(res3.getSecond().getFirst());
			
			applied = true;
		}
		
		return applied;
	}
	
	// -------------------------- N ------------------------------------
	
	private boolean applyN() {
		
		boolean applied = false;
		
		while (abABStarts.size() >= 1 && aaBStarts.size() >= 1 && bbAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab.getKey());
			
			Map.Entry<Integer, Integer> a = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(a.getKey());
			
			Map.Entry<Integer, Integer> b = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAb_ABab(a, ab);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBa_AAab(b, AAab);
			components.add(res2.getFirst().getFirst());
			abBAStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		
		// without singletons
		while (abBAStarts.size() >= 1 && aaAStarts.size() >= 1 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			Map.Entry<Integer, Integer> a = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(a.getKey());
			
			Map.Entry<Integer, Integer> b = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_ABba(a, ab);
			components.add(res1.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos1 = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos1.getFirst(), newpos1.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_AAab(b, AAab);
			components.add(res2.getFirst().getFirst());
			
			Pair<Integer, Integer> newpos2 = resolveComponent(res2.getSecond().getFirst(), false, newComponents);
			abABStarts.put(newpos2.getFirst(), newpos2.getSecond());
			
			applied = true;
		}
		// with aaA singleton
		while (abBAStarts.size() >= 1 && aaAsingletons.size() >= 1 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			int aIndex = aaAsingletons.get(0);
			
			Map.Entry<Integer, Integer> b = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_ABbaSingleton(aIndex, ab);
			aaAsingletons.remove(0);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_AAab(b, AAab);
			components.add(res2.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos2 = resolveComponent(res2.getSecond().getFirst(), false, newComponents);
			abABStarts.put(newpos2.getFirst(), newpos2.getSecond());
			
			applied = true;
		}
		// with bbB singleton
		while (abBAStarts.size() >= 1 && aaAStarts.size() >= 1 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			Map.Entry<Integer, Integer> a = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(a.getKey());
			
			int bIndex = bbBsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_ABba(a, ab);
			components.add(res1.getFirst().getFirst());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos1 = resolveComponent(res1.getSecond().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(newpos1.getFirst(), newpos1.getSecond());
			
			bbBsingletons.remove(0);
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_AAabSingleton(bIndex, AAab);
			
			components.add(res2.getFirst().getFirst());
			abABStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		// with aaA singleton and bbB singleton
		while (abBAStarts.size() >= 1 && aaAsingletons.size() >= 1 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab.getKey());
			
			int aIndex = aaAsingletons.get(0);
			
			int bIndex = bbBsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = AAa_ABbaSingleton(aIndex, ab);
			aaAsingletons.remove(0);
			components.add(res1.getFirst().getFirst());
			
			Map.Entry<Integer, Integer> AAab = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond()
					.getSecond());
			
			bbBsingletons.remove(0);
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_AAabSingleton(bIndex, AAab);
			
			components.add(res2.getFirst().getFirst());
			abABStarts.put(res2.getSecond().getFirst(), res2.getSecond().getSecond());
			
			applied = true;
		}
		
		while (abABStarts.size() >= 2 && aaBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = aaBStarts.entrySet().iterator().next();
			aaBStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = ABab_ABab(ab1, ab2);
			
			if (res1.getFirst().getFirst() == -1) {
				// singleton condition, do nothing
			} else {
				aaAStarts.put(res1.getFirst().getFirst(), res1.getFirst().getSecond());
			}
			
			Map.Entry<Integer, Integer> BBb = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAb_BBb(a, BBb);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		
		while (abABStarts.size() >= 2 && bbAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abABStarts.entrySet().iterator().next();
			abABStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> b = bbAStarts.entrySet().iterator().next();
			bbAStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = ABab_ABab(ab1, ab2);
			bbBStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			if (res1.getFirst().getFirst() == -1) {
				// singleton condition use special AAa
				int AAa = aaAsingletons.get(0); // at least one is inside
				Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAa_BBaSingleton(AAa, b);
				aaAsingletons.remove(0);
				
				components.add(res2.getFirst().getFirst());
				components.add(res2.getSecond().getFirst());
				
			} else {
				Map.Entry<Integer, Integer> AAa = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getFirst().getFirst(), res1.getFirst()
						.getSecond());
				
				Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAa_BBa(AAa, b);
				components.add(res2.getFirst().getFirst());
				components.add(res2.getSecond().getFirst());
			}
			applied = true;
		}
		
		// without singletons
		while (abBAStarts.size() >= 2 && aaAStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> a = aaAStarts.entrySet().iterator().next();
			aaAStarts.remove(a.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = ABba_ABba(ab1, ab2);
			aaBStarts.put(res1.getFirst().getFirst(), res1.getFirst().getSecond());
			
			Map.Entry<Integer, Integer> BBa = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAa_BBa(a, BBa);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		// with aaA singleton
		while (abBAStarts.size() >= 2 && aaAsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab2.getKey());
			
			int aIndex = aaAsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = ABba_ABba(ab1, ab2);
			aaBStarts.put(res1.getFirst().getFirst(), res1.getFirst().getSecond());
			
			Map.Entry<Integer, Integer> BBa = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAa_BBaSingleton(aIndex, BBa);
			aaAsingletons.remove(0);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		
		// without singleton
		while (abBAStarts.size() >= 2 && bbBStarts.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab2.getKey());
			
			Map.Entry<Integer, Integer> b = bbBStarts.entrySet().iterator().next();
			bbBStarts.remove(b.getKey());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = ABba_ABba(ab1, ab2);
			bbAStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			List<Integer> newComponents = new ArrayList<Integer>();
			Pair<Integer, Integer> newpos = resolveComponent(res1.getFirst().getFirst(), false, newComponents);
			
			Map.Entry<Integer, Integer> AAb = new AbstractMap.SimpleEntry<Integer, Integer>(newpos.getFirst(), newpos.getSecond());
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = AAb_BBb(AAb, b);
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		// with bbB singleton
		while (abBAStarts.size() >= 2 && bbBsingletons.size() >= 1) {
			
			Map.Entry<Integer, Integer> ab1 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab1.getKey());
			
			Map.Entry<Integer, Integer> ab2 = abBAStarts.entrySet().iterator().next();
			abBAStarts.remove(ab2.getKey());
			
			int bIndex = bbBsingletons.get(0);
			
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res1 = ABba_ABba(ab1, ab2);
			bbAStarts.put(res1.getSecond().getFirst(), res1.getSecond().getSecond());
			
			Map.Entry<Integer, Integer> AAb = new AbstractMap.SimpleEntry<Integer, Integer>(res1.getFirst().getFirst(), res1.getFirst().getSecond());
			
			bbBsingletons.remove(0);
			Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res2 = BBb_AAbSingleton(bIndex, AAb);
			
			components.add(res2.getFirst().getFirst());
			components.add(res2.getSecond().getFirst());
			
			applied = true;
		}
		
		return applied;
	}
	
	// -------------------------- Path Recombinations ------------------------------------
	
	// ####### d > 0 #######
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAab_BBab(Map.Entry<Integer, Integer> aa, Map.Entry<Integer, Integer> bb) {
		addSingletons1();
		return helperType1(aa.getKey(), aa.getValue(), bb.getKey(), bb.getValue(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1,
				operationList1, OperationInDel.OperationType.ForwardHelperOne, intermediateAdjacencyArrayList1, intermediateLabelArrayList1,
				activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAa_BBab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		return helperType1(a.getKey(), a.getValue(), b.getKey(), b.getValue(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				OperationInDel.OperationType.ForwardHelperOne, intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBb_AAab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed AAa_BBab
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType1(a.getValue(), a.getKey(), b.getValue(), b.getKey(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, OperationInDel.OperationType.Backward, intermediateAdjacencyArrayList2,
				intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBa_AAab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		// a and b position switched!
		return helperType1(b.getKey(), b.getValue(), a.getKey(), a.getValue(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				OperationInDel.OperationType.ForwardHelperOne, intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAb_BBab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed BBa_AAab
		// a and b position switched!
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType1(b.getValue(), b.getKey(), a.getValue(), a.getKey(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, OperationInDel.OperationType.Backward, intermediateAdjacencyArrayList2,
				intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAa_BBa(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		return helperType1(a.getKey(), a.getValue(), b.getKey(), b.getValue(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				OperationInDel.OperationType.ForwardHelperOne, intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAb_BBb(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed AAa_BBa, but switching also switches component order
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType1(b.getValue(), b.getKey(), a.getValue(), a.getKey(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, OperationInDel.OperationType.Backward, intermediateAdjacencyArrayList2,
				intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBab_BBab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		return helperType2(a.getKey(), a.getValue(), b.getKey(), b.getValue(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				OperationInDel.OperationType.Forward, intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAab_AAab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed BBab_BBab
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType2(a.getValue(), a.getKey(), b.getValue(), b.getKey(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, OperationInDel.OperationType.Backward, intermediateAdjacencyArrayList2,
				intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res2, res1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBab_ABab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		// a and b position switched!
		return helperType1(b.getKey(), b.getValue(), a.getKey(), a.getValue(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				OperationInDel.OperationType.ForwardHelperOne, intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAab_ABab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed BBab_ABab
		// a and b position switched!
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType1(b.getValue(), b.getKey(), a.getValue(), a.getKey(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, OperationInDel.OperationType.Backward, intermediateAdjacencyArrayList2,
				intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBab_ABba(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		// use ABba always backwards !
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType2(a.getKey(), a.getValue(), b.getValue(), b.getKey(), adjacenciesG1,
				adjacenciesG2, adjacencyLabelsGenom1, operationList1, OperationInDel.OperationType.Forward, intermediateAdjacencyArrayList1,
				intermediateLabelArrayList1, activeLabelsList1);
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res.getSecond(), res.getFirst());
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAab_ABba(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed BBab_ABba
		// use ABba always backwards !
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType2(a.getValue(), a.getKey(), b.getKey(), b.getValue(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, OperationInDel.OperationType.Backward, intermediateAdjacencyArrayList2,
				intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res2, res1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> ABab_ABba(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		// use ABba always backwards !
		return helperType1(a.getKey(), a.getValue(), b.getValue(), b.getKey(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				OperationInDel.OperationType.ForwardHelperOne, intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	// ####### d = 0 #######
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAa_ABba(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		// use ABba always backwards !
		return helperType1(a.getKey(), a.getValue(), b.getValue(), b.getKey(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				OperationInDel.OperationType.ForwardHelperOne, intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBb_ABba(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed AAa_ABba
		// use ABba always backwards !
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType1(a.getValue(), a.getKey(), b.getKey(), b.getValue(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, OperationInDel.OperationType.Backward, intermediateAdjacencyArrayList2,
				intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBa_ABab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		// a and b position switched!
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType1(b.getKey(), b.getValue(), a.getKey(), a.getValue(), adjacenciesG1,
				adjacenciesG2, adjacencyLabelsGenom1, operationList1, OperationInDel.OperationType.ForwardHelperOne, intermediateAdjacencyArrayList1,
				intermediateLabelArrayList1, activeLabelsList1);
		
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAb_ABab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed BBa_ABab
		// a and b position switched!
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType1(b.getValue(), b.getKey(), a.getValue(), a.getKey(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, OperationInDel.OperationType.Backward, intermediateAdjacencyArrayList2,
				intermediateLabelArrayList2, activeLabelsList2);
		
		return res;
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> ABba_ABba(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		// use ABba always backwards !
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType2(a.getValue(), a.getKey(), b.getValue(), b.getKey(), adjacenciesG1,
				adjacenciesG2, adjacencyLabelsGenom1, operationList1, OperationInDel.OperationType.Forward, intermediateAdjacencyArrayList1,
				intermediateLabelArrayList1, activeLabelsList1);
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res.getSecond(), res.getFirst());
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> ABab_ABab(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperType3(a.getKey(), a.getValue(), b.getKey(), b.getValue());
		return res;
	}
	
	// ####### d > 0, Singleton use #######
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAa_BBabSingleton(int aLabelIndex, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		return helperSingleton(aLabelIndex, b.getKey(), b.getValue(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAa_BBaSingleton(int aLabelIndex, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		return helperSingleton(aLabelIndex, b.getKey(), b.getValue(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> AAa_ABbaSingleton(int aLabelIndex, Map.Entry<Integer, Integer> b) {
		addSingletons1();
		// use ABba always backwards !
		return helperSingleton(aLabelIndex, b.getValue(), b.getKey(), adjacenciesG1, adjacenciesG2, adjacencyLabelsGenom1, operationList1,
				intermediateAdjacencyArrayList1, intermediateLabelArrayList1, activeLabelsList1);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBb_AAabSingleton(int aLabelIndex, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed AAa_BBab
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperSingleton(aLabelIndex, b.getValue(), b.getKey(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, intermediateAdjacencyArrayList2, intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBb_AAbSingleton(int aLabelIndex, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed AAa_BBa
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperSingleton(aLabelIndex, b.getValue(), b.getKey(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, intermediateAdjacencyArrayList2, intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> BBb_ABbaSingleton(int aLabelIndex, Map.Entry<Integer, Integer> b) {
		addSingletons2();
		// reversed AAa_ABba
		// use ABba always backwards !
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> res = helperSingleton(aLabelIndex, b.getKey(), b.getValue(), adjacenciesG2,
				adjacenciesG1, adjacencyLabelsGenom2, operationList2, intermediateAdjacencyArrayList2, intermediateLabelArrayList2, activeLabelsList2);
		
		// switch ordering cause reversed
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(res.getFirst().getSecond(), res.getFirst().getFirst());
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(res.getSecond().getSecond(), res.getSecond().getFirst());
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	// -------------------------- Path Recombinations base function ------------------------------------
	
	// Type 1l2 and 3l4 to 13 and 2ll4
	// second entry is possible use again resultant!
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> helperType1(int aa1, int aa2, int bb1, int bb2, int[] adjacenciesG1,
			int[] adjacenciesG2, int[] adjacencyLabelsGenom, List<OperationInDel> operationList, OperationInDel.OperationType operationType,
			List<int[]> intermediateAdjacencyArrayList, List<int[]> intermediateLabelArrayList, List<HashMap<Integer, Label>> activeLabelsList) {
		
		// find the split on Genome
		int aaSPlit = findFirstLabel(adjacenciesG2, adjacenciesG1, adjacencyLabelsGenom, aa1);
		int bbSPlit = findFirstLabel(adjacenciesG2, adjacenciesG1, adjacencyLabelsGenom, bb1);
		
		int aaSPlit2 = adjacenciesG1[aaSPlit];
		int bbSPlit2 = adjacenciesG1[bbSPlit];
		
		if (operationType == OperationInDel.OperationType.ForwardHelperOne) {
			operationList.add(new OperationInDel(aaSPlit, bbSPlit, operationType));
		} else {
			operationList.add(new OperationInDel(bbSPlit, bbSPlit, operationType));
		}
		operationList.add(new OperationInDel(aaSPlit2, bbSPlit2, operationType));
		
		adjacenciesG1[aaSPlit2] = bbSPlit2;
		adjacenciesG1[bbSPlit2] = aaSPlit2;
		
		if (aaSPlit2 == aaSPlit) {
			adjacenciesG1[bbSPlit] = bbSPlit;
			
			// i.e. for AAa with only one label on end telomer
			if (aaSPlit == aa2) {
				aa2 = aa1;
			}
			
			aa1 = bbSPlit;
			aaSPlit = 0; // else telomere operation
		} else {
			adjacenciesG1[aaSPlit] = bbSPlit;
			adjacenciesG1[bbSPlit] = aaSPlit;
		}
		
		moveLabels(adjacencyLabelsGenom, aaSPlit, aaSPlit2, bbSPlit, bbSPlit2);
		
		intermediateAdjacencyArrayList.add(adjacenciesG1.clone());
		intermediateLabelArrayList.add(adjacencyLabelsGenom.clone());
		activeLabelsList.add((HashMap<Integer, Label>) labels.clone());
		
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(aa1, bb1);
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(aa2, bb2);
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	// Type l and 1l2 to 1 and ll2
	// second entry is possible use again resultant!
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> helperSingleton(int singleLabelIndex, int bb1, int bb2, int[] adjacenciesG1,
			int[] adjacenciesG2, int[] adjacencyLabelsGenom, List<OperationInDel> operationList, List<int[]> intermediateAdjacencyArrayList,
			List<int[]> intermediateLabelArrayList, List<HashMap<Integer, Label>> activeLabelsList) {
		
		// find the split on Genome
		int bbSPlit = findFirstLabel(adjacenciesG2, adjacenciesG1, adjacencyLabelsGenom, bb1);
		
		int bbSPlit2 = adjacenciesG1[bbSPlit];
		
		// make both ends of split telomers
		adjacenciesG1[bbSPlit] = bbSPlit;
		adjacenciesG1[bbSPlit2] = bbSPlit2;
		
		// find the labels and remove 'em
		Label labelS = labels.remove(singleLabelIndex);
		
		int splitLabelIndex = adjacencyLabelsGenom[bbSPlit];
		Label labelB = labels.remove(splitLabelIndex);
		adjacencyLabelsGenom[bbSPlit] = 0;
		adjacencyLabelsGenom[bbSPlit2] = 0;
		
		Label merged = mergeLabels(labelS, labelB, -1, bbSPlit2);
		
		labels.put(splitLabelIndex, merged);
		adjacencyLabelsGenom[bbSPlit2] = splitLabelIndex;
		
		// add operations as Singleton cut
		String cut1 = IntermediateGenomesGenerator.getSignedGeneName((bbSPlit + 1) / 2, this.geneNameMap);
		String cut2 = getRightmost(labelS.getLabel().getIndex(), labelS.getLabel().isOrientation());
		operationList.add(new OperationInDel(bbSPlit, 0, OperationInDel.OperationType.Singleton, new Pair<String, String>(cut1, cut2)));
		operationList.add(new OperationInDel(bbSPlit, 0, OperationInDel.OperationType.Singleton, new Pair<String, String>(cut1, cut2)));
		
		intermediateAdjacencyArrayList.add(adjacenciesG1.clone());
		intermediateLabelArrayList.add(adjacencyLabelsGenom.clone());
		activeLabelsList.add((HashMap<Integer, Label>) labels.clone());
		
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(bbSPlit, bb1);
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(bbSPlit2, bb2);
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	// Type 1l2 and 3l4 to 1ll3 and 24
	// second entry is possible use again resultant!
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> helperType2(int aa1, int aa2, int bb1, int bb2, int[] adjacenciesG1,
			int[] adjacenciesG2, int[] adjacencyLabelsGenom, List<OperationInDel> operationList, OperationInDel.OperationType operationType,
			List<int[]> intermediateAdjacencyArrayList, List<int[]> intermediateLabelArrayList, List<HashMap<Integer, Label>> activeLabelsList) {
		
		// find the split on Genome
		int aaSPlit = findFirstLabel(adjacenciesG2, adjacenciesG1, adjacencyLabelsGenom, aa1);
		int bbSPlit = findFirstLabel(adjacenciesG2, adjacenciesG1, adjacencyLabelsGenom, bb1);
		
		int aaSPlit2 = adjacenciesG1[aaSPlit];
		int bbSPlit2 = adjacenciesG1[bbSPlit];
		
		adjacenciesG1[aaSPlit2] = bbSPlit2;
		adjacenciesG1[bbSPlit2] = aaSPlit2;
		
		adjacenciesG1[aaSPlit] = bbSPlit;
		adjacenciesG1[bbSPlit] = aaSPlit;
		
		operationList.add(new OperationInDel(aaSPlit, bbSPlit, operationType));
		operationList.add(new OperationInDel(aaSPlit2, bbSPlit2, operationType));
		
		moveLabels(adjacencyLabelsGenom, aaSPlit2, aaSPlit, bbSPlit2, bbSPlit);
		
		intermediateAdjacencyArrayList.add(adjacenciesG1.clone());
		intermediateLabelArrayList.add(adjacencyLabelsGenom.clone());
		activeLabelsList.add((HashMap<Integer, Label>) labels.clone());
		
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(aa1, bb1);
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(aa2, bb2);
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	// Type 1l2 and 3l4 to 1ll3 and 24
	// only used for ABab ABab
	// second entry is possible use again resultant!
	private Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> helperType3(int aa1, int aa2, int bb1, int bb2) {
		
		// find the split on Genome
		int aaSPlit = findFirstLabel(adjacenciesG2, adjacenciesG1, adjacencyLabelsGenom1, aa1);
		int bbSPlit = findFirstLabel(adjacenciesG2, adjacenciesG1, adjacencyLabelsGenom1, bb1);
		
		int aaSPlit2 = adjacenciesG1[aaSPlit];
		int bbSPlit2 = adjacenciesG1[bbSPlit];
		
		if (aaSPlit == aaSPlit2 && bbSPlit == bbSPlit2) {
			
			// operation
			adjacenciesG1[aaSPlit] = bbSPlit;
			adjacenciesG1[bbSPlit] = aaSPlit;
			
			operationList1.add(new OperationInDel(aaSPlit, bbSPlit, OperationInDel.OperationType.Forward));
			operationList1.add(new OperationInDel(aaSPlit, bbSPlit, OperationInDel.OperationType.Forward));
			
			int l1 = adjacencyLabelsGenom1[aaSPlit];
			int l2 = adjacencyLabelsGenom1[bbSPlit];
			
			Label label1 = labels.remove(l1);
			Label label2 = labels.remove(l2);
			
			adjacencyLabelsGenom1[aaSPlit] = 0;
			adjacencyLabelsGenom1[bbSPlit] = 0;
			
			// create new entry for new Label
			LabelIndex use = new LabelIndex(sublabelCount, true);
			
			LabelIndex index1 = new LabelIndex(label1.getLabel().getIndex(), label1.getLabel().isOrientation());
			LabelIndex index2 = new LabelIndex(label2.getLabel().getIndex(), label2.getLabel().isOrientation());
			
			LabelIndexPair pair = new LabelIndexPair(index1, index2);
			subLabels.put(sublabelCount, pair);
			sublabelCount++;
			
			Label newSingleton = new Label(-1, -1, use);
			
			labels.put(l1, newSingleton);
			aaAsingletons.add(l1);
			
			intermediateAdjacencyArrayList1.add(adjacenciesG1.clone());
			intermediateLabelArrayList1.add(adjacencyLabelsGenom1.clone());
			activeLabelsList1.add((HashMap<Integer, Label>) labels.clone());
			
			Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(-1, -1);
			Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(aa2, bb2);
			
			return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
		}
		
		// normal case with two graph results :)
		
		adjacenciesG1[aaSPlit2] = bbSPlit2;
		adjacenciesG1[bbSPlit2] = aaSPlit2;
		
		if (aaSPlit == aaSPlit2) {
			adjacenciesG1[bbSPlit] = bbSPlit;
			aa1 = bbSPlit;
		} else if (bbSPlit == bbSPlit2) {
			adjacenciesG1[aaSPlit] = aaSPlit;
			bb1 = aaSPlit;
		} else {
			adjacenciesG1[aaSPlit] = bbSPlit;
			adjacenciesG1[bbSPlit] = aaSPlit;
		}
		
		operationList1.add(new OperationInDel(aaSPlit, bbSPlit, OperationInDel.OperationType.Forward));
		operationList1.add(new OperationInDel(aaSPlit2, bbSPlit2, OperationInDel.OperationType.Forward));
		
		moveLabels(adjacencyLabelsGenom1, aaSPlit2, aaSPlit, bbSPlit2, bbSPlit);
		
		intermediateAdjacencyArrayList1.add(adjacenciesG1.clone());
		intermediateLabelArrayList1.add(adjacencyLabelsGenom1.clone());
		activeLabelsList1.add((HashMap<Integer, Label>) labels.clone());
		
		Pair<Integer, Integer> res1 = new Pair<Integer, Integer>(aa1, bb1);
		Pair<Integer, Integer> res2 = new Pair<Integer, Integer>(aa2, bb2);
		
		return new Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>(res1, res2);
	}
	
	// -------------------------- general helper ------------------------------------
	
	private int findFirstLabel(int[] adjacenciesG1, int[] adjacenciesG2, int[] adjacencyLabels, int start) {
		
		int pos = start;
		
		if (adjacencyLabels[pos] != 0) {
			return pos;
		}
		
		// test for telomers
		while (adjacenciesG2[pos] != pos || (pos == start && adjacenciesG2[pos] != adjacenciesG1[pos])) {
			
			if (adjacencyLabels[pos] != 0) {
				return pos;
			}
			
			int adj2, adj3;
			adj2 = adjacenciesG2[pos];
			adj3 = adjacenciesG1[adj2];
			pos = adj3;
		}
		
		if (adjacencyLabels[pos] != 0) {
			return pos;
		}
		
		// if used right never happens
		return -1;
	}
	
	// /////////////////////////////////////////////////////////////////////////
	// Label Operations //
	// /////////////////////////////////////////////////////////////////////////
	
	private void moveLabels(int[] adjacencyLabels, int pos, int adj1, int adj2, int adj3) {
		int useLabel = 0;
		
		int adj1LabelIndex = adjacencyLabels[pos];
		if (adj1LabelIndex == 0) {
			adj1LabelIndex = adjacencyLabels[adj1];
		}
		
		Label adj1Label = null;
		if (adj1LabelIndex > 0) {
			
			useLabel = adj1LabelIndex;
			adjacencyLabels[pos] = 0;
			adjacencyLabels[adj1] = 0;
			
			adj1Label = labels.get(adj1LabelIndex);
			labels.remove(adj1LabelIndex);
		}
		
		int adj2LabelIndex = adjacencyLabels[adj3];
		Label adj2Label = null;
		if (adj2LabelIndex > 0) {
			
			useLabel = adj2LabelIndex;
			adjacencyLabels[adj2] = 0;
			adjacencyLabels[adj3] = 0;
			
			adj2Label = labels.get(adj2LabelIndex);
			labels.remove(adj2LabelIndex);
		}
		
		if (adj1LabelIndex > 0 || adj2LabelIndex > 0) {
			
			int switchAdj1 = adj1;
			int switchAdj2 = adj3;
			
			// special telomer conditions
			if (pos == adj1) {
				adjacencyLabels[adj3] = useLabel;
				switchAdj1 = -1;
			} else if (adj2 == adj3) { // telomer 2
				switchAdj2 = -1;
				adjacencyLabels[adj1] = useLabel;
			} else { // no telomer
				adjacencyLabels[adj1] = useLabel;
				adjacencyLabels[adj3] = useLabel;
			}
			
			labels.put(useLabel, mergeLabels(adj1Label, adj2Label, switchAdj1, switchAdj2));
		}
	}
	
	private Label mergeLabels(Label moveLabel, Label setLabel, int move, int setto) {
		
		LabelIndex suspendedMove = null;
		if (moveLabel != null) {
			
			suspendedMove = new LabelIndex(moveLabel.getLabel().getIndex(), moveLabel.getLabel().isOrientation());
			
			if (moveLabel.getRight() == move) {
				suspendedMove.switchOrientation();
			}
		}
		
		// retrieve the contents of the label that is completely set
		LabelIndex suspendedSet = null;
		if (setLabel != null) {
			
			suspendedSet = new LabelIndex(setLabel.getLabel().getIndex(), setLabel.getLabel().isOrientation());
			
			if (setLabel.getLeft() == setto) {
				suspendedSet.switchOrientation();
			}
		}
		
		// create new subLabel if necessary and determine new label content
		LabelIndex use;
		if (suspendedMove != null && suspendedSet != null) {
			
			use = new LabelIndex(sublabelCount, true);
			
			LabelIndexPair pair = new LabelIndexPair(suspendedMove, suspendedSet);
			subLabels.put(sublabelCount, pair);
			sublabelCount++;
		} else if (suspendedMove != null) {
			use = suspendedMove;
		} else {
			use = suspendedSet;
		}
		
		return new Label(move, setto, use);
	}
	
	private String getRightmost(int index) {
		return getRightmost(index, true);
	}
	
	private String getRightmost(int index, boolean orientation) {
		
		if (baseLabels.containsKey(index)) {
			List<String> label = baseLabels.get(index);
			if (orientation) {
				return label.get(label.size() - 1);
			} else {
				return switchString(label.get(0));
			}
		}
		
		LabelIndexPair pair = subLabels.get(index);
		if (orientation) {
			return getRightmost(pair.getRight().getIndex(), pair.getRight().isOrientation());
		} else {
			return getRightmost(pair.getLeft().getIndex(), !pair.getLeft().isOrientation());
		}
	}
	
	private String getLeftmost(int index) {
		return getLeftmost(index, true);
	}
	
	private String getLeftmost(int index, boolean orientation) {
		
		if (baseLabels.containsKey(index)) {
			List<String> label = baseLabels.get(index);
			if (orientation) {
				return label.get(0);
			} else {
				return switchString(label.get(label.size() - 1));
			}
		}
		
		LabelIndexPair pair = subLabels.get(index);
		if (orientation) {
			return getRightmost(pair.getLeft().getIndex(), pair.getLeft().isOrientation());
		} else {
			return getRightmost(pair.getRight().getIndex(), !pair.getRight().isOrientation());
		}
	}
	
	private String switchString(String gene) {
		
		if (gene == null) {
			return null;
		}
		
		if (gene.startsWith("-")) {
			gene = gene.substring(1);
		} else {
			gene = "-" + gene;
		}
		return gene;
	}
	
}
