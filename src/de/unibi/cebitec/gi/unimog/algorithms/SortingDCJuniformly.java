package de.unibi.cebitec.gi.unimog.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import de.luschny.math.arithmetic.Xint;
import de.unibi.cebitec.gi.unimog.datastructure.Data;
import de.unibi.cebitec.gi.unimog.datastructure.IAdditionalData;
import de.unibi.cebitec.gi.unimog.datastructure.OperationList;
import de.unibi.cebitec.gi.unimog.datastructure.Pair;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.AdjacencyGraphSampling;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.ComponentSample;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.OrderedComponentList;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.SampleWeights;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.Structure;
import de.unibi.cebitec.gi.unimog.datastructure.sampling.VertexInfo;
import de.unibi.cebitec.gi.unimog.framework.MainClass;
import de.unibi.cebitec.gi.unimog.framework.MainFrame;
import de.unibi.cebitec.gi.unimog.utils.Constants;
import de.unibi.cebitec.gi.unimog.utils.Toolz;

/**
 * Class which implements the DCJ sorting algorithm where one optimal DCJ-sorting scenario is chosen uniformly among all
 * optimal scenarios.
 * 
 * @author Eyla Willing
 */
public class SortingDCJuniformly implements ISorting {
	
	/** Number of sorting scenarios for current AG */
    private Xint                     scenariosAG;
    /** Sum of distances of all components in the AG */
    private int                      dAG;
    private OrderedComponentList     orderedC;
    private ArrayList<SampleWeights> buckets;
    private int[]                    extremitiesB;
    private MainFrame                mainframe;

    /**
     * Constructor providing an instance of {@link MainFrame} to show dialog.
     * 
     * @param mainFrameInstance instance of {@link MainFrame} used in {@link MainClass}
     */
    public SortingDCJuniformly(MainFrame mainFrameInstance) {
        super();
        this.mainframe = mainFrameInstance;
    }

    @SuppressWarnings("javadoc")
    @Override
    public OperationList findOptSortSequence(final Data data, final IAdditionalData additionalData,
        final HashMap<Integer, Integer> chromMapA) {

        OperationList ops = new OperationList();
        final AdjacencyGraphSampling graph = (AdjacencyGraphSampling) data.getAdjGraph();
        this.extremitiesB = graph.getAdjacenciesGenome2();
        this.orderedC = graph.getOrderedListOfComponents();

        this.dAG = graph.getDCJdistance();
        int[] extremitiesA = graph.getAdjacenciesGenome1().clone();
        if (this.dAG == 0) {
        	// no comparison necessary, abort
        	return ops;
        }
        if (!MainFrame.IGNORE_RECOMBS && (this.orderedC.isRecombinationPossible() || graph.isRecombinationPossible())) {
            this.mainframe.confirmNoRecombs();
        }
        while (this.dAG > 1) {
            // normal without recombs
            this.scenariosAG = graph.getLowerBoundScenarios();
            this.sampleIndividually(ops, extremitiesA);
        }

        // now only 1 DCJ operation left over -> no need to "sample"
        try {
            ComponentSample lastComp = this.orderedC.getLastElement();
            final int p = lastComp.getStartIndex(); // FIXME
            final int endIndex = lastComp.getEndIndex();
            int q, r, s;
            switch (lastComp.getStructure()) {
            case CYCLE: // O -> 00; pq rs => rq ps
                // start and end are pq
                r = this.extremitiesB[p];
                q = extremitiesA[p];
                s = this.extremitiesB[q];
                extremitiesA[p] = r;
                extremitiesA[r] = p;
                extremitiesA[q] = s;
                extremitiesA[s] = q;
                ops.addOperation(new Pair<Integer, Integer>(p, r));
                ops.addOperation(new Pair<Integer, Integer>(q, s));
                break;
            case BB: // /\ -> ||
                q = endIndex;
                extremitiesA[p] = p;
                extremitiesA[q] = q;
                ops.addOperation(new Pair<Integer, Integer>(p, p));
                ops.addOperation(new Pair<Integer, Integer>(q, q));
                break;
            case AB: // V\ -> O + | ; °q | rs -> qr | °s
                // AB- path always starts in A -> startIndex in A, endIndex in B; p == q
                q = extremitiesA[p];
                s = endIndex;
                r = extremitiesA[s]; // this.extremitiesB[q]
                extremitiesA[q] = r;
                extremitiesA[r] = q;
                extremitiesA[s] = s;
                ops.addOperation(new Pair<Integer, Integer>(q, r));
                ops.addOperation(new Pair<Integer, Integer>(s, s));
                break;
            case AA: // V -> O ; °q | r° => °° + rq
                q = endIndex;
                extremitiesA[p] = q;
                extremitiesA[q] = p;
                ops.addOperation(new Pair<Integer, Integer>(p, q));
                ops.addOperation(new Pair<Integer, Integer>(0, 0));
                break;
            default:
                System.err.println("hilfe?");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        ops.addAdjacencyArrayG1(extremitiesA.clone());
        --this.dAG; // should now be 0
        return ops;
    }

    private OperationList sampleIndividually(OperationList ops, int[] extremitiesA) {
        // while (this.dAG > 1) {
        this.buckets = new ArrayList<SampleWeights>(this.dAG);

        for (Integer d_c : this.orderedC.getDistanceList()) {
            List<ComponentSample> distanceGroup = this.orderedC.getComponents(d_c);
            int numberOfSplits;
            try {
                numberOfSplits = Constants.SMALL_SPLIT_GROUP_NUMBERS[d_c];
            } catch (ArrayIndexOutOfBoundsException e) {
                if (d_c % 2 == 0) { // even
                    numberOfSplits = d_c / 2;
                } else { // odd
                    numberOfSplits = (d_c - 1) / 2;
                }
            }
            int j;
            for (j = 1; j <= numberOfSplits; j++) {
                final Xint representativeScenarios = Toolz.getScenariosOfRepresentative(this.scenariosAG, this.dAG,
                    d_c, j);
                this.buckets.add(new SampleWeights(d_c, j, distanceGroup.size(), representativeScenarios, false));
            }
            if (d_c % 2 != 0) {
                final Xint representativeScenarios = Toolz.getScenariosOfRepresentative(this.scenariosAG, this.dAG,
                    d_c, j);
                this.buckets.add(new SampleWeights(d_c, j, distanceGroup.size(), representativeScenarios, true));
            }
        }
        this.buckets.trimToSize();

        try {
            // sample from R -> (d,j)
            SampleWeights targetBucket = this.sampleFromSplitComponentSet();
            int j = targetBucket.getSplitGroupID();
            int sampledDistance = targetBucket.getGroupDistance();

            // sample component [0...#Comps in List )
            final int sampledComponentIndex = (new Random()).nextInt(targetBucket.getNumberOfComponents());
            final ComponentSample sampledComponent = (this.orderedC.getComponents(sampledDistance))
                .get(sampledComponentIndex);

            // sample cutPoints
            final VertexInfo sampledExtremities = Toolz.sampledAdjacencies(sampledDistance, j, sampledComponent,
                extremitiesA, this.extremitiesB);

            // perform sampled DCJ on sampled component
            this.performDCJoperation(sampledComponent, ops, extremitiesA, sampledExtremities, sampledDistance, j);
            // d is already updated
            this.scenariosAG = targetBucket.getScenariosPerElement();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.exit(1);
        }
        // }
        return ops;
    }
    
    /**
     * Choose random vertex in genome B whose adjacency is to be created in A. In effect, trivial cycles are extracted in the adjacency graph.
     * @param ops
     * @param extremitiesA
     * @return
     * List of sampled operation to be performed.
     */
    private OperationList sampleVertex(OperationList ops, int[] extremitiesA) {
    	// while (this.dAG > 1) {
    	
		int verticesAG  = this.orderedC.getNumberOfUnsortedComponents() + this.dAG;
		this.buckets = new ArrayList<SampleWeights>(verticesAG);
    	for (Integer d_c : this.orderedC.getDistanceList()) {
    		List<ComponentSample> distanceGroup = this.orderedC.getComponents(d_c);
    		Xint numberOfVertices = Xint.ONE;
    		numberOfVertices = numberOfVertices.add(d_c); // = d+1
    		// weight = #comps * #vertices
    		// set scenarios to weight;
    		//final Xint representativeScenarios = Toolz.getScenariosOfRepresentative(this.scenariosAG, this.dAG,d_c, j);
    		//new SampleWeights(groupDistance, splitGroupID, numberOfComponents, scenariosPerElement, isLastOddSplitGroup)
			this.buckets.add(new SampleWeights(d_c, 1, distanceGroup.size(), numberOfVertices.multiply(distanceGroup.size()), false));
    	}
    	this.buckets.trimToSize();
    	
    	try {
    		// sample from R -> (d,j)
    		SampleWeights targetBucket = this.sampleFromVertexComponentSet(verticesAG);
    		int sampledDistance = targetBucket.getGroupDistance();
    		
    		// sample component [0...#Comps in List )
    		final int sampledComponentIndex = (new Random()).nextInt(targetBucket.getNumberOfComponents());
    		final ComponentSample sampledComponent = (this.orderedC.getComponents(sampledDistance))
    				.get(sampledComponentIndex);
    		
    		// sample cutPoints
    		final VertexInfo sampledExtremities = Toolz.sampledAdjacencies(sampledDistance, 1, sampledComponent,
    				extremitiesA, this.extremitiesB);
    		
    		// perform sampled DCJ on sampled component
    		this.performDCJoperation(sampledComponent, ops, extremitiesA, sampledExtremities, sampledDistance, 1);
    		// d is already updated
    		this.scenariosAG = targetBucket.getScenariosPerElement();
    		
    	} catch (IllegalArgumentException e) {
    		e.printStackTrace();
    		System.exit(1);
    	}
    	return ops;
    }

    private void performDCJoperation(ComponentSample c_i, OperationList ops, int[] extremitiesA,
        final VertexInfo cutInfo, final int d_i, final int j) {

        final int p = cutInfo.getLeftExtremity(true);
        final int q = cutInfo.getRightExtremity(true);

        if (p == q) { // found telomere -> °q (first vertex = 0)
            // second adjacency can be rs or r°
            final int r = cutInfo.getLeftExtremity(false);
            final int s = cutInfo.getRightExtremity(false);
            // close qr to a cycle
            extremitiesA[q] = r;
            extremitiesA[r] = q;
            ops.addOperation(new Pair<Integer, Integer>(q, r));

            if (r == s) { // r is telomere: °q | r° => °° rq
                // close AA-path into cycle -> c_b non-existant, j must be 1
                ops.addOperation(new Pair<Integer, Integer>(0, 0));
                if (j != 1) {
                    throw new IllegalArgumentException("Closing AA-path into a cycle did not work. j != 1, it is " + j);
                }
                // endindex needs no update, since adjacency contains 2 extremities
                c_i.updateStructure(Structure.CYCLE);
                c_i.swapPathEnds();// it is easier to compute vertex2Extremity, if p is left of q
                this.orderedC.moveElement(d_i, d_i - 1, c_i);

            } else {// °q | rs => °s and rq
                // AA- or AB-path, extract cycle from left border
                extremitiesA[s] = s;
                ops.addOperation(new Pair<Integer, Integer>(s, s));

                // c_a
                c_i.updateStartIndex(s);
                this.orderedC.moveElement(d_i, cutInfo.getDistance(), c_i);

                // c_b
                final ComponentSample c_b = new ComponentSample(r, q, Structure.CYCLE, false);
                // it is easier to compute vertex2Extremity, if p is left of q
                this.orderedC.addElement(d_i - 1 - cutInfo.getDistance(), c_b);
            }

        } else { // cut within
            // second adjacency can be rs or r°
            if (c_i.getStructure() == Structure.BB && cutInfo.getSecondVertex() == d_i) {
                // pq | °° -> p° | °q
                extremitiesA[p] = p;
                ops.addOperation(new Pair<Integer, Integer>(p, p));
                extremitiesA[q] = q;
                ops.addOperation(new Pair<Integer, Integer>(q, q));

                // by definition d-j >= j-1 => longer component gets d-j;
                // by definition above u < v, find out whether [0..u]<? [u..d-1]

                // always get distance value of left side, always make lefthandside c_a
                // righthandside = c_b
                final ComponentSample c_b = new ComponentSample(q, c_i.getEndIndex(), Structure.AB, false);
                this.orderedC.addElement(d_i - 1 - cutInfo.getDistance(), c_b);

                c_i.updateStructure(Structure.AB);
                c_i.updateEndIndex(p);
                c_i.swapPathEnds();
                this.orderedC.moveElement(d_i, cutInfo.getDistance(), c_i);

            } else {// only extracting cycles is now possible
                final int r = cutInfo.getLeftExtremity(false);
                final int s = cutInfo.getRightExtremity(false);
                // close qr into a cycle
                extremitiesA[q] = r;
                extremitiesA[r] = q;
                ops.addOperation(new Pair<Integer, Integer>(q, r));

                if (r == s) { // r is telomere : pq | r° => qr and p°
                    // can only be AA-path, can only EXCTRACT a cycle at right end
                    // c_a must have d > 0
                    extremitiesA[p] = p;
                    ops.addOperation(new Pair<Integer, Integer>(p, p));

                    // c_a
                    c_i.updateEndIndex(p);
                    this.orderedC.moveElement(d_i, cutInfo.getDistance(), c_i);

                } else {// pq | rs
                    // full adjacencies -> extract cycle within component
                    // connect ps leftovers
                    extremitiesA[p] = s;
                    extremitiesA[s] = p;
                    if (p < q) {
                        // FIXME, then "ps" op needs to be before "qr" op
                    }
                    ops.addOperation(new Pair<Integer, Integer>(p, s));

                    // c_a
                    if (c_i.getStructure() == Structure.CYCLE && cutInfo.getFirstVertex() == 0) {
                        // careful, startadjacency is disrupted !!
                        c_i.updateEndIndex(s);
                    }
                    this.orderedC.moveElement(d_i, cutInfo.getDistance(), c_i);
                }
                // c_b
                final ComponentSample c_b = new ComponentSample(q, r, Structure.CYCLE, false);
                this.orderedC.addElement(d_i - 1 - cutInfo.getDistance(), c_b);
            }
        }
        ops.addAdjacencyArrayG1(extremitiesA.clone());
        --this.dAG;
    }

    /**
     * The current number of scenarios is distributed among all {@link SampleWeights}-intervals for this level. A random
     * number within this range is sampled. The interval in which the number lies is found and the {@link SampleWeights}
     * of the interval is returned.
     * 
     * @return sampled interval of {@link SampleWeights}
     */
    public SampleWeights sampleFromSplitComponentSet() {
        if (this.buckets.size() == 1) { // only 1 distance group left
            return this.buckets.get(0);
        }
        if (this.buckets.size() == 0 || this.scenariosAG == Xint.ZERO || this.scenariosAG.toString().equals(0)
            || this.scenariosAG.isZERO()) {
            throw new IndexOutOfBoundsException("The number of scenarios is Zero. Cannot sample.");
        }
     
       	//int nextVar = Integer.parseInt(mylong.toString());
        final Xint randomNumber = (Toolz.generateRandomXint(this.scenariosAG)).add(1);
        
        Xint additiveWeight = Xint.ZERO;
        for (int i = 0; i < this.buckets.size(); i++) {
            additiveWeight = additiveWeight.add(this.buckets.get(i).getWeight());
            if (Toolz.aKleinerGleichB(randomNumber, additiveWeight)) {
                // if bucket is greater than sampled Index, or the sampled Index is bucket border, we got our bucket
                return this.buckets.get(i);
            }
        }
        return this.buckets.get(this.buckets.size() - 1);
    }
    
    /**
     * Samples one vertex from the interval of max
     * @param max upper bound of what can be sampled
     * @return
     */
    public SampleWeights sampleFromVertexComponentSet(int max) {
    	if (this.buckets.size() == 1) { // only 1 distance group left
    		return this.buckets.get(0);
    	}
    	if (this.buckets.size() == 0 || this.scenariosAG == Xint.ZERO || this.scenariosAG.toString().equals(0)
    			|| this.scenariosAG.isZERO()) {
    		throw new IndexOutOfBoundsException("The number of scenarios is Zero. Cannot sample.");
    	}
    	
    	final Xint randomNumber = (Toolz.generateRandomXint(Xint.valueOf(max))).add(1);
    	
    	Xint additiveWeight = Xint.ZERO;
    	for (int i = 0; i < this.buckets.size(); i++) {
    		additiveWeight = additiveWeight.add(this.buckets.get(i).getScenariosPerElement());
    		if (Toolz.aKleinerGleichB(randomNumber, additiveWeight)) {
    			// if bucket is greater than sampled Index, or the sampled Index is bucket border, we got our bucket
    			return this.buckets.get(i);
    		}
    	}
    	return this.buckets.get(this.buckets.size() - 1);
    }
}