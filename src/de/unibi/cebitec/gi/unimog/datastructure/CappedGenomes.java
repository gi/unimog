package de.unibi.cebitec.gi.unimog.datastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker and Corinna Sickinger               *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @author Corinna Sickinger
 * 
 * Performs the capping for two genomes, of which the first is to be transformed into
 * the second genome.
 */
public class CappedGenomes {

    private Genome uncappedGenome1;
    private Genome uncappedGenome2;
    //The length of the original genome. Used to determine if a gene is from the original genome
    // or a cap.
    private int originalGenomeLength;
    private Genome cappedGenome1;
    private Genome cappedGenome2;
    //Even paths in the adjacency graph, represented by the start and stop markers and a boolean
    //for the genome in which the path starts.
    private List<Pair<Pair<Integer, Integer>, Boolean>> evenPaths;
    //Odd paths in the adjacency graph. Start and stop markers in the path as pair of integers
    // and a boolean to tell in which the genome the path starts.
    private List<Pair<Pair<Integer, Integer>, Boolean>> oddPaths;
    // Mapping of the original genome(input for this class) to the new mapped genome
    //Hashmap for mapping of original Genome to new GeneIDs
    private HashMap<Integer, Integer> originalToNewGenes;
    // Mapping of the new mapped genome to the original (input) genome
    //Hashmap for mapping of new GeneIDs to original Genes
    private HashMap<Integer, Integer> newToOriginalGenes;

    /**
     * Contructor of a CappedGenome.
     * Only used to initialize the important variables.
     * @param genome1 first uncapped genome.
     * @param genome2 second uncapped genome.
     */
    public CappedGenomes(final Genome genome1, final Genome genome2) {
        this.originalGenomeLength = genome1.getNumberOfGenes();
        this.uncappedGenome1 = genome1;
        this.uncappedGenome2 = genome2;
        this.evenPaths = new ArrayList<Pair<Pair<Integer, Integer>, Boolean>>();
        this.oddPaths = new ArrayList<Pair<Pair<Integer, Integer>, Boolean>>();
        this.cappedGenome1 = new Genome();
        this.cappedGenome2 = new Genome();
        originalToNewGenes = new HashMap<Integer, Integer>();
        newToOriginalGenes = new HashMap<Integer, Integer>();
    }

    /**
     * Method to cap a genome.
     * In (restrictd)DCJ telomeres create special cases. To avoid all those
     * cases in an algorithm the genomes are capped. First paths in the adjacency
     * graph are calculated. The start and stopp Position of a path are saved in
     * the entries in even- and oddPaths, a boolean tells the chromosome in which 
     * the path started.
     * For even paths two different markers are added to the start and the end of
     * the path in one genome. These two markers form a new chromosome in the other 
     * genome.
     * For odd Paths the same marker is added to the start and the end of the path.
     * 
     * The markers for the capps start with the original markers plus one.
     */
    public void performCapping() {
        int newCapNr = originalGenomeLength + 1;
        ArrayList<int[]> chromosomes1 = this.generateUncappedGenomes(uncappedGenome1);
        ArrayList<int[]> chromosomes2 = this.generateUncappedGenomes(uncappedGenome2);
        for (Pair<Pair<Integer, Integer>, Boolean> evenEntry : evenPaths) {
            //Umwandeln von Adjacencen in Marker, am besten mit extraMethode
            Integer firstMarker = evenEntry.getFirst().getFirst();
            Integer secondMarker = evenEntry.getFirst().getSecond();

            boolean firstAdded = false;
            boolean secondAdded = false;
            int numberOfChromosomes = 0;
            ArrayList<int[]> toCapp;
            ArrayList<int[]> toAdd;

            if (evenEntry.getSecond()) {
                numberOfChromosomes = chromosomes2.size();
                toCapp = chromosomes2;
                toAdd = chromosomes1;
            } else {
                numberOfChromosomes = chromosomes1.size();
                toCapp = chromosomes1;
                toAdd = chromosomes2;
            }
            int[] addedChrom = {newCapNr, -(newCapNr + 1)};
            toAdd.add(addedChrom);

            for (int i = 0; i < numberOfChromosomes; i++) {
                int[] genes = toCapp.get(i);
                int first = markerToAdjacency(genes[1],true);
                int last = markerToAdjacency(genes[genes.length - 2],false);
                if (firstMarker.equals(first)) {
                    genes[0] = newCapNr;
                    newCapNr++;
                    firstAdded = true;
                } else if (firstMarker.equals(last)) {
                    genes[genes.length - 1] = -newCapNr;
                    newCapNr++;
                    firstAdded = true;
                }
                if (secondMarker.equals(first)) {
                    genes[0] = newCapNr;
                    newCapNr++;
                    secondAdded = true;
                } else if (secondMarker.equals(last)) {
                    genes[genes.length - 1] = -newCapNr;
                    newCapNr++;
                    secondAdded = true;
                }
                if (firstAdded && secondAdded) {
                    break;
                }
            }
        }

        for (Pair<Pair<Integer, Integer>, Boolean> oddEntry : oddPaths) {

            Integer firstMarker = oddEntry.getFirst().getFirst();
            Integer secondMarker = oddEntry.getFirst().getSecond();

            ArrayList<int[]> firstChromosome;
            ArrayList<int[]> secondChromosome;
            if (oddEntry.getSecond()) {
                firstChromosome = chromosomes2;
                secondChromosome = chromosomes1;
            } else {
                firstChromosome = chromosomes1;
                secondChromosome = chromosomes2;
            }

            for (int i = 0; i < firstChromosome.size(); i++) {
                int[] firstgenes = firstChromosome.get(i);
                if (firstMarker.equals(markerToAdjacency(firstgenes[1],true))) {
                    firstgenes[0] = newCapNr;
                    break;
                } else if (firstMarker.equals(markerToAdjacency(firstgenes[firstgenes.length - 2],false))) {
                    firstgenes[firstgenes.length - 1] = -newCapNr;
                    break;
                }
            }

            for (int j = 0; j < secondChromosome.size(); j++) {
                int[] secondgenes = secondChromosome.get(j);
                if (secondMarker.equals(markerToAdjacency(secondgenes[1],true))) {
                    secondgenes[0] = newCapNr;
                    newCapNr++;
                    break;
                } else if (secondMarker.equals(markerToAdjacency(secondgenes[secondgenes.length - 2],false))) {
                    secondgenes[secondgenes.length - 1] = -newCapNr;
                    newCapNr++;
                    break;
                }
            }

        }

        generateNewMapping(chromosomes1, chromosomes2);

    }

    private void generateNewMapping(ArrayList<int[]> genomeA, ArrayList<int[]> genomeB) {
        // first use the second genome to create a mapping, so that genomeB is sorted
        int geneID = 1;
        for (int[] chrom : genomeB) {
            int[] newChrom = new int[chrom.length];
            for (int i = 0; i < chrom.length; i++) {
                newChrom[i] = geneID;
                int entry = chrom[i];
                newToOriginalGenes.put(geneID, entry);
                if (entry < 0) {
                    originalToNewGenes.put(Math.abs(entry), -geneID);
                } else {
                    originalToNewGenes.put(entry, geneID);
                }
                geneID++;
            }
            Chromosome chromosome = new Chromosome(newChrom, false);
            cappedGenome2.addChromosome(chromosome);
        }

        //now change the markers of the first genome
        for (int[] chromA : genomeA) {
            int[] newChromA = new int[chromA.length];
            for (int j = 0; j < chromA.length; j++) {
                int markerA = chromA[j];
                int mappedGene = originalToNewGenes.get(Math.abs(markerA));
                if (markerA < 0) {
                    newChromA[j] = (-1) * mappedGene;
                } else {
                    newChromA[j] = mappedGene;
                }
            }
            Chromosome newChrom = new Chromosome(newChromA, false);
            cappedGenome1.addChromosome(newChrom);
        }

    }

    
    public Pair<Integer, Integer> mapOperations(Integer first, boolean fstOrientation, Integer second, boolean scndOrientation) {

        Integer a = 0;
        Integer b = 0;
        
        if (getOriginalGene(adjacencyToMarker(first)) != 0) {
            a = this.calcOp(first, fstOrientation);
        }
        //If both numbers are the same (they are telomeric), only one original marker has to be calculated.
        if (first.equals(second)) {
            return new Pair<Integer, Integer>(a, a);
        }

        if (getOriginalGene(adjacencyToMarker(second)) != 0) {
            b = this.calcOp(second, scndOrientation);
        }

        if ((a.equals(0)) && (b.equals(0))) {
            return new Pair<Integer, Integer>(a, b);
        } else if ((a.equals(0)) || (b.equals(0))) {
            int maximum = Math.max(a, b);
            return new Pair<Integer, Integer>(maximum, maximum);
        } else {
            return new Pair<Integer, Integer>(a, b);
        }
    }

    
    private int calcOp(int adj, boolean orientation) {

        int ori = getOriginalGene(adjacencyToMarker(adj));
        if (!orientation) {
            ori = -1 * ori;
        }
        if (((orientation) && (ori > 0)) || ((!orientation) && (ori < 0))) {
            if (adj % 2 == 0) {
                return (Math.abs(ori) * 2);
            } else {
                return ((Math.abs(ori) * 2) - 1);
            }
        } else if (((orientation) && (ori < 0)) || ((!orientation) && (ori > 0))) {
            if (adj % 2 == 0) {
                return ((Math.abs(ori) * 2) - 1);
            } else {
                return (Math.abs(ori) * 2);
            }
        }

        return 0;
    }

    /**
     * @param id id representing a gene
     * @return the original gene name associated with the given id
     */
    public int getOriginalGene(int id) {
        if (Math.abs(newToOriginalGenes.get(Math.abs(id))) > originalGenomeLength) {
            return 0;
        } else {
            if (id < 0) {
                return (-1 * newToOriginalGenes.get(Math.abs(id)));
            } else {
                return newToOriginalGenes.get(id);
            }
        }
    }

    /**
     * Method to generate a list with int[], each array representing a chromosome.
     * The first and last entry of each array are empty.
     * @return list with chromosomes
     */
    private ArrayList<int[]> generateUncappedGenomes(Genome genome) {
        ArrayList<Chromosome> chromList = genome.getGenome();
        ArrayList<int[]> uncapped = new ArrayList<int[]>();
        for (Chromosome chrom : chromList) {
            int[] originalGenes = chrom.getGenes();
            int[] nullCaps = new int[originalGenes.length + 2];
            System.arraycopy(originalGenes, 0, nullCaps, 1, originalGenes.length);
            uncapped.add(nullCaps);
        }
        return uncapped;
    }

    /**
     * Calculates a marker from a given adjacency.
     * Every marker has two ends, for example 
     * Marker 5: adj = 9,10
     * @param adj
     *      The adjacency to be converted.
     * @return calculated int, representing the original Marker. 
     *      
     */
    private int adjacencyToMarker(int adj) {
        int back = 0;
        if (adj % 2 == 0) {
            back = adj / 2;
        } else {
            back = (adj + 1) / 2;
        }
        return back;
    }
    
    private int markerToAdjacency(int marker,boolean start){
        int back = 0;
        if(marker<0){
            if(start){
                back = Math.abs(marker)*2;
            } else {
                back = Math.abs(marker)*2-1;
            }
        } else {
            if(start){
                back = marker*2-1;
            } else {
                back = marker*2;
            }
        }
        
        return back;
    }

    public List<Pair<Pair<Integer, Integer>, Boolean>> getEvenPaths() {
        return this.evenPaths;
    }

    public List<Pair<Pair<Integer, Integer>, Boolean>> getOddPaths() {
        return this.oddPaths;
    }

    public Genome getCappedGenome1() {
        return this.cappedGenome1;
    }

    public Genome getCappedGenome2() {
        return this.cappedGenome2;
    }
}