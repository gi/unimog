/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.unibi.cebitec.gi.unimog.datastructure;

import java.awt.Color;

/**
 * Replacement of former ChromFragment.
 * Saves full cut info of a genome including colour info. 
 * 
 * @author tgatter
 */
public class ChromosomeFragment {
    
    // one cut can at most contain two colours
    private Color colour1, colour2;
    private String gene1, gene2;

    public ChromosomeFragment() {
        gene1 = "";
        gene2 = "";
    }

    public ChromosomeFragment(Color colour1, Color colour2, String gene1, String gene2) {
        this.colour1 = colour1;
        this.colour2 = colour2;
        this.gene1 = gene1;
        this.gene2 = gene2;
    }

    

    public Color getColour1() {
        return colour1;
    }

    public void setColour1(Color colour1) {
        this.colour1 = colour1;
    }

    public Color getColour2() {
        return colour2;
    }

    public void setColour2(Color colour2) {
        this.colour2 = colour2;
    }

    public String getGene1() {
        return gene1;
    }

    public void setGene1(String gene1) {
        this.gene1 = gene1;
    }

    public String getGene2() {
        return gene2;
    }

    public void setGene2(String gene2) {
        this.gene2 = gene2;
    }
    
    
    
}
