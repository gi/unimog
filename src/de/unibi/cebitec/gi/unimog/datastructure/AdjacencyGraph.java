package de.unibi.cebitec.gi.unimog.datastructure;

import java.util.ArrayList;
import java.util.List;

import de.luschny.math.arithmetic.Xint;
import de.luschny.math.factorial.FactorialPrimeSwing;
import de.unibi.cebitec.gi.unimog.utils.Toolz;
/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker and Corinna Sickinger               *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * This class provides the data structure for the Adjacency Graph of two genomes. It contains methods to save the
 * genomes in tables which allow fast computation of the numbers of odd/even paths and cycles in the graph. The
 * computation of odd/even paths and cycles is also carried out here directly. DCJ distance: N-(C+I(2)
 * 
 * @author -Rolf Hilker-
 */
public class AdjacencyGraph {

    protected int           nbOfGenes     = 0;
    protected int[]         adjacenciesGenome1;
    protected int[]         adjacenciesGenome2;   // needed 4 calculation & sorting
    protected List<Integer> compDistances;
    private boolean[]       belongToEvenP;
    private int             dcjDistance   = 0;
    private Xint            lowerBoundScenarios;
    private boolean         capping       = false; // boolean to decide if the genomes need to be capped
    private CappedGenomes   cappedGenomes = null;

    /**
     * Constructor of a new instance. Both genomes MUST contain the same number of genes!
     * 
     * @param genome1
     *            First genome to evaluate in the graph
     * @param genome2
     *            Second genome to evaluate in the graph
     */
    public AdjacencyGraph(final Genome genome1, final Genome genome2) {
        this.compDistances = new ArrayList<Integer>();
        this.nbOfGenes = genome1.getNumberOfGenes();
        final int genomeLength = 2 * genome1.getNumberOfGenes() + 1;
        this.adjacenciesGenome1 = Toolz.parseAdjacencies(genome1, genomeLength);
        this.adjacenciesGenome2 = Toolz.parseAdjacencies(genome2, genomeLength);
        this.calculatePathsAndCycles();
    }

    /**
     * Constructor for cloning.
     * 
     * @param adjG1
     *            adjg1
     * @param adjG2
     *            adjg2
     * @param dcjDistance
     *            distance of the adjacency graph
     * @param belongToEvenP
     *            belonging to even path of an extremity
     * @param compDistances
     *            list of distances of the component of the adjacency graph
     * @param lowerBoundScenarios
     *            the number of different scenarios that are left
     */
    protected AdjacencyGraph(final int[] adjG1, final int[] adjG2, final int dcjDistance,
        final boolean[] belongToEvenP, final List<Integer> compDistances, final Xint lowerBoundScenarios) {
        // private AdjacencyGraph(final int nbOfGenes, final int[] adjG1, final int[] adjG2, final int nbCycles,
        // final int nbOddPaths, final int nbEvenPath, final boolean[] belongToEvenP, final List<Integer> compDistances)
        // {
        // this.nbOfGenes = nbOfGenes;
        this.adjacenciesGenome1 = adjG1;
        this.adjacenciesGenome2 = adjG2;
        this.dcjDistance = dcjDistance;
        this.belongToEvenP = belongToEvenP;
        this.compDistances = compDistances;
        this.lowerBoundScenarios = lowerBoundScenarios;
    }

    /**
     * Constructor of a new instance. Both genomes MUST contain the same number of genes! This Constructor is used for
     * genomes that must be capped before they are proccessed.
     * 
     * @param genome1
     *            First genome to evaluate in the graph
     * @param genome2
     *            Second genome to evaluate in the graph
     * @param capping
     *            boolean to decide if the genomes need to be capped. True, if they have to be capped
     */
    public AdjacencyGraph(final Genome genome1, final Genome genome2, final boolean capping) {
        this.compDistances = new ArrayList<Integer>();
        this.cappedGenomes = new CappedGenomes(genome1, genome2);
        this.nbOfGenes = genome1.getNumberOfGenes();
        final int genomeLength = 2 * genome1.getNumberOfGenes() + 1;
        this.adjacenciesGenome1 = Toolz.parseAdjacencies(genome1, genomeLength);
        this.adjacenciesGenome2 = Toolz.parseAdjacencies(genome2, genomeLength);
        this.capping = capping;
        this.calculatePathsAndCycles();
    }

    // MOVED TO TOOLZ:JAVA
    // /**
    // * Parses the adjacency graph. The result contains at each index (which represents a head or tail of a gene) its
    // * neighbor and if there it is a telomere it contains itself.
    // * @param genome Genome to parse
    // * @param genomeLength Length of the genome as int value
    // * @return list of adjacent extremities
    // */
    // protected int[] parseAdjacencies(final Genome genome, int genomeLength) {
    //
    // // genome: -1 3 4| 2 5| -6,-7| array from 1-14 for 7 genes!
    // // head = odd, tail = even, entry 0 is empty!
    // final int[] genomeAdjacencies = new int[genomeLength];
    //
    // for (int i = 0; i < genome.getNumberOfChromosomes(); ++i) {
    // int[] chromosome = genome.getChromosome(i).getGenes();
    // int length = chromosome.length;
    // int geneNumberLeft;
    // int geneNumberRight;
    //
    // // Part for processing telomeres, thus we choose "false" as input for "abstractGene"
    // geneNumberLeft = Toolz.abstractGene(chromosome[0], false);
    // geneNumberRight = Toolz.abstractGene(chromosome[length - 1], true); // Right telomere is left in adjacency:
    // // true
    //
    // if (genome.getChromosome(i).isCircular()) {
    // genomeAdjacencies[geneNumberLeft] = geneNumberRight; // save
    // genomeAdjacencies[geneNumberRight] = geneNumberLeft;
    // // can already save telomere entries
    // } else {
    // genomeAdjacencies[geneNumberLeft] = geneNumberLeft;
    // genomeAdjacencies[geneNumberRight] = geneNumberRight;
    // }
    //
    // for (int j = 1; j < chromosome.length; ++j) {
    //
    // geneNumberLeft = Toolz.abstractGene(chromosome[j - 1], true);
    // geneNumberRight = Toolz.abstractGene(chromosome[j], false);
    //
    // genomeAdjacencies[geneNumberLeft] = geneNumberRight; // save
    // genomeAdjacencies[geneNumberRight] = geneNumberLeft;
    // }
    // }
    // return genomeAdjacencies;
    // }

    // /** THIS WAS MOVED TO TOOLZ.JAVA
    // * Abstracts a gene to a positive value of 2*gene or 2*gene-1. If the gene is a left telomere then its position is
    // * right in the adjacency and left for a right telomere. For all inner adjacencies the left gene is on the left
    // side
    // * of the adjacency and therefore a head if the gene is in reverse orientation and a tail otherwise. The same
    // holds
    // * for the right gene of an adjacency just in the other way round.
    // *
    // * @param gene
    // * the gene to be abstracted
    // * @param left
    // * if the gene is the left or right one in adjacency
    // * @return the abstracted gene
    // */
    // public int abstractGene(final int gene, final boolean left) {
    // /* Entry is either right, thus a tail if > 0 or left, thus a tail if < 0 */
    // if (gene <= 0 && !left || gene >= 0 && left) {
    // return 2 * Math.abs(gene);
    // }
    // // else : Entry is either right, thus a tail if < 0 or left, thus a tail if > 0
    // return 2 * Math.abs(gene) - 1;
    // }
    // public int abstractGene(final int gene, final boolean left) {
    // // if gene is forward direction && left
    //
    // // if gene is backward direction && right
    //
    // /* Entry is either right, thus a tail if > 0 or left, thus a tail if < 0 */
    // if (gene <= 0 && !left || gene >= 0 && left) {
    // return 2 * Math.abs(gene);
    // }
    // // else : Entry is either right, thus a tail if < 0 or left, thus a tail if > 0
    // return 2 * Math.abs(gene) - 1;
    // }

    /**
     * Calculates the number of paths and cycles in the graph. Additionally the array saving if an extremity belongs to
     * an even path is generated here. At first all values are <code>false</code> and if an extremity belongs to an even
     * path its entry turns to <code>true</code>.
     */
    private void calculatePathsAndCycles() {
        int edgesCount = 0;
        boolean[] visited = new boolean[this.adjacenciesGenome1.length];
        this.belongToEvenP = new boolean[this.adjacenciesGenome1.length];
        int numberOfCycles = 0;
        int numberOfOddPaths = 0;

        for (int i = 1; i < this.adjacenciesGenome1.length; ++i) {
            if (!visited[i]) {
                visited[i] = true; // i was now visited
                boolean choose1stGenome = false; // chooses target genome of current edge
                int extremityPath1 = i;
                int extremityPath2 = i;
                int nextExtremityPath1 = 0;
                int nextExtremityPath2 = 0;
                ++edgesCount;
                nextExtremityPath1 = this.adjacenciesGenome1[i]; // get next extremities
                nextExtremityPath2 = this.adjacenciesGenome2[i];
                int secondCap = 0;

                while (!visited[nextExtremityPath1] && nextExtremityPath1 != extremityPath1) {
                    visited[nextExtremityPath1] = true;
                    ++edgesCount;
                    extremityPath1 = nextExtremityPath1;
                    if (choose1stGenome) { // chooses target genome of current edge
                        nextExtremityPath1 = this.adjacenciesGenome1[extremityPath1];
                        choose1stGenome = false;
                    } else {
                        nextExtremityPath1 = this.adjacenciesGenome2[extremityPath1];
                        choose1stGenome = true;
                    }
                } // path1 is finished, now walk along path2 if path1 was not a cycle
                if (extremityPath2 == nextExtremityPath1 && extremityPath1 != extremityPath2) {
                    // check if path1 is a cycle
                    this.compDistances.add((edgesCount - 2) / 2);
                    ++numberOfCycles;
                } else {
                    choose1stGenome = true;
                    while (nextExtremityPath2 != extremityPath2) {
                        visited[nextExtremityPath2] = true; // can't be circular anymore
                        ++edgesCount;
                        extremityPath2 = nextExtremityPath2;
                        if (choose1stGenome) {
                            nextExtremityPath2 = this.adjacenciesGenome1[extremityPath2];
                            choose1stGenome = false;
                        } else {
                            nextExtremityPath2 = this.adjacenciesGenome2[extremityPath2];
                            choose1stGenome = true;
                        }
                    }
                    if (edgesCount % 2 == 0) {
                        this.compDistances.add(edgesCount / 2);

                        // update array saving true if an an extremity belongs to an even path
                        this.belongToEvenP[extremityPath2] = true;
                        if (choose1stGenome) {
                            nextExtremityPath2 = this.adjacenciesGenome1[extremityPath2];
                            choose1stGenome = false;
                        } else {
                            nextExtremityPath2 = this.adjacenciesGenome2[extremityPath2];
                            choose1stGenome = true;
                        }
                        secondCap = extremityPath2;
                        while (nextExtremityPath2 != extremityPath2) {
                            extremityPath2 = nextExtremityPath2;
                            this.belongToEvenP[extremityPath2] = true;
                            if (choose1stGenome) {
                                nextExtremityPath2 = this.adjacenciesGenome1[extremityPath2];
                                choose1stGenome = false;
                            } else {
                                nextExtremityPath2 = this.adjacenciesGenome2[extremityPath2];
                                choose1stGenome = true;
                            }
                        }
                        // Capping for even paths
                        if (this.capping) {
                            Pair<Integer, Integer> evenPair = new Pair<Integer, Integer>(extremityPath1, secondCap);
                            Boolean firstGenome = choose1stGenome;
                            this.cappedGenomes.getEvenPaths().add(
                                new Pair<Pair<Integer, Integer>, Boolean>(evenPair, firstGenome));
                        }
                    } else {
                        this.compDistances.add((edgesCount - 1) / 2);
                        ++numberOfOddPaths;
                        // Capping for odd paths
                        if (this.capping) {
                            Pair<Integer, Integer> oddPair = new Pair<Integer, Integer>(extremityPath2, extremityPath1);
                            Boolean firstGenome = choose1stGenome;
                            this.cappedGenomes.getOddPaths().add(
                                new Pair<Pair<Integer, Integer>, Boolean>(oddPair, firstGenome));
                        }
                    }
                }
                edgesCount = 0;
            }
        }
        //System.out.println(this.nbOfGenes + "\t" + numberOfCycles + "\t" + numberOfOddPaths);
        this.dcjDistance = this.nbOfGenes - numberOfCycles - (numberOfOddPaths / 2);
        //this.calculateLowerBoundScenarios();
    }

    /* determines number of possible optimal DCJ sorting scenarios */
    private void calculateLowerBoundScenarios() {
        Xint zaehler = (new FactorialPrimeSwing().factorial(this.dcjDistance));
        Xint nenner = Xint.ONE;

        for (int d_i : this.compDistances) {
            if (d_i < 2)
                continue;
            final Xint tempPower = Xint.valueOf(d_i + 1).toPowerOf(d_i - 1);
            zaehler = zaehler.multiply(tempPower);
            nenner = nenner.multiply(new FactorialPrimeSwing().factorial(d_i));
        }
        // (zaehlerSumme)!/nenner * prod
        zaehler = zaehler.divide(nenner);
        this.lowerBoundScenarios = zaehler;
    }

    /**
     * @return the adjacency structure for genome1.
     */
    public int[] getAdjacenciesGenome1() {
        return this.adjacenciesGenome1;
    }

    /**
     * @return the adjacency structure for genome2.
     */
    public int[] getAdjacenciesGenome2() {
        return this.adjacenciesGenome2;
    }

    /**
     * Method for cloning an instance.
     * 
     * @return the cloned instance
     */
    @Override
    public AdjacencyGraph clone() {
        final List<Integer> cloneDistances = new ArrayList<Integer>(this.compDistances.size());
        for (Integer distance : this.compDistances) {
            cloneDistances.add(distance);
        }
        return new AdjacencyGraph(this.adjacenciesGenome1.clone(), this.adjacenciesGenome2.clone(), this.dcjDistance,
            this.belongToEvenP.clone(), cloneDistances, this.lowerBoundScenarios);
    }

    /**
     * Returns the array containing if an extremity belongs to an even path.
     * 
     * @return the belonging to an Even Path
     */
    public boolean[] getBelongToEvenPath() {
        return this.belongToEvenP;
    }

    /**
     * @return The single distances of each component as list.
     */
    public List<Integer> getCompDistances() {
        return this.compDistances;
    }

    /** @return the overall DCJ distance of the initial Adjacency Graph. */
    public int getDCJdistance() {
        return this.dcjDistance;
    }

    /** @return the number of sorting scenarios without recombinations */
    public Xint getLowerBoundScenarios() {
        if (this.lowerBoundScenarios == null){
            this.calculateLowerBoundScenarios();
        }
        return this.lowerBoundScenarios;
    }

    /**
     * Getter for the capped genomes.
     * 
     * @return the instance of the datatype of capped genomes
     */
    public CappedGenomes getCappedGenomes() {
        return this.cappedGenomes;
    }

    /**
     * Compares this adjacency graph with another one.
     * 
     * @param ag2
     *            the adjacency graph to be compared with
     * @return <code>true</code> if both adjacency graphs are equal, <code>false</code> otherwise.
     */
    public boolean equals(final AdjacencyGraph ag2) {
        final List<Integer> ag2distances = ag2.getCompDistances();
        for (Integer distance : this.compDistances) {
            if (!ag2distances.contains(distance))
                return false;
        }
        for (Integer distance : ag2distances) {
            if (!this.compDistances.contains(distance))
                return false;
        }
        if (this.dcjDistance != ag2.getDCJdistance())
            return false;
        if (this.lowerBoundScenarios != ag2.getLowerBoundScenarios())
            return false;
        int[] ag2ad1 = ag2.getAdjacenciesGenome1();
        if (this.adjacenciesGenome1.length != ag2ad1.length)
            return false;
        for (int i = 0; i < this.adjacenciesGenome1.length; i++) {
            if (this.adjacenciesGenome1[i] != ag2ad1[i])
                return false;
        }
        int[] ag2ad2 = ag2.getAdjacenciesGenome2();
        if (this.adjacenciesGenome2.length != ag2ad2.length)
            return false;
        for (int i = 0; i < this.adjacenciesGenome2.length; i++) {
            if (this.adjacenciesGenome2[i] != ag2ad2[i])
                return false;
        }

        boolean[] belong2evenPath = ag2.getBelongToEvenPath();
        if (this.belongToEvenP.length != belong2evenPath.length)
            return false;
        for (int i = 0; i < belong2evenPath.length; i++) {
            if (this.belongToEvenP[i] != belong2evenPath[i])
                return false;
        }
        // TODO compare capped genomes
        return true;
    }
}