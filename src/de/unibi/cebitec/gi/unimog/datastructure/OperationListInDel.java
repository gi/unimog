
package de.unibi.cebitec.gi.unimog.datastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class extends the normal OperationLists to contain suspended genes for 
 * InDel Operations.
 * @author Thomas Gatter - tgatter(at)cebitec.uni-bielefeld.de
 */
public class OperationListInDel extends OperationList{
    
    private ArrayList<int[]> adjacencyLabelsG1 = new ArrayList<int[]>();
    
    private HashMap<Integer, List<String>> baseLabels; // never change
    private HashMap<Integer, LabelIndexPair> subLabels; // one set containing all is enough
        
    private List<HashMap<Integer, Label>> activeLabelsG1 = new ArrayList<HashMap<Integer, Label>>();


    private List<List<Integer>> singletonsG1 = new ArrayList<List<Integer>>();
    private List<List<Integer>> singletonsCircularG1 = new ArrayList<List<Integer>>();

    public ArrayList<int[]> getAdjacencyLabelsG1() {
        return adjacencyLabelsG1;
    }

    public List<HashMap<Integer, Label>> getActiveLabelsG1() {
        return activeLabelsG1;
    }

    public HashMap<Integer, List<String>> getBaseLabels() {
        return baseLabels;
    }

    public HashMap<Integer, LabelIndexPair> getSubLabels() {
        return subLabels;
    }

    public void setBaseLabels(HashMap<Integer, List<String>> baseLabels) {
        this.baseLabels = baseLabels;
    }

    public void setSubLabels(HashMap<Integer, LabelIndexPair> subLabels) {
        this.subLabels = subLabels;
    }

    public List<List<Integer>> getSingletonsG1() {
        return singletonsG1;
    }
    
    public List<List<Integer>> getSingletonsCircularG1() {
        return singletonsCircularG1;
    }
    
    public void addAdjacencyLabelsG1(int[] adjLabel) {
        adjacencyLabelsG1.add(adjLabel);
    }
    
    public void addActiveLabelsG1(HashMap<Integer, Label> labels) {
        activeLabelsG1.add(labels);
    }
    
    public void addSingletonsG1(List<Integer> singletons) {
        singletonsG1.add(singletons);
    }
    
    public void addSingletonsCircularG1(List<Integer> singletons) {
        singletonsCircularG1.add(singletons);
    }
}
