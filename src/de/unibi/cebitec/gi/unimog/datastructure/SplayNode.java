package de.unibi.cebitec.gi.unimog.datastructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker and Corinna Sickinger               *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * Representation of a node in a splaytree.
 * @author Corinna Sickinger
 */
public class SplayNode {

    /**
     * Is node root of the tree, or not.
     */
    private boolean root;
    /**
     * The marker stored in this node.
     */
    private int marker;
    /**
     * If this flag is on, the whole subtree from this node is reversed.
     */
    private boolean reverseFlag;
    /**
     * The highest marker in all children.
     */
    private int biggestChild;
    /**
     * The chromosome number of this node.
     */
    private int chromosomeNumber;
    /**
     * The parent of this node.
     */
    private SplayNode parent;
    /**
     * Left and right child of this node. 
     */
    private SplayNode leftChild, rightChild;
    /**
     * Tells if this node is a right or a left child of its parent.
     */
    private boolean left, right;
    /**
     * The size of the subtree with this node.
     */
    private int subtreeSize;
    private SplayTree parentTree;

    /**
     * Constructor for a new splay node. Only called if a new telomere is created.
     * Hence the marker is always 0.
     * @param newChromosomeNumber
     *          The number of the new telomere. A chromosome wiht a 0 is always the
     *          beginning of a chromosom.
     * @param nodeHolder 
     *          The splaytree, where all the nodes are stored.
     */
    public SplayNode(int newChromosomeNumber, SplayTree nodeHolder) {
        this.marker = 0;
        this.chromosomeNumber = newChromosomeNumber;
        this.parentTree = nodeHolder;
        this.reverseFlag = false;
        this.root = false;
        this.leftChild = null;
        this.rightChild = null;
    }

    /**
     * The constructor of a splay node. With this constructor whole splay trees are created.
     * @param parent
     *          The parent node of the splay node to be created.
     * @param newNodes 
     *          A list with all the nodes to be inserted into the tree.
     *          The list is split in the middle. The integer of the middle node
     *          is the marker of this node. The other two halves of the list are
     *          used to create two (or more) new trees, one left and one right of
     *          this node.
     */
    public SplayNode(SplayNode parent, ArrayList<Integer> newNodes) {
        reverseFlag = false;
        
        if (parent != null) {
            this.parent = parent;
            root = false;
        } else {
            root = true;
        }
        this.parent = parent;
        if (newNodes.size() == 2) {
            this.marker = newNodes.get(0);
            this.setLeftChild(null);
            ArrayList<Integer> smallArray = new ArrayList<Integer>(Arrays.asList(newNodes.get(1)));
            this.setRightChild(new SplayNode(this, smallArray));

        } else if (newNodes.size() == 1) {
            this.marker = newNodes.get(0);
            this.setLeftChild(null);
            this.setRightChild(null);
        } else {
            int half = newNodes.size() / 2;
            this.marker = newNodes.get(half);
            List<Integer> leftList = newNodes.subList(0, half);
            ArrayList<Integer> arrayLeft = new ArrayList<Integer>(leftList);
            this.setLeftChild(new SplayNode(this, arrayLeft));
            if (newNodes.subList(half + 1, newNodes.size()).isEmpty()) {
                this.setRightChild(null);
            } else {
                List<Integer> rightList = newNodes.subList(half + 1, newNodes.size());
                ArrayList<Integer> arrayRight = new ArrayList<Integer>(rightList);
                this.setRightChild(new SplayNode(this, arrayRight));
            }
        }
    }

    /** This method initializes each node with its chromosome number.
     * The array in the SplayTree class is filled with references to each node.
     * @param nodeHolder
     *          The parent class with all the informations about the nodes.
     * @param genomeSize
     *          The size of the genome.
     * @param chromosome
     *          The chromosome of the parent node.
     * @return The highest chromosome of this subtree.
     */
    public int initialize(SplayTree nodeHolder, int genomeSize, int chromosome) {
        //Wenn links == null -> this.chromosomnumber = chromosom
        //Wenn rechts == null -> nix
        int rightSize = 0;
        parentTree = nodeHolder;

        if (this.leftChild != null && this.rightChild != null) {
            if (this.getMarker() == 0) {
                this.chromosomeNumber = this.leftChild.initialize(nodeHolder, genomeSize, chromosome) + 1;
                rightSize = this.rightChild.initialize(nodeHolder, genomeSize, chromosomeNumber);

                nodeHolder.setNode(this, genomeSize + chromosomeNumber);

            } else {
                this.chromosomeNumber = this.leftChild.initialize(nodeHolder, genomeSize, chromosome);
                rightSize = this.rightChild.initialize(nodeHolder, genomeSize, this.chromosomeNumber);

                nodeHolder.setNode(this, marker);

            }
        } else if (this.leftChild == null && this.rightChild == null) {
            if (this.getMarker() == 0) {
                this.chromosomeNumber = chromosome + 1;
                rightSize = 0;
                nodeHolder.setNode(this, genomeSize + chromosomeNumber);
            } else {
                this.chromosomeNumber = chromosome;
                rightSize = 0;
                nodeHolder.setNode(this, marker);
            }

        } else if (this.leftChild == null) {
            if (this.getMarker() == 0) {
                this.chromosomeNumber = chromosome + 1;
                rightSize = this.rightChild.initialize(nodeHolder, genomeSize, chromosomeNumber);
                nodeHolder.setNode(this, genomeSize + chromosomeNumber);
            } else {
                this.chromosomeNumber = chromosome;
                rightSize = this.rightChild.initialize(nodeHolder, genomeSize, chromosomeNumber);
                nodeHolder.setNode(this, marker);
            }

        } else { //rightChild is null
            if (this.getMarker() == 0) {
                this.chromosomeNumber = this.leftChild.initialize(nodeHolder, genomeSize, chromosome) + 1;
                rightSize = 0;
                nodeHolder.setNode(this, genomeSize + chromosomeNumber);
            } else {
                this.chromosomeNumber = this.leftChild.initialize(nodeHolder, genomeSize, chromosome);
                rightSize = 0;
                nodeHolder.setNode(this, marker);
            }

        }
        if (rightSize > this.chromosomeNumber) {
            return rightSize;
        } else {
            return this.chromosomeNumber;
        }
    }
    
    public int getChromosomeNumber(){
        int back = 0;
        if (this.marker == 0) {
            back++;
        }
        if (this.leftChild != null) {
            back = back + this.leftChild.getChromosomeNumber();
        }
        if (this.rightChild != null) {
            back = back + this.rightChild.getChromosomeNumber();
        }
        return back;
    }

    public ArrayList<Integer> traverse(final boolean direction) {
        ArrayList<Integer> back = new ArrayList<Integer>();
        boolean traversalOrder = direction;
        if (this.reverseFlag) {
            if (direction) {
                traversalOrder = false;
            } else {
                traversalOrder = true;
            }
        }
        if (!traversalOrder) {
            if ((this.leftChild == null) && (this.rightChild == null)) {
                back.add(this.marker);
            } else if (this.leftChild == null) {
                back.add(this.marker);
                back.addAll(this.rightChild.traverse(traversalOrder));
            } else if (this.rightChild == null) {
                back.addAll(this.leftChild.traverse(traversalOrder));
                back.add(this.marker);
            } else {
                back.addAll(this.leftChild.traverse(traversalOrder));
                back.add(this.marker);
                back.addAll(this.rightChild.traverse(traversalOrder));
            }
        } else {
            if ((this.leftChild == null) && (this.rightChild == null)) {
                back.add(-this.marker);
            } else if (this.leftChild == null) {
                back.addAll(this.rightChild.traverse(traversalOrder));
                back.add(-this.marker);
            } else if (this.rightChild == null) {
                back.add(-this.marker);
                back.addAll(this.leftChild.traverse(traversalOrder));
            } else {
                back.addAll(this.rightChild.traverse(traversalOrder));
                back.add(-this.marker);
                back.addAll(this.leftChild.traverse(traversalOrder));
            }
        }
        return back;
    }

    /**
     * The reverse flag is pushed down to the next layer of the tree.
     */
    public void pushDown() {
        SplayNode temp = this.leftChild;
        this.flipFlag();
        this.flipMarker();
        this.setLeftChild(this.rightChild);
        this.setRightChild(temp);
        if (this.leftChild != null) {
            this.leftChild.flipFlag();
        }
        if (this.rightChild != null) {
            this.rightChild.flipFlag();
        }
    }

    /**
     * This method does a right rotation on the tree.
     */
    public void rightRotate() {
        if (!parent.isRoot()) {
            SplayNode grandparent = parent.getParent();
            boolean rightOfGp = parent.isRight();
            SplayNode temp = this.getParent();
            temp.setLeftChild(this.getRightChild());
            if (temp.getLeftChild() != null) {
                temp.getLeftChild().setParent(temp);
            }
            this.setRightChild(temp);
            temp.setParent(null);
            temp.setParent(this);
            this.setParent(null);
            this.setParent(grandparent);
            if (rightOfGp) {
                grandparent.setRightChild(this);
                this.setRight(true);
                this.setLeft(false);
            } else {
                grandparent.setLeftChild(this);
                this.setLeft(true);
                this.setRight(false);
            }
        } else {
            SplayNode temp = this.getParent();
            temp.setLeftChild(this.getRightChild());
            if (temp.getLeftChild() != null) {
                temp.getLeftChild().setParent(temp);
            }
            this.setRightChild(temp);
            temp.setParent(null);
            temp.setParent(this);
            this.setRoot(true);
        }

    }

    /**
     * This method does a left rotation on the tree.
     */
    public void leftRotate() {
        if (!parent.isRoot()) {
            SplayNode grandparent = parent.getParent();
            boolean rightOfGp = parent.isRight();
            SplayNode temp = this.getParent();
            temp.setRightChild(this.getLeftChild());
            if (temp.getRightChild() != null) {
                temp.getRightChild().setParent(temp);
            }
            this.setLeftChild(temp);
            temp.setParent(null);
            temp.setParent(this);
            this.setParent(null);
            this.setParent(grandparent);
            if (rightOfGp) {
                grandparent.setRightChild(this);
                this.setRight(true);
                this.setLeft(false);
            } else {
                grandparent.setLeftChild(this);
                this.setLeft(true);
                this.setRight(false);
            }

        } else {
            SplayNode temp = this.getParent();
            temp.setRightChild(this.getLeftChild());
            if (temp.getRightChild() != null) {
                temp.getRightChild().setParent(temp);
            }
            this.setLeftChild(temp);
            temp.setParent(null);
            temp.setParent(this);
            this.setRoot(true);
        }

    }

    /**
     * The splay operation.
     * Most essential operation in a splay tree.
     */
    public void splay() {

        if (this.reverseFlag) {
            this.pushDown();
        }

        if (this.parent != null && this.parent.getParent() != null && this.parent.getParent().isReverseFlag()) {
            this.parent.getParent().pushDown();
        }
        if (this.parent != null && this.parent.isReverseFlag()) {
            this.parent.pushDown();
        }

        if (this.isReverseFlag()) {
            this.pushDown();
        }
        if (this.isRoot()) {
            parentTree.setRoot(this);
            parentTree.getRoot().setSize();
            parentTree.getRoot().initializeBiggestChild();
        } else if (parent.isRoot()) {
            // Zig Step.
            if (right) {
                leftRotate();
                parentTree.setRoot(this);
                parentTree.getRoot().setSize();
                parentTree.getRoot().initializeBiggestChild();
            } else {
                rightRotate();
                parentTree.setRoot(this);
                parentTree.getRoot().setSize();
                parentTree.getRoot().initializeBiggestChild();
            }

        } else {
            // Zig-zig step for right respectivly left children.
            if (right && parent.isRight()) {
                this.parent.leftRotate();
                this.leftRotate();
            } else if (left && parent.isLeft()) {
                this.parent.rightRotate();
                this.rightRotate();
                // Zig-zag step.
            } else if (right && parent.isLeft()) {
                this.leftRotate();
                this.rightRotate();
            } else if (left && parent.isRight()) {
                this.rightRotate();
                this.leftRotate();
            }
            this.splay();
        }
    }

    /**
     * Gets the ith node of the tree.
     * Countig starts at 0.
     * 
     * @param i number of the searched Node.
     * @param flag the flag of the parent, to decide in which direction to go.
     * @return the ith Node
     */
    public SplayNode getIthNode(int i, boolean flag) {
        boolean parentFlag = flag;
        if (flag && reverseFlag) {
            parentFlag = false;
        } else if (!flag && reverseFlag) {
            parentFlag = true;
        }
        if (this.leftChild != null && this.rightChild != null) {
            if (parentFlag) {
                int rightsize = this.rightChild.getSubtreeSize();
                if (i < rightsize) {
                    return this.rightChild.getIthNode(i, parentFlag);
                } else if (i > rightsize) {
                    return this.leftChild.getIthNode(i - rightsize - 1, parentFlag);
                } else {
                    return this;
                }
            } else {
                int leftsize = this.leftChild.getSubtreeSize();
                if (i < leftsize) {
                    return this.leftChild.getIthNode(i, parentFlag);
                } else if (i > leftsize) {
                    return this.rightChild.getIthNode(i - leftsize - 1, parentFlag);
                } else {
                    return this;
                }
            }
        } else if (this.leftChild == null && this.rightChild == null) {
            return this;
        } else if (this.leftChild == null) {
            if (parentFlag) {
                int rightsize = this.rightChild.getSubtreeSize();
                if (i < rightsize) {
                    return this.rightChild.getIthNode(i, parentFlag);
                } else {
                    return this;
                }
            } else {
                if (i > 0) {
                    return this.rightChild.getIthNode(i - 1, parentFlag);
                } else {
                    return this;
                }
            }
        //Case rightChild is null
        } else {
            if (parentFlag) {
                if (i > 0) {
                    return this.leftChild.getIthNode(i - 1, parentFlag);
                } else {
                    return this;
                }
            } else {
                int leftsize = this.leftChild.getSubtreeSize();
                if (i < leftsize) {
                    return this.leftChild.getIthNode(i, parentFlag);
                } else {
                    return this;
                }
            }
        }
    }

    /**
     * @return the position of this node in the tree.
     */
    public int getPosition() {
        this.splay();
        if (this.isReverseFlag()) {
            if (this.rightChild != null) {
                return this.rightChild.getSubtreeSize() + 1;
            } else {
                return 1;
            }
        } else {
            if (this.leftChild != null) {
                return this.leftChild.getSubtreeSize() + 1;
            } else {
                return 1;
            }
        }
    }

    /**
     * @return the smallest node in this tree, which is the first k-node
     */
    public SplayNode getPositionSmallestNode(boolean reverseFlag) {
        boolean newRevFlag = reverseFlag;
        if (reverseFlag && reverseFlag) {
            newRevFlag = false;
        } else if (!reverseFlag && reverseFlag) {
            newRevFlag = true;
        }
        if (newRevFlag) {
            if (rightChild == null) {
                return this;
            } else {
                return rightChild.getPositionSmallestNode(newRevFlag);
            }
        } else {
            if (leftChild == null) {
                return this;
            } else {
                return leftChild.getPositionSmallestNode(newRevFlag);
            }
        }
    }

    /**
     * @return the biggest node, which is the last k-node in the tree
     */
    public SplayNode getPositionBiggestNode(boolean reverseFlag) {
        boolean newRevFlag = reverseFlag;
        if (reverseFlag && reverseFlag) {
            newRevFlag = false;
        } else if (!reverseFlag && reverseFlag) {
            newRevFlag = true;
        }
        if (newRevFlag) {
            if (leftChild == null) {
                return this;
            } else {
                return leftChild.getPositionBiggestNode(newRevFlag);
            }
        } else {
            if (rightChild == null) {
                return this;
            } else {
                return rightChild.getPositionBiggestNode(newRevFlag);
            }
        }
    }

    /**
     * Calculates and sets the subtree size for each node in the tree below the node on which this
     * method is called and returns the subtree size of the node on which
     * this method is called.
     * @return the subtree size of the node on which this method is called.
     */
    public int setSize() {
        if ((leftChild == null) && (rightChild == null)) {
            subtreeSize = 1;
        } else if (leftChild == null) {
            subtreeSize = rightChild.setSize() + 1;
        } else if (rightChild == null) {
            subtreeSize = leftChild.setSize() + 1;
        } else {
            subtreeSize = leftChild.setSize() + rightChild.setSize() + 1;
        }
        return subtreeSize;
    }

    /**
     * @return the current subtree size for this node. Does not calculate anything,
     * just returns the value!
     */
    public int getSubtreeSize() {
        return subtreeSize;
    }

    /**
     * Sets the biggest child of this node.
     * The biggest child is the node with the highest marker(absolute)
     */
    public int initializeBiggestChild() {
        int absMarker = Math.abs(marker);
        if (leftChild == null && rightChild == null) {
            this.biggestChild = absMarker;
        } else if (leftChild == null) {
            int rightMarker = rightChild.initializeBiggestChild();
            if (absMarker > rightMarker) {
                this.biggestChild = absMarker;
            } else {
                this.biggestChild = rightMarker;
            }
        } else if (rightChild == null) {
            int leftMarker = leftChild.initializeBiggestChild();
            if (absMarker > leftMarker) {
                this.biggestChild = absMarker;
            } else {
                this.biggestChild = leftMarker;
            }
        } else {
            int leftM = leftChild.initializeBiggestChild();
            int rightM = rightChild.initializeBiggestChild();
            if ((absMarker > leftM) && (absMarker > rightM)) {
                this.biggestChild = absMarker;
            } else if ((leftM > absMarker) && (leftM > rightChild.biggestChild)) {
                this.biggestChild = leftM;
            } else if ((rightM > absMarker) && (rightM > leftChild.getBiggestChild())) {
                this.biggestChild = rightM;
            }
        }
        return this.biggestChild;
    }

    public void setBiggestChild() {
        int absMarker = Math.abs(marker);
        if ((leftChild == null) && (rightChild == null)) {
            this.biggestChild = absMarker;
        } else if (leftChild == null) {
            if (absMarker > rightChild.getBiggestChild()) {
                this.biggestChild = absMarker;
            } else {
                this.biggestChild = rightChild.getBiggestChild();
            }
        } else if (rightChild == null) {
            if (absMarker > leftChild.getBiggestChild()) {
                this.biggestChild = absMarker;
            } else {
                this.biggestChild = leftChild.getBiggestChild();
            }
        } else {
            if ((absMarker > leftChild.getBiggestChild()) && (absMarker > rightChild.getBiggestChild())) {
                this.biggestChild = absMarker;
            } else if ((leftChild.getBiggestChild() > absMarker) && (leftChild.getBiggestChild() > rightChild.biggestChild)) {
                this.biggestChild = leftChild.getBiggestChild();
            } else if ((rightChild.getBiggestChild() > absMarker) && (rightChild.getBiggestChild() > leftChild.getBiggestChild())) {
                this.biggestChild = rightChild.getBiggestChild();
            }
        }
    }

    public void changeChromosome(int newChromosome) {
        this.setChromosomeNumber(newChromosome);
        if (this.leftChild != null) {
            this.leftChild.changeChromosome(newChromosome);
        } else if (this.rightChild != null) {
            this.rightChild.changeChromosome(newChromosome);
        }
    }

    /**
     * @return the biggest child below this node.
     */
    public int getBiggestChild() {
        return this.biggestChild;
    }

    /**
     * @return the parent of this node
     */
    public SplayNode getParent() {
        return this.parent;
    }

    /**
     * Sets the parent node of this node.
     * @param parent the node to set as parent for this node
     */
    public void setParent(SplayNode parent) {
        if (parent != null) {
            this.setRoot(false);
        }
        this.parent = parent;
    }

    //public int getChromosomNumber() {
    //    return chromosomNumber;
    //}

    /**
     * Sets the chromosome number for this node.
     * @param chromosomeNumber the chromosome number to set in this node
     */
    public void setChromosomeNumber(int chromosomeNumber) {
        this.chromosomeNumber = chromosomeNumber;
    }

    public int getMarker() {
        if (this.isReverseFlag()) {
            return this.marker * (-1);
        } else {
            return this.marker;
        }

    }

    /**
     * @return true, if the reverse flag is set, false otherwise
     */
    public boolean isReverseFlag() {
        return this.reverseFlag;
    }

    public void flipFlag() {
        this.reverseFlag = !this.reverseFlag;
    }

    public void flipMarker() {
        this.marker = this.marker * (-1);
    }

    /**
     * Sets the reverse flag for this node
     * @param reverseFlag true, if the reverse flag should be set and false otherwise.
     */
    public void setReverseFlag(boolean reverseFlag) {
        this.reverseFlag = reverseFlag;
    }

    public SplayNode getLeftChild() {
        return this.leftChild;
    }

    public final void setLeftChild(SplayNode leftChild) {
        this.leftChild = leftChild;
        if (leftChild != null) {
            this.leftChild.setLeft(true);
            this.leftChild.setRight(false);
        }

    }

    public SplayNode getRightChild() {
        return rightChild;
    }

    public final void setRightChild(SplayNode rightChild) {
        this.rightChild = rightChild;
        if (rightChild != null) {
            this.rightChild.setRight(true);
            this.rightChild.setLeft(false);
        }

    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public boolean isRoot() {
        return root;
    }

    public void setRoot(boolean root) {
        if (root) {
            if (this.parent != null) {
                this.parent = null;
            }
            this.setLeft(false);
            this.setRight(false);
        }
        this.root = root;
    }
}
