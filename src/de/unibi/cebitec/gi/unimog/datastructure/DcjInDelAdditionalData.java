
package de.unibi.cebitec.gi.unimog.datastructure;

import java.util.HashMap;

/**
 *
 * @author Thomas Gatter
 */
public class DcjInDelAdditionalData implements IAdditionalData{
    
    private LabeledAdjacencyGraph graph;
    private HashMap<Integer, String> geneNameMap;

    public DcjInDelAdditionalData(LabeledAdjacencyGraph graph, HashMap<Integer, String> geneNameMap) {
        this.graph = graph;
        this.geneNameMap = geneNameMap;
    }

    public LabeledAdjacencyGraph getGraph() {
        return graph;
    }

    public HashMap<Integer, String> getGeneNameMap() {
        return geneNameMap;
    }
    
}
