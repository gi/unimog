package de.unibi.cebitec.gi.unimog.datastructure.sampling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Class provides an ordered List of components which are grouped by distances of each component.
 * 
 * @author Eyla Willing
 */
public class OrderedComponentList {// extends HashMap<Integer, List<ComponentSample>> {

    private int                                     numberOfComponents;
    private HashMap<Integer, List<ComponentSample>> ocl;
    private int                                     numAA;
    private int                                     numBB;

    /** Constructor. */
    public OrderedComponentList() {
        this.ocl = new HashMap<Integer, List<ComponentSample>>();
        this.numberOfComponents = 0;
        this.numAA = 0;
        this.numBB = 0;
    }

    /**
     * Adds a {@link ComponentSample} to this ordered component list
     * 
     * @param distance
     *            place where to add the {@link ComponentSample}
     * @param newComponent
     *            {@link ComponentSample} to be added
     * @return <code>true</code> if addition was successful, <code>false</code> otherwise.
     */
    public boolean addElement(final int distance, ComponentSample newComponent) {
        if (newComponent == null)
            throw new IllegalArgumentException("The component you want to add is NULL.");
        if (distance == 0) {
            return false;
        }
        if (this.ocl.containsKey(distance)) {
            List<ComponentSample> comps = this.ocl.get(distance);
            if (comps.contains(newComponent)) {
                return false;
            }
            final boolean returnValue = this.ocl.get(distance).add(newComponent);
            if (returnValue)
                ++this.numberOfComponents;
            return returnValue;
        }
        ++this.numberOfComponents;
        switch (newComponent.getStructure()) {
        case AA:
            ++this.numAA;
            break;
        case BB:
            ++this.numBB;
        }
        List<ComponentSample> newDistanceList = new ArrayList<ComponentSample>();
        newDistanceList.add(newComponent);
        this.ocl.put(distance, newDistanceList);
        return true; // FIXME check of comp.equals(newComponent);
    }

    /**
     * Delete an element from the list of components.
     * 
     * @param distance
     *            List where to find the component
     * @param oldComponent
     *            component to be removed
     * @return <code>true</code> if removal was possible, <code>false</code> otherwise
     */
    public boolean removeElement(final int distance, ComponentSample oldComponent) {
        if (oldComponent == null)
            throw new IllegalArgumentException("The component you want to remove is NULL.");
        List<ComponentSample> tempList = this.ocl.get(distance);
        if (tempList != null) {
            final boolean returnValue = tempList.remove(oldComponent);
            if (returnValue) {
                --this.numberOfComponents;
                switch (oldComponent.getStructure()) {
                case AA:
                    --this.numAA;
                    break;
                case BB:
                    --this.numBB;
                }
            }
            if (tempList.isEmpty()) {
                this.ocl.remove(distance);
            }
            return returnValue;
        }
        return false;
    }

    /**
     * Append component, on which a DCJ operation was performed to the list of components which now have the same
     * distance value.
     * 
     * @param oldDistance
     *            list where component is currently appended to
     * @param newDistance
     *            new destination list for component
     * @param component
     *            considered component
     * @return <code>true</code> if addition and removal were possible, <code>false</code> if one of these operation
     *         fails.
     */
    public boolean moveElement(final int oldDistance, final int newDistance, ComponentSample component) {
        final boolean isRemoved = this.removeElement(oldDistance, component);
        if (newDistance == 0 || !isRemoved) {
            return isRemoved;
        }
        final boolean isAdded = this.addElement(newDistance, component);
        if (!isAdded) {
            // revert "remove" changes
            this.addElement(oldDistance, component);
        }
        return isAdded;
    }

    /**
     * Provides all components that have the requested <code>distance</code>.
     * 
     * @param distance
     *            requested distance
     * @return All components with distance <code>distance</code>.
     */
    public List<ComponentSample> getComponents(final int distance) {
        if (this.ocl.containsKey(distance)) {
            return this.ocl.get(distance);
        }
        return new ArrayList<ComponentSample>();
    }

    /** @return the number of keys, i.e. the number of different distances for the whole adjacency graph */
    public int getNumberOfDifferentDistances() {
        return this.ocl.size();
    }

    /** @return A Set of distances stored in the {@link OrderedComponentList}. */
    public Set<Integer> getDistanceList() {
        return this.ocl.keySet();
    }

    /** @return the number of unsorted components in the Adjacency Graph. */
    public int getNumberOfUnsortedComponents() {
        return this.numberOfComponents;
    }

    /** @return the last left-over element of the whole {@link OrderedComponentList}. */
    public ComponentSample getLastElement() {
        if (this.numberOfComponents != 1) {
            throw new IllegalArgumentException("Number of remaining components != 1, it is: " + this.numberOfComponents);
        }
        if (!this.ocl.containsKey(1)) {
            throw new IndexOutOfBoundsException("Distance of last component is not 1.");
        }
        return this.ocl.get(1).get(0);
        // Set<Integer> key = this.keySet();
        // List<ComponentSample> list = this.get(key.iterator().next());
        // return list.iterator().next();
    }

    /**
     * Prints which distances are in the {@link OrderedComponentList} and how many entries (aka {@link ComponentSample})
     * each distance has.
     * 
     * @return listing as {@link String}
     */
    public String printElementList() {
        final StringBuilder builder = new StringBuilder();
        for (Integer key : this.ocl.keySet()) {
            builder.append(key + ": " + this.ocl.get(key).size() + "\t");
        }
        return builder.toString();
    }

    /**
     * Compares two {@link OrderedComponentList}s with each other. For each distance the entry in the original must be
     * equal to the entry in the compared {@link OrderedComponentList}.
     * 
     * @param ocl
     *            {@link OrderedComponentList} to be compared with
     * @return <code>true</code> if {@link OrderedComponentList}s are equal, <code>false</code> otherwise
     */
    public boolean equals(OrderedComponentList ocl) {
        if (this.getNumberOfDifferentDistances() != ocl.getNumberOfDifferentDistances())
            return false;
        if (this.getNumberOfUnsortedComponents() != ocl.getNumberOfUnsortedComponents())
            return false;
        for (Integer key : this.ocl.keySet()) {
            if (!ocl.containsDistance(key))
                return false;
            final List<ComponentSample> original = this.ocl.get(key);
            final List<ComponentSample> copy = ocl.getComponents(key);
            if (original.size() != copy.size())
                return false;
            boolean found = false;
            outerloop: for (ComponentSample origComp : original) {
                found = false;
                for (ComponentSample copyComp : copy) {
                    if (origComp.equals(copyComp)) {
                        found = true;
                        continue outerloop;
                    }
                }
            }
            if (!found)
                return false;
        }
        return true;
    }

    @SuppressWarnings("javadoc")
    @Override
    public OrderedComponentList clone() {
        OrderedComponentList clone = new OrderedComponentList();
        for (Integer key : this.ocl.keySet()) {
            for (ComponentSample comp : this.ocl.get(key)) {
                clone.addElement(key, comp.clone());
            }
        }
        return clone;
    }

    /**
     * Checks whether or not a specific distance is stored in the {@link OrderedComponentList}.
     * 
     * @param distance
     *            to be searched for
     * @return <code>true</code> if key is in {@link OrderedComponentList}, <code>false</code> otherwise.
     */
    public boolean containsDistance(final int distance) {
        return this.ocl.containsKey(distance);
    }

    /** @return <code>true</code> if there are AA- and BB-paths such that a recombination is possible. */
    public boolean isRecombinationPossible() {
        return (this.numAA > 0 && this.numBB > 0);
    }
}