package de.unibi.cebitec.gi.unimog.datastructure.sampling;

import de.luschny.math.arithmetic.Xint;

/**
 * Class provides data structure for uniform sampling a split group.
 * 
 * @author Eyla Willing
 */
public class SampleWeights {

    private final Xint weight;
    private final int  groupDistance;
    private final int  splitGroupID;
    private final int  numberOfComponents;
    private final Xint scenariosPerElement;
    private int        numberOfSplitGroupElements;

    /**
     * Constructor.
     * 
     * @param groupDistance
     *            ID of current component
     * @param splitGroupID
     *            ratio at which to split
     * @param numberOfComponents
     *            number of components that have this groupDistance
     * @param scenariosPerElement
     *            number of scenarios per element of this split group
     * @param isLastOddSplitGroup
     *            <code>true</code> if SplitGorup is last group of odd distance, <code>false</code> otherwise
     */
    public SampleWeights(final int groupDistance, final int splitGroupID, final int numberOfComponents,
        final Xint scenariosPerElement, final boolean isLastOddSplitGroup) {
        this.groupDistance = groupDistance;
        this.splitGroupID = splitGroupID;
        this.numberOfComponents = numberOfComponents;
        this.scenariosPerElement = scenariosPerElement;
        this.numberOfSplitGroupElements = this.groupDistance + 1;
        if (isLastOddSplitGroup) {
            this.numberOfSplitGroupElements /= 2.;
        }
        this.weight = this.scenariosPerElement.multiply(this.numberOfSplitGroupElements).multiply(
            this.numberOfComponents);
    }

    /** @return weight of split group (number of elements x number of scenarios for each element). */
    public Xint getWeight() {
        return this.weight;
    }

    /** @return ID of component currently observed */
    public int getGroupDistance() {
        return this.groupDistance;
    }

    /** @return split group ID (ratio at which to split) */
    public int getSplitGroupID() {
        return this.splitGroupID;
    }

    /** @return number of scenarios for a single element of this split group */
    public Xint getScenariosPerElement() {
        return this.scenariosPerElement;
    }

    /** @return the number of components with the same distance and same splitgroup. */
    public int getNumberOfComponents() {
        return this.numberOfComponents;
    }

    /** @return the number of elements that belong to this split group (depends on distance and splitgroup id. */
    public int getNumberOfSplitGroupElements() {
        return this.numberOfSplitGroupElements;
    }

    @SuppressWarnings("javadoc")
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("\t" + this.groupDistance);
        string.append("\tR_" + this.splitGroupID);
        string.append("\t" + this.scenariosPerElement);
        string.append("\t*" + this.numberOfSplitGroupElements);
        string.append("\t*" + this.numberOfComponents);
        string.append("\t" + this.weight);
        return string.toString();
    }
}