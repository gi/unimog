package de.unibi.cebitec.gi.unimog.datastructure.sampling;



/**
 * Determines structure of a {@link ComponentSample} of the {@link AdjacencyGraphSampling}.
 * 
 * @author Eyla Willing
 */
public enum Structure {
    /** AA-path */
    AA("AA  "),

    /** BB-path */
    BB("BB  "),

    /** AB-path */
    AB("AB  "),

    /** Cycle */
    CYCLE("CYCLE"),

    /** No structure specified */
    NONE("Unknown");

    private final String guiName;

    private Structure(final String guiName) {
        this.guiName = guiName;
    }

    /**
     * @return gui name of component structure
     */
    public String getGuiName() {
        return this.guiName;
    }

    /**
     * Returns the selected {@link Structure} depending on the id given.
     * 
     * @param id
     * @return {@link Structure} according to id
     */
    public static Structure getStructure(final int id) {
        switch (id) {
        case 1:
            return Structure.AA;
        case 2:
            return Structure.BB;
        case 3:
            return Structure.AB;
        case 4:
            return Structure.CYCLE;
        default:
            return Structure.NONE;
        }
    }
}