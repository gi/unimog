package de.unibi.cebitec.gi.unimog.datastructure.sampling;

import de.unibi.cebitec.gi.unimog.datastructure.Component;

/***************************************************************************
 * Copyright (C) 2010 by Rolf Hilker <br>
 * rhilker a t cebitec.uni-bielefeld.de<br>
 * This program is free software; you can redistribute it and/or modify <br>
 * it under the terms of the GNU General Public License as published by <br>
 * the Free Software Foundation; either version 2 of the License, or <br>
 * (at your option) any later version. <br>
 * This program is distributed in the hope that it will be useful, <br>
 * but WITHOUT ANY WARRANTY; without even the implied warranty of <br>
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the <br>
 * GNU General Public License for more details.<br>
 * You should have received a copy of the GNU General Public License <br>
 * along with this program; if not, write to the <br>
 * Free Software Foundation, Inc., <br>
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 ***************************************************************************/

/**
 * Class that provides a data structure for saving components.
 * 
 * @author Eyla Willing
 */
public class ComponentSample extends Component {

    private Structure structure;

    /**
     * Standard constructor.
     * 
     * @param startIndex
     *            The start index of this component
     * @param endIndex
     *            The end index of this component
     * @param structure
     *            The type of this component
     * @param oriented
     *            <code>true</code> if oriented and <code>false</code> otherwise.
     */
    public ComponentSample(final int startIndex, final int endIndex, final Structure structure, final boolean oriented) {
        super(startIndex, endIndex, 0, oriented);
        this.structure = structure;
    }

    /** @return {@link Structure} of the component */
    public Structure getStructure() {
        return this.structure;
    }

    /**
     * Updates the {@link Structure} of the component.
     * 
     * @param structure
     *            new {@link Structure} of the component
     */
    public void updateStructure(final Structure structure) {
        this.structure = structure;
    }

    /** Easy way to turn a BA-path into an AB-path. */
    public void swapPathEnds() {
        final int oldEnd = super.endIndex;
        super.endIndex = super.startIndex;
        super.startIndex = oldEnd;
    }

    /**
     * Sets new end index for the component.
     * 
     * @param endIndex
     *            extremity at end of the component
     */
    public void updateEndIndex(final int endIndex) {
        super.endIndex = endIndex;
    }

    /**
     * Sets new start index for the component.
     * 
     * @param startIndex
     *            extremity at the beginning of the component
     */
    public void updateStartIndex(final int startIndex) {
        super.startIndex = startIndex;
    }

    @SuppressWarnings("javadoc")
    @Override
    public String toString() {
        final StringBuilder string = new StringBuilder();
        string.append(this.structure);
        string.append("\tstart: " + this.getStartIndex());
        string.append("\tend: " + this.getEndIndex() + "\t");
        return string.toString();
    }

    @SuppressWarnings("javadoc")
    @Override
    public ComponentSample clone() {
        return new ComponentSample(super.startIndex, super.endIndex, this.structure, false);
    }

    /**
     * Compares a {@link ComponentSample} to another component.
     * 
     * @param component
     *            to be compared with
     * @return <code>true</code> iff both components share the same parameters, <code>false</code> otherwise.
     */
    public boolean equals(final ComponentSample component) {
        if (component.getEndIndex() != this.getEndIndex()) {
            return false;
        }
        if (component.getStartIndex() != this.getStartIndex()) {
            return false;
        }
        if (component.getStructure() != this.getStructure()) {
            return false;
        }
        return true;
    }
}