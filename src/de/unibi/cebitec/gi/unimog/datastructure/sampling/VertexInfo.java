package de.unibi.cebitec.gi.unimog.datastructure.sampling;

/**
 * Stores Pair of extremities (order is crucial) along with id of vertices sampled and distance of left hand component.
 * 
 * @author Eyla Willing
 */
public class VertexInfo {

    private final int distance;
    private final int firstVertex;
    private final int secondVertex;
    private int extremity1left  = -1;
    private int extremity1right = -1;
    private int extremity2left  = -1;
    private int extremity2right = -1;

    /**
     * Constructor.
     * 
     * @param distance
     *            new distance of left hand of component
     * @param firstVertex
     *            index of first vertex
     * @param secondVertex
     *            index of second vertex
     */
    public VertexInfo(final int distance, final int firstVertex, final int secondVertex) {
        this.distance = distance;
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
    }

    /** @return new distance of left hand of component */
    public int getDistance() {
        return this.distance;
    }

    /** @return first vertex */
    public int getFirstVertex() {
        return this.firstVertex;
    }

    /** @return second vertex */
    public int getSecondVertex() {
        return this.secondVertex;
    }

    /**
     * Gets left extremity either from first vertex or from second vertex, depending on the argument.
     * 
     * @param fromFirstVertex
     *            <code>true</code> if taken from first vertex, <code>false</code> if taken from second vertex
     * @return left extremity from either first or second vertex.
     */
    public int getLeftExtremity(final boolean fromFirstVertex) {
        return (fromFirstVertex) ? this.extremity1left : this.extremity2left;
    }

    /**
     * Gets right extremity either from first vertex or from second vertex, depending on the argument.
     * 
     * @param fromFirstVertex
     *            <code>true</code> if taken from first vertex, <code>false</code> if taken from second vertex
     * @return right extremity from either first or second vertex.
     */
    public int getRightExtremity(final boolean fromFirstVertex) {
        return (fromFirstVertex) ? this.extremity1right : this.extremity2right;
    }

    /**
     * Sets Extremities of first vertex (first adjacency).
     * 
     * @param left
     *            left extremity
     * @param right
     *            right extremity
     */
    public void setFirstAdjacency(final int left, final int right) {
        this.extremity1left = left;
        this.extremity1right = right;
    }

    /**
     * Sets Extremities of second vertex (second adjacency).
     * 
     * @param left
     *            left extremity
     * @param right
     *            right extremity
     */
    public void setSecondAdjacency(final int left, final int right) {
        this.extremity2left = left;
        this.extremity2right = right;
    }
}