package de.unibi.cebitec.gi.unimog.datastructure.sampling;

import java.util.ArrayList;
import java.util.List;

import de.luschny.math.arithmetic.Xint;
import de.unibi.cebitec.gi.unimog.datastructure.AdjacencyGraph;
import de.unibi.cebitec.gi.unimog.datastructure.Genome;

/**
 * This class provides the data structure for the Adjacency Graph (AG) of two genomes. It contains methods to save the
 * genomes in tables which allow fast computation of different connected components of the AG. Also the number of
 * scenarios is computed here.
 * 
 * @author Eyla Willing
 */
public class AdjacencyGraphSampling extends AdjacencyGraph {

//    public static int            maxD_A             = 0;
//    public static int            maxD_B             = 0;
    // private static final FactorialPrimeSwing fac = new FactorialPrimeSwing();
    private int                  numberOfUnsortedAA = 0;
    private int                  numberOfUnsortedBB = 0;
    private List<Integer>        compDistances;
    private OrderedComponentList componentList;

    // private static Xint[][] W;
    // private static Xint[][] Z;
    // private static Xint[] S;
    // private static Xint[] shreek;

    /**
     * Constructor of a new instance. Both genomes MUST contain the same number of genes!
     * 
     * @param genome1
     *            First {@link Genome} to evaluate in the graph
     * @param genome2
     *            Second {@link Genome} to evaluate in the graph
     */
    public AdjacencyGraphSampling(final Genome genome1, final Genome genome2) {
        super(genome1, genome2);
        this.compDistances = new ArrayList<Integer>();
        this.componentList = new OrderedComponentList();
//        AdjacencyGraphSampling.maxD_A = 0;
//        AdjacencyGraphSampling.maxD_B = 0;
        this.calculatePathsAndCycles();
    }

    /**
     * Constructor for cloning.
     * 
     * @param adjG1
     *            adjacencies for genome 1
     * @param adjG2
     *            adjacencies for genome 2
     * @param dcjDistance
     *            DCJ distance of genome 1 and 2
     * @param belongToEvenP
     *            belonging to even path of an extremity
     * @param compDistances
     *            List of distances of components of the adjacency graph
     * @param scenarios
     *            Number of different optimal scenarios that are left
     * @param ocl
     *            List of components of the adjacency graph ordered according to their distance
     */
    private AdjacencyGraphSampling(final int[] adjG1, final int[] adjG2, final int dcjDistance,
        final boolean[] belongToEvenP, final List<Integer> compDistances, final Xint scenarios,
        final OrderedComponentList ocl) {
        super(adjG1, adjG2, dcjDistance, belongToEvenP, compDistances, scenarios);
        this.componentList = ocl;
    }

    /**
     * Calculates the number of paths and cycles in the graph and if they are unsorted (d_DCJ >1) they are stored in a
     * list.
     */
    private void calculatePathsAndCycles() {
        int edgesCount = 0;
        boolean[] visited = new boolean[this.adjacenciesGenome1.length];
        int maxA = 0;
        int maxB = 0;

        for (int i = 1; i < this.adjacenciesGenome1.length; ++i) {
            if (!visited[i]) {
                visited[i] = true; // i is now visited
                boolean chooseGenomeA = false; // chooses target genome of current edge
                ++edgesCount;

                // adjacency in A
                int pWalkRight = i;
                int qWalkRight = this.adjacenciesGenome1[i]; // get partner-extremity

                // adjacency in B
                int pWalkLeft = i;
                int qWalkLeft = this.adjacenciesGenome2[i]; // get partner-extremity

                // first we walk to the right
                while (!visited[qWalkRight] && qWalkRight != pWalkRight) {
                    // unvisited AND no telomere
                    visited[qWalkRight] = true;
                    ++edgesCount;
                    // form full adjacency of genome at the other end of this edge
                    pWalkRight = qWalkRight;
                    if (chooseGenomeA) { // chooses genome at the other end of this edge
                        qWalkRight = this.adjacenciesGenome1[pWalkRight];
                        chooseGenomeA = false;
                    } else {
                        qWalkRight = this.adjacenciesGenome2[pWalkRight];
                        chooseGenomeA = true;
                    }
                }
                // stopped: reached telomere OR reached first adjacency of cycle again
                final int rightEnd = qWalkRight;

                // stop if we reached a cycle
                if (pWalkLeft == qWalkRight && pWalkRight != pWalkLeft) {
                    final int distance = ((edgesCount - 2) / 2);
                    // ++this.numberOfCycles;

                    if (distance > 0) {
                        this.compDistances.add(distance);
                        final ComponentSample unsortedCycle = new ComponentSample(pWalkLeft, qWalkLeft,
                            Structure.CYCLE, false);
                        this.componentList.addElement(distance, unsortedCycle);
                    }

                } else { // if we reached telomere: pR = qR => walk left to finish path
                    chooseGenomeA = true; // start at beginner vertex again
                    while (qWalkLeft != pWalkLeft) { // can't be circular anymore
                        visited[qWalkLeft] = true;
                        ++edgesCount;
                        pWalkLeft = qWalkLeft;
                        if (chooseGenomeA) { // currently in genome A
                            qWalkLeft = this.adjacenciesGenome1[pWalkLeft];
                            chooseGenomeA = false;
                        } else { // currently in genome B
                            qWalkLeft = this.adjacenciesGenome2[pWalkLeft];
                            chooseGenomeA = true;
                        }
                    }// finished

                    if (edgesCount % 2 == 0) { // even path (AA-or BB-path)
                        final int distance = edgesCount / 2;

                        if (distance > 0) {
                            this.compDistances.add(distance);
                            final ComponentSample unsortedEvenPath = new ComponentSample(pWalkLeft, rightEnd,
                                Structure.AA, false);
                            if (chooseGenomeA) { // if next genome would have been A -> currently in Genome B
                                ++this.numberOfUnsortedBB;
                                if (maxB < distance)
                                    maxB = distance;
                                unsortedEvenPath.updateStructure(Structure.BB);
                            } else {
                                ++this.numberOfUnsortedAA;
                                if (maxA < distance)
                                    maxA = distance;
                            }
                            this.componentList.addElement(distance, unsortedEvenPath);
                        }

                    } else { // odd path -> AB-path
                        final int distance = (edgesCount - 1) / 2;
                        // ++this.numberOfOddPaths;

                        if (distance > 0) {
                            this.compDistances.add(distance);
                            final ComponentSample unsortedABpath = new ComponentSample(pWalkLeft, rightEnd,
                                Structure.AB, false);
                            if (chooseGenomeA) { // leftEnd = start in B (next genome would have been A
                                unsortedABpath.swapPathEnds();
                            }
                            this.componentList.addElement(distance, unsortedABpath);
                        }
                    }
                }
                edgesCount = 0;
            }
        }

        // if (this.isRecombinationPossible()) {
        // System.out.println("AHHHH; RECOMB IS POSSIBLE");
        // if (maxA > maxB) {
        // int temp = maxA;
        // maxA = maxB;
        // maxB = temp;
        // }
        // // need recalculation (expansion) if borders are getting bigger
        // if (maxA > AdjacencyGraphSampling.maxD_A) {
        // AdjacencyGraphSampling.maxD_A = maxA;
        // if (maxB > AdjacencyGraphSampling.maxD_B) {
        // AdjacencyGraphSampling.maxD_B = maxB;
        // }
        // Recombination.setUpTables();
        // } else {
        // if (maxB > AdjacencyGraphSampling.maxD_B) {
        // AdjacencyGraphSampling.maxD_B = maxB;
        // Recombination.setUpTables();
        // } else {
        // // do nothing
        // }
        // }
        // }
        // distance and Xint are calculated by super class (if not any more uncomment "++this.numberOf..."
    }

    /**
     * @return <code>true</code> if the Adjacency Graph can be sampled individually, i.e. no recombinations occur,
     *         <code>false</code> otherwise.
     */
    public boolean isRecombinationPossible() {
        return (this.numberOfUnsortedAA > 0 && this.numberOfUnsortedBB > 0);
    }

//    /**
//     * @return a {@link Pair} of integers where the first is the number of AA-paths and the second is the number of
//     *         BB-paths.
//     */
//    public Pair<Integer, Integer> getAABBpaths() {
//        return new Pair(this.numberOfUnsortedAA, this.numberOfUnsortedBB);
//    }

    /**
     * @return all {@link ComponentSample} of the {@link AdjacencyGraphSampling} ordered according to their distances
     *         (only unsorted components are considered).
     */
    public OrderedComponentList getOrderedListOfComponents() {
        return this.componentList;
    }
}