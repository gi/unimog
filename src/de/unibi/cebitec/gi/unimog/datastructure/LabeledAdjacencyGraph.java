/* To change this template, choose Tools | Templates
 * and open the template in the editor. */
package de.unibi.cebitec.gi.unimog.datastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * This class extends the Datastructure of the Adjacency Graph by adding Labels
 * instead of ignoring unique Genes.
 * 
 * @author Malte Mattheis
 */
public class LabeledAdjacencyGraph extends AdjacencyGraph implements IAdditionalData {
	
	/** All Labels saved with unique ID. */
	private HashMap<Integer, Label>				activeLabels;
	private HashMap<Integer, List<String>>		baseLabels;
	private HashMap<Integer, LabelIndexPair>	subLabels;
	/** Labels in Genome 1. Index is extremity Index and value is LabelID. 0 == no Label */
	private int[]								adjacencyLabelsGenom1;
	/** Labels in Genome 2. Index is extremity Index and value is LabelID. 0 == no Label */
	private int[]								adjacencyLabelsGenom2;
	/** Label IDs of Singletons */
	private ArrayList<Integer>					aaAsingletons;
	private ArrayList<Integer>					bbBsingletons;
	private ArrayList<Integer>					aaAsingletonsCircular;
	private ArrayList<Integer>					bbBsingletonsCircular;
	
	private int									activeLabelCount;
	private int									subLabelCount;
	
	private int									lambda;
	private int									aaA;
	private int									aaB;
	private int									aaAB;
	private int									bbA;
	private int									bbB;
	private int									bbAB;
	private int									abAB;
	private int									abBA;
	private Map<Integer, Integer>				aaAStarts;
	private Map<Integer, Integer>				aaBStarts;
	private Map<Integer, Integer>				aaABStarts;
	private Map<Integer, Integer>				bbAStarts;
	private Map<Integer, Integer>				bbBStarts;
	private Map<Integer, Integer>				bbABStarts;
	private Map<Integer, Integer>				abABStarts;
	private Map<Integer, Integer>				abBAStarts;
	
	private ArrayList<Integer>					otherComponenents;
	
	/**
	 * TODO add javadoc
	 * 
	 * @param genome1
	 *            core genome of genome 1
	 * @param genome2
	 *            core genome of genome 2
	 * @param ag
	 *            adjacency graph of core genomes
	 * @param suspendedGenes
	 *            removed genes
	 * @param backMap
	 *            relation of unique identifier and input name for genes
	 * @param fullGenome1
	 *            genome 1 without removed genes
	 * @param g1Map
	 * @param fullGenome2
	 *            genome 2 without removed genes
	 * @param g2Map
	 */
	public LabeledAdjacencyGraph(final Genome genome1, final Genome genome2, final AdjacencyGraph ag,
			final List<String> suspendedGenes, final HashMap<Integer, String> backMap, final Genome fullGenome1,
			final HashMap<Integer, String> g1Map, final Genome fullGenome2, final HashMap<Integer, String> g2Map) {
		
		super(ag.getAdjacenciesGenome1(), ag.getAdjacenciesGenome2(), genome1.getNumberOfGenes(), ag
				.getBelongToEvenPath(), ag.getCompDistances(), null);
		activeLabelCount = 1;
		
		activeLabels = new HashMap<Integer, Label>();
		baseLabels = new HashMap<Integer, List<String>>();
		subLabels = new HashMap<Integer, LabelIndexPair>();
		
		aaAsingletons = new ArrayList<Integer>();
		bbBsingletons = new ArrayList<Integer>();
		aaAsingletonsCircular = new ArrayList<Integer>();
		bbBsingletonsCircular = new ArrayList<Integer>();
		
		cleanStarts();
		
		adjacencyLabelsGenom1 = generateLables(suspendedGenes, backMap, fullGenome1, g1Map, true);
		adjacencyLabelsGenom2 = generateLables(suspendedGenes, backMap, fullGenome2, g2Map, false);
		countPaths();
	}
	
	private void cleanStarts() {
		aaAStarts = new HashMap<Integer, Integer>();
		aaBStarts = new HashMap<Integer, Integer>();
		aaABStarts = new HashMap<Integer, Integer>();
		bbAStarts = new HashMap<Integer, Integer>();
		bbBStarts = new HashMap<Integer, Integer>();
		bbABStarts = new HashMap<Integer, Integer>();
		abABStarts = new HashMap<Integer, Integer>();
		abBAStarts = new HashMap<Integer, Integer>();
		
		otherComponenents = new ArrayList<Integer>();
	}
	
	/**
	 * Generates the Labelarray and saves labels into labels-hashmap.
	 * 
	 * @param suspendedGenesList
	 *            all suspendes genes are possible labels
	 * @param backMap
	 *            intern Integer to realname hashmap
	 * @param fullGenome
	 *            the full Genome with suspended Genes included
	 * @param gMap
	 *            internal used integer to original gene name.
	 * @param isGenome1
	 *            is first genome?
	 * @return Array of labelnumbers at positions.
	 */
	private int[] generateLables(final List<String> suspendedGenesList, final HashMap<Integer, String> backMap,
			final Genome fullGenome, final HashMap<Integer, String> gMap, boolean isGenome1) {
		
		int[] adjacencyLabels = new int[adjacenciesGenome1.length];
		HashMap<String, Integer> inverseBackMap = new HashMap<String, Integer>();
		for (int counter : backMap.keySet()) {
			inverseBackMap.put(backMap.get(counter), counter);
		}
		
		HashSet<String> suspendedGenes = new HashSet<String>(suspendedGenesList);
		for (int i = 0; i < fullGenome.getNumberOfChromosomes(); i++) {
			Chromosome chrom = fullGenome.getChromosome(i);
			int[] genes = chrom.getGenes();
			
			for (int j = 0; j < genes.length; j++) {
				String geneName = gMap.get(genes[j]);
				if (suspendedGenes.contains(geneName)) { // it is a Label
					ArrayList<String> label = new ArrayList<String>();
					if (genes[j] < 0) {
						geneName = "-" + geneName;
					}
					label.add(geneName);
					boolean telomerLeft = false;
					boolean telomerRight = false;
					boolean startOver = false;
					int right = j + 1;
					if (chrom.isCircular() && right == genes.length) {
						right = 0;
						startOver = true;
					}
					if (j == 0) {
						telomerLeft = true;
					}
					/* Do not extend Label to left. There cannot be an other
					 * Label that was not detected until now on the Left side */
					if (j == genes.length - 1 && !chrom.isCircular()) {
						telomerRight = true;
					} else {
						while (right <= genes.length - 1) {
							geneName = gMap.get(genes[right]);
							if (suspendedGenes.contains(geneName)) { // extend
								// label
								// to
								// right
								if (genes[right] < 0) {
									geneName = "-" + geneName;
								}
								label.add(geneName);
								if (right == genes.length - 1) {
									telomerRight = true;
									if (chrom.isCircular() && !telomerLeft) {
										right = 0;
										telomerRight = false;
										startOver = true;
										continue;
									}
								}
							} else {
								break;
							}
							right++;
						}
						
					}
					
					if (!(chrom.isCircular() && j == 0 && !telomerRight
							&& suspendedGenes.contains(gMap.get(genes[genes.length - 1])) && suspendedGenes
								.contains(gMap.get(genes[0])))) {
						
						// put in the base labels
						baseLabels.put(subLabelCount, label);
						LabelIndex index = new LabelIndex(subLabelCount, true);
						subLabelCount++;
						
						String leftBorder = null;
						String rightBorder = null;
						// size of label + left border + right border
						
						if (!telomerLeft) { // no Telomer on the left --> set
							// left Border
							if (genes[j - 1] < 0) {
								leftBorder = "-" + gMap.get(genes[j - 1]);
							} else {
								leftBorder = gMap.get(genes[j - 1]);
							}
						} else if (chrom.isCircular()) {
							if (genes[genes.length - 1] < 0) {
								leftBorder = "-" + gMap.get(genes[genes.length - 1]);
							} else {
								leftBorder = gMap.get(genes[genes.length - 1]);
							}
						}
						if (!telomerRight) { // no Telomer on the right --> set
							// right Border
							if (genes[(j + label.size()) % (genes.length)] < 0) {
								rightBorder = "-" + gMap.get(genes[(j + label.size()) % (genes.length)]);
							} else {
								rightBorder = gMap.get(genes[(j + label.size()) % (genes.length)]);
							}
						} else if (chrom.isCircular()) {
							if (genes[0] < 0) {
								rightBorder = "-" + gMap.get(genes[0]);
							} else {
								rightBorder = gMap.get(genes[0]);
							}
						}
						
						// increase j to right - 1 == Jump to next Gene not
						// already in the Label
						if (!startOver) {
							j = right - 1;
						} else {
							j = genes.length;
						}
						
						// create the label with empty left and right
						Label fullLabel = new Label(index);
						activeLabels.put(activeLabelCount, fullLabel);
						activeLabelCount++;
						
						int internLeft = 0;
						int internRight = 0;
						boolean leftReverse = false;
						boolean rightReverse = false;
						
						boolean isSingleton = false;
						
						// Detect if Singleton: Both borders telomeres (== null)
						// Or Circular Singleton: right Border == first
						// Labelentry; left Border == last Labelentry
						if (leftBorder == null && rightBorder == null) {
							isSingleton = true;
							lambda++;
							if (isGenome1) {
								aaA++;
								aaAsingletons.add(activeLabelCount - 1);
							} else {
								bbB++;
								bbBsingletons.add(activeLabelCount - 1);
							}
						} else if (label.size() >= 1 && leftBorder != null
								&& leftBorder.equals(label.get(label.size() - 1)) && rightBorder != null
								&& rightBorder.equals(label.get(0))) {
							
							isSingleton = true;
							if (isGenome1) {
								aaAsingletonsCircular.add(activeLabelCount - 1);
							} else {
								bbBsingletonsCircular.add(activeLabelCount - 1);
							}
							lambda++;
						}
						if (chrom.isCircular() && telomerLeft && telomerRight) {
							isSingleton = true;
						}
						
						if (!isSingleton) { // add Label to adjacencyLabels
							// Table
							if (leftBorder != null) {
								if (inverseBackMap.containsKey(leftBorder)) {
									internLeft = inverseBackMap.get(leftBorder);
								} else if (leftBorder.startsWith("-")) {
									leftReverse = true;
									internLeft = inverseBackMap.get(leftBorder.substring(1));
								} else {
									leftReverse = true;
									internLeft = inverseBackMap.get("-" + leftBorder);
								}
							}
							if (rightBorder != null) {
								if (inverseBackMap.containsKey(rightBorder)) {
									internRight = inverseBackMap.get(rightBorder);
								} else if (rightBorder.startsWith("-")) {
									rightReverse = true;
									internRight = inverseBackMap.get(rightBorder.substring(1));
								} else {
									rightReverse = true;
									internRight = inverseBackMap.get("-" + rightBorder);
								}
							}
							
							if (internLeft != 0) {
								if (!leftReverse) {
									fullLabel.setLeft(2 * Math.abs(internLeft));
									adjacencyLabels[2 * Math.abs(internLeft)] = activeLabelCount - 1;
								} else {
									fullLabel.setLeft(2 * Math.abs(internLeft) - 1);
									adjacencyLabels[2 * Math.abs(internLeft) - 1] = activeLabelCount - 1;
								}
							}
							if (internRight != 0) {
								if (!rightReverse) {
									fullLabel.setRight(2 * Math.abs(internRight) - 1);
									adjacencyLabels[2 * Math.abs(internRight) - 1] = activeLabelCount - 1;
								} else {
									fullLabel.setRight(2 * Math.abs(internRight));
									adjacencyLabels[2 * Math.abs(internRight)] = activeLabelCount - 1;
								}
							}
						}
					} else {
						j = right - 1;
					}
				}
			} // end for genes in Chromosome
		} // end for all Chromosomes
		
		return adjacencyLabels;
	}
	
	/**
	 * Identifies and counts Pathes and InDel-Types.
	 */
	public void countPaths() {
		
		cleanStarts();
		boolean[] visited = new boolean[adjacenciesGenome1.length];
		
		for (int i = 1; i < adjacenciesGenome1.length; i++) {
			if (!visited[i]) {
				visited[i] = true;
				ArrayList<Boolean> runsPath1 = new ArrayList<Boolean>();
				ArrayList<Boolean> runsPath2 = new ArrayList<Boolean>();
				// true = run in A, false = run in B
				boolean choose1stGenome = false;
				int extremityPath1 = i;
				int extremityPath2 = i;
				int nextExtremityPath1 = adjacenciesGenome1[i];
				int nextExtremityPath2 = adjacenciesGenome2[i];
				boolean path1EndA = false;
				boolean path2EndA = false;
				boolean isCircular = false;
				
				if (isLabeled(true, extremityPath1)) {
					runsPath1.add(true);
					runsPath2.add(true);
				}
				
				while (!visited[nextExtremityPath1] && nextExtremityPath1 != extremityPath1) {
					// nextExtremityPath1 == extremityPath1 ==> Telomer
					visited[nextExtremityPath1] = true;
					extremityPath1 = nextExtremityPath1;
					if (choose1stGenome) {
						nextExtremityPath1 = adjacenciesGenome1[extremityPath1];
					} else {
						nextExtremityPath1 = adjacenciesGenome2[extremityPath1];
					}
					choose1stGenome = !choose1stGenome;
					if (isLabeled(choose1stGenome, extremityPath1)) {
						if (runsPath1.isEmpty() || !runsPath1.isEmpty()
								&& runsPath1.get(runsPath1.size() - 1) != choose1stGenome) {
							runsPath1.add(choose1stGenome);
						}
					}
				}
				if (isLabeled(!choose1stGenome, extremityPath1)) {
					if (runsPath1.isEmpty() || !runsPath1.isEmpty()
							&& runsPath1.get(runsPath1.size() - 1) != !choose1stGenome) {
						runsPath1.add(!choose1stGenome);
					}
				}
				path1EndA = !choose1stGenome;
				if (extremityPath2 == nextExtremityPath1 && extremityPath1 != extremityPath2) {
					// circle
					isCircular = true;
				} else {
					choose1stGenome = true;
					while (nextExtremityPath2 != extremityPath2) {
						visited[nextExtremityPath2] = true;
						extremityPath2 = nextExtremityPath2;
						if (choose1stGenome) {
							nextExtremityPath2 = adjacenciesGenome1[extremityPath2];
						} else {
							nextExtremityPath2 = adjacenciesGenome2[extremityPath2];
						}
						choose1stGenome = !choose1stGenome;
						if (isLabeled(choose1stGenome, extremityPath2)) {
							if (runsPath2.isEmpty() || !runsPath2.isEmpty()
									&& runsPath2.get(runsPath2.size() - 1) != choose1stGenome) {
								runsPath2.add(choose1stGenome);
							}
						}
					}
					if (isLabeled(!choose1stGenome, extremityPath2)) {
						if (runsPath2.isEmpty() || !runsPath2.isEmpty()
								&& runsPath2.get(runsPath2.size() - 1) != !choose1stGenome) {
							runsPath2.add(!choose1stGenome);
						}
					}
					path2EndA = !choose1stGenome;
				}
				
				Boolean[] runs = combinePathRuns(runsPath1.toArray(new Boolean[runsPath1.size()]), path1EndA,
						runsPath2.toArray(new Boolean[runsPath2.size()]), path2EndA, isCircular);
				
				if (runs.length > 0 && !isCircular) {
					if (path1EndA && path2EndA) { // AA-Path
						if (runs[0] && runs[runs.length - 1]) { // both runends
							// in A
							aaA++;
							aaAStarts.put(extremityPath1, extremityPath2);
						} else if (!runs[0] && !runs[runs.length - 1]) { // both
							// runends
							// in
							// B
							aaB++;
							aaBStarts.put(extremityPath1, extremityPath2);
						} else { // one end in A, one end in B
							aaAB++;
							
							if (runs[0]) {
								aaABStarts.put(extremityPath1, extremityPath2);
							} else {
								aaABStarts.put(extremityPath2, extremityPath1);
							}
						}
					} else if (!path1EndA && !path2EndA) { // BB-Path
						if (runs[0] && runs[runs.length - 1]) { // both runends
							// in A
							bbA++;
							bbAStarts.put(extremityPath1, extremityPath2);
						} else if (!runs[0] && !runs[runs.length - 1]) { // both
							// runends
							// in
							// B
							bbB++;
							bbBStarts.put(extremityPath1, extremityPath2);
						} else { // one end in A, one end in B
							bbAB++;
							if (runs[0]) {
								bbABStarts.put(extremityPath1, extremityPath2);
							} else {
								bbABStarts.put(extremityPath2, extremityPath1);
							}
						}
					} else { // AB-Path
						if (runs[0] && !runs[runs.length - 1]) {
							abAB++;
							if (path1EndA) {
								abABStarts.put(extremityPath1, extremityPath2);
							} else {
								abABStarts.put(extremityPath2, extremityPath1);
							}
						} else if (!runs[0] && runs[runs.length - 1]) {
							abBA++;
							if (path1EndA) {
								abBAStarts.put(extremityPath1, extremityPath2);
							} else {
								abBAStarts.put(extremityPath2, extremityPath1);
							}
						} else {
							// save other components for sorting
							otherComponenents.add(extremityPath1);
						}
					}
				} else {
					
					if (isCircular && runs.length > 0 && runs[0].equals(runs[runs.length - 1])) {
						// if this is the case, the starting position might
						// have to be corrected not to start midrun
						int start = extremityPath1;
						
						// mode describes which genome to search
						// if A seach for first B and vice versa
						boolean mode = !runs[0];
						
						int adj2, adj3;
						if (mode) {
							adj2 = adjacenciesGenome2[extremityPath1];
							adj3 = adjacenciesGenome1[adj2];
						} else {
							adj2 = adjacenciesGenome1[extremityPath1];
							adj3 = adjacenciesGenome2[adj2];
						}
						
						extremityPath1 = adj3;
						
						while (extremityPath1 != start) {
							// either loop full or break when found
							
							if (isLabeled(mode, extremityPath1)) {
								break;
							}
							
							if (mode) {
								adj2 = adjacenciesGenome2[extremityPath1];
								adj3 = adjacenciesGenome1[adj2];
							} else {
								adj2 = adjacenciesGenome1[extremityPath1];
								adj3 = adjacenciesGenome2[adj2];
							}
							
							extremityPath1 = adj3;
						}
					}
					
					otherComponenents.add(extremityPath1);
				}
				if (runs.length > 0) {
					lambda += Math.ceil((runs.length + 1.0) / 2.0);
				}
			}
		}
	}
	
	/**
	 * Combines two half pathruns to one.
	 * 
	 * @param path1Runs
	 *            runs in Path1
	 * @param path1A
	 *            Path1 ends in A?
	 * @param path2Runs
	 *            runs in Path2
	 * @param path2A
	 *            Path2 ends in A?
	 * @param isCircular
	 *            is the Path circular?
	 * @return combined runs starting at an A-end.
	 */
	private Boolean[] combinePathRuns(Boolean[] path1Runs, boolean path1A, Boolean[] path2Runs, boolean path2A,
			boolean isCircular) {
		/* If Path1 ends in A: flip Path1Runs and add Path2Runs Else flip
		 * Path2Runs and add Path1Runs
		 * 
		 * Result: runs-Array starting with the first run seen from the A side */
		
		ArrayList<Boolean> runs = new ArrayList<Boolean>();
		if (path1A && path2A || !path1A && !path2A || path1A) {
			for (int i = path1Runs.length - 1; i >= 0; i--) {
				if (runs.isEmpty() || runs.get(runs.size() - 1) != path1Runs[i]) {
					runs.add(path1Runs[i]);
				}
			}
			for (int i = 0; i < path2Runs.length; i++) {
				if (runs.isEmpty() || runs.get(runs.size() - 1) != path2Runs[i]) {
					runs.add(path2Runs[i]);
				}
			}
		} else {
			for (int i = path2Runs.length - 1; i >= 0; i--) {
				if (runs.isEmpty() || runs.get(runs.size() - 1) != path2Runs[i]) {
					runs.add(path2Runs[i]);
				}
			}
			for (int i = 0; i < path1Runs.length; i++) {
				if (runs.isEmpty() || runs.get(runs.size() - 1) != path1Runs[i]) {
					runs.add(path1Runs[i]);
				}
			}
		}
		
		if (isCircular && runs.size() >= 2 && runs.get(0) == runs.get(runs.size() - 1)) {
			runs.remove(runs.size() - 1);
		}
		
		Boolean[] result = runs.toArray(new Boolean[runs.size()]);
		return result;
	}
	
	/**
	 * 
	 * @param genome1
	 *            true - genome1; false genome2
	 * @param extremity
	 *            extremity index
	 * @return if the extremity is labeled.
	 */
	private boolean isLabeled(boolean genome1, int extremity) {
		if (genome1) {
			if (adjacencyLabelsGenom1[extremity] != 0) {
				return true;
			}
		} else {
			if (adjacencyLabelsGenom2[extremity] != 0) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @return the lambda
	 */
	public int getLambda() {
		return lambda;
	}
	
	/**
	 * @return the aaA
	 */
	public int getAaA() {
		return aaA;
	}
	
	/**
	 * @return the aaB
	 */
	public int getAaB() {
		return aaB;
	}
	
	/**
	 * @return the aaAB
	 */
	public int getAaAB() {
		return aaAB;
	}
	
	/**
	 * @return the bbA
	 */
	public int getBbA() {
		return bbA;
	}
	
	/**
	 * @return the bbB
	 */
	public int getBbB() {
		return bbB;
	}
	
	/**
	 * @return the bbAB
	 */
	public int getBbAB() {
		return bbAB;
	}
	
	/**
	 * @return the abAB
	 */
	public int getAbAB() {
		return abAB;
	}
	
	/**
	 * @return the abBA
	 */
	public int getAbBA() {
		return abBA;
	}
	
	public int[] getAdjacencyLabelsGenom1() {
		return adjacencyLabelsGenom1;
	}
	
	public int[] getAdjacencyLabelsGenom2() {
		return adjacencyLabelsGenom2;
	}
	
	public ArrayList<Integer> getComponents() {
		ArrayList<Integer> components = new ArrayList<Integer>();
		
		components.addAll(otherComponenents);
		components.addAll(aaAStarts.keySet());
		components.addAll(aaBStarts.keySet());
		components.addAll(aaABStarts.keySet());
		components.addAll(bbAStarts.keySet());
		components.addAll(bbBStarts.keySet());
		components.addAll(bbABStarts.keySet());
		components.addAll(abABStarts.keySet());
		components.addAll(abBAStarts.keySet());
		
		return components;
	}
	
	public Map<Integer, Integer> getaaAStarts() {
		return aaAStarts;
	}
	
	public Map<Integer, Integer> getaaBStarts() {
		return aaBStarts;
	}
	
	public Map<Integer, Integer> getaaABStarts() {
		return aaABStarts;
	}
	
	public Map<Integer, Integer> getbbAStarts() {
		return bbAStarts;
	}
	
	public Map<Integer, Integer> getbbBStarts() {
		return bbBStarts;
	}
	
	public Map<Integer, Integer> getbbABStarts() {
		return bbABStarts;
	}
	
	public Map<Integer, Integer> getabABStarts() {
		return abABStarts;
	}
	
	public Map<Integer, Integer> getabBAStarts() {
		return abBAStarts;
	}
	
	public ArrayList<Integer> getOtherComponenents() {
		return otherComponenents;
	}
	
	public ArrayList<Integer> getaaAsingletons() {
		return aaAsingletons;
	}
	
	public ArrayList<Integer> getbbBsingletons() {
		return bbBsingletons;
	}
	
	public ArrayList<Integer> getaaAsingletonsCircular() {
		return aaAsingletonsCircular;
	}
	
	public ArrayList<Integer> getbbBsingletonsCircular() {
		return bbBsingletonsCircular;
	}
	
	public HashMap<Integer, Label> getActiveLabels() {
		return activeLabels;
	}
	
	public HashMap<Integer, List<String>> getBaseLabels() {
		return baseLabels;
	}
	
	public HashMap<Integer, LabelIndexPair> getSubLabels() {
		return subLabels;
	}
	
	public int getSubLabelCount() {
		return subLabelCount;
	}
}