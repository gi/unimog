
package de.unibi.cebitec.gi.unimog.datastructure;

/**
 * Stores a pair of label indices. 
 * This way a binary tree is created.
 * @author Thomas Gatter - tgatter@cebitec.uni-bielefel.de
 */
public class LabelIndexPair {
    
    LabelIndex left;
    LabelIndex right;

    public LabelIndexPair(LabelIndex left, LabelIndex right) {
        this.left = left;
        this.right = right;
    }

    public LabelIndex getLeft() {
        return left;
    }

    public void setLeft(LabelIndex left) {
        this.left = left;
    }

    public LabelIndex getRight() {
        return right;
    }

    public void setRight(LabelIndex right) {
        this.right = right;
    }
    
    
    
}
