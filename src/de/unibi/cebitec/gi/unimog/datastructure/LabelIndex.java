
package de.unibi.cebitec.gi.unimog.datastructure;

/**
 *
 * @author Thomas Gatter - tgatter@cebitec.uni-bielefeld.de
 */
public class LabelIndex {
    
    private int index;
    private boolean orientation;

    public LabelIndex(int index, boolean orientation) {
        this.index = index;
        this.orientation = orientation;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isOrientation() {
        return orientation;
    }

    public void setOrientation(boolean orientation) {
        this.orientation = orientation;
    }
    
    public void switchOrientation() {
        if (orientation) {
            orientation = false;
        } else {
            orientation = true;
        }
    }
    
    
    
}
