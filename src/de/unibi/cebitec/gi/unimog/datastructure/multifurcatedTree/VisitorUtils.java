package de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree;

import java.util.List;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker                                     *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * @author -Rolf Hilker-
 * <p>
 * Contains some utility methods used by more than one node visitor.
 */
public class VisitorUtils {

    public VisitorUtils() {
        //Do not instantiate
    }

    /**
     * Returns if a white node in the tree becomes a leaf corresponding to its
     * children. It becomes a leaf if there is no white node below this node.
     *
     * @param node the node to check
     * @return true if the node becomes a leaf, false otherwise
     */
    public static boolean becomesALeaf(final Node node) {
        VisitorWhiteNodes whiteNodeVisitor = new VisitorWhiteNodes();
        List<Node> children = node.getNodeChildren();
        for (int i = 0; i < children.size(); ++i) {
            children.get(i).topDown(whiteNodeVisitor);
        }
        return !whiteNodeVisitor.isAWhiteChild();
    }

    /**
     * Checks if the current node has a white parent at some level above in the tree.
     *
     * @param node the node to check
     * @return true if it has a white parent, false, if not
     */
    public static boolean hasWhiteParent(final Node node) {

        Node parent = node.getParent();
        while (!parent.isRoot() && parent.getNodeType() != NodeType.WHITE) {
            parent = parent.getParent();
        }
        if (parent.getNodeType() == NodeType.WHITE) {
            return true;
        }
        return false;
    }

    /**
     * Tests if node is branching node in contracted compTree
     *
     * @param parent: node that is tested
     * @return true if node is a branching node, else false
     **/
    public static boolean isBranchingNode(final Node parent) {
        //catch case with round branching Node
        boolean roundBranchingNode = (parent.getNodeType() != NodeType.SQUARE);

        //determines number of remaining children after contracting the tree
        int nbWhiteChildren = 0;
        for (Node someChild : parent.getNodeChildren()) {
            boolean isBlackLeaf = false;
            if (someChild.getNodeType() == NodeType.WHITE) {
                nbWhiteChildren++;
            } else {
                // black leaves are cut
                isBlackLeaf = becomesALeaf(someChild);
                if (!isBlackLeaf) {
                    nbWhiteChildren++;
                }
            }
            if (roundBranchingNode && isBranchingNode(someChild)) {
                // Round black-node and child square-node become one branching node in contracted compTree,
                // if one of them is a branching node
                return true;
            }
        }
        if (nbWhiteChildren == 2) {
            // parent is branching node if there is at least one white node above the parent
            // because either all black nodes are melted together and there is a connection to a white node
            // or there is a direct connection to a white node
            Node grandParent = parent.getParent();
            if (grandParent.getNodeType() == NodeType.WHITE) {
                return true;
            } else {
                //searches for white node in whole upper tree
                while (grandParent.getNodeType() != NodeType.WHITE && !grandParent.isRoot()) {
                    if (grandParent.getNodeChildren().size() > 1) {
                        for (Node child : grandParent.getNodeChildren()) {
                            if (!becomesALeaf(child) || child.getNodeType() == NodeType.WHITE) {
                                return true;
                            }
                        }
                    }
                    grandParent = grandParent.getParent();
                }
            }

        } else {
            // if parent has more than two white children then it is a branching node
            return true;
        }

        return false;
    }

}
