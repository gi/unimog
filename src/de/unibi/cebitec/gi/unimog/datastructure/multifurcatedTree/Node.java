package de.unibi.cebitec.gi.unimog.datastructure.multifurcatedTree;

import java.util.ArrayList;
import java.util.List;

/***************************************************************************
 *   Copyright (C) 2010 by Rolf Hilker                                     *
 *   rhilker   a t  cebitec.uni-bielefeld.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * Defines a <tt>node</tt> constructor to create a new <tt>node</tt> and has
 * some methods to manipulate a created <tt>node</tt>.
 *
 * @author Pina Krell, Rolf Hilker
 */

public class Node implements Traversable, Cloneable {

    // Vector with the children of a node
    private int index = -1;
    private List<Node> nodeChildren;
    // Parent of the Node
    private Node parent;
    private NodeType nodeType;

    /**
     * Constructs a <tt>node</tt> with a given name, parent, an incoming edge
     * length and a key-object to be stored in the <tt>node</tt> .
     *
     * @param nodeType The node type to store within the current <tt>node</tt>
     * @param parent   A <tt>node</tt> which is the parent of the <tt>node</tt> to be
     *                 created
     */
    public Node(final NodeType nodeType, final Node parent) {
        this.nodeType = nodeType;
        if (parent != null) {
            parent.addChild(this);
        }
        this.nodeChildren = new ArrayList<Node>();
    }

    /**
     * Returns the depth of the Node.
     *
     * @return depth the depth of the Node
     */
    public Integer getDepth() {
        Node node = this;
        Integer depth = 0;
        while (!node.isRoot()) {
            node = node.getParent();
            depth++;
        }
        return depth;
    }

    /**
     * Clones the node.
     *
     * @return newNode the copy of the original node.
     */
    @Override
    public Node clone() {

        Node newNode;
        if (this.nodeType == null) {
            newNode = new Node(null, null);
        } else {
            newNode = new Node(this.nodeType, null);
        }
        if (!this.isLeaf()) {
            for (Node n : this.getNodeChildren()) {
                newNode.getNodeChildren().add(n.clone());
                newNode.getNodeChildren().get(newNode.getNodeChildren().size() - 1).parent = newNode;
            }
        }
        return newNode;
    }

    /**
     * Bottom up through the tree.
     *
     * @param nodeVisitor the nodeVisitor
     */
    public void bottomUp(final NodeVisitor nodeVisitor) {
        for (Node nodeChild : this.nodeChildren) {
            nodeChild.bottomUp(nodeVisitor);
        }
        nodeVisitor.visit(this);
    }

    /**
     * Top down through the tree.
     *
     * @param nodeVisitor the nodeVisitor
     */
    public void topDown(final NodeVisitor nodeVisitor) {
        nodeVisitor.visit(this);

        for (Node nodeChild : this.nodeChildren) {
            nodeChild.topDown(nodeVisitor);
        }
    }

    /**
     * Adds a new child (also a <tt>node</tt>) to a <tt>nodes</tt> vector of
     * children.
     *
     * @param newChild The <tt>node</tt> to be set as a new child
     */
    public void addChild(final Node newChild) {
        this.nodeChildren.add(newChild);
        newChild.parent = this;
    }

    /**
     * Deletes all children (<tt>nodes</tt>) of a <tt>node</tt>.
     */
    public void clearChildren() {
        this.nodeChildren.clear();
    }

    /**
     * Parses the object stored in a <tt>node</tt> to String.
     *
     * @return key The key object parsed to a string
     */
    @Override
    public String toString() {
        final StringBuilder out = new StringBuilder();
        if (!this.isLeaf()) {
            out.append('(');
            for (Node nodeChild : this.nodeChildren) {
                out.append(nodeChild.toString() + ",");
            }
            out.deleteCharAt(out.length() - 1);
            out.append(")");
        }
        return out.toString();
    }

    /**
     * Traverses a <tt>node</tt> with different visitors.
     *
     * @param nodeVisitor Visitor-type with which the <tt>node</tt> should be traversed
     */
    @Override
    public void traverse(final NodeVisitor nodeVisitor) {
        // visits the actual node
        nodeVisitor.visit(this);
        // calls the method traverse on all children of the current node
        for (Node nodeChild : this.nodeChildren) {
            nodeChild.traverse(nodeVisitor);
        }
    }

    /**
     * Getter for the parent of a <tt>node</tt>.
     *
     * @return parent The parent <tt>node</tt> of a <tt>node</tt>
     */
    public Node getParent() {
        return this.parent;
    }

    /**
     * Setter to change parent or make node root
     **/
    public void setParent(Node newParent) {
        this.parent = newParent;
    }

    /**
     * Boolean whether a <tt>node</tt> is a <tt>leaf</tt> or not. A
     * <tt>leaf</tt> does not contain children (<tt>nodes</tt>).
     *
     * @return true if the <tt>node</tt> is a leaf
     */
    public boolean isLeaf() {
        return (this.nodeChildren.isEmpty());
    }

    /**
     * Boolean whether a <tt>node</tt> is a <tt>root</tt> of a tree or not.
     * Means that the <tt>node</tt> has no parent.
     *
     * @return true if the <tt>node</tt> is the <tt>root</tt>
     */
    public boolean isRoot() {
        return this.parent == null;
    }

    /**
     * Sets a specific key-object to store within the actual <tt>node</tt>.
     *
     * @param nodeType Node type to store within the <tt>node</tt>
     */
    public void setNodeType(final NodeType nodeType) {
        this.nodeType = nodeType;
    }

    /**
     * Getter for the key-object which is stored in the <tt>node</tt>.
     *
     * @return key A key object which is stored in the <tt>node</tt>
     */
    public NodeType getNodeType() {
        return this.nodeType;
    }

    /**
     * Getter for the vector of children-nodes of the actual <tt>node</tt>.
     *
     * @return nodeChildren The vector of children of a <tt>node</tt>
     */
    public List<Node> getNodeChildren() {
        return this.nodeChildren;
    }

    /**
     * Sets a vector of children (<tt>nodes</tt>) to a <tt>node</tt>.
     *
     * @param children A vector of children-nodes
     */
    public void setNodeChildren(final List<Node> children) {
        this.nodeChildren = children;
    }

    /**
     * Add all children of a given list to current Node
     *
     * @param children List of children which should be added
     **/
    public void addAllChildren(List<Node> children) {
        for (Node child : children) {
            addChild(child);
        }
    }

    /**
     * Checks if node color is black. Return true if node is black or square node.
     *
     * @return true if nodeType equals square or black
     **/
    public boolean hasDarkColor() {
        return this.nodeType.equals(NodeType.SQUARE) || this.nodeType.equals(NodeType.BLACK);
    }

    /**
     * Checks if node has nodeType white
     *
     * @return true if nodeType equal white
     **/
    public boolean isWhiteNode() {
        return this.nodeType.equals(NodeType.WHITE);
    }

    public boolean isGreyNode(){
        return this.nodeType.equals(NodeType.GREY);
    }

    /**
     * Set index based on traversal algorithm (BFT/DFT).
     * Only round nodes get an index.
     *
     * @param index
     **/
    public void setIndex(int index) {
        if (!this.nodeType.equals(NodeType.SQUARE)) {
            this.index = index;
        }
    }

    /**
     * Get node index
     *
     * @return node index
     **/
    public int getIndex() {
        return this.index;
    }
}