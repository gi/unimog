

package de.unibi.cebitec.gi.unimog.datastructure;

/**
 * This is a wrapper class for Operations on InDelType computations.
 * 
 * For OperationType Backward, the operation has been made on the second Genome, not the first.
 * Accordingly, the operation Ints have to be processed to show them in the graph panel correctly.
 * 
 * OperationType InDel could be used to draw such operations differently in the Graph Panel.
 * 
 * @author Thomas Gatter - tgatter(at)cebitec.uni-bielefeld.de
 */
public class OperationInDel extends Pair<Integer, Integer>{

    private OperationType type;
    
    private Object specialInfo = "";
    
    public OperationInDel(Integer o1, Integer o2) {
        super(o1, o2);
        type = OperationType.Forward;
    }
    
    public OperationInDel(Integer o1, Integer o2, OperationType type) {
        super(o1, o2);
        this.type = type;
    }
    
    public OperationInDel(Integer o1, Integer o2, OperationType type, Object special) {
        this(o1, o2, type);
        specialInfo = special;
    }

    public OperationType getType() {
        return type;
    }

    /**
     * Returns a special info of some kind that can be used for postprocessing
     * Insert: label for drawing
     * @return 
     */
    public Object getSpecialInfo() {
        return specialInfo;
    }
    
    public enum OperationType {
        Forward,
        ForwardInDel,
        ForwardHelperOne,
        Backward,
        BackwardInDel,
        Insert,
        Delete,
        Singleton;
    }
    
}
