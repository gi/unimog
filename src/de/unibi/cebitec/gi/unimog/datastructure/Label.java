

package de.unibi.cebitec.gi.unimog.datastructure;

/**
 * Saves all Data needed for one InDelLabel
 * @author gatter
 */
public class Label {
    
    private int left;
    private int right;
    private LabelIndex label;

    public Label(LabelIndex label) {
        left = -1;
        right = -1;
        this.label = label;
    }
    
    public Label(int left, int right, LabelIndex label) {
        this.left = left;
        this.right = right;
        this.label = label;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public LabelIndex getLabel() {
        return label;
    }

    public void setLabel(LabelIndex label) {
        this.label = label;
    }

    
}
